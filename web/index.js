'use strict';

const debug = require('debug')('apostilla:web');
const http = require('http');
const chalk = require('chalk');
const express = require('express');
const asyncify = require('express-asyncify');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const { errors } = require('common');
const api = require('./src/api');

const port = process.env.PORT_PROXY || 3300;
const app = asyncify(express());
const server = http.createServer(app);

app.use(bodyParser.json({ limit: '300mb' }));
app.use(fileUpload());
app.use(cors());

app.use('/api', api);

// Express Error Handler
app.use((err, req, res, next) => {
  debug(`Error: ${err.message}`);

  if (err.message.match(/not found/)) {
    return res.status(404).send({ code: -1, message: err.message });
  }

  if (err.message.match(/jwt expired/)) {
    return res.status(401).send({ code: -1, message: 'Su sesión ha expirado, ingrese nuevamente al sistema.' });
  }

  if (err.message.match(/No authorization/)) {
    return res.status(403).send({ code: -1, message: 'No tiene permisos para realizar esta operación.' });
  }

  if (err.message.match(/invalid token/)) {
    return res.status(403).send({ code: -1, message: 'Su token es inválido.' });
  }

  res.status(500).send({ code: -1, message: err.message });
});

process.on('uncaughtException', errors.handleFatalError);
process.on('unhandledRejection', errors.handleFatalError);

server.listen(port, () => {
  console.log(`${chalk.green('[apostilla-web]')} server listening on port ${port}`);
});

module.exports = server;
