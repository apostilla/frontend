'use strict';

const debug = require('debug')('apostilla:web:api');
const express = require('express');
const request = require('request-promise-native');
const asyncify = require('express-asyncify');
const api = asyncify(express.Router());
const { graphql, find, validarAcceso, validarCrear } = require('./lib/util');
// const util = require('./lib/util');
const { apiUrl, apiToken } = require('./config');



// Lista de documentos
api.get('/documentos', async (req, res, next) => {
  debug('Lista de documentos');

  let result;
  try {
    result = await graphql({
      query: `
        query lista {
          documentos(order: "id_entidad", estado: ACTIVO) {
            count
            rows {
              id
              nombre
              descripcion
              entidad_nombre
              entidad_sigla
              entidad_codigo_portal
              precio
              codigo_portal
              monto
              otros
            }
          }
          parametroBuscar(name: "URL_PORTAL_UNICO") {
            valor
          }
        }
      `
    });
  } catch (e) {
    return next(e);
  }

  let urlPortal = result.parametroBuscar.valor;
  let documentos = result.documentos.rows;
  documentos = documentos.map(item => {
    item.url = `${urlPortal}/tramite.html?id=${item.codigo_portal}`;
    return item;
  });

  res.send(documentos);
});

const observaciones = ['CORRECCION DE DATOS', 'DUPLICIDAD', 'FALLECIDO', 'FALSIFICACION DE DOCUMENTOS', 'HOMONIMIA', 'INVESTIGACION POLICIAL', 'MULTIPLICIDAD', 'Observacion', 'OBSERVADO POR EL SEGIP', 'OBSERVADO POR LA APS', 'SUPLANTACION'];

// Obteniendo datos de interoperabilidad
api.post('/registrar-datos', async (req, res, next) => {
  debug('Solicitud - Obteniendo datos de interoperabilidad');

  const { tipoDoc, nroDoc, fecNacimiento, complemento } = req.body;

  if (!tipoDoc) {
    return next(new Error('El tipo de documento es obligatorio'));
  }

  if (!nroDoc) {
    return next(new Error('La cédula de identidad es obligatoria'));
  }

  if (!fecNacimiento) {
    return next(new Error('La fecha de nacimiento es obligatoria'));
  }

  const options = {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${apiToken}`
    },
    json: true
  };

  if (tipoDoc === 'CI') {
    let url = `${apiUrl}system/persona-segip/${nroDoc}?fechaNacimiento=${fecNacimiento}`;
    if (complemento) {
      url += `&complemento=${complemento}`;
    }

    options.url = url;

    let result;
    try {
      result = await request(options);
      console.log('======================================= SEGIP');
      console.log(result);
      if (result.code === 1) {
        result = result.data;
      } else {
        return res.send({ observacion: 'observado' });
      }
    } catch (e) {
      console.log('ERROR Segip', e.error);
      if (e.error === undefined) {
        return next(new Error('La información ingresada no es correcta o tiene una distinta codificación de caracteres.'));
      } else {
        return next(new Error((e.error ? e.error.message : e.error) || e.message));
      }
    }
    res.send({
      nombres: '',
      paterno: '',
      materno: '',
      nacionalidad: '',
      mensaje: result.mensaje
    });
  } else if (tipoDoc === 'PASAPORTE') {
    options.url = `${apiUrl}system/persona-digemig/${nroDoc}?fechaNacimiento=${fecNacimiento}`;

    let result;
    try {
      result = await request(options);
      console.log('Respuesta DIGEMIG', result);
      if (result.code === 1) {
        result = result.data;
      } else {
        return next(new Error(result.message));
      }
    } catch (e) {
      console.log('ERROR Digemig', e.error);
      if (e.error === undefined) {
        return next(new Error('La información ingresada no es correcta o tiene una distinta codificación de caracteres.'));
      } else {
        return next(new Error((e.error ? e.error.message : e.error) || e.message));
      }
    }
    res.send({
      nombres: result.nombre,
      paterno: result.primerApellido,
      materno: result.segundoApellido,
      nacionalidad: result.codNacionalidad,
      situacion: result.situacionMigratoria,
      tipo: result.tipo
    });
  } else {
    return next(new Error('No existe ese tipo de documento'));
  }
});

// Obteniendo datos de interoperabilidad - Min Salud
api.post('/obtener-formacion-academica', async (req, res, next) => {
  debug('Solicitud - Obteniendo datos de interoperabilidad');

  const { matricula, apPaterno, apMaterno } = req.body;

  const options = {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${apiToken}`
    },
    json: true
  };

  let url = `${apiUrl}eregister/minsalud/${matricula}/${apPaterno}/${apMaterno}`;

  options.url = url;

  let result;
  try {
    result = await request(options);
    console.log('======================================/otros=');
    console.log(result);
    // console.log(observaciones, result.message, observaciones.indexOf(result.message));

    if (result.code === 1) {
      result = result.data;
    } else {
      if (find(result.message, observaciones)) {
        return res.send({ observacion: result.message });
      } else {
        return next(new Error(result.message));
      }
    }
  } catch (e) {
    console.log('ERROR Min Salud', e.error);
    if (e.error === undefined) {
      return next(new Error('La información ingresada no es correcta o tiene una distinta codificación de caracteres.'));
    } else {
      return next(new Error((e.error ? e.error.message : e.error) || e.message));
    }
  }
  res.send(result.body);
});

// Registrando solicitud de apostilla
api.post('/registrar', async (req, res, next) => {
  debug('Registrando solicitud de apostilla: ' + JSON.stringify(req.body));

  let ips = req.headers['x-forwarded-for'] || req.connection.remoteAddress

  ips = ips.replace('::ffff:','');

  const ip = ips.split(', ').length > 0 ? ips.split(', ')[0] : ips;

  console.log('ip de la peticion: ' + ip);

  let result;
  try {
    result = await graphql({
      query: `
        mutation agregar($tramite: NewTramite!, $ip: String) {
          tramiteAdd(tramite: $tramite, ip: $ip) {
            cod_seguimiento
            pagos {
              id
              pdf
              documento {
                id
                nombre
                codigo_portal
              }
            }
          }
        }
      `,
      variables: {
        tramite: req.body,
        ip: ip
      }
    });
  } catch (e) {
    return next(e);
  }

  res.send(result.tramiteAdd);
});

// Registrando 1 solicitud con 1 documento con 1 archivo adjunto
api.post('/registrar-solicitud', validarCrear, async(req, res, next) => {
  debug('Registrando solicitud de apostilla: ' + JSON.stringify(req.body));

  let result;
  try {
    result = await graphql({
      query: `
        mutation agregar($tramite: NewTramite!) {
          tramiteAdd(tramite: $tramite) {
            cod_seguimiento
          }
        }
      `,
      variables: {
        tramite: req.body
      }
    });
  } catch (e) {
    return next(e);
  }
  const respuesta = {
    codigoSeguimiento: result.tramiteAdd.cod_seguimiento
  };
  res.send(respuesta);
});

// Obtener trámite
api.get('/tramite/:code', async (req, res, next) => {
  const { code } = req.params;
  debug(`Obteniendo trámite ${code}`);

  const options = {
    url: `${apiUrl}eregister/tramite/${code}`,
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${apiToken}`
    },
    json: true
  };

  let result;
  try {
    result = await request(options);
    console.log(result);
    if (result.code === 1) {
      result = result.data;
      delete result.id;
    } else {
      return next(new Error(result.message));
    }
  } catch (e) {
    return next(e);
  }

  res.send(result);
});

// Obtener trámite (con menos datos)
api.get('/tramiteInfo/:code', validarAcceso, async (req, res, next) => {
  console.log("Iniciando la obtencion de informacion");
  const { code } = req.params;
  debug(`Obteniendo trámite ${code}`);

  const options = {
    url: `${apiUrl}eregister/tramiteInfo/${code}`,
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${apiToken}`
    },
    json: true
  };

  let result;
  try {
    result = await request(options);

    if (result.code === 1) {
      result = result.data;
      delete result.id;
    } else {
      return next(new Error(result.message));
    }
  } catch (e) {
    return next(e);
  }

  res.send(result);
});

// Buscando apostilla
api.post('/verificar', async (req, res, next) => {
  debug('Buscando apostilla');

  const { code, number, date } = req.body;

  let result;
  try {
    result = await graphql({
      query: `
        query buscar($code: String!, $number: String!, $date: Date!) {
          tramiteBuscar(code: $code, number: $number, date: $date) {
            tramite_solicitante_nombres
            tramite_solicitante_primer_apellido
            tramite_solicitante_segundo_apellido
            tramite_solicitante_nro_documento
            tramite_solicitante_fecha_nacimiento
            tramite_solicitante_tipo_documento
            documento_entidad_nombre
            documento_descripcion
            documento_nombre
            id_archivo
          }
        }
      `,
      variables: {
        code,
        number,
        date
      }
    });
  } catch (e) {
    return next(e);
  }

  let tramiteDoc = result.tramiteBuscar;

  const options = {
    url: `${apiUrl}eregister/pdf`,
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${apiToken}`
    },
    body: { id_archivo: tramiteDoc.id_archivo },
    json: true
  };

  try {
    result = await request(options);
  } catch (e) {
    return next(e);
  }

  delete tramiteDoc.id_archivo;
  if (result.code === 1) {
    tramiteDoc.pdf = result.data;
    tramiteDoc.url = `download/${code}/${number}/${date}`;
  } else {
    return next(result.message || 'No se pudo obtener el PDF.');
  }

  res.send(tramiteDoc);
});

// Subiendo PDF en base64
api.post('/upload/:idTramiteDoc', validarAcceso, async (req, res, next) => {
  debug('Subiendo un PDF en base64 ');

  const { idTramiteDoc } = req.params;
  const { pdf } = req.body;
  let result;

  const id_entidad = req.audit.id_entidad;

  const options = {
    url: `${apiUrl}eregister/uploadBase64/${idTramiteDoc}`,
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${apiToken}`
    },
    body: { pdf, id_entidad },
    json: true
  };

  try {
    result = await request(options);
    console.log(result);
  } catch (e) {
    console.log('ERRORRRRR', e);
    return next(new Error((e.error ? e.error.message : e.error) || e.message));
  }

  if (result.code !== 1) {
    return next(result.message || 'No se pudo crear el PDF.');
  }
  res.send(result.message);
});

// Descargando apostilla
api.get('/download/:code/:number/:date', async (req, res, next) => {
  debug('Descargando PDF apostilla');

  const { code, number, date } = req.params;

  console.log('data', code, number, date);

  let result;
  try {
    result = await graphql({
      query: `
        query buscar($code: String!, $number: String!, $date: Date!) {
          tramiteBuscar(code: $code, number: $number, date: $date) {
            id_archivo
          }
        }
      `,
      variables: {
        code,
        number,
        date
      }
    });
  } catch (e) {
    return next(e);
  }

  let tramiteDoc = result.tramiteBuscar;

  const options = {
    url: `${apiUrl}eregister/pdf-download-proxy/${tramiteDoc.id_archivo}`,
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${apiToken}`
    },
    json: true
  };

  try {
    result = await request(options);
  } catch (e) {
    return next(e);
  }

  console.log('RESPUESTA PDF', result);

  const { filename, mimetype, sizeFile, pdf } = result;
  res.setHeader('Content-disposition', `attachment; filename=${filename}`);
  res.setHeader('Content-type', mimetype);
  res.setHeader('Size-file', sizeFile);
  res.send(pdf);
});

// Obteniendo costo de la apostilla
api.get('/costo', async (req, res, next) => {
  debug('Obteniendo costo de la apostilla');

  let result;
  try {
    result = await graphql({
      query: `
        query buscar {
          parametroBuscar(name: "COSTO_APOSTILLA") {
            valor
          }
        }
      `
    });
  } catch (e) {
    return next(e);
  }

  res.send(result.parametroBuscar);
});

// Obtiene los tramites de una entidad
api.get('/tramites/:sigla', validarAcceso, async (req, res, next) => {
  debug('Iniciando la obtencion de los tramites disponibles para cada entidad', req.params.sigla);
  let result;
  try {
    if (!req.params || !req.params.sigla) throw Error('Es necesario enviar la sigla de la entidad.');
    let sigla = req.params.sigla;

    result = await graphql({
      query: `
      query buscar($id: Int!) {
        documentos(id_entidad: $id) {
          count
          rows {
            nombre
            descripcion
            codigo_portal
          }
        }
      }
      `,
      variables: {
        id: req.audit.id_entidad
      }
    });
  } catch (e) {
    return (e);
  }
  res.send(result.documentos);
});

module.exports = api;
