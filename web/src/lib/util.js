'use strict';

const debug = require('debug')('apostilla:util');
const { graphqlUrl, apiToken } = require('../config');
const request = require('request');
const _ = require('lodash');
const { verify } = require('../../../application/src/lib/auth');
const { config } = require('common');

const validarCrear = async (req, res, next) => {
  try {
    const cabeceraValida = await validarCabecera(req);
    if(!cabeceraValida.valido) throw Error('La cabecera no es valida');
    const data = cabeceraValida.data;
    const puedeCrear =  await module.exports.puedeCrear(data.id_entidad, req.body);
    if(!puedeCrear) throw Error('No existen los permisos necesarios.');

  } catch (e) {
    console.log("Revisando el error en la creacion", e);
    return next(new Error(e.message));
  }
  return next();
}

const validarCabecera = async (req) => {
  let respuesta = {
    valido: false
  };
  try {
    if(!req.headers.authorization) throw Error('No existen las credenciales autorizadas.');
    const data = await verify(req.headers.authorization.replace('Bearer ', ''), config.auth.secret);
    if(!data.id_entidad) throw Error('No existe el identificador de la entidad');
    respuesta.valido = true;
    respuesta.data = data;
  } catch (e) {
    console.log("Revisando el error en la validacion de la cabecera", e);
    respuesta.valido = false;
  }
  return respuesta;
}

const validarAcceso  = async (req, res, next) => {
  try {
    const cabeceraValida= await validarCabecera(req);
    if(!cabeceraValida.valido) throw Error('Los datos de la petición no son válidos.');
    req.audit = { id_entidad: cabeceraValida.data.id_entidad };
  } catch (e) {
    console.log("Error de acceso al servicio", e);
    return next(new Error(e.message));
  }
  return next();
}

const puedeCrear = async (idEntidad, datos) => {

  let valido= false;
  let documentosEntidad = [];
  let documentos = datos.documentos || [];
  let resultado;
  try {
    resultado = await graphql({
      query: `
        query buscar($idEntidad: Int!){
          documentos(id_entidad: $idEntidad) {
            count
            rows {
              id
              id_entidad
              entidad_sigla
              nombre
              codigo_portal
            }
          }
        }
      `,
      variables: {
        idEntidad
      }
    });

    _.map(documentos, item => {
      item.codigo_portal = item.codigoPortal;
    });
    if(resultado.count === 0) throw Error('No existen documentos registrados para la entidad.');
    documentosEntidad = resultado.documentos.rows;
    const interseccion = _.intersectionBy(documentosEntidad, documentos, 'codigo_portal') || [];

    console.log('documentosEntidad: ' + JSON.stringify(documentosEntidad));
    console.log('documentos: ' + JSON.stringify(documentos));

    if(interseccion.length !== documentos.length) throw Error('Algunos tramites no corresponden a la entidad.');
    valido = true;
  } catch (e) {
    debug('Error en la validacion de la peticion',e)
    valido = false;
  }
  return valido;
}

function http (options) {
  return new Promise((resolve, reject) => {
    request(options, function (error, response, body) {
      if (error) {
        reject(error);
        return;
      }

      const { statusCode } = response;
      debug('error:', error);
      debug('statusCode:', statusCode);
      debug('body:', body);

      if ([400, 500].indexOf(statusCode) !== -1) {
        reject(body);
      }

      resolve(body);
    });
  });
}

function parseErrorGraphql (errors) {
  if (Array.isArray(errors)) {
    let msg = [];
    errors.map(el => {
      if (el.message.indexOf('NOT_AUTHORIZED') === 0) {
        msg.push('No tiene los permisos necesarios para realizar esta operación.');
      } else {
        let message = '<p>';
        if (el.name) {
          message += `<strong>Error: </strong>${el.name} <br />`;
        }
        message += `<strong>Mensaje: </strong> ${el.message}</p>`;
        msg.push(message);
      }
    });
    return msg.join('');
  }
}

async function graphql (query) {
  const options = {
    method: 'POST',
    url: `${graphqlUrl}`,
    headers: {
      'Authorization': `Bearer ${apiToken}`
    },
    json: true,
    body: query
  };

  let result;
  try {
    debug('---------------------------', result);
    result = await http(options);
    if (result.errors) {
      throw new Error(parseErrorGraphql(result.errors));
    }
    result = result.data;
  } catch (e) {
    debug('ERROR *********************************************************');
    debug('Error Graphql', e);
    if (e.errors) {
      throw new Error(parseErrorGraphql(e.errors));
    } else {
      if (e.message && e.message.indexOf('ECONNREFUSED') !== -1) {
        throw new Error('No se pudo establecer conexión con el servidor, inténtelo dentro unos minutos.');
      } else {
        throw new Error(e.message);
      }
    }
  }

  return result;
}

function find (text, array) {
  for (let i in array) {
    if (text.indexOf(array[i]) !== -1) {
      return true;
    }
  }
  return false;
}

module.exports = {
  graphql,
  find,
  puedeCrear,
  validarCabecera,
  validarAcceso,
  validarCrear
};
