# Cancillería - Apostilla (Backend)
=======================

### Requisitos previos antes de la instalación

### Datos

     1.- Datos de la persona que tendrá el rol de ADMINISTRADOR
     2.- Datos del usuario ADMINISTRADOR (usuario y correo electrónico)
     3.- Datos de cuenta de correo electrónico para envío de notificaciones
     4.- Datos de la oficina central

### Servicios requeridos

Servicios en ejecución de: SEGIP y FUNDEMPRESA.

Este manual de instalación es para una instalación mínima de Debian.

## Instalación de herramientas previas.

Actualizar el repositorio:

```sh
$ sudo apt-get update
```

Instalar utilidades (requirido para versiones de Debian con una instalación mínima.)

```sh
$ sudo apt-get install make g++ cron
```

Instalar descompresor de archivos para archivos tar v2

```sh
$ sudo apt-get install bzip2
```

Previo, verificar e instalar descargador de contenido:

```sh
$ sudo apt-get install curl
```

Instalar Git
```sh
$ sudo apt-get install -y git git-core
```
Para prevenir problemas con certificados
```sh
$ sudo apt-get install ca-certificates
```

Instalar npm
```sh
$ sudo apt-get install npm
```
Instalar pdftk para manejo de pdfs, en particular para el merge de dfs.
```sh
$ sudo apt-get install pdftk
```
### Instalar Node

Se usa la version LTS v8.9.1

###  Debian y Ubuntu 
##### Instalación de librerias build-essential
   ```sh
$ sudo apt-get install build-essential libssl-dev 
   ```
##### Instalando NVM manejador de versiones de Node
```sh
$ sudo npm install -g n
$ sudo n 8.9.1
```
### Creando enlace simbólico
```sh
$ sudo ln -s /usr/bin/nodejs /usr/bin/node
```

Actualización del paquete npm:
```sh
$ sudo npm install -g npm
```

###  (Instalar las fuentes para la generacion de pdf - necesario en caso de Debian)
```sh
$ sudo apt-get install libfontconfig
```

### Instalación de PostgreSQL

Ejecutar la siguiente instrucción para instalar postgres:

```sh
$ sudo apt-get install postgresql-9.6 postgresql-client-9.6
```
De no funcionar lo anterior. Hacer (segun https://www.postgresql.org/download/linux/debian/)
1. Agregar en /etc/apt/sources.list/pgdg.list
```sh
$ deb http://apt.postgresql.org/pub/repos/apt/ jessie-pgdg main
```
2. 
```sh
$ wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | \
  sudo apt-key add -
$ sudo apt-get update
```
Nota: esto instalara la v9.4 de pg. Update

Crear la base de datos:

```sh
$ sudo su - postgres
$ psql

create user <apostilla> with password '<apostilla>';
alter role <apostilla> with createdb;
create database <apostilla> with owner <apostilla>;
\q
exit
```

### Clonar el backend

Para evitar el mensaje: "server certificate verification failed". Haga:

```sh
$ git config  --global http.sslverify false
```

Clonar la rama por defecto: master.
```sh
$ git clone https://gitlab.geo.gob.bo/agetic/cancilleria-apostilla-backend
```

###  Ingresar en el directorio correspondiente
```
$ cd cancilleria-apostilla-backend
```

###  Instalar los utilitarios

```sh
$ sudo npm install sequelize-cli -g
$ sudo npm install async -g
$ sudo npm install phantomjs-prebuilt  -g
$ sudo npm install phantomjs -g
$ sudo npm install pg -g
$ sudo npm install -g eslint
```
---

###  Descargar dependencias con npm
Ingresar a cada directorio en el siguiente orden: common, infrastructure, domain, application, web
e instalar las dependencias con el siguiente comando
```
$ npm i
```
---

###  1.- Configurar conexión a la base de datos

Renombrar common/src/config/db.js.sample por common/src/config/db.js

```sh
$ cp common/src/config/db.js.sample common/src/config/db.js
```

```sh
$ nano common/src/config/db.js
```

Ejemplo contenido del archivo:

```
  database: process.env.DB_NAME || '<dato>',
  username: process.env.DB_USER || '<dato>',
  password: process.env.DB_PASS || '<dato>',
  host: process.env.DB_HOST || '<dato>',
```

### Inicializar creacion de tablas de la Base de datos
```
cd infrastructure
npm run setup
```
### Inicializar seeders la Base de datos
```
cd infrastructure
npm run seeders
```

### Servicios Web
Configurar en parametros seeders

- SEGIP: http://test.local.agetic.gob.bo/kong/segip/v2/status
- FUNDEMPRESA: http://test.local.agetic.gob.bo/kong/fundempresa/v1

# Ejecucion de la aplicacion

### 4.- Inicializar el servidor
Para iniciar el backend de la aplicación, ejecute el siguiente comando:
```
$ cd application
$ npm run start
```
Para iniciar el backend para el portal web, ejecute el siguiente comando:
```
$ cd web
$ npm run start
```

## Envio de correo con postfix
sudo apt-get install postfix
nano /etc/postfix/main.cf
poner en relayhost
relayhost = smtp.agetic.gob.bo

# Documentación para servicios web

### **Configuraciones**.

Abrir el archivo "config.js"

```sh
sudo nano document/config.js
```

ejemplo:

```javascript
module.exports = {
    "init": true,
    "port": 1337,
    "protocol": "http",
    "hostname": "localhost"
}
```

1. **init** abrir la documentación automaticamente en un navegador al iniciar el servidor.
2. **port** puerto de salida de información.
3. **protocol** conjunto de reglas que rigen el intercambio de información
4. **hostname** punto de inicio y final de las transferencias de datos.

### **Instalación**.

* Ingresar al directorio "document"

```sh
cd document/
```

* Descargar dependencias

```sh
npm install
```

* Inicializar el servidor

```sh
npm start
```