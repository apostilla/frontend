const fs = require("fs");
const opn = require('opn');
const cors = require('cors');
const express = require('express');
const settings = require('./config');

/**
 * @author  AGETIC G-J
 * @description Iniciar Server
 */
new(function (app, protocol, hostname, port, init) {

    const source = `${__dirname}/src`;

    app.use("/style", express.static(`${__dirname}/public/site.css`));

    app.use("/script", express.static(`${__dirname}/public/redoc.standalone.js`));

    const url = /^(localhost|127.0.0.1)$/i.test(hostname) ?
        `${protocol}://${hostname}:${port}/` :
        `${protocol}://${hostname}`;

    fs.readdir(source, function (err, collection) {

        collection.map(function (name) {

            if (/.json$/i.test(name)) {
                app.use(`/${name}`, express.static(`${source}/${name}`));
            }
        });
    });

    function view(res, file) {

        res.writeHead(200, {
            "Content-Type": "text/html",
        });
        res.end(
            `
            <!DOCTYPE html>
            <html>
                <head>
                    <title>CANCILLERÍA - APOSTILLA</title>
                    <meta charset="utf-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700|Roboto:300,400,700" rel="stylesheet">
                    <link media="all" rel="stylesheet" href="${url}style" />
                </head>
                <body>
                    <redoc spec-url='${url}${file}.json'></redoc>
                    <script src="${url}script"></script>
                </body>
            </html>
            `
        );
    }

    /**
     * @async
     * @return {html}  Pagina principal.
     * @description    Respuesta de servicio a la petición del verbo GET para la ruta "/".
     */
    app.get('/', cors(), function (req, res) {
        view(res, 'apostilla');
    });

    /**
     * @async
     * @return {html}  Pagina principal.
     * @description    Respuesta de servicio a la petición del verbo GET para la ruta "/sereci".
     */
    app.get('/sereci', cors(), function (req, res) {
        view(res, 'apostilla');
    });

    /**
     * @async
     * @return {html}  Pagina principal.
     * @description    Respuesta de servicio a la petición del verbo GET para la ruta "/digemig".
     */
    app.get('/digemig', cors(), function (req, res) {
        view(res, 'digemig');
    });

    app.listen(port, () => {
        console.log(`Server running at port ${port}`);
    });

    init && opn(url);

})(express(), settings.protocol, settings.hostname, settings.port, settings.init);