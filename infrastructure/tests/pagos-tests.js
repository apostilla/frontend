'use strict';

const test = require('ava');
const { config, errors } = require('common');
const db = require('../');

let repositories;

test.beforeEach(async () => {
  if (!repositories) {
    repositories = await db(config.db).catch(errors.handleFatalError);
  }
});

test.serial('pagos#findAll', async t => {
  const { pagos } = repositories;
  let lista = await pagos.findAll();

  t.is(lista.count, 10, 'Se tiene 10 registros en la bd');
});

test.serial('pagos#findById', async t => {
  const { pagos } = repositories;
  const id = 1;

  let pagosVar = await pagos.findById(id);

  t.is(pagosVar.id, id, 'Se recuperó el registro mediante un id');
});

test.serial('pagos#createOrUpdate - exists', async t => {
  const { pagos } = repositories;
  const newData = { id: 1, estado: 'PENDIENTE' };

  let pagosVar = await pagos.createOrUpdate(newData);

  t.is(pagosVar.estado, newData.estado, 'Actualizando registro pagos');
});

test.serial('pagos#createOrUpdate - new', async t => {
  const { pagos } = repositories;
  const nuevoPago = {
    cpt: { 'name': 'Book the First', 'author': { 'first_name': 'Bob', 'last_name': 'White' } },
    tipo: 'ENTIDAD',
    fecha_pago: '2017-11-10',
    estado: 'PENDIENTE',
    tipo_documento: 'CI',
    nro_documento: '123456789',
    titular_factura: 'CONDE',
    id_tramite_doc: 1,
    _user_created: 1,
    _created_at: new Date()
  };

  let pagosVar = await pagos.createOrUpdate(nuevoPago);

  t.true(typeof pagosVar.id === 'number', 'Comprobando que el nuevo tramite tenga un id');
  t.is(pagosVar.tipo, nuevoPago.tipo, 'Creando registro - tipo');
  t.is(pagosVar.estado, nuevoPago.estado, 'Creando registro - estado');
  t.is(pagosVar.id_tramite_doc, nuevoPago.id_tramite_doc, 'Creando registro - id_tramite_doc');

  test.id = pagosVar.id;
});

test.serial('pagos#findAll#filter#tipo', async t => {
  const { pagos } = repositories;
  let lista = await pagos.findAll({ tipo: 'CANCILLERIA' });
  t.true(lista.rows.length >= 0, 'Se tiene registros en la bd');
});
/*
test.serial('pagos#findAll#filter#fecha_pago', async t => {
  const { pagos } = repositories;
  let lista = await pagos.findAll({ fecha_pago: '2017-11-10' });

  t.true(lista.rows.length >= 1, 'Se tiene registros en la bd');
});
*/
test.serial('pagos#findAll#filter#estado', async t => {
  const { pagos } = repositories;
  let lista = await pagos.findAll({ estado: 'HABILITADO' });
  t.true(lista.rows.length >= 1, 'Se tiene registros en la bd');
});

test.serial('pagos#findAll#filter#nroDocumento', async t => {
  const { pagos } = repositories;
  let lista = await pagos.findAll({ nro_documento: '123456789' });
  t.true(lista.rows.length >= 1, 'Se tiene registros en la bd');
});

test.serial('pagos#findAll#filter#titularFactura', async t => {
  const { pagos } = repositories;
  let lista = await pagos.findAll({ titular_factura: 'CONDE' });
  t.true(lista.rows.length >= 1, 'Se tiene registros en la bd');
});

test.serial('pagos#delete', async t => {
  const { pagos } = repositories;
  let deleted = await pagos.deleteItem(test.id);

  t.is(deleted, 1, 'pago eliminado');
});
