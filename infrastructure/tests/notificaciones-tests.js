'use strict';

const test = require('ava');
const { config, errors } = require('common');
const db = require('../');
let nuevoNotificacionObj;
let repositories;

test.beforeEach(async () => {
  if (!repositories) {
    repositories = await db(config.db).catch(errors.handleFatalError);
  }
});

test.serial('Notificacion#findAll', async t => {
  const { notificaciones } = repositories;
  let lista = await notificaciones.findAll();

  t.is(lista.count, 10, 'Se tiene 10 registros en la bd');
});

test.serial('Notificacion#findById', async t => {
  const { notificaciones } = repositories;
  const id = 1;

  let notificacion = await notificaciones.findById(id);

  t.is(notificacion.id, id, 'Se recuperó el registro mediante un id');
});

test.serial('Notificacion#createOrUpdate - exists', async t => {
  const { notificaciones } = repositories;
  const newData = { id: 2, titulo: 'NUEVO TITULO' };

  let notificacion = await notificaciones.createOrUpdate(newData);

  t.is(notificacion.titulo, newData.titulo, 'Actualizando registro notificacion');
});

test.serial('Notificacion#createOrUpdate - new', async t => {
  const { notificaciones } = repositories;
  const nuevoNotificacion = {
    tipo: 'CPT',
    titulo: 'ASDFA ASDF',
    email_sender: 'yo@gmail.com',
    mensaje: 'Aasdlkjf alkfsaf asf',
    estado: 'LEIDO',
    _user_created: 1,
    _created_at: new Date()
  };
  let notificacion = await notificaciones.createOrUpdate(nuevoNotificacion);
  t.true(typeof notificacion.id === 'number', 'Comprobando que el nuevo usuario tenga un id');
  t.is(notificacion.tipo, nuevoNotificacion.tipo, 'Creando registro - tipo');
  t.is(notificacion.titulo, nuevoNotificacion.titulo, 'Creando registro - titulo');
  t.is(notificacion.email_sender, nuevoNotificacion.email_sender, 'Creando registro - email_sender');
  t.is(notificacion.mensaje, nuevoNotificacion.mensaje, 'Creando registro - mensaje');
  t.is(notificacion.estado, nuevoNotificacion.estado, 'Creando registro - estado');

  test.idNotificacion = notificacion.id;
  nuevoNotificacionObj = nuevoNotificacion;
});

test.serial('Notificacion#findAll#filter', async t => {
  const { notificaciones } = repositories;
  let lista = await notificaciones.findAll({ tipo: nuevoNotificacionObj.tipo, titulo: nuevoNotificacionObj.titulo, mensaje: nuevoNotificacionObj.mensaje, estado: nuevoNotificacionObj.estado });

  t.is(lista.rows.length, 1, 'Filtrando datos');
});

test.serial('Notificacion#delete', async t => {
  const { notificaciones } = repositories;
  let deleted = await notificaciones.deleteItem(test.idNotificacion);

  t.is(deleted, 1, 'Notificacion eliminado');
});
