'use strict';

const test = require('ava');
const { config, errors } = require('common');
const db = require('../');

let repositories;

test.beforeEach(async () => {
  if (!repositories) {
    repositories = await db(config.db).catch(errors.handleFatalError);
  }
});

test.serial('Archivo#findAll', async t => {
  const { archivos } = repositories;
  let lista = await archivos.findAll();

  t.is(lista.count, 10, 'Se tiene 10 registros en la bd');
});

test.serial('Archivo#findById', async t => {
  const { archivos } = repositories;
  const id = 1;

  let archivo = await archivos.findById(id);

  t.is(archivo.id, id, 'Se recuperó el registro mediante un id');
});

test.serial('Archivo#createOrUpdate - exists', async t => {
  const { archivos } = repositories;
  const newData = { id: 2, nombre: 'NUEVO NOMBRE ARCHIVO' };

  let archivo = await archivos.createOrUpdate(newData);

  t.is(archivo.nombre, newData.nombre, 'Actualizando registro archivo');
});

test.serial('Archivo#createOrUpdate - new', async t => {
  const { archivos } = repositories;
  const nuevoArchivo = {
    nombre: 'ARCHIVO BLBLA',
    titulo: 'Cualquier cosa',
    mimetype: 'mytype etc',
    ruta: '/ruta/',
    tamano: '1.2',
    img_ancho: 6,
    img_alto: 7,
    es_imagen: true,
    _user_created: 1,
    _created_at: new Date()
  };

  let archivo = await archivos.createOrUpdate(nuevoArchivo);

  t.true(typeof archivo.id === 'number', 'Comprobando que el nuevo archivo tenga un id');
  t.is(archivo.nombre, nuevoArchivo.nombre, 'Creando registro - nombre');
  t.is(archivo.titulo, nuevoArchivo.titulo, 'Creando registro - titulo');
  t.is(archivo.mimetype, nuevoArchivo.mimetype, 'Creando registro - mimetype');
  t.is(archivo.ruta, nuevoArchivo.ruta, 'Creando registro - ruta');
  t.is(archivo.tamano, nuevoArchivo.tamano, 'Creando registro - tamano');
  t.is(archivo.img_ancho, nuevoArchivo.img_ancho, 'Creando registro - img_ancho');
  t.is(archivo.img_alto, nuevoArchivo.img_alto, 'Creando registro - img_alto');
  t.is(archivo.es_imagen, nuevoArchivo.es_imagen, 'Creando registro - es_imagen');

  test.idArchivo = archivo.id;
});

test.serial('Archivo#findAll#filter', async t => {
  const { archivos } = repositories;
  let lista = await archivos.findAll({nombre: 'ARCHIVO BLBLA'});

  t.is(lista.rows.length, 1, 'Filtrando datos');
});

test.serial('Archivo#delete', async t => {
  const { archivos } = repositories;
  let deleted = await archivos.deleteItem(test.idArchivo);

  t.is(deleted, 1, 'archivo eliminado');
});
