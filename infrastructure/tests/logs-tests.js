'use strict';

const test = require('ava');
const { config, errors } = require('common');
const db = require('../');

let repositories;

test.beforeEach(async () => {
  if (!repositories) {
    repositories = await db(config.db).catch(errors.handleFatalError);
  }
});

test.serial('Log#findAll', async t => {
  const { logs } = repositories;
  let lista = await logs.findAll();

  t.is(lista.count, 50, 'Se tiene 50 registros en la bd');
});

test.serial('Log#findById', async t => {
  const { logs } = repositories;
  const id = 1;

  let log = await logs.findById(id);

  t.is(log.id, id, 'Se recuperó el registro mediante un id');
});

test.serial('Log#createOrUpdate - update', async t => {
  const { logs } = repositories;
  const newData = {
    id: 1,
    nivel: 'ERROR',
    tipo: 'BASE DE DATOS',
    mensaje: 'Sequelize bd',
    ip: '255.255.255.254',
    referencia: 'sequelize bd error',
    fecha: new Date()
  };

  let log = await logs.createOrUpdate(newData);

  t.is(log.nivel, newData.nivel, 'Actualizando registro log');
});

test.serial('Log#createOrUpdate - new', async t => {
  const { logs } = repositories;
  const nuevoLog = {
    nivel: 'ERROR',
    tipo: 'BASE DE DATOS',
    mensaje: 'Error sequelize',
    referencia: 'Muchos errores sequelize',
    ip: '255.255.255.255',
    fecha: new Date()
  };

  let log = await logs.createOrUpdate(nuevoLog);

  t.true(typeof log.id === 'number', 'Comprobando que el nuevo log tenga un id');
  t.is(log.tipo, nuevoLog.tipo, 'Creando registro - tipo');
  t.is(log.mensaje, nuevoLog.mensaje, 'Creando registro - mensaje');
  t.is(log.referencia, nuevoLog.referencia, 'Creando registro - referencia');
  t.is(log.ip, nuevoLog.ip, 'Creando registro - ip');
  t.is(log.fecha.getTime(), nuevoLog.fecha.getTime(), 'Creando registro - fecha');

  test.idLog = log.id;
});

test.serial('Log#findAll#filter#mensaje', async t => {
  const { logs } = repositories;
  let lista = await logs.findAll({ mensaje: 'sequel' });

  t.is(lista.count, 2, 'Se tiene 2 registros en la bd');
});

test.serial('Log#findAll#filter#tipo', async t => {
  const { logs } = repositories;
  let lista = await logs.findAll({ tipo: 'BASE DE DATOS' });

  t.is(lista.count, 2, 'Se tiene 2 registros en la bd');
});

test.serial('Log#findAll#filter#referencia', async t => {
  const { logs } = repositories;
  let lista = await logs.findAll({ referencia: 'seque' });

  t.is(lista.count, 2, 'Se tiene 2 registros en la bd');
});

test.serial('Log#findAll#filter#ip', async t => {
  const { logs } = repositories;
  let lista = await logs.findAll({ ip: '255.255.255' });

  t.is(lista.count, 2, 'Se tiene 2 registros en la bd');
});

test.serial('Log#findAll#filter#fecha', async t => {
  const { logs } = repositories;
  let lista = await logs.findAll({ fecha: new Date() });

  t.is(lista.count, 2, 'Se tiene 2 registros en la bd');
});

test.serial('Log#delete', async t => {
  const { logs } = repositories;
  let deleted = await logs.deleteItem(test.idLog);

  t.true(deleted, 'Usuario eliminado');
});
