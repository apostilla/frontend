'use strict';

const test = require('ava');
const { config, errors } = require('common');
const db = require('../');

let repositories;

test.beforeEach(async () => {
  if (!repositories) {
    repositories = await db(config.db).catch(errors.handleFatalError);
  }
});

test.serial('Documento#findAll', async t => {
  const { documentos } = repositories;
  let lista = await documentos.findAll();

  t.true(lista.count >= 20, 'Se tiene 20 registros en la bd');
});

test.serial('Documento#findById', async t => {
  const { documentos } = repositories;
  const id = 1;

  let documento = await documentos.findById(id);

  t.is(documento.id, id, 'Se recuperó el registro mediante un id');
});

test.serial('Documento#createOrUpdate - exists', async t => {
  const { documentos } = repositories;
  const newData = { id: 2, nombre: 'NUEVO NOMBRE' };

  let documento = await documentos.createOrUpdate(newData);

  t.is(documento.nombre, newData.nombre, 'Actualizando registro documento');
});

test.serial('Documento#createOrUpdate - new', async t => {
  const { documentos } = repositories;
  const nuevoDocumento = {
    nombre: 'MI NUEVO DOUCMENTO',
    descripcion: 'Cualquier cosa vaa aqui',
    tramite_ppte: {},
    estado: 'ACTIVO',
    token_ppte: 'FSDFJKASHF3434534DSFSD44',
    codigo_portal: 'sjfas',
    cuenta: 12345,
    monto: 100,
    id_entidad: 1,
    _user_created: 1,
    _created_at: new Date()
  };

  let documento = await documentos.createOrUpdate(nuevoDocumento);

  t.true(typeof documento.id === 'number', 'Comprobando que el nuevo documento tenga un id');
  t.is(documento.nombre, nuevoDocumento.nombre, 'Creando registro - nombre');
  t.is(documento.descripcion, nuevoDocumento.descripcion, 'Creando registro - descripcion');
  t.is(JSON.stringify(documento.tramite_ppte), JSON.stringify(nuevoDocumento.tramite_ppte), 'Creando registro - tramite_ppte');
  t.is(documento.estado, nuevoDocumento.estado, 'Creando registro - estado');
  t.is(documento.token_ppte, nuevoDocumento.token_ppte, 'Creando registro - token_ppte');
  t.is(documento.codigo_portal, nuevoDocumento.codigo_portal, 'Creando registro - codigo_portal');

  test.idDocumento = documento.id;
});

test.serial('Documento#findAll#filter', async t => {
  const { documentos } = repositories;
  let lista = await documentos.findAll({ nombre: 'MI NUEVO DOUCMENTO', estado: 'ACTIVO', id_entidad: 1 });
  t.is(lista.rows.length, 1, 'Filtrando datos');
});

test.serial('Documento#delete', async t => {
  const { documentos } = repositories;
  let deleted = await documentos.deleteItem(test.idDocumento);

  t.is(deleted, 1, 'documento eliminado');
});
