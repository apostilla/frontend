'use strict';

const test = require('ava');
const { config, errors } = require('common');
const db = require('../');

let repositories;

test.beforeEach(async () => {
  if (!repositories) {
    repositories = await db(config.db).catch(errors.handleFatalError);
  }
});

test.serial('Departamento#findAll', async t => {
  const { departamentos } = repositories;
  let lista = await departamentos.findAll();

  t.true(lista.count >= 9, 'Se tiene 10 registros en la bd');
});

test.serial('Departamento#findById', async t => {
  const { departamentos } = repositories;
  const id = 1;

  let Departamento = await departamentos.findById(id);

  t.is(Departamento.id, id, 'Se recuperó el registro mediante un id');
});

test.serial('Departamento#createOrUpdate - new', async t => {
  const { departamentos } = repositories;

  let lista = await departamentos.findAll();
  const nuevoDepartamento = {
    id: lista.count + 1,
    nombre: 'test',
    codigo: 'TEST',
    _user_created: 1,
    _created_at: new Date()
  };

  let Departamento = await departamentos.createOrUpdate(nuevoDepartamento);

  t.true(typeof Departamento.id === 'number', 'Comprobando que el nuevo Departamento tenga un id');
  t.is(Departamento.Departamento, nuevoDepartamento.Departamento, 'Creando registro - Departamento');
  t.is(Departamento.codigo, nuevoDepartamento.codigo, 'Creando registro - codigo');
  t.is(Departamento.nombre, nuevoDepartamento.nombre, 'Creando registro - nombre');

  test.id = Departamento.id;
});

test.serial('Departamento#createOrUpdate - exists', async t => {
  const { departamentos } = repositories;
  const newData = { id: test.id, codigo: 'NN' };

  let Departamento = await departamentos.createOrUpdate(newData);

  t.is(Departamento.codigo, newData.codigo, 'Actualizando registro Departamento');
});

test.serial('departamento#findAll#filter#codigo', async t => {
  const { departamentos } = repositories;
  let variable = 'CBBA';
  let Departamento = await departamentos.findAll({ codigo: variable });
  t.true(Departamento.rows.length > 0, 'Se recuperó el registro mediante un codigo');
});

test.serial('Departamento#delete', async t => {
  const { departamentos } = repositories;
  let deleted = await departamentos.deleteItem(test.id);

  t.is(deleted, 1, 'Departamento eliminado');
});
