'use strict';

const test = require('ava');
const { config, errors } = require('common');
const db = require('../');

let repositories;

test.beforeEach(async () => {
  if (!repositories) {
    repositories = await db(config.db).catch(errors.handleFatalError);
  }
});

test.serial('PaisesDestino#findAll', async t => {
  const { paises_destinos } = repositories;
  let lista = await paises_destinos.findAll();

  t.is(lista.count, 115, 'Se tiene 115 registros en la bd');
});

test.serial('PaisesDestino#findById', async t => {
  const { paises_destinos } = repositories;
  const id = 1;

  let paisesDestinos = await paises_destinos.findById(id);

  t.is(paisesDestinos.id, id, 'Se recuperó el registro mediante un id');
});

test.serial('PaisesDestino#createOrUpdate - exists', async t => {
  const { paises_destinos } = repositories;
  const newData = { id: 1, miembro: false };

  let paisesDestino = await paises_destinos.createOrUpdate(newData);

  t.is(paisesDestino.codigo, newData.codigo, 'Actualizando registro paisesDestino');
});

test.serial('PaisesDestino#createOrUpdate - new', async t => {
  const { paises_destinos } = repositories;
  const nuevoPaisDestino = {
    nombre: 'test',
    miembro: false,
    _user_created: 1,
    _created_at: new Date()
  };

  let paisesDestino = await paises_destinos.createOrUpdate(nuevoPaisDestino);

  t.true(typeof paisesDestino.id === 'number', 'Comprobando que el nuevo pais destino tenga un id');
  t.is(paisesDestino.miembro, nuevoPaisDestino.miembro, 'Creando registro - pais destino');
  t.is(paisesDestino.nombre, nuevoPaisDestino.nombre, 'Creando registro - nombre');

  test.id = paisesDestino.id;
});

test.serial('PaisesDestino#findAll#filter#Miembro', async t => {
  const { paises_destinos } = repositories;
  let tramite = await paises_destinos.findAll({ miembro: true });

  t.true(tramite.rows.length >= 1, 'Se recuperó el registro mediante miembro');
});

test.serial('PaisesDestino#delete', async t => {
  const { paises_destinos } = repositories;
  let deleted = await paises_destinos.deleteItem(test.id);

  t.is(deleted, 1, 'paisesDestino eliminado');
});
