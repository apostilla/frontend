'use strict';

const test = require('ava');
const { config, errors } = require('common');
const db = require('../');

let repositories;

test.beforeEach(async () => {
  if (!repositories) {
    repositories = await db(config.db).catch(errors.handleFatalError);
  }
});

test.serial('Tramites#findAll', async t => {
  const { tramites } = repositories;
  let lista = await tramites.findAll();

  t.true(lista.count >= 20, 'Se tiene 20 registros en la bd');
});

test.serial('Tramites#findById', async t => {
  const { tramites } = repositories;
  const id = 1;

  let tramite = await tramites.findById(id);

  t.is(tramite.id, id, 'Se recuperó el registro mediante un id');
});

test.serial('Tramites#createOrUpdate - new', async t => {
  const { tramites } = repositories;
  const nuevotramite = {
    fecha_inicio: '1999-10-10 00:00:00',
    fecha_fin: '1999-10-10 00:00:00',
    estado: 'SOLICITADO',
    cod_seguimiento: parseInt(Math.random() * (10000000 - 1)) + '',
    // id_pais_destino: 1,
    id_solicitante: 1,
    _user_created: 1,
    _created_at: new Date()
  };

  let tramite = await tramites.createOrUpdate(nuevotramite);

  t.true(typeof tramite.id === 'number', 'Comprobando que el nuevo tramite tenga un id');
  t.is(tramite.estado, nuevotramite.estado, 'Creando registro - estado');
  t.is(tramite.cod_seguimiento, nuevotramite.cod_seguimiento, 'Creando registro - cod_seguimiento');
  // t.is(tramite.id_pais_destino, nuevotramite.id_pais_destino, 'Creando registro - id_pais_destino');
  t.is(tramite.id_solicitante, nuevotramite.id_solicitante, 'Creando registro - id_solicitante');

  test.id = tramite.id;
  test.code = tramite.cod_seguimiento;
});

test.serial('Tramites#findByCode', async t => {
  const { tramites } = repositories;
  let lista = await tramites.findByCode(test.code);

  console.log(lista);
  t.true(!!lista, 'Se recuperó el registro mediante un código de seguimiento');
});

test.serial('Tramites#createOrUpdate - exists', async t => {
  const { tramites } = repositories;
  const newData = { id: test.id, estado: 'FINALIZADO' };

  let tramite = await tramites.createOrUpdate(newData);

  t.is(tramite.estado, newData.estado, 'Actualizando registro tramite');
});

/*
test.serial('Tramites#findAll#filter#idPaisDestino', async t => {
  const { tramites } = repositories;
  let tramite = await tramites.findAll({ id_pais_destino: '8' });

  t.true(tramite.count >= 1, 'Se recuperó el registro mediante id_pais_destino');
});
*/
test.serial('Tramites#findAll#filter#CodSeguimiento', async t => {
  const { tramites } = repositories;
  let tramite = await tramites.findAll({ cod_seguimiento: '6' });

  t.true(tramite.rows.length >= 1, 'Se recuperó el registro mediante cod_seguimiento');
});

test.serial('Tramites#findAll#filter#Estado', async t => {
  const { tramites } = repositories;
  let tramite = await tramites.findAll({ estado: 'EN_PROCESO' });

  t.true(tramite.rows.length > 1, 'Se recuperó el registro mediante estado');
});

test.serial('Tramites#delete', async t => {
  const { tramites } = repositories;
  let deleted = await tramites.deleteItem(test.id);

  t.is(deleted, 1, 'tramite eliminado');
});
