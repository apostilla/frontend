'use strict';

const test = require('ava');
const { config, errors } = require('common');
const db = require('../');

let repositories;

test.beforeEach(async () => {
  if (!repositories) {
    repositories = await db(config.db).catch(errors.handleFatalError);
  }
});

test.serial('solicitante#findAll', async t => {
  const { solicitantes } = repositories;
  let lista = await solicitantes.findAll();

  t.true(lista.count >= 20, 'Se tiene 20 registros en la bd');
});

test.serial('solicitante#findById', async t => {
  const { solicitantes } = repositories;
  const id = 1;

  let solicitante = await solicitantes.findById(id);

  t.is(solicitante.id, id, 'Se recuperó el registro mediante un id');
});

test.serial('solicitante#createOrUpdate - exists', async t => {
  const { solicitantes } = repositories;
  const newData = { id: 1, nombres: 'Nuevo nombre' };

  let solicitante = await solicitantes.createOrUpdate(newData);

  t.is(solicitante.nombres, newData.nombres, 'Actualizando registro solicitante');
});

test.serial('solicitante#createOrUpdate - new', async t => {
  const { solicitantes } = repositories;
  const nuevoSolicitante = {
    tipo: 'NATURAL',
    nombres: 'fernando',
    primer_apellido: 'conde',
    segundo_apellido: 'conde',
    email: 'test@gmail.com',
    telefono: '1234567890',
    fecha_nacimiento: '1999-10-10',
    tipo_documento: 'CI',
    nro_documento: '123456789',
    direccion: 'xxxx xxxx xxx xx',
    id_departamento: 1,
    id_provincia: 1,
    id_municipio: 1,
    _user_created: 1,
    _created_at: new Date()
  };

  let solicitante = await solicitantes.createOrUpdate(nuevoSolicitante);

  t.true(typeof solicitante.id === 'number', 'Comprobando que el nuevo solicitante tenga un id');
  t.is(solicitante.tipo, nuevoSolicitante.tipo, 'Creando registro - tipo');
  t.is(solicitante.nombres, nuevoSolicitante.nombres, 'Creando registro - nombres');
  t.is(solicitante.primer_apellido, nuevoSolicitante.primer_apellido, 'Creando registro - primer_apellido');
  t.is(solicitante.segundo_apellido, nuevoSolicitante.segundo_apellido, 'Creando registro - segundo_apellido');
  t.is(solicitante.email, nuevoSolicitante.email, 'Creando registro - email');
  t.is(solicitante.telefono, nuevoSolicitante.telefono, 'Creando registro - telefono');
  t.is(solicitante.fecha_nacimiento, nuevoSolicitante.fecha_nacimiento, 'Creando registro - fecha_nacimiento');
  t.is(solicitante.tipo_documento, nuevoSolicitante.tipo_documento, 'Creando registro - tipo_documento');
  t.is(solicitante.nro_documento, nuevoSolicitante.nro_documento, 'Creando registro - nro_documento');
  t.is(solicitante.direccion, nuevoSolicitante.direccion, 'Creando registro - direccion');

  test.id = solicitante.id;
});

test.serial('solicitante#findAll#filter#Departamento', async t => {
  const { solicitantes } = repositories;
  let tramite = await solicitantes.findAll({ id_departamento: 1 });

  t.true(tramite.rows.length >= 1, 'Se recuperó el registro mediante un id_departamento');
});

test.serial('solicitante#findAll#filter#Provincia', async t => {
  const { solicitantes } = repositories;
  let tramite = await solicitantes.findAll({ id_provincia: 1 });

  t.true(tramite.rows.length >= 1, 'Se recuperó el registro mediante un id_provincia');
});

test.serial('solicitante#findAll#filter#municipio', async t => {
  const { solicitantes } = repositories;
  let tramite = await solicitantes.findAll({ id_municipio: 1 });

  t.true(tramite.rows.length >= 1, 'Se recuperó el registro mediante un id_municipio');
});

test.serial('solicitante#findAll#filter#Tipo', async t => {
  const { solicitantes } = repositories;
  let variable = 'NATURAL';
  let tramite = await solicitantes.findAll({ tipo: variable });

  t.true(tramite.rows.length > 1, 'Se recuperó el registro mediante un tipo');
});

test.serial('solicitante#findAll#filter#TipoDocumento', async t => {
  const { solicitantes } = repositories;
  let variable = 'PASAPORTE';
  let tramite = await solicitantes.findAll({ tipo_documento: variable });

  t.true(tramite.rows.length > 1, 'Se recuperó el registro mediante un tipo_documento');
});

test.serial('solicitante#findAll#filter#nombre_completo', async t => {
  const { solicitantes } = repositories;
  let lista = await solicitantes.findAll({ nombre_completo: 'a' });
  t.true(lista.rows.length > 1, 'Se tiene registros en la bd');
});

test.serial('solicitante#delete', async t => {
  const { solicitantes } = repositories;
  let deleted = await solicitantes.deleteItem(test.id);

  t.is(deleted, 1, 'solicitante eliminado');
});
