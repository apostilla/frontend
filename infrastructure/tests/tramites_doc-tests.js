'use strict';

const test = require('ava');
const { config, errors } = require('common');
const db = require('../');

let repositories;

test.beforeEach(async () => {
  if (!repositories) {
    repositories = await db(config.db).catch(errors.handleFatalError);
  }
});

test.serial('tramitesDoc#findAll', async t => {
  const { tramites_doc } = repositories;
  let lista = await tramites_doc.findAll();
  t.true(lista.count >= 20, 'Se tiene 20 reistros en la bd');
});

test.serial('tramitesDocs#findById', async t => {
  const { tramites_doc } = repositories;
  const id = 1;

  let tramiteDoc = await tramites_doc.findById(id);

  t.is(tramiteDoc.id, id, 'Se recuperó el registro mediante un id');
});

test.serial('tramitesDocs#createOrUpdate - new', async t => {
  const { tramites_doc } = repositories;
  const code = '' + Math.random() * (50000 - 1);
  const nuevotramiteDoc = {
    pago_entidad: false,
    pago_cancilleria: false,
    cod_seguridad: code,
    nro_apostilla: '123456',
    fecha_expedicion: new Date(2017, 10, 10, 0, 0, 0),
    estado: 'APOSTILLADO',
    observacion: '',
    id_tramite: 1,
    id_documento: 1,
    id_archivo: 1,
    _user_created: 1,
    _created_at: new Date()
  };

  let tramiteDoc = await tramites_doc.createOrUpdate(nuevotramiteDoc);

  t.true(typeof tramiteDoc.id === 'number', 'Comprobando que el nuevo tramite tenga un id');
  t.is(tramiteDoc.pago_entidad, nuevotramiteDoc.pago_entidad, 'Creando registro - pago_entidad');
  t.is(tramiteDoc.pago_cancilleria, nuevotramiteDoc.pago_cancilleria, 'Creando registro - pago_cancilleria');
  t.is(tramiteDoc.cod_seguridad, nuevotramiteDoc.cod_seguridad, 'Creando registro - cod_seguridad');
  t.is(tramiteDoc.nro_apostilla, nuevotramiteDoc.nro_apostilla, 'Creando registro - nro_apostilla');
  t.is(tramiteDoc.estado, nuevotramiteDoc.estado, 'Creando registro - estado');
  t.is(tramiteDoc.id_tramite, nuevotramiteDoc.id_tramite, 'Creando registro - id_tramite');
  t.is(tramiteDoc.id_documento, nuevotramiteDoc.id_documento, 'Creando registro - id_documento');
  t.is(tramiteDoc.id_archivo, nuevotramiteDoc.id_archivo, 'Creando registro - id_archivo');

  test.id = tramiteDoc.id;
  test.code = code;
  test.number = '123456';
  test.date = new Date(2017, 10, 10, 0, 0, 0);
});

test.serial('tramitesDocs#findByCode', async t => {
  const { tramites_doc } = repositories;
  const { code, number, date } = test;

  let tramiteDoc = await tramites_doc.findByCode(code, number, date);
  console.log('---------------------------------------------------------');
  console.log(tramiteDoc);

  t.is(tramiteDoc.cod_seguridad, code, 'Se recuperó el registro mediante un code');
  t.is(tramiteDoc.nro_apostilla, number, 'Se recuperó el registro mediante un number');
  // t.is(tramiteDoc.fecha_expedicion, date, 'Se recuperó el registro mediante un date');
});

test.serial('tramitesDocs#createOrUpdate - exists', async t => {
  const { tramites_doc } = repositories;
  const newData = { id: test.id, estado: 'REVISADO_ENTIDAD' };

  let tramiteDoc = await tramites_doc.createOrUpdate(newData);

  t.is(tramiteDoc.estado, newData.estado, 'Actualizando registro tramites_doc');
});

test.serial('tramitesDocs#findAll#filter#pagoEntidad', async t => {
  const { tramites_doc } = repositories;
  let lista = await tramites_doc.findAll({ pago_entidad: true });
  t.true(lista.rows.length >= 1, 'Se busco por pago_entidad');
});

test.serial('tramitesDocs#findAll#filter#pagoCancilleria', async t => {
  const { tramites_doc } = repositories;
  let lista = await tramites_doc.findAll({ pago_cancilleria: true });
  t.true(lista.rows.length >= 1, 'Se busco por pago_cancilleria');
});

test.serial('tramitesDocs#findAll#filter#fechaExpedicion', async t => {
  const { tramites_doc } = repositories;
  let lista = await tramites_doc.findAll({ fecha_expedicion: new Date(2017, 10, 10, 0, 0, 0) });
  t.true(lista.rows.length >= 1, 'Se busco por fechaExpedicion');
});

test.serial('tramitesDocs#findAll#filter#cod_seguridad', async t => {
  const { tramites_doc } = repositories;
  let lista = await tramites_doc.findAll({ cod_seguridad: '8' });
  t.true(lista.rows.length >= 1, 'Se busco por cod_seguridad');
});

test.serial('tramitesDocs#findAll#filter#nro_apostilla', async t => {
  const { tramites_doc } = repositories;
  let lista = await tramites_doc.findAll({ nro_apostilla: '123456' });
  t.true(lista.rows.length >= 1, 'Se busco por nro_apostilla');
});

test.serial('tramitesDocs#delete', async t => {
  const { tramites_doc } = repositories;
  let deleted = await tramites_doc.deleteItem(test.id);

  t.is(deleted, 1, 'tramite eliminado');
});
