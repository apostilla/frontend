'use strict';

const test = require('ava');
const { config, errors } = require('common');
const db = require('../');

let repositories;

test.beforeEach(async () => {
  if (!repositories) {
    repositories = await db(config.db).catch(errors.handleFatalError);
  }
});

test.serial('municipio#findAll', async t => {
  const { municipios } = repositories;
  let lista = await municipios.findAll();

  t.true(lista.count >= 339, 'Se tiene 339 registros en la bd');
});

test.serial('municipio#findById', async t => {
  const { municipios } = repositories;
  const id = 1;

  let municipio = await municipios.findById(id);

  t.is(municipio.id, id, 'Se recuperó el registro mediante un id');
});

test.serial('municipio#findAll#filter#idProvincia', async t => {
  const { municipios } = repositories;
  let variable = 1;
  let tramite = await municipios.findAll({ id_provincia: variable });

  t.true(tramite.rows.length > 1, 'Se recuperó el registro mediante un id_provincia');
});

test.serial('municipio#createOrUpdate - exists', async t => {
  const { municipios } = repositories;
  const newData = { id: 1, id_provincia: 1 };

  let municipio = await municipios.createOrUpdate(newData);

  t.is(municipio.id_provincia, newData.id_provincia, 'Actualizando registro municipio');
});

test.serial('municipio#createOrUpdate - new', async t => {
  const { municipios } = repositories;
  let lista = await municipios.findAll();
  const nuevomunicipio = {
    id: lista.count + 1,
    nombre: 'test',
    id_provincia: 1,
    _user_created: 1,
    _created_at: new Date()
  };

  let municipio = await municipios.createOrUpdate(nuevomunicipio);

  t.true(typeof municipio.id === 'number', 'Comprobando que el nuevo municipio tenga un id');
  t.is(municipio.municipio, nuevomunicipio.municipio, 'Creando registro - municipio');
  t.is(municipio.id_provincia, nuevomunicipio.id_provincia, 'Creando registro - id_provincia');
  t.is(municipio.nombre, nuevomunicipio.nombre, 'Creando registro - nombre');

  test.id = municipio.id;
});

test.serial('municipio#delete', async t => {
  const { municipios } = repositories;
  let deleted = await municipios.deleteItem(test.id);

  t.is(deleted, 1, 'municipio eliminado');
});
