'use strict';

const test = require('ava');
const { config, errors } = require('common');
const db = require('../');

let repositories;

test.beforeEach(async () => {
  if (!repositories) {
    repositories = await db(config.db).catch(errors.handleFatalError);
  }
});

test.serial('Firma#findAll', async t => {
  const { firmas } = repositories;
  let lista = await firmas.findAll();

  t.is(lista.count, 10, 'Se tiene 10 registros en la bd');
});

test.serial('Firma#findById', async t => {
  const { firmas } = repositories;
  const id = 1;

  let firma = await firmas.findById(id);
  t.is(firma.id, id, 'Se recuperó el registro mediante un id');
});

test.serial('Firma#createOrUpdate - new', async t => {
  const { firmas } = repositories;
  const nuevoFirma = {
    base64: 'Mi nueva base 64..',
    fecha_firma: '2017-11-01',
    id_archivo: 1,
    id_usuario: 1,
    certificado_token: JSON.stringify({ name: 'chavo el ocho', fecha: new Date() }),
    _user_created: 1,
    _created_at: new Date()
  };

  let firma = await firmas.createOrUpdate(nuevoFirma);

  t.true(typeof firma.id === 'number', 'Comprobando que el nuevo firma tenga un id');
  t.is(firma.base64, nuevoFirma.base64, 'Creando registro - base64');
  // t.is(new Date(firma.fecha_firma), new Date(nuevoFirma.fecha_firma), 'Creando registro - fecha_firma');

  test.idFirma = firma.id;
});

test.serial('Firma#createOrUpdate - exists', async t => {
  const { firmas } = repositories;
  const newData = { id: test.idFirma, base64: 'NUEVO BASE64' };

  let firma = await firmas.createOrUpdate(newData);

  t.is(firma.base64, newData.base64, 'Actualizando registro firma');
});

test.serial('Firma#findAll#filter', async t => {
  const { firmas } = repositories;
  let lista = await firmas.findAll({ id_usuario: 1 });
  t.true(lista.count > 0, 'Filtrando datos');
});

test.serial('Firma#delete', async t => {
  const { firmas } = repositories;
  let deleted = await firmas.deleteItem(test.idFirma);

  t.is(deleted, 1, 'firma eliminado');
});
