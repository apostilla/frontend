'use strict';

const test = require('ava');
const { config, errors } = require('common');
const db = require('../');

let repositories;

test.beforeEach(async () => {
  if (!repositories) {
    repositories = await db(config.db).catch(errors.handleFatalError);
  }
});

test.serial('provincia#findAll', async t => {
  const { provincias } = repositories;
  let lista = await provincias.findAll();

  t.is(lista.count, 112, 'Se tiene 10 registros en la bd');
});

test.serial('provincia#findById', async t => {
  const { provincias } = repositories;
  const id = 1;

  let provincia = await provincias.findById(id);

  t.is(provincia.id, id, 'Se recuperó el registro mediante un id');
});

test.serial('provincia#createOrUpdate - exists', async t => {
  const { provincias } = repositories;
  const newData = { id: 1, id_departamento: 1 };

  let provincia = await provincias.createOrUpdate(newData);

  t.is(provincia.codigo, newData.codigo, 'Actualizando registro provincia');
});

test.serial('provincia#createOrUpdate - new', async t => {
  const { provincias } = repositories;
  let lista = await provincias.findAll();
  const nuevoprovincia = {
    id: lista.count + 1,
    nombre: 'test',
    id_departamento: 1,
    _user_created: 1,
    _created_at: new Date()
  };

  let provincia = await provincias.createOrUpdate(nuevoprovincia);

  t.true(typeof provincia.id === 'number', 'Comprobando que el nuevo provincia tenga un id');
  t.is(provincia.provincia, nuevoprovincia.provincia, 'Creando registro - provincia');
  t.is(provincia.id_departamento, nuevoprovincia.id_departamento, 'Creando registro - id_departamento');
  t.is(provincia.nombre, nuevoprovincia.nombre, 'Creando registro - nombre');

  test.id = provincia.id;
});

test.serial('provincia#findAll#filter#idDerpartamento', async t => {
  const { provincias } = repositories;
  let variable = 1;
  let provincia = await provincias.findAll({ id_departamento: variable });

  t.true(provincia.rows.length > 0, 'Se recuperó el registro mediante un id_departamento');
});

test.serial('provincia#delete', async t => {
  const { provincias } = repositories;
  let deleted = await provincias.deleteItem(test.id);

  t.is(deleted, 1, 'provincia eliminado');
});
