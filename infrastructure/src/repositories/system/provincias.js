'use strict';

const { getQuery } = require('../../lib/util');
const { deleteItemModel } = require('../queries');

module.exports = function userRepository (models, Sequelize) {
  const { provincias, departamentos } = models;
  const Op = Sequelize.Op;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    query.include = [
      {
        attributes: ['nombre'],
        model: departamentos
      }
    ];

    if (params.id_departamento) {
      query.where.id_departamento = params.id_departamento;
    }

    if (params.nombre) {
      query.where.nombre = {
        [Op.iLike]: `%${params.nombre}%`
      };
    }

    return provincias.findAndCountAll(query);
  }

  function findById (id) {
    return provincias.findOne({
      where: {
        id
      },
      include: [
        {
          attributes: ['nombre'],
          model: departamentos
        }
      ],
      raw: true
    });
  }

  async function createOrUpdate (usuario) {
    const cond = {
      where: {
        id: usuario.id
      }
    };

    const existingprovincias = await provincias.findOne(cond);

    if (existingprovincias) {
      const updated = await provincias.update(usuario, cond);
      return updated ? provincias.findOne(cond) : existingprovincias;
    }

    const result = await provincias.create(usuario);
    return result.toJSON();
  }

  async function deleteItem (id) {
    return deleteItemModel(id, provincias);
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
