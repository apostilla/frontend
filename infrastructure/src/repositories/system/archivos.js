'use strict';

const { getQuery } = require('../../lib/util');
const { deleteItemModel } = require('../queries');

module.exports = function archivosRepository (models, Sequelize) {
  const { archivos } = models;
  const Op = Sequelize.Op;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    if (params.nombre) {
      query.where.nombre = {
        [Op.iLike]: `%${params.nombre}%`
      };
    }

    return archivos.findAndCountAll(query);
  }

  function findById (id) {
    return archivos.findOne({
      where: {
        id
      }
    });
  }

  async function createOrUpdate (data, t) {
    const cond = {
      where: {
        id: data.id
      }
    };

    const item = await archivos.findOne(cond);

    if (item) {
      if (t) {
        cond.transaction = t;
      }
      try {
        const updated = await archivos.update(data, cond);
        return updated ? archivos.findOne(cond) : item;
      } catch (error) {
        if (t) {
          t.rollback();
        }
      }
    }

    let result;
    try {
      result = await archivos.create(data, t ? { transaction: t } : {});
    } catch (error) {
      if (t) {
        t.rollback();
      }
    }
    return result.toJSON();
  }

  async function deleteItem (id) {
    return deleteItemModel(id, archivos);
  }

  return {
    findAll,
    findById,
    deleteItem,
    createOrUpdate
  };
};
