'use strict';

const { getQuery } = require('../../lib/util');
const { deleteItemModel } = require('../queries');

module.exports = function userRepository (models, Sequelize) {
  const { municipios, provincias } = models;
  const Op = Sequelize.Op;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    query.include = [
      {
        attributes: ['nombre'],
        model: provincias
      }
    ];

    if (params.id_provincia) {
      query.where.id_provincia = params.id_provincia;
    }

    if (params.nombre) {
      query.where.nombre = {
        [Op.iLike]: `%${params.nombre}%`
      };
    }

    return municipios.findAndCountAll(query);
  }

  function findById (id) {
    return municipios.findOne({
      where: {
        id
      },
      include: [
        {
          attributes: ['nombre'],
          model: provincias
        }
      ],
      raw: true
    });
  }

  async function createOrUpdate (usuario) {
    const cond = {
      where: {
        id: usuario.id
      }
    };

    const existingmunicipios = await municipios.findOne(cond);

    if (existingmunicipios) {
      const updated = await municipios.update(usuario, cond);
      return updated ? municipios.findOne(cond) : existingmunicipios;
    }

    const result = await municipios.create(usuario);
    return result.toJSON();
  }

  async function deleteItem (id) {
    return deleteItemModel(id, municipios);
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
