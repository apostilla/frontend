'use strict';

const { getQuery } = require('../../lib/util');
const { deleteItemModel } = require('../queries');

module.exports = function notificacionesRepository (models, Sequelize) {
  const { notificaciones } = models;
  const Op = Sequelize.Op;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    if (params.tipo) {
      query.where.tipo = params.tipo;
    }
    if (params.titulo) {
      query.where.titulo = {
        [Op.iLike]: `%${params.titulo}%`
      };
    }
    if (params.mensaje) {
      query.where.mensaje = {
        [Op.iLike]: `%${params.mensaje}%`
      };
    }

    if (params.estado) {
      query.where.estado = params.estado;
    }

    return notificaciones.findAndCountAll(query);
  }
  function findById (id) {
    return notificaciones.findOne({
      where: {
        id
      }
    });
  }

  async function createOrUpdate (data) {
    const cond = {
      where: {
        id: data.id
      }
    };

    const item = await notificaciones.findOne(cond);

    if (item) {
      const updated = await notificaciones.update(data, cond);
      return updated ? notificaciones.findOne(cond) : item;
    }

    const result = await notificaciones.create(data);
    return result.toJSON();
  }

  async function deleteItem (id) {
    return deleteItemModel(id, notificaciones);
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
