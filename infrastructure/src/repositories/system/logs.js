'use strict';

const { getQuery } = require('../../lib/util');

module.exports = function rolesRepository (models, Sequelize) {
  const { logs, usuarios } = models;
  const Op = Sequelize.Op;

  function findAll (params = {}) {
    let query = getQuery(params);

    query.where = {};

    query.include = [{
      attributes: ['usuario', 'nombres', 'primer_apellido', 'segundo_apellido'],
      model: usuarios
    }];

    if (params.nivel) {
      query.where.nivel = params.nivel;
    }

    if (params.tipo) {
      query.where.tipo = {
        [Op.iLike]: `%${params.tipo}%`
      };
    }

    if (params.mensaje) {
      query.where.mensaje = {
        [Op.iLike]: `%${params.mensaje}%`
      };
    }

    if (params.referencia) {
      query.where.referencia = {
        [Op.iLike]: `%${params.referencia}%`
      };
    }

    if (params.ip) {
      query.where.ip = {
        [Op.iLike]: `%${params.ip}%`
      };
    }

    if (params.fecha) {
      query.where.fecha = {
        [Op.lte]: new Date(params.fecha),
        [Op.gte]: new Date(new Date(params.fecha) - 24 * 60 * 60 * 1000)
      };
    }

    if (params.id_usuario) {
      query.where.id_usuario = params.id_usuario;
    }

    return logs.findAndCountAll(query);
  }

  function findById (id) {
    return logs.findOne({
      where: {
        id
      },
      include: [{
        attributes: ['usuario', 'nombres', 'primer_apellido', 'segundo_apellido'],
        model: usuarios
      }],
      raw: true
    });
  }

  async function createOrUpdate (rol) {
    const cond = {
      where: {
        id: rol.id
      }
    };

    const item = await logs.findOne(cond);

    if (item) {
      const updated = await logs.update(rol, cond);
      return updated ? logs.findOne(cond) : item;
    }

    const result = await logs.create(rol);
    return result.toJSON();
  }

  async function deleteItem (id) {
    const cond = {
      where: {
        id
      }
    };

    const item = await logs.findOne(cond);

    if (item) {
      const deleted = await logs.destroy(cond);
      return !!deleted;
    }

    return false;
  }

  return {
    findAll,
    findById,
    deleteItem,
    createOrUpdate
  };
};
