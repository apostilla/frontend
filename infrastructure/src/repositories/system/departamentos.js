'use strict';

const { getQuery } = require('../../lib/util');
const { deleteItemModel } = require('../queries');

module.exports = function departamentosRepository (models, Sequelize) {
  const { departamentos } = models;
  const Op = Sequelize.Op;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    if (params.codigo) {
      query.where.codigo = {
        [Op.iLike]: `%${params.codigo}%`
      };
    }

    if (params.nombre) {
      query.where.nombre = {
        [Op.iLike]: `%${params.nombre}%`
      };
    }

    return departamentos.findAndCountAll(query);
  }

  function findById (id) {
    return departamentos.findById(id);
  }

  async function createOrUpdate (departamento) {
    const cond = {
      where: {
        id: departamento.id
      }
    };

    const existingDepartamentos = await departamentos.findOne(cond);

    if (existingDepartamentos) {
      const updated = await departamentos.update(departamento, cond);
      return updated ? departamentos.findOne(cond) : existingDepartamentos;
    }

    const result = await departamentos.create(departamento);
    return result.toJSON();
  }

  async function deleteItem (id) {
    return deleteItemModel(id, departamentos);
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
