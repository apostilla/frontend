'use strict';

const { getQuery, errorHandler, encrypt } = require('../../lib/util');
const { deleteItemModel } = require('../queries');

module.exports = function usuariosRepository (models, Sequelize) {
  const { usuarios, roles, entidades } = models;
  const Op = Sequelize.Op;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    query.include = [
      {
        attributes: ['nombre'],
        model: roles,
        as: 'rol'
      },
      {
        attributes: ['nombre', 'sigla'],
        model: entidades,
        as: 'entidad'
      }
    ];

    if (params.usuario) {
      query.where.usuario = {
        [Op.iLike]: `%${params.usuario}%`
      };
    }

    if (params.email) {
      query.where.email = {
        [Op.iLike]: `%${params.email}%`
      };
    }

    if (params.estado) {
      query.where.estado = params.estado;
    }

    if (params.id_rol) {
      query.where.id_rol = params.id_rol;
    }

    if (params.id_entidad) {
      query.where.id_entidad = params.id_entidad;
    }

    if (params.departamento) {
      query.where[Op.or] = [
        { departamento: { [Op.iLike]: `%${params.departamento}%` } }
      ];
    }

    if (params.nombre_completo) {
      query.where[Op.or] = [
        { nombres: { [Op.iLike]: `%${params.nombre_completo}%` } },
        { primer_apellido: { [Op.iLike]: `%${params.nombre_completo}%` } },
        { segundo_apellido: { [Op.iLike]: `%${params.nombre_completo}%` } }
      ];
    }

    if (params.listado_id_roles) {
      if (params.listado_id_roles.length > 0) {
        query.where.id_rol = {
          [Op.or]: params.listado_id_roles
        };
      }
    }

    return usuarios.findAndCountAll(query);
  }

  function findById (id) {
    return usuarios.findOne({
      where: {
        id
      },
      include: [
        {
          attributes: ['nombre'],
          model: roles,
          as: 'rol'
        },
        {
          attributes: ['nombre', 'sigla'],
          model: entidades,
          as: 'entidad'
        }
      ],
      raw: true
    });
  }
  // find user
  function findByUsername (usuario, include = true) {
    let cond = {
      where: {
        usuario
      }
    };

    if (include) {
      cond.include = [
        {
          attributes: ['nombre'],
          model: roles,
          as: 'rol'
        },
        {
          attributes: ['nombre', 'sigla'],
          model: entidades,
          as: 'entidad'
        }
      ];
      cond.raw = true;
    }
    return usuarios.findOne(cond);
  }

  async function createOrUpdate (usuario) {
    const cond = {
      where: {
        id: usuario.id
      }
    };

    const item = await usuarios.findOne(cond);

    if (item) {
      let updated;
      try {
        updated = await usuarios.update(usuario, cond);
      } catch (e) {
        errorHandler(e);
      }
      return updated ? usuarios.findOne(cond) : item;
    }

    let result;
    try {
      usuario.contrasena = encrypt(usuario.contrasena);
      result = await usuarios.create(usuario);
    } catch (e) {
      errorHandler(e);
    }
    return result.toJSON();
  }

  async function deleteItem (id) {
    return deleteItemModel(id, usuarios);
  }

  return {
    findAll,
    findById,
    findByUsername,
    createOrUpdate,
    deleteItem
  };
};
