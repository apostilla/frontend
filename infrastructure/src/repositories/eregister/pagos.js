'use strict';

const { getQuery } = require('../../lib/util');
const { deleteItemModel } = require('../queries');

module.exports = function userRepository (models, Sequelize) {
  const { pagos, tramites_doc, documentos } = models;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    query.include = [
      {
        attributes: ['id', 'estado'],
        model: tramites_doc,
        include: [{
          attributes: ['id', 'id_entidad'],
          model: documentos
        }],
        raw: true
      }
    ];

    if (params.cpt) {
      query.where.cpt = params.cpt;
    }

    if (params.tipo) {
      query.where.tipo = params.tipo;
    }

    if (params.fecha_pago) {
      query.where.fecha_pago = params.fecha_pago;
    }

    if (params.estado) {
      query.where.estado = params.estado;
    }

    if (params.nro_documento) {
      query.where.nro_documento = params.nro_documento;
    }

    if (params.titular_factura) {
      query.where.titular_factura = params.titular_factura;
    }

    if (params.id_entidad) {
      query.where['$tramites_doc.documento.id_entidad$'] = params.id_entidad;
    }

    if (params.id_tramite_doc) {
      query.where.id_tramite_doc = params.id_tramite_doc;
    }

    return pagos.findAndCountAll(query);
  }

  function findById (id) {
    return pagos.findOne({
      where: {
        id
      },
      include: [
        {
          attributes: ['estado'],
          model: tramites_doc
        }
      ],
      raw: true
    });
  }

  // find by idCpt
  function findByIdCpt (idCPT) {
    let cond = {
      where: {
        id_cpt: idCPT
      }
    };
    return pagos.findOne(cond);
  }

  async function createOrUpdate (data, t) {
    let cond = {
      where: {
        id: data.id
      }
    };

    const item = await pagos.findOne(cond);

    if (item) {
      if (t) {
        cond.transaction = t;
      }
      try {
        const updated = await pagos.update(data, cond);
        return updated ? pagos.findOne(cond) : item;
      } catch (error) {
        if (t) {
          t.rollback();
        }
      }
    }

    let result;
    try {
      result = await pagos.create(data, t ? { transaction: t } : {});
    } catch (error) {
      if (t) {
        t.rollback();
      }
    }
    return result.toJSON();
  }

  async function deleteItem (id) {
    return deleteItemModel(id, pagos);
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem,
    findByIdCpt
  };
};
