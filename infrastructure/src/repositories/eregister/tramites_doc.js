'use strict';

const { getQuery, errorHandler } = require('../../lib/util');
const { deleteItemModel } = require('../queries');

module.exports = function userRepository (models, Sequelize) {
  const { tramites_doc, tramites, documentos, archivos, firmas, solicitantes, entidades } = models;
  const Op = Sequelize.Op;

  function findAll (params = {}) {
    let query = getQuery(params, ['documento_nombre']);
    query.where = {};
    query.include = [
      {
        attributes: [
          'id',
          'cod_seguimiento'
        ],
        model: tramites,
        include: [{
          attributes: [
            'id',
            'nombres',
            'primer_apellido',
            'segundo_apellido',
            'nro_documento',
            'email',
            'tipo_documento',
            'fecha_nacimiento'
          ],
          model: solicitantes
        }],
        raw: true
      },
      {
        attributes: [
          'id',
          'id_entidad',
          'nombre',
          'derivar',
          'cuenta',
          'monto',
          'otros'
        ],
        model: documentos
      },
      {
        attributes: [
          'id',
          'nombre'
        ],
        model: archivos
      }
    ];

    if (params.pago_entidad) {
      query.where.pago_entidad = params.pago_entidad;
    }

    if (params.pago_cancilleria) {
      query.where.pago_cancilleria = params.pago_cancilleria;
    }

    if (params.fecha_expedicion) {
      if (typeof params.fecha_expedicion === 'string') {
        params.fecha_expedicion = new Date(params.fecha_expedicion);
      }
      query.where.fecha_expedicion = {
        [Op.gt]: new Date(params.fecha_expedicion.getTime() - 1000),
        [Op.lt]: new Date(params.fecha_expedicion.getTime() + 24 * 60 * 60 * 1000)
      };
    }

    if (params.cod_seguridad) {
      query.where.cod_seguridad = {
        [Op.iLike]: `%${params.cod_seguridad}%`
      };
    }

    if (params.nro_apostilla) {
      query.where.nro_apostilla = params.nro_apostilla;
    }

    // Filtrando estados
    if (params.listado_tramites && params.estado) {
      let estados = params.listado_tramites;

      if (estados.indexOf(params.estado) !== -1) {
        query.where.estado = params.estado;
      } else {
        query.where.estado = {
          [Op.or]: estados
        };
      }
    } else if (params.listado_tramites) {
      query.where.estado = {
        [Op.or]: params.listado_tramites
      };
    } else if (params.estado) {
      query.where.estado = params.estado;
    }

    if (params.id_entidad) {
      query.where['$documento.id_entidad$'] = params.id_entidad;
    }

    if (params.id_documento) {
      query.where.id_documento = params.id_documento;
    }

    if (params.id_tramite) {
      query.where.id_tramite = params.id_tramite;
    }

    if (params.cod_seguimiento) {
      query.where['$tramite.cod_seguimiento$'] = {[Op.iLike]: `%${params.cod_seguimiento}%`};
    }

    if (params.id) {
      query.where.id = params.id;
    }

    if (params.departamento) {
      const departamentos = params.departamento.split(',');
      query.where.departamento = {
        [Op.or]: departamentos
      }
    }

    if (params.nombre_completo) {
      query.where[Op.or] = [
        { '$tramite.solicitante.nombres$': { [Op.iLike]: `%${params.nombre_completo}%` } },
        { '$tramite.solicitante.primer_apellido$': { [Op.iLike]: `%${params.nombre_completo}%` } },
        { '$tramite.solicitante.segundo_apellido$': { [Op.iLike]: `%${params.nombre_completo}%` } }
      ];
    }

    if (params.nro_documento) {
      query.where['$tramite.solicitante.nro_documento$'] = {
        [Op.iLike]: `%${params.nro_documento}%`
      };
    }

    return tramites_doc.findAndCountAll(query);
  }

  function findById (id) {
    return tramites_doc.findOne({
      where: {
        id
      },
      include: [
        {
          attributes: [
            'id',
            'cod_seguimiento',
            'fecha_inicio',
            'fecha_fin'
          ],
          model: tramites,
          include: [{
            attributes: [
              'id',
              'nombres',
              'primer_apellido',
              'segundo_apellido',
              'tipo_documento',
              'nro_documento',
              'email',
              'telefono',
              'fecha_nacimiento',
              'observacion'
            ],
            model: solicitantes
          }],
          raw: true
        },
        {
          attributes: [
            'id',
            'id_entidad',
            'nombre',
            'cuenta',
            'descripcion',
            'precio',
            'monto',
            'duracion',
            'derivar',
            'otros'
          ],
          model: documentos,
          include: [{
            attributes: [
              'nombre',
              'descripcion',
              'sigla'
            ],
            model: entidades,
            as: 'entidad'
          }],
          raw: true
        },
        {
          attributes: [
            'id',
            'nombre',
            'titulo',
            'tamano'
          ],
          model: archivos
        }
      ],
      raw: true
    });
  }

  function findByArchivoId (id_archivo) {
    return tramites_doc.findOne({
      where: {
        id_archivo
      },
      include: [
        {
          attributes: [
            'id',
            'cod_seguimiento',
            'fecha_inicio',
            'fecha_fin'
          ],
          model: tramites,
          include: [{
            attributes: [
              'id',
              'nombres',
              'primer_apellido',
              'segundo_apellido',
              'tipo_documento',
              'nro_documento',
              'email',
              'telefono',
              'fecha_nacimiento',
              'observacion'
            ],
            model: solicitantes
          }],
          raw: true
        },
        {
          attributes: [
            'id',
            'id_entidad',
            'nombre',
            'cuenta',
            'descripcion',
            'precio',
            'monto',
            'duracion',
            'derivar',
            'otros'
          ],
          model: documentos,
          include: [{
            attributes: [
              'nombre',
              'descripcion',
              'sigla'
            ],
            model: entidades,
            as: 'entidad'
          }],
          raw: true
        },
        {
          attributes: [
            'id',
            'nombre',
            'titulo',
            'tamano'
          ],
          model: archivos
        }
      ],
      raw: true
    });
  }

  function findByCode (code, number, date) {
    if (typeof date === 'string') {
      date = new Date(date);
    }
    return tramites_doc.findOne({
      where: {
        cod_seguridad: code,
        nro_apostilla: number,
        fecha_expedicion: {
          [Op.gt]: new Date(date.getTime() - 1000),
          [Op.lt]: new Date(date.getTime() + 24 * 60 * 60 * 1000)
        },
        estado: 'APOSTILLADO'
      },
      include: [
        {
          attributes: ['id', 'cod_seguimiento'],
          model: tramites,
          include: [{
            attributes: [
              'id',
              'nombres',
              'primer_apellido',
              'segundo_apellido',
              'nro_documento',
              'tipo_documento',
              'fecha_nacimiento'
            ],
            model: solicitantes
          }],
          raw: true
        },
        {
          attributes: ['id', 'id_entidad', 'nombre', 'descripcion'],
          model: documentos,
          include: [
            {
              attributes: ['nombre'],
              model: entidades,
              as: 'entidad'
            }
          ],
          raw: true
        }
      ],
      raw: true
    });
  }

  async function createOrUpdate (data, t) {
    let cond = {
      where: {
        id: data.id
      }
    };

    const item = await tramites_doc.findOne(cond);

    if (item) {
      if (t) {
        cond.transaction = t;
      }
      let updated;
      try {
        updated = await tramites_doc.update(data, cond);
      } catch (e) {
        if (t) {
          t.rollback();
        }
        errorHandler(e);
      }
      return updated ? tramites_doc.findOne(cond) : item;
    }

    let result;
    try {
      result = await tramites_doc.create(data, t ? { transaction: t } : {});
    } catch (e) {
      if (t) {
        t.rollback();
      }
      errorHandler(e);
    }
    return result.toJSON();
  }

  async function deleteItem (id) {
    return deleteItemModel(id, tramites_doc);
  }

  return {
    findAll,
    findById,
    findByArchivoId,
    findByCode,
    createOrUpdate,
    deleteItem
  };
};
