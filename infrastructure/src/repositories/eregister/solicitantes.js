'use strict';

const { getQuery, errorHandler } = require('../../lib/util');
const { deleteItemModel } = require('../queries');

module.exports = function userRepository (models, Sequelize) {
  const { solicitantes } = models;
  const Op = Sequelize.Op;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    query.include = [
      // {
      //   attributes: ['nombre'],
      //   model: departamentos
      // },
      // {
      //   attributes: ['nombre'],
      //   model: provincias
      // },
      // {
      //   attributes: ['nombre'],
      //   model: municipios
      // }
    ];

    // if (params.id_departamento) {
    //   query.where.id_departamento = params.id_departamento;
    // }

    // if (params.id_provincia) {
    //   query.where.id_provincia = params.id_provincia;
    // }

    // if (params.id_municipio) {
    //   query.where.id_municipio = params.id_municipio;
    // }

    if (params.nombre_completo) {
      query.where[Op.or] = [
        { nombres: { [Op.iLike]: `%${params.nombre_completo}%` } },
        { primer_apellido: { [Op.iLike]: `%${params.nombre_completo}%` } },
        { segundo_apellido: { [Op.iLike]: `%${params.nombre_completo}%` } }
      ];
    }

    if (params.tipo) {
      query.where.tipo = params.tipo;
    }

    if (params.tipo_documento) {
      query.where.tipo_documento = params.tipo_documento;
    }

    if (params.nro_documento) {
      query.where.nro_documento = params.nro_documento;
    }

    if (params.matricula) {
      query.where.matricula = params.matricula;
    }

    if (params.email) {
      query.where.email = params.email;
    }

    return solicitantes.findAndCountAll(query);
  }

  function findById (id) {
    return solicitantes.findOne({
      where: {
        id
      }
    });
  }

  function findByDoc (tipoDoc, nroDoc) {
    return solicitantes.findOne({
      where: {
        tipo_documento: tipoDoc,
        nro_documento: nroDoc
      }
    });
  }

  async function createOrUpdate (solicitud, t) {
    let cond = {
      where: {
        id: solicitud.id
      }
    };

    const item = await solicitantes.findOne(cond);

    if (item) {
      if (t) {
        cond.transaction = t;
      }
      let updated;
      try {
        updated = await solicitantes.update(solicitud, cond);
      } catch (e) {
        if (t) {
          t.rollback();
        }
        errorHandler(e);
      }
      return updated ? solicitantes.findOne(cond) : item;
    }

    let result;
    try {
      result = await solicitantes.create(solicitud, t ? { transaction: t } : {});
    } catch (e) {
      if (t) {
        t.rollback();
      }
      errorHandler(e);
    }

    return result.toJSON();
  }

  async function deleteItem (id) {
    return deleteItemModel(id, solicitantes);
  }

  return {
    findAll,
    findById,
    findByDoc,
    createOrUpdate,
    deleteItem
  };
};
