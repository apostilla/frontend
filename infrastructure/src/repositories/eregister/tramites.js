'use strict';

const { getQuery, errorHandler } = require('../../lib/util');
const { deleteItemModel } = require('../queries');

module.exports = function userRepository (models, Sequelize) {
  const { tramites, solicitantes, tramites_doc, documentos, entidades, pagos } = models;
  const Op = Sequelize.Op;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    query.include = [
      /* {
        attributes: ['nombre'],
        model: paises_destino
      },
      */
      {
        attributes: [
          'nombres',
          'primer_apellido',
          'segundo_apellido'
        ],
        model: solicitantes
      }
    ];
/*
    if (params.id_pais_destino) {
      query.where.id_pais_destino = params.id_pais_destino;
    }
*/
    if (params.fecha_inicio) {
      query.where.fecha_inicio = params.fecha_inicio;
    }

    if (params.fecha_fin) {
      query.where.fecha_fin = params.fecha_fin;
    }

    if (params.estado) {
      query.where.estado = params.estado;
    }

    if (params.cod_seguimiento) {
      query.where.cod_seguimiento = {
        [Op.iLike]: `%${params.cod_seguimiento}%`
      };
    }

    return tramites.findAndCountAll(query);
  }

  function findById (id) {
    return tramites.findOne({
      where: {
        id
      },
      include: [
       /* {
          attributes: ['nombre'],
          model: paises_destino
        },
        */
        {
          attributes: [
            'nombres',
            'primer_apellido',
            'segundo_apellido'
          ],
          model: solicitantes
        }
      ],
      raw: true
    });
  }

  async function findOneCode (code) {
    return tramites.findOne({
      attributes: [ 'id' ],
      where: {
        cod_seguimiento: code
      }
    });
  }

  async function findByCode (code) {
    let tramite = await tramites.findOne({
      attributes: [
        'id',
        'fecha_inicio',
        'fecha_fin',
        'titular_factura',
        'nro_documento',
        'tipo_documento',
        'estado'
      ],
      where: {
        cod_seguimiento: code
      },
      include: [
        {
          attributes: [
            'nombres',
            'primer_apellido',
            'segundo_apellido',
            'email',
            'telefono',
            'fecha_nacimiento',
            'tipo_documento',
            'nro_documento',
            'observacion'
          ],
          model: solicitantes
        }
      ],
      raw: true
    });

    if (tramite) {
      let docs = await tramites_doc.findAll({
        attributes: [
          'id',
          'pago_entidad',
          'pago_cancilleria',
          'cod_seguridad',
          'nro_apostilla',
          'fecha_expedicion',
          'estado',
          'observacion',
          'firmante_documento_nombre',
          'firmante_documento_cargo',
          'firmante_documento_entidad',
          'digital',
          'otros',
          'departamento',
          'fecha_inicio',
          'fecha_fin'
        ],
        where: {
          id_tramite: tramite.id
        },
        include: [
          {
            attributes: [
              'nombre',
              'descripcion',
              'precio',
              'monto',
              'derivar',
              'duracion',
              'otros',
              'codigo_portal'
            ],
            model: documentos,
            include: [
              {
                attributes: ['nombre', 'sigla'],
                model: entidades,
                as: 'entidad'
              }
            ],
            raw: true
          },
          {
            attributes: [
              'titular_factura',
              'nro_documento',
              'tipo_documento'
            ],
            model: pagos
          }
        ],
        raw: true
      });

      tramite.documentos = docs;
    }

    return tramite;
  }

  async function findByCode2 (code) {
    let tramite = await tramites.findOne({
      attributes: [
        'id'
      ],
      where: {
        cod_seguimiento: code
      },
      include: [
        {
          attributes: [
            'nombres',
            ['primer_apellido','primerApellido'],
            ['segundo_apellido','segundoApellido'],
            ['fecha_nacimiento','fechaNacimiento'],
            ['tipo_documento','tipoDocumento'],
            ['nro_documento','nroDocumento']
          ],
          model: solicitantes
        }
      ],
      raw: true
    });

    if (tramite) {
      let docs = await tramites_doc.findAll({
        attributes: [
          ['pago_cancilleria','pagoCancilleria'],
          'estado',
          'observacion',
          'digital',
          'departamento'
        ],
        where: {
          id_tramite: tramite.id
        },
        include: [
          {
            attributes: [
              'nombre',
              ['codigo_portal','codigoPortal']
            ],
            model: documentos,
            raw: true,
          }
        ],
        raw: true
      });

      tramite.infoDoc = docs;
    }

    return tramite;
  }

  async function createOrUpdate (data, t) {
    let cond = {
      where: {
        id: data.id
      }
    };

    const item = await tramites.findOne(cond);

    if (item) {
      let updated;
      if (t) {
        cond.transaction = t;
      }
      try {
        updated = await tramites.update(data, cond);
      } catch (e) {
        if (t) {
          t.rollback();
        }
        errorHandler(e);
      }
      return updated ? tramites.findOne(cond) : item;
    }

    let result;
    try {
      result = await tramites.create(data, t ? { transaction: t } : {});
    } catch (e) {
      if (t) {
        t.rollback();
      }
      errorHandler(e);
    }

    return result.toJSON();
  }

  async function deleteItem (id) {
    return deleteItemModel(id, tramites);
  }

  return {
    findAll,
    findById,
    findByCode,
    findByCode2,
    findOneCode,
    createOrUpdate,
    deleteItem
  };
};
