'use strict';

const { getQuery } = require('../../lib/util');
const { deleteItemModel } = require('../queries');

module.exports = function userRepository (models, Sequelize) {
  const { paises_destino } = models;
  const Op = Sequelize.Op;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    if (params.miembro) {
      query.where.miembro = params.miembro;
    }

    if (params.nombre) {
      query.where.nombre = {
        [Op.iLike]: `%${params.nombre}%`
      };
    }

    return paises_destino.findAndCountAll(query);
  }

  function findById (id) {
    return paises_destino.findOne({
      where: {
        id
      },
      raw: true
    });
  }

  async function createOrUpdate (currentObject) {
    const cond = {
      where: {
        id: currentObject.id
      }
    };

    const existingPaisesDestinos = await paises_destino.findOne(cond);

    if (existingPaisesDestinos) {
      const updated = await paises_destino.update(currentObject, cond);
      return updated ? paises_destino.findOne(cond) : existingPaisesDestinos;
    }

    const result = await paises_destino.create(currentObject);
    return result.toJSON();
  }

  async function deleteItem (id) {
    return deleteItemModel(id, paises_destino);
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
