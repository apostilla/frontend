'use strict';

const { getQuery } = require('../../lib/util');
const { deleteItemModel } = require('../queries');

module.exports = function firmasRepository (models, Sequelize) {
  const { firmas, archivos, usuarios, entidades } = models;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    query.include = [
      {
        attributes: ['nombre'],
        model: archivos
      },
      {
        attributes: ['nombres'],
        model: usuarios
      }
    ];

    if (params.id_usuario) {
      query.where.id_usuario = params.id_usuario;
    }

    if (params.fecha_firma) {
      query.where.fecha_firma = params.fecha_firma;
    }
    return firmas.findAndCountAll(query);
  }

  function findById (id) {
    return firmas.findOne({
      where: {
        id
      },
      include: [
        {
          attributes: ['nombre'],
          model: archivos
        },
        {
          attributes: ['nombres'],
          model: usuarios
        }
      ],
      raw: true
    });
  }
  // find firma by id_archivo
  function findByArchivoId (id, include = true) {
    let cond = {
      where: {
        id_archivo: id
      }
    };

    if (include) {
      cond.include = [
        {
          model: usuarios,
          as: 'usuario',
          include: [{
            model: entidades,
            as: 'entidad'
          }]
        }
      ];
      cond.raw = true;
    }
    return firmas.findOne(cond);
  }

  function findByArchivoIdCancilleria (id, include = true) {
    let cond = {
      where: {
        id_archivo: id
      }
    };

    if (include) {
      cond.include = [
        {
          where: {
            id_rol: 4
          },
          model: usuarios,
          as: 'usuario',
          include: [{
            model: entidades,
            as: 'entidad'
          }]
        }
      ];
      cond.raw = true;
    }
    return firmas.findOne(cond);
  }

  async function createOrUpdate (firma) {
    const cond = {
      where: {
        id: firma.id
      }
    };

    const item = await firmas.findOne(cond);

    if (item) {
      const updated = await firmas.update(firma, cond);
      return updated ? firmas.findOne(cond) : item;
    }

    const result = await firmas.create(firma);
    return result.toJSON();
  }

  async function deleteItem (id) {
    return deleteItemModel(id, firmas);
  }

  return {
    findAll,
    findById,
    findByArchivoId,
    findByArchivoIdCancilleria,
    deleteItem,
    createOrUpdate
  };
};
