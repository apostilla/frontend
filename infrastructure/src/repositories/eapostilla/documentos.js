'use strict';

const { getQuery } = require('../../lib/util');
const { deleteItemModel } = require('../queries');

module.exports = function documentosRepository (models, Sequelize) {
  const { documentos, entidades } = models;
  const Op = Sequelize.Op;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    query.include = [
      {
        attributes: ['nombre', 'descripcion', 'sigla', 'codigo_portal'],
        model: entidades,
        as: 'entidad'
      }
    ];

    if (params.id_entidad) {
      query.where.id_entidad = params.id_entidad;
    }

    if (params.nombre) {
      query.where.nombre = {
        [Op.iLike]: `%${params.nombre}%`
      };
    }

    if (params.estado) {
      query.where.estado = params.estado;
    }

    if (params.sigla) {
      query.where['$entidad.sigla$'] = params.sigla;
    }

    if (params.codigo_portal) {
      query.where.codigo_portal = params.codigo_portal;
    }
    return documentos.findAndCountAll(query);
  }

  function findById (id) {
    return documentos.findOne({
      where: {
        id
      },
      include: [
        {
          attributes: ['nombre'],
          model: entidades,
          as: 'entidad'
        }
      ],
      raw: true
    });
  }

  function findByCode (codigo_portal) {
    return documentos.findOne({
      where: {
        codigo_portal
      },
      include: [
        {
          attributes: ['nombre'],
          model: entidades,
          as: 'entidad'
        }
      ],
      raw: true
    });
  }

  async function createOrUpdate (documento) {
    const cond = {
      where: {
        id: documento.id
      }
    };

    const item = await documentos.findOne(cond);

    if (item) {
      const updated = await documentos.update(documento, cond);
      return updated ? documentos.findOne(cond) : item;
    }

    const result = await documentos.create(documento);
    return result.toJSON();
  }

  async function deleteItem (id) {
    return deleteItemModel(id, documentos);
  }

  return {
    findAll,
    findById,
    findByCode,
    deleteItem,
    createOrUpdate
  };
};
