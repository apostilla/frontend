'use strict';

// Definiendo asociaciones de las tablas
module.exports = function associations (models) {
  const { roles, usuarios, auxiliares, entidades, departamentos, provincias, municipios, solicitantes, tramites, tramites_doc, pagos, documentos, firmas, archivos, modulos, notificaciones, permisos, logs } = models;

  // Asociaciones tabla usuarios
  usuarios.belongsTo(entidades, { foreignKey: { name: 'id_entidad', allowNull: false }, as: 'entidad' });
  entidades.hasMany(usuarios, { foreignKey: { name: 'id_entidad', allowNull: false }, as: 'entidad' });

  usuarios.belongsTo(roles, { foreignKey: { name: 'id_rol', allowNull: false }, as: 'rol' });
  roles.hasMany(usuarios, { foreignKey: { name: 'id_rol', allowNull: false }, as: 'rol' });

  // Asociaciones tabla usuarios
  auxiliares.belongsTo(entidades, { foreignKey: { name: 'id_entidad', allowNull: false }, as: 'entidad' });
  // entidades.hasMany(auxiliares, { foreignKey: { name: 'id_entidad', allowNull: false }, as: 'entidad' });

  auxiliares.belongsTo(roles, { foreignKey: { name: 'id_rol', allowNull: false }, as: 'rol' });
  // roles.hasMany(auxiliares, { foreignKey: { name: 'id_rol', allowNull: false }, as: 'rol' });

  // Asociaciones tabla provincias
  departamentos.hasMany(provincias, { foreignKey: { name: 'id_departamento', allowNull: false } });
  provincias.belongsTo(departamentos, { foreignKey: { name: 'id_departamento', allowNull: false } });

  // Asociaciones tabla municipios
  provincias.hasMany(municipios, { foreignKey: { name: 'id_provincia', allowNull: false } });
  municipios.belongsTo(provincias, { foreignKey: { name: 'id_provincia', allowNull: false } });

  // Asociaciones tabla solicitantes
  // departamentos.hasMany(solicitantes, { foreignKey: { name: 'id_departamento'} });
  // solicitantes.belongsTo(departamentos, { foreignKey: { name: 'id_departamento'} });

  // provincias.hasMany(solicitantes, { foreignKey: { name: 'id_provincia'} });
  // solicitantes.belongsTo(provincias, { foreignKey: { name: 'id_provincia'} });

  // municipios.hasMany(solicitantes, { foreignKey: { name: 'id_municipio'} });
  // solicitantes.belongsTo(municipios, { foreignKey: { name: 'id_municipio'} });

  // Asociaciones tabla tramites
  // paises_destino.hasMany(tramites, { foreignKey: { name: 'id_pais_destino', allowNull: false } });
  // tramites.belongsTo(paises_destino, { foreignKey: { name: 'id_pais_destino', allowNull: false } });

  solicitantes.hasMany(tramites, { foreignKey: { name: 'id_solicitante', allowNull: false } });
  tramites.belongsTo(solicitantes, { foreignKey: { name: 'id_solicitante', allowNull: false } });

  // Asociaciones tabla pagos
  tramites_doc.hasMany(pagos, { foreignKey: { name: 'id_tramite_doc', allowNull: false } });
  pagos.belongsTo(tramites_doc, { foreignKey: { name: 'id_tramite_doc', allowNull: false } });

  // Asociaciones tabla tramites_doc
  tramites.hasMany(tramites_doc, { foreignKey: { name: 'id_tramite', allowNull: false } });
  tramites_doc.belongsTo(tramites, { foreignKey: { name: 'id_tramite', allowNull: false } });

  documentos.hasMany(tramites_doc, { foreignKey: { name: 'id_documento', allowNull: false } });
  tramites_doc.belongsTo(documentos, { foreignKey: { name: 'id_documento', allowNull: false } });

  archivos.hasMany(tramites_doc, { foreignKey: { name: 'id_archivo', allowNull: false } });
  tramites_doc.belongsTo(archivos, { foreignKey: { name: 'id_archivo', allowNull: false } });

  // Asociaciones tablas permisos - roles
  permisos.belongsTo(roles, { foreignKey: { name: 'id_rol', allowNull: false }, as: 'rol' });
  roles.hasMany(permisos, { foreignKey: { name: 'id_rol', allowNull: false } });

  // Asociaciones tablas permisos - modulos
  permisos.belongsTo(modulos, { foreignKey: { name: 'id_modulo', allowNull: false }, as: 'modulo' });
  modulos.hasMany(permisos, { foreignKey: { name: 'id_modulo', allowNull: false } });

  // Asociaciones tablas entidad - documentos
  documentos.belongsTo(entidades, { foreignKey: { name: 'id_entidad', allowNull: false }, as: 'entidad' });
  entidades.hasMany(documentos, { foreignKey: { name: 'id_entidad', allowNull: false } });

  // Asociaciones tablas firmas - archivos
  firmas.belongsTo(archivos, { foreignKey: { name: 'id_archivo', allowNull: false } });
  archivos.hasMany(firmas, { foreignKey: { name: 'id_archivo', allowNull: false } });

  // Asociaciones tablas firmas - usuarios
  firmas.belongsTo(usuarios, { foreignKey: { name: 'id_usuario', allowNull: false } });
  usuarios.hasMany(firmas, { foreignKey: { name: 'id_usuario', allowNull: false } });

  // Asociaciones tablas modulos - sección
  modulos.belongsTo(modulos, { foreignKey: 'id_modulo' });
  modulos.hasMany(modulos, { foreignKey: 'id_modulo' });
  modulos.belongsTo(modulos, { foreignKey: 'id_seccion' });
  modulos.hasMany(modulos, { foreignKey: 'id_seccion' });

  // Asociaciones tablas notificaciones - usuarios via sender
  notificaciones.belongsTo(usuarios, { foreignKey: 'id_sender' });
  usuarios.hasMany(notificaciones, { foreignKey: 'id_sender' });

  // Asociaciones tablas notificaciones - usuarios via receiver
  notificaciones.belongsTo(usuarios, { foreignKey: 'id_receiver' });
  usuarios.hasMany(notificaciones, { foreignKey: 'id_receiver' });

  // Asociaciones tablas logs - usuarios via receiver
  logs.belongsTo(usuarios, { foreignKey: 'id_usuario' });
  usuarios.hasMany(logs, { foreignKey: 'id_usuario' });

  return models;
};
