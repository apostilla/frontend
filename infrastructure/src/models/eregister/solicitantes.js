'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    tipo: {
      type: DataTypes.ENUM,
      values: ['NATURAL', 'JURIDICA'],
      defaultValue: 'NATURAL',
      allowNull: false,
      xlabel: lang.t('fields.tipo')
    },
    nombres: {
      type: DataTypes.STRING(100),
      allowNull: false,
      xlabel: lang.t('fields.nombres')
    },
    primer_apellido: {
      type: DataTypes.STRING(100),
      xlabel: lang.t('fields.primer_apellido')
    },
    segundo_apellido: {
      type: DataTypes.STRING(100),
      xlabel: lang.t('fields.segundo_apellido')
    },
    nacionalidad: {
      type: DataTypes.STRING(100),
      xlabel: lang.t('fields.nacionalidad')
    },
    email: {
      type: DataTypes.STRING(200),
      allowNull: false,
      xlabel: lang.t('fields.email')
    },
    telefono: {
      type: DataTypes.STRING(50),
      xlabel: lang.t('fields.telefono')
    },
    fecha_nacimiento: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      xlabel: lang.t('fields.fecha_nacimiento')
    },
    tipo_documento: {
      type: DataTypes.ENUM,
      values: ['CI', 'EXTRANJERO', 'PASAPORTE', 'NIT'],
      defaultValue: 'CI',
      allowNull: false,
      xlabel: lang.t('fields.tipo_documento')
    },
    nro_documento: {
      type: DataTypes.STRING(50),
      allowNull: false,
      xlabel: lang.t('fields.nro_documento')
    },
    direccion: {
      type: DataTypes.TEXT,
      xlabel: lang.t('fields.direccion')
    },
    matricula: {
      type: DataTypes.INTEGER,
      xlabel: lang.t('fields.matricula')
    },
    observacion: {
      type: DataTypes.TEXT,
      xlabel: lang.t('fields.observacion')
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let Solicitantes = sequelize.define('solicitantes', fields, {
    timestamps: false
  });

  return Solicitantes;
};
