'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    pago_entidad: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false,
      xlabel: lang.t('fields.pago_entidad')
    },
    pago_cancilleria: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false,
      xlabel: lang.t('fields.pago_cancilleria')
    },
    cod_seguridad: {
      type: DataTypes.STRING(20),
      xlabel: lang.t('fields.cod_seguridad')
    },
    nro_apostilla: {
      type: DataTypes.STRING(20),
      xlabel: lang.t('fields.nro_apostilla')
    },
    fecha_expedicion: {
      type: DataTypes.DATE,
      xlabel: lang.t('fields.fecha_expedicion')
    },
    estado: {
      type: DataTypes.ENUM,
      values: [
        'CREADO',
        'REVISADO_ENTIDAD',
        'OBSERVADO_ENTIDAD_REVISOR',
        'OBSERVADO_ENTIDAD_FIRMANTE',
        'FIRMADO_ENTIDAD',
        'REVISADO_CANCILLERIA',
        'OBSERVADO_CANCILLERIA_REVISOR',
        'OBSERVADO_CANCILLERIA_FIRMANTE',
        'APOSTILLADO'
      ],
      defaultValue: 'CREADO',
      allowNull: false,
      xlabel: lang.t('fields.estado')
    },
    observacion: {
      type: DataTypes.TEXT,
      allowNull: true,
      xlabel: lang.t('fields.observacion')
    },
    firmante_documento_nombre: {
      type: DataTypes.STRING(150),
      allowNull: true,
      xlabel: lang.t('fields.nombre')
    },
    firmante_documento_cargo: {
      type: DataTypes.STRING(150),
      allowNull: true,
      xlabel: lang.t('fields.cargo')
    },
    firmante_documento_entidad: {
      type: DataTypes.STRING(150),
      allowNull: true,
      xlabel: lang.t('fields.entidad')
    },
    digital: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false,
      xlabel: lang.t('fields.digital')
    },
    otros: {
      type: DataTypes.JSON,
      allowNull: true,
      field: 'otros'
    },
    departamento: {
      type: DataTypes.STRING(50),
      defaultValue: 'LA PAZ',
      xlabel: lang.t('fields.departamento')
    },
    fecha_inicio: {
      type: DataTypes.DATE,
      xlabel: lang.t('fields.fecha_inicio')
    },
    fecha_fin: {
      type: DataTypes.DATE,
      xlabel: lang.t('fields.fecha_fin')
    },
    _user_updated_auxiliar: {
      type: DataTypes.INTEGER,
      xlabel: lang.t('fields._user_updated_auxiliar')
    },
    _updated_at_auxiliar: {
      type: DataTypes.DATE,
      xlabel: lang.t('fields._updated_at_auxiliar')
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let TramitesDoc = sequelize.define('tramites_doc', fields, {
    timestamps: false
  });

  return TramitesDoc;
};
