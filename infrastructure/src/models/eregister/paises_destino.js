'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    nombre: {
      type: DataTypes.STRING(100),
      allowNull: false,
      xlabel: lang.t('fields.nombre')
    },
    miembro: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
      allowNull: false,
      xlabel: lang.t('fields.miembro')
    },
    sigla: {
      type: DataTypes.STRING(3),
      allowNull: false,
      xlabel: lang.t('fields.sigla')
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let PaisesDestino = sequelize.define('paises_destino', fields, {
    timestamps: false
  });

  return PaisesDestino;
};
