'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    fecha_inicio: {
      type: DataTypes.DATE,
      allowNull: false,
      xlabel: lang.t('fields.fecha_inicio')
    },
    fecha_fin: {
      type: DataTypes.DATE,
      xlabel: lang.t('fields.fecha_fin')
    },
    estado: {
      type: DataTypes.ENUM,
      values: ['SOLICITADO', 'EN_PROCESO', 'FINALIZADO'],
      defaultValue: 'SOLICITADO',
      allowNull: false,
      xlabel: lang.t('fields.estado')
    },
    cod_seguimiento: {
      type: DataTypes.STRING(20),
      unique: true,
      allowNull: false,
      xlabel: lang.t('fields.cod_seguimiento')
    },
    tipo_documento: {
      type: DataTypes.ENUM,
      values: ['CI', 'PASAPORTE', 'NIT', 'EXTRANJERO'],
      xlabel: lang.t('fields.tipo_documento')
    },
    nro_documento: {
      type: DataTypes.STRING(20),
      xlabel: lang.t('fields.nro_documento')
    },
    titular_factura: {
      type: DataTypes.STRING(100),
      xlabel: lang.t('fields.titular_factura')
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let Tramites = sequelize.define('tramites', fields, {
    timestamps: false
  });

  return Tramites;
};
