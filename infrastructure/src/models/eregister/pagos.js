'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    id_cpt: {
      type: DataTypes.INTEGER,
      xlabel: lang.t('fields.id_cpt')
    },
    cpt: {
      type: DataTypes.JSON,
      xlabel: lang.t('fields.cpt')
    },
    tipo: {
      type: DataTypes.ENUM,
      values: ['ENTIDAD', 'CANCILLERIA'],
      defaultValue: 'ENTIDAD',
      allowNull: false,
      xlabel: lang.t('fields.tipo')
    },
    fecha_pago: {
      type: DataTypes.DATE,
      xlabel: lang.t('fields.fecha_pago')
    },
    estado: {
      type: DataTypes.ENUM,
      values: ['PENDIENTE', 'HABILITADO', 'PAGADO'],
      defaultValue: 'PENDIENTE',
      allowNull: false,
      xlabel: lang.t('fields.estado')
    },
    tipo_documento: {
      type: DataTypes.ENUM,
      values: ['CI', 'PASAPORTE', 'NIT', 'EXTRANJERO'],
      xlabel: lang.t('fields.tipo_documento')
    },
    nro_documento: {
      type: DataTypes.STRING(20),
      xlabel: lang.t('fields.nro_documento')
    },
    titular_factura: {
      type: DataTypes.STRING(100),
      xlabel: lang.t('fields.titular_factura')
    },
    fecha_nacimiento: {
      type: DataTypes.DATEONLY,
      xlabel: lang.t('fields.fecha_nacimiento')
    },
    observacion: {
      type: DataTypes.TEXT,
      xlabel: lang.t('fields.observacion')
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let Pagos = sequelize.define('pagos', fields, {
    timestamps: false
  });

  return Pagos;
};
