'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    usuario: {
      type: DataTypes.STRING(50),
      unique: true,
      allowNull: false,
      xlabel: lang.t('fields.usuario')
    },
    contrasena: {
      type: DataTypes.STRING(255),
      allowNull: false,
      xlabel: lang.t('fields.contrasena')
    },
    email: {
      type: DataTypes.STRING(100),
      unique: true,
      xlabel: lang.t('fields.email'),
      validate: {
        isEmail: true
      }
    },
    nombres: {
      type: DataTypes.STRING(100),
      allowNull: false,
      xlabel: lang.t('fields.nombres')
    },
    primer_apellido: {
      type: DataTypes.STRING(100),
      xlabel: lang.t('fields.primer_apellido')
    },
    segundo_apellido: {
      type: DataTypes.STRING(100),
      xlabel: lang.t('fields.segundo_apellido')
    },
    telefono: {
      type: DataTypes.STRING(30),
      xlabel: lang.t('fields.telefono')
    },
    tour: {
      type: DataTypes.JSON,
      xlabel: lang.t('fields.tour')
    },
    estado: {
      type: DataTypes.ENUM,
      values: ['ACTIVO', 'INACTIVO'],
      defaultValue: 'ACTIVO',
      allowNull: false,
      xlabel: lang.t('fields.estado')
    },
    cargo: {
      type: DataTypes.STRING(255),
      xlabel: lang.t('fields.cargo')
    },
    departamento: {
      type: DataTypes.STRING(50),
      defaultValue: 'LA PAZ',
      xlabel: lang.t('fields.departamento')
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let Users = sequelize.define('usuarios', fields, {
    timestamps: false
  });

  return Users;
};
