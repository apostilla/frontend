'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    codigo: {
      type: DataTypes.STRING(10),
      allowNull: false,
      xlabel: lang.t('fields.codigo')
    },
    nombre: {
      type: DataTypes.STRING(50),
      allowNull: false,
      xlabel: lang.t('fields.nombre')
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let Departamentos = sequelize.define('departamentos', fields, {
    timestamps: false
  });

  return Departamentos;
};
