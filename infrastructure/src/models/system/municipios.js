'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    nombre: {
      type: DataTypes.STRING(150),
      allowNull: false,
      xlabel: lang.t('fields.nombre')
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let Municipios = sequelize.define('municipios', fields, {
    timestamps: false
  });

  return Municipios;
};
