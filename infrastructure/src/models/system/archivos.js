'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    nombre: {
      type: DataTypes.STRING(255),
      xlabel: lang.t('fields.nombre'),
      allowNull: false
    },
    titulo: {
      type: DataTypes.STRING(100),
      xlabel: lang.t('fields.titulo')
    },
    mimetype: {
      type: DataTypes.STRING(100),
      allowNull: false,
      xlabel: lang.t('fields.mimetype')
    },
    ruta: {
      type: DataTypes.STRING(255),
      xlabel: lang.t('fields.ruta'),
      allowNull: false
    },
    tamano: {
      type: DataTypes.DECIMAL,
      xlabel: lang.t('fields.tamano')
    },
    img_ancho: {
      type: DataTypes.INTEGER,
      xlabel: lang.t('fields.img_ancho')
    },
    img_alto: {
      type: DataTypes.INTEGER,
      xlabel: lang.t('fields.img_alto')
    },
    es_imagen: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      xlabel: lang.t('fields.es_imagen'),
      defaultValue: false
    }
  };
  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let Archivos = sequelize.define('archivos', fields, {
    timestamps: false
  });

  return Archivos;
};
