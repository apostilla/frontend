'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    nivel: {
      type: DataTypes.ENUM,
      values: ['ERROR', 'INFO', 'ADVERTENCIA'],
      defaultValue: 'ERROR',
      allowNull: false,
      xlabel: lang.t('fields.nivel')
    },
    tipo: {
      type: DataTypes.STRING(50),
      xlabel: lang.t('fields.tipo')
    },
    mensaje: {
      type: DataTypes.TEXT,
      allowNull: false,
      xlabel: lang.t('fields.mensaje')
    },
    referencia: {
      type: DataTypes.TEXT,
      xlabel: lang.t('fields.referencia')
    },
    ip: {
      type: DataTypes.STRING(100),
      xlabel: lang.t('fields.ip')
    },
    fecha: {
      type: DataTypes.DATE,
      allowNull: false,
      xlabel: lang.t('fields.fecha'),
      defaultValue: DataTypes.NOW
    }
  };

  let Logs = sequelize.define('logs', fields, {
    timestamps: false
  });

  return Logs;
};
