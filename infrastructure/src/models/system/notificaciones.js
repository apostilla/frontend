'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    tipo: {
      type: DataTypes.ENUM,
      values: ['ALERTA', 'MENSAJE', 'CPT', 'CONTACTO', 'RECLAMO'],
      defaultValue: 'CONTACTO',
      allowNull: false,
      xlabel: lang.t('fields.tipo')
    },
    titulo: {
      type: DataTypes.STRING(255),
      xlabel: lang.t('fields.titulo'),
      allowNull: false
    },
    email_sender: {
      type: DataTypes.STRING(50),
      xlabel: lang.t('fields.email_sender')
    },
    mensaje: {
      type: DataTypes.TEXT,
      xlabel: lang.t('fields.mensaje'),
      allowNull: false
    },
    estado: {
      type: DataTypes.ENUM,
      values: ['CREADO', 'VISTO', 'LEIDO'],
      defaultValue: 'CREADO',
      allowNull: false,
      xlabel: lang.t('fields.estado')
    }
  };
  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let Notificaciones = sequelize.define('notificaciones', fields, {
    timestamps: false
  });

  return Notificaciones;
};
