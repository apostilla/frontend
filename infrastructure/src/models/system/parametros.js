'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    nombre: {
      type: DataTypes.STRING(100),
      unique: true,
      allowNull: false,
      xlabel: lang.t('fields.nombre')
    },
    valor: {
      type: DataTypes.TEXT,
      allowNull: false,
      xlabel: lang.t('fields.valor')
    },
    label: {
      type: DataTypes.STRING(100),
      allowNull: false,
      xlabel: lang.t('fields.label')
    },
    descripcion: {
      type: DataTypes.TEXT,
      xlabel: lang.t('fields.descripcion')
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let Parametros = sequelize.define('parametros', fields, {
    timestamps: false
  });

  return Parametros;
};
