'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    base64: {
      type: DataTypes.TEXT,
      allowNull: false,
      xlabel: lang.t('fields.base64')
    },
    fecha_firma: {
      type: DataTypes.DATE,
      allowNull: false,
      xlabel: lang.t('fields.fecha_firma')
    },
    certificado_token: {
      type: DataTypes.JSON,
      allowNull: false,
      xlabel: lang.t('fields.certificado_token')
    }
  };
  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let Firmas = sequelize.define('firmas', fields, {
    timestamps: false
  });

  return Firmas;
};
