'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    nombre: {
      type: DataTypes.STRING(255),
      xlabel: lang.t('fields.nombre'),
      allowNull: false
    },
    descripcion: {
      type: DataTypes.TEXT,
      xlabel: lang.t('fields.descripcion')
    },
    tramite_ppte: {
      type: DataTypes.JSON,
      xlabel: lang.t('fields.tramite_ppte')
    },
    codigo_portal: {
      type: DataTypes.STRING(50),
      unique: true,
      allowNull: false,
      xlabel: lang.t('fields.codigo_portal')
    },
    estado: {
      type: DataTypes.ENUM,
      values: ['ACTIVO', 'INACTIVO'],
      defaultValue: 'ACTIVO',
      allowNull: false,
      xlabel: lang.t('fields.estado')
    },
    derivar: {
      type: DataTypes.STRING(20),
      values: ['ENTIDAD', 'CANCILLERIA'],
      allowNull: false,
      defaultValue: 'ENTIDAD',
      xlabel: lang.t('fields.derivar')
    },
    precio: {
      type: DataTypes.DECIMAL(10, 2),
      allowNull: false,
      defaultValue: 0,
      xlabel: lang.t('fields.precio')
    },
    cuenta: {
      type: DataTypes.STRING(20),
      allowNull: false,
      field: 'cuenta'
    },
    monto: {
      type: DataTypes.JSON,
      allowNull: false,
      field: 'monto'
    },
    otros: {
      type: DataTypes.JSON,
      allowNull: true,
      field: 'otros'
    },
    token_ppte: {
      type: DataTypes.TEXT,
      xlabel: lang.t('fields.token_ppte')
    },
    duracion: {
      type: DataTypes.INTEGER(11),
      defaultValue: 0,
      xlabel: lang.t('fields.duracion')
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let Documentos = sequelize.define('documentos', fields, {
    timestamps: false
  });

  return Documentos;
};
