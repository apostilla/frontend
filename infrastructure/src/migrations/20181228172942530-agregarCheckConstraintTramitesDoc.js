'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`
      alter table tramites_docs
  add constraint check_nro_apostilla check ((estado = 'APOSTILLADO' AND nro_apostilla is not null) or
                                            (estado != 'APOSTILLADO' and nro_apostilla is null) );
      `);
  },
  down: (queryInterface, Sequelize) => {}
};
