'use strict';
const md5File = require('md5-file');
const _ = require('lodash');
module.exports = {
  up: async (queryInterface, Sequelize) => {
    let cambios = [];
    const rutas = await queryInterface.sequelize.query(`select * from parametros where nombre in ('PATH_DOCS', 'PATH_PDFS');`);
    const rutaBase = `${rutas[0][0].valor}${rutas[0][1].valor}`;

    const docsFirmados =  await queryInterface.sequelize.query(`
      SELECT a.nombre, f.id, f.fecha_firma, f.id_archivo
      FROM firmas AS f
      INNER JOIN archivos AS a ON a.id=f.id_archivo
      WHERE a.nombre NOT LIKE 'SIN_ARCHIVO';
    `);
    const ids= [];
    cambios = docsFirmados[0].map(item => {
      try {
        const rutaDocumento = `${rutaBase}/${item.nombre}.pdf`;
        const hashPdf = md5File.sync(rutaDocumento);
        console.log("Revisando los resultados", hashPdf);
        ids.push(item.id);
        return queryInterface.sequelize.query(`UPDATE firmas set base64='${hashPdf}'where id=${item.id};`);
      } catch (e) {
        return;
      }

    })
    cambios = _.compact(cambios);
    console.log("Ids", ids);
    return cambios;

  },
  down: (queryInterface, Sequelize) => {}
};
