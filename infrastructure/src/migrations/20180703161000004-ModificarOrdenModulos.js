'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`
      ALTER TABLE modulos drop CONSTRAINT modulos_orden_key;
      update modulos set orden = orden + 1 where orden not in (1,2);
      update modulos set orden = 3 where id = 24;
      alter table modulos add constraint modulos_orden_key unique (orden);
      `);
  },
  down: (queryInterface, Sequelize) => {}
};
