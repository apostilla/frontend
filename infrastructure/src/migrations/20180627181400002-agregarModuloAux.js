'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    const modulos = [
      {
        ruta: 'auxiliares',
        label: 'auxiliares',
        icono: null,
        orden: 23,
        estado: "ACTIVO",
        visible: true,
        id_modulo: 6,
        id_seccion: null,
        _user_created: 1,
        _created_at: new Date()
      }
    ];

    return queryInterface.bulkInsert('modulos', modulos);
  },
  down: (queryInterface, Sequelize) => {}
};
