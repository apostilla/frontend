'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('paises_destinos', 'sigla', {
      type: Sequelize.STRING(3)
    });
  },
  down: (queryInterface, Sequelize) => {}
};
