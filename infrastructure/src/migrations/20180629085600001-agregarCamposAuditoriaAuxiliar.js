'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return [
      queryInterface.addColumn('tramites_docs', '_user_updated_auxiliar', {
        type: Sequelize.INTEGER
      }),
      queryInterface.addColumn('tramites_docs', '_updated_at_auxiliar', {
        type: Sequelize.DATE
      })
    ]
  },
  down: (queryInterface, Sequelize) => {}
};
