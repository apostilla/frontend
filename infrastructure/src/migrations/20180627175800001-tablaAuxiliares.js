'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('auxiliares', {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      usuario: {
        type: Sequelize.STRING(50),
        unique: true,
        allowNull: false
      },
      contrasena: {
        type: Sequelize.STRING(255),
        allowNull: false
      },
      email: {
        type: Sequelize.STRING(100),
        unique: true,
        validate: {
          isEmail: true
        }
      },
      nombres: {
        type: Sequelize.STRING(100),
        allowNull: false
      },
      primer_apellido: {
        type: Sequelize.STRING(100)
      },
      segundo_apellido: {
        type: Sequelize.STRING(100)
      },
      telefono: {
        type: Sequelize.STRING(30)
      },
      tour: {
        type: Sequelize.JSON
      },
      estado: {
        type: Sequelize.ENUM,
        values: ['ACTIVO', 'INACTIVO'],
        defaultValue: 'ACTIVO',
        allowNull: false
      },
      cargo: {
        type: Sequelize.STRING(255)
      },
      departamento: {
        type: Sequelize.STRING(50),
        defaultValue: 'LA PAZ'
      },
      id_entidad: {
        type: Sequelize.INTEGER
      },
      id_rol: {
        type: Sequelize.INTEGER
      },
      _user_created: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      _created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      _user_updated: {
        type: Sequelize.INTEGER
      },
      _updated_at: {
        type: Sequelize.DATE
      },
    });
  },
  down: (queryInterface, Sequelize) => {}
};
