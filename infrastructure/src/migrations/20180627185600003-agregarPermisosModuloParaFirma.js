'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    const cambios = Array(8).fill().map((_, indice) => {
      let item = {
        create: false,
        read: false,
        update: false,
        delete: false,
        firma: false,
        csv: false,
        activo: false,
        id_modulo: 24,
        id_rol: indice+1,
        _user_created: 1,
        _created_at: new Date()
      };
      if(indice+1 === 7) {
        item.create = true;
        item.read = true;
        item.update = true;
        item.delete = true;
      }
      return item;
    });

    return queryInterface.bulkInsert('permisos', cambios);
  },
  down: (queryInterface, Sequelize) => {}
};
