'use strict';
const md5File = require('md5-file');
const _ = require('lodash');
module.exports = {
  up: async (queryInterface, Sequelize) => {
    let cambios = [];
    const ids= [];
    const rutaPrincipal = await queryInterface.sequelize.query(`select * from parametros where nombre like 'PATH_DOCS';`);
    const rutaPdf = await queryInterface.sequelize.query(`select * from parametros where nombre like 'PATH_PDFS';`);
    const rutaBase = `${rutaPrincipal[0][0].valor}${rutaPdf[0][0].valor}`;

    const docsFirmados =  await queryInterface.sequelize.query(`
      SELECT a.nombre, f.id, f.fecha_firma, f.id_archivo
      FROM firmas AS f
      INNER JOIN archivos AS a ON a.id=f.id_archivo
      WHERE a.nombre NOT LIKE 'SIN_ARCHIVO';
    `);
    console.log("Revisando a ruta principal", rutaBase);
    console.log("Revisando la longitud de docs", docsFirmados.length);
    docsFirmados[0].map(item => {
      try {
        const rutaDocumento = `${rutaBase}/${item.nombre}.pdf`;
        const hashPdf = md5File.sync(rutaDocumento);
        console.log("Revisando los resultados", hashPdf);
        ids.push(item.id);
        cambios.push(queryInterface.sequelize.query(`UPDATE firmas set base64='${hashPdf}'where id=${item.id};`));
      } catch (e) {
        return;
      }

    });
    cambios = _.compact(cambios);
    console.log("Ids", ids);
    return cambios;

  },
  down: (queryInterface, Sequelize) => {}
};
