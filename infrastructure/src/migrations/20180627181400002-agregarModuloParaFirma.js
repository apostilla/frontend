'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    const modulos = [
      {
        ruta: 'para_firma',
        label: 'Para Firma',
        icono: 'fingerprint',
        orden: 24,
        estado: "ACTIVO",
        visible: true,
        id_modulo: null,
        id_seccion: null,
        _user_created: 1,
        _created_at: new Date()
      }
    ];

    return queryInterface.bulkInsert('modulos', modulos);
  },
  down: (queryInterface, Sequelize) => {}
};
