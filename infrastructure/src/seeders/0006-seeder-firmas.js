'use strict';

const casual = require('casual');
const { setTimestampsSeeder } = require('../lib/util');

let items = [];

// Agregando datos aleatorios para desarrollo
if (typeof process.env.NODE_ENV === 'undefined' || process.env.NODE_ENV !== 'production') {
  items = Array(10).fill().map((_, i) => {
    let item = {
      base64: `JVBERi0xLjUKJbXtrvsKMyAwIG9iago8PCAvTGVuZ3RoIDQgMCBSCiAgIC9GaWx0ZXIgL0ZsYXRlRGVjb2RlCj4+CnN0cmVhbQp4nNV934/kOHLmu/6KfJx9qLT4S6QeZ+2Bbw2f97weHA4YG4VaVfVM`,
      id_archivo: casual.integer(1, 10),
      id_usuario: casual.integer(1, 10),
      fecha_firma: casual.date('YYYY-MM-DD HH:mm'),
      certificado_token: JSON.stringify({name: 'chavo el ocho', fecha: new Date()})
    };

    return item;
  });

  // Asignando datos de log y timestamps a los datos
  items = setTimestampsSeeder(items);
}

module.exports = {
  up (queryInterface, Sequelize) {
    if (items.length) {
      return queryInterface.bulkInsert('firmas', items, {});
    }
  },

  down (queryInterface, Sequelize) { }
};
