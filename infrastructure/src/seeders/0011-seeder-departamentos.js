'use strict';

const { setTimestampsSeeder } = require('../lib/util');

let items = [
  { id: 1, nombre: 'Chuquisaca', codigo: 'CH' },
  { id: 2, nombre: 'La Paz', codigo: 'LP' },
  { id: 3, nombre: 'Cochabamba', codigo: 'CBBA' },
  { id: 4, nombre: 'Oruro', codigo: 'OR' },
  { id: 5, nombre: 'Potosí', codigo: 'PT' },
  { id: 6, nombre: 'Tarija', codigo: 'TJ' },
  { id: 7, nombre: 'Santa Cruz', codigo: 'SC' },
  { id: 8, nombre: 'Beni', codigo: 'BN' },
  { id: 9, nombre: 'Pando', codigo: 'PD' }
  // { id: 10, nombre: 'NINGUNO', codigo: 'NN' }
];

// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('departamentos', items, {});
  },
  down (queryInterface, Sequelize) { }
};
