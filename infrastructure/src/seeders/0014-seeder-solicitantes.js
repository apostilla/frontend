'use strict';

const casual = require('casual');
const { setTimestampsSeeder } = require('../lib/util');

let items = [];

// Agregando datos aleatorios para desarrollo
if (typeof process.env.NODE_ENV === 'undefined' || process.env.NODE_ENV !== 'production') {
  items = Array(20).fill().map((_, i) => {
    let item = {
      tipo: casual.random_element(['NATURAL', 'JURIDICA']),
      nombres: casual.first_name,
      primer_apellido: casual.last_name,
      segundo_apellido: casual.last_name,
      email: casual.email,
      telefono: casual.phone,
      fecha_nacimiento: casual.date('YYYY-MM-DD'),
      tipo_documento: casual.random_element(['CI', 'PASAPORTE', 'NIT']),
      nro_documento: casual.integer(1, 20),
      direccion: casual.address,
      matricula: casual.integer(1, 20)
    };
    return item;
  });

  // Asignando datos de log y timestamps a los datos
  items = setTimestampsSeeder(items);
}

module.exports = {
  up (queryInterface, Sequelize) {
    if (items.length) {
      return queryInterface.bulkInsert('solicitantes', items, {});
    }
  },

  down (queryInterface, Sequelize) { }
};
