'use strict';

const casual = require('casual');
const { setTimestampsSeeder } = require('../lib/util');

let items = [];

// Agregando datos aleatorios para desarrollo
if (typeof process.env.NODE_ENV === 'undefined' || process.env.NODE_ENV !== 'production') {
  items = Array(10).fill().map((_, i) => {
    let item = {
      cpt: '{ "name": "Book the First", "author": { "first_name": "Bob", "last_name": "White" } }',
      tipo: casual.random_element(['ENTIDAD', 'CANCILLERIA']),
      fecha_pago: casual.date('YYYY-MM-DD'),
      estado: casual.random_element(['PENDIENTE', 'HABILITADO', 'PAGADO']),
      tipo_documento: casual.random_element(['CI', 'PASAPORTE', 'NIT', 'EXTRANJERO']),
      nro_documento: casual.integer(1000000, 100000000),
      titular_factura: casual.full_name,
      id_tramite_doc: casual.integer(1, 20)
    };
    return item;
  });

  // Asignando datos de log y timestamps a los datos
  items = setTimestampsSeeder(items);
}

module.exports = {
  up (queryInterface, Sequelize) {
    if (items.length) {
      return queryInterface.bulkInsert('pagos', items, {});
    }
  },

  down (queryInterface, Sequelize) { }
};
