'use strict';

const casual = require('casual');
const { setTimestampsSeeder } = require('../lib/util');

let items = [];

// Agregando datos aleatorios para desarrollo
if (typeof process.env.NODE_ENV === 'undefined' || process.env.NODE_ENV !== 'production') {
  items = Array(20).fill().map((_, i) => {
    let item = {
      pago_entidad: casual.random_element([false, true]),
      pago_cancilleria: casual.random_element([false, true]),
      cod_seguridad: casual.phone,
      nro_apostilla: casual.integer(1, 20),
      fecha_expedicion: casual.date('YYYY-MM-DD'),
      estado: casual.random_element(['CREADO', 'REVISADO_ENTIDAD', 'OBSERVADO_ENTIDAD_REVISOR', 'OBSERVADO_ENTIDAD_FIRMANTE', 'FIRMADO_ENTIDAD', 'REVISADO_CANCILLERIA', 'OBSERVADO_CANCILLERIA_REVISOR', 'OBSERVADO_CANCILLERIA_FIRMANTE', 'APOSTILLADO']),
      observacion: casual.text,
      id_tramite: casual.integer(1, 20),
      id_documento: casual.integer(1, 10),
      id_archivo: 1,
      firmante_documento_nombre: casual.name,
      firmante_documento_cargo: casual.name_prefix,
      firmante_documento_entidad: casual.company_name,
      digital: casual.random_element([false, true]),
      otros: casual.random_element(['[{"type": "text","nombre": "Campo extra 1","label": "Campo extra 1","value": "Valor de campo extra 1"},{"type": "text","nombre": "Campo extra 2","label": "Campo extra 2","value": "Valor de campo extra 2"}]', '[{"type": "text","nombre": "Campo extra 1","label": "Campo extra 1","value": "Valor de campo extra 1"}]', '[{}]'])
    };
    return item;
  });

  // Asignando datos de log y timestamps a los datos
  items = setTimestampsSeeder(items);
}

module.exports = {
  up (queryInterface, Sequelize) {
    if (items.length) {
      return queryInterface.bulkInsert('tramites_docs', items, {});
    }
  },

  down (queryInterface, Sequelize) { }
};
