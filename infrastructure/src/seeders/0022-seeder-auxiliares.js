'use strict';

const casual = require('casual');
const { encrypt, setTimestampsSeeder } = require('../lib/util');
const contrasena = encrypt('123456');

let items = [
];

// Agregando datos aleatorios para desarrollo
if (typeof process.env.NODE_ENV === 'undefined' || process.env.NODE_ENV !== 'production') {
  items.push({
    usuario: 'revisore',
    contrasena,
    email: 'revisore@apostilla.gob.bo',
    nombres: 'Revisor',
    primer_apellido: 'Entidad',
    estado: 'ACTIVO',
    id_rol: 6,
    id_entidad: 1
  });

  let auxiliares = Array(12).fill().map((_, i) => {
    let item = {
      usuario: casual.username,
      contrasena,
      email: casual.email,
      nombres: casual.first_name,
      primer_apellido: casual.last_name,
      segundo_apellido: casual.last_name,
      telefono: casual.phone,
      estado: casual.random_element(['ACTIVO', 'INACTIVO']),
      id_rol: 6,
      id_entidad: casual.integer(1, 10)
    };

    return item;
  });

  items = items.concat(auxiliares);
}

// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('auxiliares', items, {});
  },

  down (queryInterface, Sequelize) { }
};
