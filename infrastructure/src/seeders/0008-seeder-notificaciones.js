'use strict';

const casual = require('casual');
const { setTimestampsSeeder } = require('../lib/util');

let items = [];

// Agregando datos aleatorios para desarrollo
if (typeof process.env.NODE_ENV === 'undefined' || process.env.NODE_ENV !== 'production') {
  items = Array(10).fill().map((_, i) => {
    let item = {
      id_sender: casual.integer(1, 10),
      id_receiver: casual.integer(1, 10),
      tipo: casual.random_element(['ALERTA', 'MENSAJE', 'CPT', 'CONTACTO', 'RECLAMO']),
      titulo: casual.title,
      email_sender: casual.email,
      mensaje: casual.description,
      estado: casual.random_element(['CREADO', 'VISTO', 'LEIDO'])
    };

    return item;
  });

  // Asignando datos de log y timestamps a los datos
  items = setTimestampsSeeder(items);
}

module.exports = {
  up (queryInterface, Sequelize) {
    if (items.length) {
      return queryInterface.bulkInsert('notificaciones', items, {});
    }
  },

  down (queryInterface, Sequelize) { }
};
