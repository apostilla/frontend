'use strict';

const casual = require('casual');
const { setTimestampsSeeder } = require('../lib/util');

let items = [];

// Agregando datos aleatorios para desarrollo
if (typeof process.env.NODE_ENV === 'undefined' || process.env.NODE_ENV !== 'production') {
  items = Array(20).fill().map((_, i) => {
    let item = {
      id_entidad: casual.integer(1, 10),
      nombre: casual.title,
      descripcion: casual.short_description,
      estado: casual.random_element(['ACTIVO', 'INACTIVO']),
      derivar: casual.random_element(['ENTIDAD', 'CANCILLERIA']),
      token_ppte: casual.uuid,
      codigo_portal: casual.title,
      precio: casual.integer(10, 100),
      cuenta: casual.card_number(),
      monto: '[{},{}]',
      otros: casual.random_element(['[{"type": "text","nombre": "Campo extra 1","label": "Campo extra 1","value": "Valor de campo extra 1"},{"type": "text","nombre": "Campo extra 2","label": "Campo extra 2","value": "Valor de campo extra 2"}]', '[{"type": "text","nombre": "Campo extra 1","label": "Campo extra 1","value": "Valor de campo extra 1"}]', '[{}]'])
    };

    return item;
  });

  // Asignando datos de log y timestamps a los datos
  items = setTimestampsSeeder(items);
}

module.exports = {
  up (queryInterface, Sequelize) {
    if (items.length) {
      return queryInterface.bulkInsert('documentos', items, {});
    }
  },

  down (queryInterface, Sequelize) { }
};
