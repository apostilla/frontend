'use strict';
const lang = require('../../lang');

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.addColumn('entidades', 'token', {
      type: Sequelize.TEXT,
      xlabel: lang.t('fields.token')
    });
  },

  down (queryInterface, Sequelize) { }
};
