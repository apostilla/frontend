'use strict';

const { setTimestampsSeeder } = require('../../lib/util');

let items = [
  { nombre: 'DIGEMIG_CERTIFICACION', valor: 'http://servicios.migracion.gob.bo/agetic_test/api/Certificacion', label: 'DIGEMIG Certificación', descripcion: 'DIGEMIG Certificación Devuelve en base 64 el acta de consignación de documentos, que es el documento con el cual la persona realizara el pago.' },
  { nombre: 'DIGEMIG_VERIFICACION', valor: 'http://servicios.migracion.gob.bo/agetic_test/api/Verificacion', label: 'DIGEMIG Verificación', descripcion: 'Devolvera 1 si los datos son correctos y 0 si los datos no se encuentran en la base de datos' },
  { nombre: 'DIGEMIG_TRAMITES', valor: '1567', label: 'DIGEMIG lista de trámites', descripcion: 'Lista de trámites del DIGEMIG para las ordenes de pago' },
  { nombre: 'SIREPRO_MATRICULAS_URL', valor: 'http://test.agetic.gob.bo/kong/fake/sirepro/v2/matriculas/', label: 'SIREPRO matrículas URL', descripcion: 'SIREPRO matrículas URL' },
  { nombre: 'SIREPRO_MATRICULAS_TOKEN', valor: '', label: 'SIREPRO matrículas TOKEN', descripcion: 'SIREPRO matrículas TOKEN' }
];

// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('parametros', items, {});
  },

  down (queryInterface, Sequelize) { }
};
