'use strict';

const casual = require('casual');
const { setTimestampsSeeder } = require('../lib/util');

let items = [];

// Agregando datos aleatorios para desarrollo
if (typeof process.env.NODE_ENV === 'undefined' || process.env.NODE_ENV !== 'production') {
  items = Array(20).fill().map((_, i) => {
    let item = {
      id_solicitante: casual.integer(1, 20),
      // id_pais_destino: casual.integer(1, 40),
      fecha_inicio: casual.date('YYYY-MM-DD'),
      fecha_fin: casual.date('YYYY-MM-DD'),
      tipo_documento: casual.random_element(['CI', 'PASAPORTE']),
      nro_documento: casual.integer(1000000, 100000000),
      titular_factura: casual.full_name,
      estado: casual.random_element(['SOLICITADO', 'EN_PROCESO', 'FINALIZADO']),
      cod_seguimiento: casual.integer(1, 1000000000)
    };
    return item;
  });

  // Asignando datos de log y timestamps a los datos
  items = setTimestampsSeeder(items);
}

module.exports = {
  up (queryInterface, Sequelize) {
    if (items.length) {
      return queryInterface.bulkInsert('tramites', items, {});
    }
  },

  down (queryInterface, Sequelize) { }
};
