'use strict';

const { setTimestampsSeeder } = require('../lib/util');

let items = [
  { nombre: 'SUPERADMIN', descripcion: 'Super administrador' },
  { nombre: 'ADMIN CANCILLERIA', descripcion: 'Administrador de la cancillería' },
  { nombre: 'REVISOR CANCILLERIA', descripcion: 'Revisor de la cancillería' },
  { nombre: 'FIRMADOR CANCILLERIA', descripcion: 'Firmador de la cancillería' },
  { nombre: 'ADMIN ENTIDAD', descripcion: 'Administrador de la entidad' },
  { nombre: 'AUXILIAR ENTIDAD', descripcion: 'Auxiliar de la entidad' },
  { nombre: 'FIRMADOR ENTIDAD', descripcion: 'Firmador de la entidad' },
  { nombre: 'SOLICITANTE', descripcion: 'Usuario solicitante' }
];

// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('roles', items, {});
  },

  down (queryInterface, Sequelize) { }
};
