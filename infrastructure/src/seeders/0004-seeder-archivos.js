'use strict';

const casual = require('casual');
const { setTimestampsSeeder } = require('../lib/util');

let items = [
  {
    nombre: 'SIN_ARCHIVO',
    titulo: 'Sin archivo',
    mimetype: 'type/archivo',
    ruta: '/documentos',
    es_imagen: false
  }
];

// Agregando datos aleatorios para desarrollo
if (typeof process.env.NODE_ENV === 'undefined' || process.env.NODE_ENV !== 'production') {
  let archivos = Array(9).fill().map((_, i) => {
    let item = {
      nombre: casual.name,
      titulo: casual.title,
      mimetype: casual.mime_type,
      ruta: '/documentos',
      tamano: casual.integer(1, 3),
      img_ancho: casual.integer(200, 400),
      img_alto: casual.integer(200, 400),
      es_imagen: casual.boolean
    };

    return item;
  });

  items = items.concat(archivos);
}

// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('archivos', items, {});
  },

  down (queryInterface, Sequelize) { }
};
