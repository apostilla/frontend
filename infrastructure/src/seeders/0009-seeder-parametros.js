'use strict';

const { setTimestampsSeeder } = require('../lib/util');
const templateCorreo = require('./templates/correo-tmpl');

let items = [
  { nombre: 'PATH_DOCS', valor: '/home/user', label: 'Ruta documentos', descripcion: 'Ruta de acceso a todos los ducumentos' },
  { nombre: 'EMAIL_ORIGEN', valor: 'apostilla@rree.gob.bo', label: 'Remitente', descripcion: 'Email del remitente del sistema para envío de correos' },
  { nombre: 'EMAIL_HOST', valor: 'smtp.agetic.gob.bo', label: 'Host', descripcion: 'Host del servicio de correos para envío de correos' },
  { nombre: 'EMAIL_PORT', valor: 587, label: 'Puerto', descripcion: 'Puerto para envío de correos' },
  { nombre: 'SEGIP_TOKEN', valor: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIxWGswY1pheWJxMXd6M2RjUjlQaER4Rmd1eHJpbVBVRiIsInVzZXIiOiJvZ3V0aWVycmV6IiwiZXhwIjoxNTI2NTg1NTM4LCJpYXQiOjE1MTg4MDk1Mzh9.E8kqSP-gdI0EJu7D30FUCUOcLZNADTIABEj4Et7pGrU', label: 'Token Segip', descripcion: 'Token para acceder al servicio de segip' },
  { nombre: 'SEGIP_CONTRASTACION_URL', valor: 'https://interoperabilidad.agetic.gob.bo/fake/segip/v2/personas/', label: 'Servicio Segip contrastacion', descripcion: 'Url del servicio de segip contrastacion' },
  { nombre: 'SEGIP_BUSCAR_PERSONA_URL', valor: 'https://interoperabilidad.agetic.gob.bo/fake/segip/v2/personas/', label: 'Servicio Segip buscar persona', descripcion: 'Url del servicio de segip para buscar una persona por ci, fecha de nacimiento y complemento si existe' },
  { nombre: 'PPTE_LOGIN_URL', valor: 'https://test.agetic.gob.bo/pagos-api/autenticacion/', label: 'Servicio PPTE login', descripcion: 'Url del servicio de PPTE para ingresar como usuario regular.' },
  { nombre: 'PPTE_TRAMITES_URL', valor: 'https://test.agetic.gob.bo/pagos-api/entidades/{id_entidad}/tramites?limite=30', label: 'Servicio PPTE: Obtener documentos', descripcion: 'Url del servicio de PPTE para obtner los documentos (tramites) de la entidad en cuestion' },
  { nombre: 'PPTE_USUARIOSAPLICACION_URL', valor: 'https://test.agetic.gob.bo/pagos-api/usuariosAplicacion', label: 'Servicio PPTE: Obtener usuarios aplicaciones', descripcion: 'Url del servicio de PPTE para obtner los usuarios aplicaciones de la entidad en cuestion' },
  { nombre: 'PPTE_CPT_URL', valor: 'https://test.agetic.gob.bo/pagos-api/codigosPagoTramites', label: 'Servicio PPTE: Crear cpt', descripcion: 'Url del servicio de PPTE para crear cptes' },
  { nombre: 'PPTE_TOKEN_TRAMITE_URL', valor: 'https://test.agetic.gob.bo/pagos-api/entidades/{id_entidad}/tramites/{id_tramite}/token{id_token}', label: 'Servicio PPTE: Generar token para tramite', descripcion: 'Url del servicio de PPTE para generar token para tramite' },
  { nombre: 'JWT_TOKEN_EXPIRATION', valor: 240, label: 'Tiempo de expiración del Token', descripcion: 'Tiempo de expiración del Token JWT en minutos' },
  { nombre: 'PATH_PDFS', valor: '/pdfs', label: 'Directorio para guardar archivos', descripcion: 'Directorio para guardar documentos escaneados.' },
  { nombre: 'PATH_JAR', valor: '/home/apostilla/cancilleria-apostilla-backend/application/src/lib/FirmaPkcs11PdfLib.jar', label: '.Jar que obtiene firma de un PDF', descripcion: 'Archivo jar que extrae la firma de un PDF.' },
  { nombre: 'PATH_CSV', valor: '/csv', label: 'Directorio para guardar archivos csv', descripcion: 'Directorio para guardar csvs.' },
  { nombre: 'TEMPLATE_CORREO_BASE', valor: templateCorreo, label: 'Template Doc Apostillado html', descripcion: 'Este template es para notificar al usuario que su documento ha sido apostillado.' },
  { nombre: 'COSTO_APOSTILLA', valor: '70', label: 'Costo de la apostilla', descripcion: 'Costo de la Apostilla en bolivianos' },
  { nombre: 'URL_PORTAL', valor: `https://test.agetic.gob.bo/apostilla/`, label: 'URL del portal', descripcion: 'URL para acceder al portal de la apostilla' },
  { nombre: 'URL_ADMIN', valor: `https://test.agetic.gob.bo/apostilla-admin/`, label: 'URL del administrador', descripcion: 'URL para acceder al administrador de la apostilla' },
  { nombre: 'PORTAL_TRAMITES_ENTIDADES_URL', valor: 'https://test.agetic.gob.bo/portal-unico-api/api/v1/entidad', label: 'Servicio Portal Tramites: Obtener entidades', descripcion: 'Url del servicio del portal de tramites para obtener las entidades.' },
  { nombre: 'PORTAL_TRAMITES_DOC_URL', valor: 'https://test.agetic.gob.bo/portal-unico-api/api/v1/entidad/{id_entidad}/tramite', label: 'Servicio Portal Tramites: Obtener documentos', descripcion: 'Url del servicio de portal de tramites para obtner los documentos (tramites) de la entidad en cuestion' },
  { nombre: 'PORTAL_TRAMITES_TOKEN', valor: 'cCgUH8Inz2qYtrgtW6a9dMcLbKsUUInLbf5R1521118555772', label: 'Servicio Portal Tramites: Token', descripcion: 'Token del servicio de portal de tramites para obtner entidades y los documentos (tramites) de la entidad en cuestion' },
  { nombre: 'URL_PORTAL_UNICO', valor: 'https://test.agetic.gob.bo/portal-unico', label: 'Página web del Portal único de trámites', descripcion: 'Página web del Portal único de trámites para la interoperabilidad con el mismo' },
  { nombre: 'NRO_APOSTILLA', valor: '0', label: 'Número de apostilla', descripcion: 'Contador para el número de apostilla' },
  { nombre: 'MAX_FILESIZE', valor: '5', label: 'Tamaño máximo subida de archivos', descripcion: 'Tamaño máximo para la subida de archivos representado en MB (MegaBytes)' },
  { nombre: 'DIGEMIG_TOKEN', valor: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIxWGswY1pheWJxMXd6M2RjUjlQaER4Rmd1eHJpbVBVRiIsInVzZXIiOiJvZ3V0aWVycmV6IiwiZXhwIjoxNTI2NTg1NTM4LCJpYXQiOjE1MTg4MDk1Mzh9.E8kqSP-gdI0EJu7D30FUCUOcLZNADTIABEj4Et7pGrU', label: 'Token DIGEMIG', descripcion: 'Token para acceder al servicio de DIGEMIG' },
  { nombre: 'DIGEMIG_DOCUMENTO_URL', valor: 'https://interoperabilidad.agetic.gob.bo/fake/digemig/v2/personas/', label: 'Servicio DIGEMIG documento', descripcion: 'Url del servicio de DIGEMIG para obtener el estado migratorio de una persona en base a su documento' },
  { nombre: 'SMS_URL', valor: 'http://mensajeria.agetic.gob.bo/api/v1/', label: 'Url del servicio de envio de sms', descripcion: 'URL del servicio de envío de sms' },
  { nombre: 'REGIONALES', valor: 'LA PAZ,COCHABAMBA,SANTA CRUZ,TARIJA,BENI,PANDO,ORURO,CHUQUISACA,POTOSI', label: 'Regionales de Cancilleria', descripcion: 'Regionales de Cancilleria, Para aumentar una regional se debe agregar la nueva regional separado de una coma, tenga cuidado en modificar esto porque alterará significativamente el funcionamiento del sistema' }
];

// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('parametros', items, {});
  },

  down (queryInterface, Sequelize) { }
};
