'use strict';

const { setTimestampsSeeder } = require('../lib/util');

let items = [
  {
    label: 'Trámites',
    ruta: 'tramites',
    icono: 'assignment',
    orden: 1,
    estado: 'ACTIVO',
    visible: false
  },
  {
    label: 'Solicitudes',
    ruta: 'tramitesDoc',
    icono: 'assignment',
    orden: 2,
    estado: 'ACTIVO',
    visible: true
  },
  {
    label: 'Pagos',
    ruta: 'pagos',
    icono: 'monetization_on',
    orden: 5,
    estado: 'ACTIVO',
    visible: true
  },
  {
    label: 'Solicitantes',
    ruta: 'solicitantes',
    icono: 'person',
    orden: 6,
    estado: 'ACTIVO',
    visible: false
  },
  {
    label: 'Documentos',
    ruta: 'documentos',
    icono: 'folder',
    orden: 7,
    estado: 'ACTIVO',
    visible: true
  },
  {
    label: 'Configuraciones',
    ruta: 'config',
    icono: 'settings',
    orden: 8,
    estado: 'ACTIVO',
    visible: true
  },
  {
    label: 'Entidades',
    ruta: 'entidades',
    orden: 10,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 6
  },
  {
    label: 'Usuarios',
    ruta: 'usuarios',
    orden: 11,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 6
  },
  {
    label: 'Módulos y permisos',
    ruta: 'modulos',
    orden: 12,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 6
  },
  {
    label: 'Preferencias',
    ruta: 'parametros',
    orden: 13,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 6
  },
  {
    label: 'Logs del sistema',
    ruta: 'logs',
    orden: 14,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 6
  },
  {
    label: 'Permisos',
    ruta: 'permisos',
    orden: 15,
    estado: 'ACTIVO',
    visible: false,
    id_modulo: 6
  },
  {
    label: 'Roles',
    ruta: 'roles',
    orden: 16,
    estado: 'ACTIVO',
    visible: false,
    id_modulo: 6
  },
  {
    label: 'Departamentos',
    ruta: 'departamentos',
    orden: 17,
    estado: 'ACTIVO',
    visible: false,
    id_modulo: 6
  },
  {
    label: 'Provincias',
    ruta: 'provincias',
    orden: 18,
    estado: 'ACTIVO',
    visible: false,
    id_modulo: 6
  },
  {
    label: 'Municipios',
    ruta: 'municipios',
    orden: 19,
    estado: 'ACTIVO',
    visible: false,
    id_modulo: 6
  },
  {
    label: 'Archivos',
    ruta: 'archivos',
    orden: 20,
    estado: 'ACTIVO',
    visible: false,
    id_modulo: 6
  },
  {
    label: 'Notificaciones',
    ruta: 'notificaciones',
    orden: 21,
    estado: 'ACTIVO',
    visible: false,
    id_modulo: 6
  },
  {
    label: 'Firmas',
    ruta: 'firmas',
    orden: 22,
    estado: 'ACTIVO',
    visible: false,
    id_modulo: 6
  },
  {
    label: 'Segip',
    ruta: 'segip',
    orden: 23,
    estado: 'ACTIVO',
    visible: false,
    id_modulo: 6
  },
  {
    ruta: 'reportes',
    label: 'Reportes',
    icono: 'description',
    orden: 9,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: null,
    id_seccion: null
  },
  {
    ruta: 'historial',
    label: 'Historial',
    icono: 'library_books',
    orden: 4,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: null,
    id_seccion: null
  },
  {
    ruta: 'auxiliares',
    label: 'Auxiliares',
    icono: null,
    orden: 24,
    estado: "ACTIVO",
    visible: true,
    id_modulo: 6,
    id_seccion: null
  },
  {
    ruta: 'para_firma',
    label: 'Para firma',
    icono: 'fingerprint',
    orden: 3,
    estado: "ACTIVO",
    visible: true,
    id_modulo: null,
    id_seccion: null
  }
];

// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('modulos', items, {});
  },

  down (queryInterface, Sequelize) { }
};
