'use strict';

const casual = require('casual');
const { encrypt, setTimestampsSeeder } = require('../lib/util');
const contrasena = encrypt('123456');

// Agregando datos para producción
let items = [
  {
    nombre: 'Ministerio de Relaciones Exteriores',
    descripcion: 'Ministerio de Relaciones Exteriores - Cancillería',
    codigo_portal: '331',
    sigla: 'MINRREE',
    email: 'mreuno@rree.gob.bo',
    telefonos: '(591-2) 2408900,2409114,2408595',
    direccion: 'Plaza Murillo c. Ingavi esq. c. Junín',
    web: 'www.cancilleria.gob.bo',
    estado: 'ACTIVO'
  }
];

// Agregando datos aleatorios para desarrollo
if (typeof process.env.NODE_ENV === 'undefined' || process.env.NODE_ENV !== 'production') {
  let lista = Array(9).fill().map((_, i) => {
    let item = {
      nombre: casual.company_name,
      descripcion: casual.text,
      sigla: casual.company_suffix,
      codigo_portal: casual.title,
      email: casual.email,
      telefonos: `${casual.phone},${casual.phone}`,
      direccion: casual.address,
      web: casual.url,
      estado: 'ACTIVO',
      info: '{ "name": "Book the First", "author": { "first_name": "Bob", "last_name": "White" } }',
      subdomain: casual.domain,
      usuario_ppte: casual.username,
      pass_ppte: contrasena
    };

    return item;
  });
  items = items.concat(lista);
}

// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('entidades', items, {});
  },

  down (queryInterface, Sequelize) { }
};
