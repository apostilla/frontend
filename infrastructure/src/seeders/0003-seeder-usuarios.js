'use strict';

const casual = require('casual');
const { encrypt, setTimestampsSeeder } = require('../lib/util');
const contrasena = encrypt('123456');

let items = [
  {
    usuario: 'admin',
    contrasena,
    email: 'admin@apostilla.gob.bo',
    nombres: 'Administrador',
    primer_apellido: 'Sistema',
    estado: 'ACTIVO',
    id_rol: 1,
    id_entidad: 1
  },
  {
    usuario: 'solicitante',
    contrasena,
    email: 'solicitante@apostilla.gob.bo',
    nombres: 'Solicitante',
    primer_apellido: 'Apostilla',
    estado: 'ACTIVO',
    id_rol: 8,
    id_entidad: 1
  },
  {
    usuario: 'adminc',
    contrasena,
    email: 'adminc@apostilla.gob.bo',
    nombres: 'Administrador',
    primer_apellido: 'Cancilleria',
    estado: 'ACTIVO',
    id_rol: 2,
    id_entidad: 1
  }
];

// Agregando datos aleatorios para desarrollo
if (typeof process.env.NODE_ENV === 'undefined' || process.env.NODE_ENV !== 'production') {
  items.push({
    usuario: 'revisorc',
    contrasena,
    email: 'revisorc@apostilla.gob.bo',
    nombres: 'Revisor',
    primer_apellido: 'Cancilleria',
    estado: 'ACTIVO',
    id_rol: 3,
    id_entidad: 1
  });
  items.push({
    usuario: 'firmadorc',
    contrasena,
    email: 'firmadorc@apostilla.gob.bo',
    nombres: 'Firmador',
    primer_apellido: 'Cancilleria',
    estado: 'ACTIVO',
    id_rol: 4,
    id_entidad: 1
  });
  items.push({
    usuario: 'admine',
    contrasena,
    email: 'admine@apostilla.gob.bo',
    nombres: 'Administrador',
    primer_apellido: 'Entidad',
    estado: 'ACTIVO',
    id_rol: 5,
    id_entidad: 1
  });
  items.push({
    usuario: 'revisore',
    contrasena,
    email: 'revisore@apostilla.gob.bo',
    nombres: 'Revisor',
    primer_apellido: 'Entidad',
    estado: 'ACTIVO',
    id_rol: 6,
    id_entidad: 1
  });
  items.push({
    usuario: 'firmadore',
    contrasena,
    email: 'firmadore@apostilla.gob.bo',
    nombres: 'Firmador',
    primer_apellido: 'Entidad',
    estado: 'ACTIVO',
    id_rol: 7,
    id_entidad: 1
  });

  let usuarios = Array(12).fill().map((_, i) => {
    let item = {
      usuario: casual.username,
      contrasena,
      email: casual.email,
      nombres: casual.first_name,
      primer_apellido: casual.last_name,
      segundo_apellido: casual.last_name,
      telefono: casual.phone,
      estado: casual.random_element(['ACTIVO', 'INACTIVO']),
      id_rol: casual.integer(2, 7),
      id_entidad: casual.integer(1, 10)
    };

    return item;
  });

  items = items.concat(usuarios);
}

// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('usuarios', items, {});
  },

  down (queryInterface, Sequelize) { }
};
