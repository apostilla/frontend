'use strict';

const casual = require('casual');

let items = [];

// Agregando datos aleatorios para desarrollo
if (typeof process.env.NODE_ENV === 'undefined' || process.env.NODE_ENV !== 'production') {
  items = Array(50).fill().map((_, i) => {
    let item = {
      nivel: casual.random_element(['ERROR', 'INFO', 'ADVERTENCIA']),
      tipo: casual.word,
      mensaje: casual.sentence,
      referencia: casual.sentences(2),
      ip: casual.ip,
      fecha: casual.date('YYYY-MM-DD'),
      id_usuario: casual.integer(1, 10)
    };

    return item;
  });
}

module.exports = {
  up (queryInterface, Sequelize) {
    if (items.length) {
      return queryInterface.bulkInsert('logs', items, {});
    }
  },

  down (queryInterface, Sequelize) { }
};
