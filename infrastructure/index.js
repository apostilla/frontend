'use strict';

const defaults = require('defaults');
const Sequelize = require('sequelize');
const util = require('./src/lib/util');
const transaction = require('./src/lib/transaction');
const hooks = require('./src/hooks');
const associations = require('./src/associations');
const path = require('path');
const debug = require('debug')('apostilla:db');

module.exports = async function (config) {
  config = defaults(config, {
    dialect: 'postgres',
    pool: {
      max: 10,
      min: 0,
      idle: 10000
    },
    query: {
      raw: true
    }
  });

  let sequelize = new Sequelize(config);

  // Cargando todos los modelos que se encuentran en la carpeta models y en sus subcarpetas
  let models = util.loadModels(path.join(__dirname, 'src', 'models'), sequelize, { exclude: ['index.js'] });
  models = util.convertLinealObject(models);
  // debug('Models', models);

  // Cargando asociaciones entre las tablas
  models = associations(models);

  // Iniciando Hooks de Sequelize
  hooks.init(sequelize);

  // Cargando todos los repositorios que se encuentran en la carpeta repositories y en sus subcarpetas
  let repositories = util.loadRepositories(path.join(__dirname, 'src', 'repositories'), models, Sequelize, { exclude: ['index.js', 'queries.js'] });
  repositories = util.convertLinealObject(repositories);
  repositories.transaction = transaction(sequelize);
  
  debug('Capa infraestructura - Repositorios cargados correctamente');

  await sequelize.authenticate();

  if (config.setup) {
    await sequelize.sync({ force: true });
  }

  return repositories;
};
