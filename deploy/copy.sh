#!/usr/bin/env bash
echo - Copiando archivos para el deploy -
pwd
echo 1. Copiando Capa Common
rm -rf roles/common/files
mkdir roles/common/files
cp -rf ../common roles/common/files
rm -rf roles/common/files/common/node_modules
rm -f roles/common/files/common/src/config/db.js
cp roles/common/files/common/src/config/db.js.sample roles/common/files/common/src/config/db.js
rm -f roles/common/files/common/src/config/correo.js
cp roles/common/files/common/src/config/correo.js.sample roles/common/files/common/src/config/correo.js
echo 2. Copiando Capa de Infrastructura
rm -rf roles/infrastructure/files
mkdir roles/infrastructure/files
cp -rf ../infrastructure roles/infrastructure/files
rm -rf roles/infrastructure/files/infrastructure/node_modules
echo 3. Copiando Capa de Dominio
rm -rf roles/domain/files
mkdir roles/domain/files
cp -rf ../domain roles/domain/files
rm -rf roles/domain/files/domain/node_modules
echo 4. Copiando Capa de Aplicacación
rm -rf roles/application/files/application
cp -rf ../application roles/application/files
rm -rf roles/application/files/application/node_modules
echo 5. Copiando Capa de Proxy web
rm -rf roles/web/files/web
cp -rf ../web roles/web/files
rm -rf roles/web/files/web/node_modules
echo 6. ¡Finalizado!