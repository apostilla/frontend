# Cancilleria - Apostilla (Backend)

### Actualización de código fuente

```sh
$ git pull origin master
```

### Reiniciar la aplicación
```sh
$ pm2 restart all
```