'use strict';

const i18n = require('./src/lib/i18n');
const errors = require('./src/lib/errors');
const sequelizeCrud = require('./src/lib/sequelize-crud');
const correo = require('./src/lib/correo');
const texto = require('./src/lib/texto');
const config = require('./src/config');

module.exports = {
  i18n,
  errors,
  config,
  sequelizeCrud,
  correo,
  texto
};
