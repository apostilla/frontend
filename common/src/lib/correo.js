/*
 * Envio de correo
 datosEnvio:
 {
    para: ['rhuayller@agetic.gob.bo'],
    titulo: '[Cancilleria Bolivia] Tu documento ha sido apostillado!',
    mensaje: 'Este es el texto bla bla',
    html: '',
    attachments: [{ path: '/home/ramiro/Documents/documentos/test1.pdf', filename: 'CertificadoNacimiento.pdf' }]
  }
 */
module.exports.enviar = (datosEnvio) => {
  const nodemailer = require('nodemailer');
  const smtpTransport = require('nodemailer-smtp-transport');
  const correoConfig = require('../config/correo');
  const transporter = nodemailer.createTransport(smtpTransport(correoConfig));
  const opciones = {
    from: correoConfig.origen,
    to: datosEnvio.para,
    subject: datosEnvio.titulo,
    text: datosEnvio.mensaje,
    html: datosEnvio.html,
    attachments: datosEnvio.attachments
  };
  return new Promise((resolve, reject) => {
    transporter.sendMail(opciones, (error, info) => {
      if (error === null) {
        resolve(info);
      } else {
        if (error.code === 'UNABLE_TO_VERIFY_LEAF_SIGNATURE') { resolve(error); } else {
          reject(error);
        }
      }
    });
  });
};
