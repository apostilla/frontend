'use strict';

module.exports = {
  nano (template, data) {
    // return template.replace(/\{([\w\.]*)\}/g, function (str, key) {
    return template.replace(/\{([\w.]*)\}/g, function (str, key) {
      let keys = key.split('.');
      let v = data[keys.shift()];
      for (let i = 0, l = keys.length; i < l; i++) { v = v[keys[i]]; }
      return (typeof v !== 'undefined' && v !== null) ? v : '';
    });
  }
};
