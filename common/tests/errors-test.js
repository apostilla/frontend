'use strict';

const t = require('ava');
const common = require('../');

t('Clases para el manejo de errores', t => {
  const Errors = common.errors;

  let error;

  // InternalServerError
  error = new Errors.InternalServerError('Internal Server Error');
  t.is('Internal Server Error', error.message);
  t.is(500, error.statusCode);

  error = new Errors.InternalServerError('');
  t.is('', error.message);

  error = new Errors.InternalServerError(null);
  t.is('', error.message);

  // NotFoundError
  error = new Errors.NotFoundError('Not found error');
  t.is('Not found error', error.message);
  t.is(404, error.statusCode);

  error = new Errors.NotFoundError('');
  t.is('', error.message);

  error = new Errors.NotFoundError(null);
  t.is('', error.message);

  // BadRequestError
  error = new Errors.BadRequestError('Bad Request Error');
  t.is('Bad Request Error', error.message);
  t.is(400, error.statusCode);

  error = new Errors.BadRequestError('');
  t.is('', error.message);

  error = new Errors.BadRequestError(null);
  t.is('', error.message);

  // NoPermissionError
  error = new Errors.NoPermissionError('No Permission Error');
  t.is('No Permission Error', error.message);
  t.is(403, error.statusCode);

  error = new Errors.NoPermissionError('');
  t.is('', error.message);

  error = new Errors.NoPermissionError(null);
  t.is('', error.message);

  // UnauthorizedError
  error = new Errors.UnauthorizedError('Unauthorized Error');
  t.is('Unauthorized Error', error.message);
  t.is(401, error.statusCode);

  error = new Errors.UnauthorizedError('');
  t.is('', error.message);

  error = new Errors.UnauthorizedError(null);
  t.is('', error.message);

  // ValidationError
  error = new Errors.ValidationError('Validation Error');
  t.is('Validation Error', error.message);
  t.is(422, error.statusCode);

  error = new Errors.ValidationError('');
  t.is('', error.message);

  error = new Errors.ValidationError(null);
  t.is('', error.message);

  // RequestEntityTooLargeError
  error = new Errors.RequestEntityTooLargeError('Request Entity Too Large Error');
  t.is('Request Entity Too Large Error', error.message);
  t.is(413, error.statusCode);

  error = new Errors.RequestEntityTooLargeError('');
  t.is('', error.message);

  error = new Errors.RequestEntityTooLargeError(null);
  t.is('', error.message);

  // UnsupportedMediaTypeError
  error = new Errors.UnsupportedMediaTypeError('Unsupported Media Type Error');
  t.is('Unsupported Media Type Error', error.message);
  t.is(415, error.statusCode);

  error = new Errors.UnsupportedMediaTypeError('');
  t.is('', error.message);

  error = new Errors.UnsupportedMediaTypeError(null);
  t.is('', error.message);

  // EmailError
  error = new Errors.EmailError('Email Error');
  t.is('Email Error', error.message);
  t.is(500, error.statusCode);

  error = new Errors.EmailError('');
  t.is('', error.message);

  error = new Errors.EmailError(null);
  t.is('', error.message);

  // DataImportError
  error = new Errors.DataImportError('Data Import Error');
  t.is('Data Import Error', error.message);
  t.is(500, error.statusCode);

  error = new Errors.DataImportError('');
  t.is('', error.message);

  error = new Errors.DataImportError(null);
  t.is('', error.message);

  // MethodNotAllowedError
  error = new Errors.MethodNotAllowedError('Method Not Allowed Error');
  t.is('Method Not Allowed Error', error.message);
  t.is(405, error.statusCode);

  error = new Errors.MethodNotAllowedError('');
  t.is('', error.message);

  error = new Errors.MethodNotAllowedError(null);
  t.is('', error.message);

  // TooManyRequestsError
  error = new Errors.TooManyRequestsError('Too Many Requests Error');
  t.is('Too Many Requests Error', error.message);
  t.is(429, error.statusCode);

  error = new Errors.TooManyRequestsError('');
  t.is('', error.message);

  error = new Errors.TooManyRequestsError(null);
  t.is('', error.message);

  // TokenRevocationError
  error = new Errors.TokenRevocationError('Token Revocation Error');
  t.is('Token Revocation Error', error.message);
  t.is(503, error.statusCode);

  error = new Errors.TokenRevocationError('');
  t.is('', error.message);

  error = new Errors.TokenRevocationError(null);
  t.is('', error.message);

  // IncorrectUsage
  error = new Errors.IncorrectUsage('Incorrect Usage');
  t.is('Incorrect Usage', error.message);
  t.is(400, error.statusCode);

  error = new Errors.IncorrectUsage('');
  t.is('', error.message);

  error = new Errors.IncorrectUsage(null);
  t.is('', error.message);

  // Maintenance
  error = new Errors.Maintenance('Maintenance');
  t.is('Maintenance', error.message);
  t.is(503, error.statusCode);

  error = new Errors.Maintenance('');
  t.is('', error.message);

  error = new Errors.Maintenance(null);
  t.is('', error.message);
});
