# CANCILLERÍA - APOSTILLA

El sistema de la Apostilla tiene como principal objetivo automatizar la emisión de Apostillas para documentos públicos solicitados por personas bolivianas y extranjeras en territorio boliviano, además de la verificación en línea de apostillas desde entidades en el extranjero vía web.

# INTEROPERABILIDAD
Con el objetivo de la simplificación de tramites, el proyecto se integra consumiendo servicios web de otras instituciones como:
- FundEmpresa. Para recuperar información de datos de las matriculas de las empresas privadas
- Segip. Para la consulta y busqueda de personas Bolivianas. Especificamente constrastación de datos.

# TECNOLOGIAS USADAS
El presente repositorio publica servicios en formator REST con JSON y se integra para lo visual utilizando angular en el siguiente repositorio [https://gitlab.geo.gob.bo/agetic/cancilleria-apostilla-frontend](https://gitlab.geo.gob.bo/agetic/cancilleria-apostilla-frontend)

## Arquitectura DDD (Domain Driven Design)
Conjunto de patrones principos y práticas que nos permite resolver y entender los problemas del negocio (Dominio) en el diseño de sistemas orientados a objetos.

Consta de las siguientes capas:
### Infrastructure
Capa que soporta a las capas superiores en diferentes maneras, tales como: definicion de modelos, etc.
### Domain
Capa que representa el core de la logica de negocio, el corazon de la aplicacion.
### Application
Capa que coordina la logica del dominio.
Se subdivide en 3 subcapas:
- Capa Api-Rest
- Capa Graphql
- Capa Proxy server
### Presentation
Capa que interactua con el usuario y conecta con la capa de aplicacion a través de servicios web.
Se subdivide en 2 subcapas:
- Capa Administrador
- Capa Portal
### Common
Capa que contiene elementos utiles para las 3 primeas capas, tales como librerias.

## NodeJS
Node es un intérprete Javascript del lado del servidor. Su meta es permitir construir aplicaciones escalables y código que maneje miles de conexiones simultáneas.
 
## Javascript
JavaScript es un lenguaje interpretado línea a línea por el navegador.

## Postgresql
PostgreSQL es un sistema de base de datos objeto-relacional de código abierto.

## Graphql
Es un lenguaje de queries para definir qué datos queremos pedir a un API. Lo más interesante es que con GraphQL es el cliente, el Frontend, el que decide qué datos pedir y de qué forma al servidor, así no es necesario modificar el Backend, simplemente se cambia la query de GQL en el cliente.

## VueJS (para el frontend)
VueJS es Javascript, contiene un conjunto de librerías útiles para el desarrollo de aplicaciones web y propone una serie de patrones de diseño.

## OpenApi (para la documentación de servicios web)

La especificación OpenAPI (OAS) define una descripción estándar, interfaz de lenguaje de programación para API REST, que permite a humanos y computadoras descubrir y comprender las capacidades de un servicio sin requerir acceso a código fuente.

# INSTALAR LA APLICACION.

Para la instalacion del proyecto revise el archivo [INSTALL.md](INSTALL.md).

Para configurar Supervisor ver [SUPERVISOR.md](SUPERVISOR.md) 

