'use strict';

const debug = require('debug')('apostilla:log');

module.exports = function setupLog (logs) {
  function formatReference (error) {
    let text = [];

    if (error.fileName) {
      text.push(`Nombre del archivo: ${error.fileName}`);
    }

    if (error.columnNumber) {
      text.push(`Número de columna: ${error.columnNumber}`);
    }

    if (error.lineNumber) {
      text.push(`Número de línea: ${error.lineNumber}`);
    }

    if (error.stack) {
      text.push(error.stack);
    }

    if (text.length) {
      return text.join('\n');
    } else {
      return JSON.stringify(error);
    }
  }

  async function error (mensaje = 'Error desconocido', tipo = '', error, idUsuario) {
    const data = {
      nivel: 'ERROR',
      mensaje,
      tipo,
      referencia: formatReference(error),
      fecha: new Date()
    };

    try {
      await logs.createOrUpdate(data);
    } catch (e) {
      debug('ERROR LOG:', e.message);
      console.log('ERROR LOG:', e.message, e);
    }
  }

  async function warning (mensaje, tipo = '', referencia, idUsuario) {
    const data = {
      nivel: 'ADVERTENCIA',
      mensaje,
      tipo,
      referencia,
      fecha: new Date()
    };

    try {
      await logs.createOrUpdate(data);
    } catch (e) {
      debug('ADVERTENCIA LOG:', e.message);
      console.log('ADVERTENCIA LOG:', e.message);
    }
  }

  async function info (mensaje, tipo = '', referencia, idUsuario) {
    const data = {
      nivel: 'INFO',
      mensaje,
      tipo,
      referencia,
      fecha: new Date()
    };

    try {
      await logs.createOrUpdate(data);
    } catch (e) {
      debug('INFO LOG:', e.message);
      console.log('INFO LOG:', e.message);
    }
  }

  return {
    error,
    warning,
    info
  };
};
