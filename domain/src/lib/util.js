'use strict';

const fs = require('fs');
const path = require('path');
const crypto = require('crypto');
const Log = require('./log');
const Response = require('./response');
let log;
let res;

/**
 *
 * @param {array} elements: Array de elementos a eliminar de 'list'
 * @param {array} list: Array de elementos
 */
function removeAll (elements, list) {
  var ind;

  for (var i = 0, l = elements.length; i < l; i++) {
    while ((ind = list.indexOf(elements[i])) > -1) {
      list.splice(ind, 1);
    }
  }
}

function encrypt (password) {
  let shasum = crypto.createHash('sha256');
  shasum.update(password);
  return shasum.digest('hex');
}

/**
 * Cargando los repositorios en la carpeta especificada
 *
 * @param {string} PATH: Path del directorio de donde se cargará los modelos del sistema
 * @param {object} models: Objeto con todos los modelos de la bd
 * @param {object} res: objeto con respuestas predeterminadas
 * @param {object} opts: Json de configuración
 */
function loadServices (PATH, repositories, opts = {}) {
  if (!log) {
    log = Log(repositories.logs);
  }
  if (!res) {
    res = Response(log);
  }
  let files = fs.readdirSync(PATH);
  let services = {};

  if (opts.exclude) {
    removeAll(opts.exclude, files);
  }

  files.forEach(function (file) {
    let pathFile = path.join(PATH, file);
    if (fs.statSync(pathFile).isDirectory()) {
      services[file] = loadServices(pathFile, repositories, opts);
    } else {
      file = file.replace('.js', '');
      services[file] = require(pathFile)(repositories, res);
    }
  });

  return services;
}

function convertLinealObject (data) {
  let ob = {};
  for (let i in data) {
    for (let j in data[i]) {
      ob[j] = data[i][j];
    }
  }
  return ob;
}
/**
 * Genera string random segun el parametro Int m
 * @param {Int} m
 */
function randomStr (m) {
  let s = '';
  const r = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789';
  for (let i = 0; i < m; i++) {
    s += r.charAt(Math.floor(Math.random() * r.length));
  }
  return s;
}

function generateNroApostilla () {
  return Math.floor(Math.random() * 9999999999999) + 1000000000000;
}

// Código definidos en base a la libreria geoip-lite
const departamentos = {
  '01': 'CHUQUISACA',
  '02': 'COCHABAMBA',
  '03': 'BENI',
  '04': 'LA PAZ',
  '05': 'ORURO',
  '06': 'PANDO',
  '07': 'POTOSI',
  '08': 'SANTA CRUZ',
  '09': 'TARIJA'
};

function getDepartamento (geo) {
  if (geo && geo.country && geo.country === 'BO') {
    if (geo.region && departamentos[geo.region]) {
      if (['04', '05', '07'].indexOf(geo.region) !== -1) {
        return departamentos['04'];
      }
      if (['01', '02', '09'].indexOf(geo.region) !== -1) {
        return departamentos['02'];
      }
      if (['03', '06', '08'].indexOf(geo.region) !== -1) {
        return departamentos['08'];
      }
    }
  }
  return departamentos['04'];
}

function getDepartamentoReal (geo) {
  if (geo && geo.country && geo.country === 'BO') {
    if (geo.region && departamentos[geo.region]) {
        return departamentos[geo.region];
    }
  }
  return departamentos['04'];
}

module.exports = {
  loadServices,
  convertLinealObject,
  randomStr,
  generateNroApostilla,
  encrypt,
  getDepartamento,
  getDepartamentoReal
};
