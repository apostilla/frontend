'use strict';

const debug = require('debug')('apostilla:service:entidad');

module.exports = function entidadService (repositories, res) {
  const { entidades } = repositories;

  async function findAll (params = {}, idRol, idEntidad) {
    debug('Lista de entidades|filtros');

    let lista;
    try {
      // id -> rol nombre

      // 1 -> SUPERADMIN
      // 2 -> ADMIN CANCILLERIA
      // 3 -> REVISOR CANCILLERIA
      // 4 -> FIRMADOR CANCILLERIA

      // 5 -> ADMIN ENTIDAD
      // 6 -> REVISOR ENTIDAD
      // 7 -> FIRMADOR ENTIDAD

      // 8 -> SOLICITANTE

      switch (idRol) {
        case 5: // rol: ADMIN ENTIDAD
          params['idEntidad'] = idEntidad;
          break;
      }

      lista = await entidades.findAll(params);
    } catch (e) {
      return res.error(e);
    }

    if (!lista) {
      return res.error(new Error(`Error al obtener la lista de entidades`));
    }

    return res.success(lista);
  }

  async function findById (id) {
    debug('Buscando entidad por ID');

    let entidad;
    try {
      entidad = await entidades.findById(id);
    } catch (e) {
      return res.error(e);
    }

    if (!entidad) {
      return res.error(new Error(`Entidad ${id} not found`));
    }

    return res.success(entidad);
  }

  async function createOrUpdate (data) {
    debug('Crear o actualizar entidad');

    let entidad;
    try {
      entidad = await entidades.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    if (!entidad) {
      return res.error(new Error(`El entidad no pudo ser creado`));
    }

    return res.success(entidad);
  }

  async function deleteItem (id) {
    debug('Eliminando entidad');

    let deleted;
    try {
      deleted = await entidades.deleteItem(id);
    } catch (e) {
      return res.error(e);
    }

    if (deleted === -1) {
      return res.error(new Error(`No existe la entidad`));
    }

    if (deleted === 0) {
      return res.error(new Error(`La entidad ya fue eliminada`));
    }

    return res.success(deleted > 0);
  }
  async function obtenerIdEntidad (codigoPortal) {
    debug(' Obteniendo el id_entidad via codigo de portal');
    let entidad;
    try {
      entidad = await entidades.obtenerIdEntidad(`${codigoPortal}`);
      if(!entidad) throw Error('No existe la entidad');
      return res.success(entidad);
    } catch (e) {
      return res.error(e);
    }
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem,
    obtenerIdEntidad
  };
};
