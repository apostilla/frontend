'use strict';

const debug = require('debug')('apostilla:service:firma');

module.exports = function firmaService (repositories, res) {
  const { firmas } = repositories;

  async function findAll (params = {}) {
    debug('Lista de firmas|filtros');

    let lista;
    try {
      lista = await firmas.findAll(params);
    } catch (e) {
      return res.error(e);
    }

    if (!lista) {
      return res.error(new Error(`Error al obtener la lista de firmas`));
    }

    return res.success(lista);
  }

  async function findById (id) {
    debug('Buscando firma por ID');

    let firma;
    try {
      firma = await firmas.findById(id);
    } catch (e) {
      return res.error(e);
    }

    if (!firma) {
      return res.error(new Error(`Firma ${id} not found`));
    }

    return res.success(firma);
  }

  async function findByArchivo (id) {
    debug('Buscando firma por id archivo');

    let firma;
    try {
      firma = await firmas.findByArchivoId(id);
    } catch (e) {
      return res.error(e);
    }

    if (!firma) {
      return res.error(new Error(`Archivo con id ${id} not found`));
    }

    return res.success(firma);
  }

  async function findByArchivoCancilleria (id) {
    debug('Buscando firma por id archivo');

    let firma;
    try {
      firma = await firmas.findByArchivoIdCancilleria(id);
    } catch (e) {
      return res.error(e);
    }

    if (!firma) {
      return res.error(new Error(`Archivo con id ${id} not found`));
    }

    return res.success(firma);
  }


  async function createOrUpdate (data) {
    debug('Crear o actualizar firma');

    let firma;
    try {
      firma = await firmas.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    if (!firma) {
      return res.error(new Error(`El firma no pudo ser creado`));
    }

    return res.success(firma);
  }

  async function deleteItem (id) {
    debug('Eliminando firma');

    let deleted;
    try {
      deleted = await firmas.deleteItem(id);
    } catch (e) {
      return res.error(e);
    }

    if (deleted === -1) {
      return res.error(new Error(`No existe la firma`));
    }

    if (deleted === 0) {
      return res.error(new Error(`La firma ya fue eliminada`));
    }

    return res.success(deleted > 0);
  }

  return {
    findAll,
    findById,
    findByArchivo,
    findByArchivoCancilleria,
    createOrUpdate,
    deleteItem
  };
};
