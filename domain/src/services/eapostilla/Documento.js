'use strict';

const debug = require('debug')('apostilla:service:documento');

module.exports = function documentoService (repositories, res) {
  const { documentos } = repositories;

  async function findAll (params = {}, idRol, idEntidad) {
    debug('Lista de documentos|filtros');

    let lista;
    try {
      // Filtrando por entidades SUPERADMIN | ADMIN CANCILLERIA | SOLICITANTE
      if ([5, 6, 7].indexOf(idRol) !== -1) {
        params.id_entidad = idEntidad;
      }

      lista = await documentos.findAll(params);
    } catch (e) {
      return res.error(e);
    }

    if (!lista) {
      return res.error(new Error(`Error al obtener la lista de documentos`));
    }

    return res.success(lista);
  }

  async function findById (id) {
    debug('Buscando documento por ID');

    let documento;
    try {
      documento = await documentos.findById(id);
    } catch (e) {
      return res.error(e);
    }

    if (!documento) {
      return res.error(new Error(`Documento ${id} not found`));
    }

    return res.success(documento);
  }

  async function createOrUpdate (data) {
    debug('Crear o actualizar documento');

    let documento;
    try {
      documento = await documentos.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    if (!documento) {
      return res.error(new Error(`El documento no pudo ser creado`));
    }

    return res.success(documento);
  }

  async function deleteItem (id) {
    debug('Eliminando documento');

    let deleted;
    try {
      deleted = await documentos.deleteItem(id);
    } catch (e) {
      return res.error(e);
    }

    if (deleted === -1) {
      return res.error(new Error(`No existe el documento`));
    }

    if (deleted === 0) {
      return res.error(new Error(`El documento ya fue eliminado`));
    }

    return res.success(deleted > 0);
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
