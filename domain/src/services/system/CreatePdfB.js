'use strict';

const debug = require('debug')('apostilla:service:segip');
const path = require('path');
const fs = require('fs');
const PDFDOC = require('pdfkit');

module.exports = function segipServiceB (repositories, res) {
  async function createApostillaPDfB (myObj, output) {
    return new Promise(async (resolve, reject) => {
      const {parametros} = repositories;
      let myPdf = new PDFDOC();

      let urlPortal;
      let url;
      try {
        urlPortal = await parametros.findByName('URL_PORTAL');
        url = urlPortal.valor;
      } catch (e) {
        return res.error(e);
      }

      if (output === undefined) {
        output = path.join(__dirname.replace('domain/src/services/system', 'common/public'), 'output.pdf');
      }

      let logo1 = path.join(__dirname.replace('domain/src/services/system', 'common/public/images'), 'logo-escudo.png');
      let logo2 = path.join(__dirname.replace('domain/src/services/system', 'common/public/images'), 'fondo-escudo.png');
      let logo3 = path.join(__dirname.replace('domain/src/services/system', 'common/public/images'), 'tejido.png');

      let cambio = 4;

      myPdf.image(logo1, 220, 31, {width: 164});
      myPdf.image(logo2, 148, 316, {width: 311 , height: 260});
      myPdf.image(logo3, 0, 752, {width: 612});

      myPdf.fontSize(8).text('Esta Apostilla certifica únicamente la autenticidad de la firma, la calidad en que el signatario del documento haya actuado y, en su caso, la identidad del sello o timbre del que el documento público esté revestido', 65, 184, {
        align: 'justify',
        lineBreak: true,
        width: 482
      });
      myPdf.fontSize(8).text('(Esta Apostilla no certifica el contenido del documento para el cual se expidió)', 65, 205, {
        align: 'justify',
        lineBreak: true,
        width: 482
      });
      myPdf.fontSize(8).text('[Esta Apostilla se puede verificar en la dirección siguiente: ' + url + '.]', 65, 216, {
        align: 'justify',
        lineBreak: true,
        width: 482
      });

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 13)
        .text('Apostille', 65, 245 - cambio, {
          align: 'center',
          width: 482
        });

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('(Convention de La Haye du 5 octobre de 1961)', 65, 260 - cambio, {
          align: 'center',
          width: 482
        });

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('1. País:', 96, 287 - cambio);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('Country/Pays:', 96, 297 - cambio)
        .text(`${myObj.pais}`, 290, 292 - cambio);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('El presente documento público', 65, 320 - cambio, {
          align: 'center',
          width: 482
        });
      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('This public document / Le présent acte public', 65, 330 - cambio, {
          align: 'center',
          width: 482
        });

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('2. ha sido firmado por', 96, 353 - cambio);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('has been signed by', 96, 365 - cambio)
        .text('a été signé par', 96, 377 - cambio)
        .text(`${myObj.firmadoPor}`, 320, 365 - cambio);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('3. quien actúa en calidad de', 96, 400 - cambio);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('acting in the capacity of', 96, 412 - cambio);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('agissant en qualité de', 96, 424 - cambio)
        .text(`${myObj.cargo}`, 320, 417 - cambio);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('4. y está revestido del sello / timbre de ', 96, 447 - cambio);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('bears the seal / stamp of', 96, 459 - cambio);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('est revêtu du sceau / timbre de', 96, 471 - cambio)
        .text(`${myObj.selloDe?myObj.selloDe:''}`, 320, 459 - cambio);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('Certificado', 65, 494 - cambio, {
          align: 'center',
          width: 482
        });

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('Certified / Attesté', 65, 506 - cambio, {
          align: 'center',
          width: 482
        });

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('5. en', 96, 529 - cambio);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('at/à', 96, 541 - cambio)
        .text(`${myObj.lugar}`, 210, 535 - cambio);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('6. el día', 314, 529 - cambio);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('the/le', 314, 541 - cambio)
        .text(`${myObj.fecha}`, 420, 535 - cambio);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('7. por', 96, 564 - cambio);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('by/ par', 96, 576 - cambio)
        .text(`${myObj.por}`, 300, 570 - cambio);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('8. bajo el número', 96, 599 - cambio);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('N.º / sous n°', 96, 611 - cambio)
        .text(`${myObj.nroApostilla}`, 300, 611 - cambio);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('9. sello / Timbre:', 96, 640 - cambio);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('Seal/ stamp:', 96, 652 - cambio)
        .text(`${myObj.sello?myObj.sello:''}`, 200, 651 - cambio, {align: 'center', width: 100});

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('10. Firma:', 96, 677 - cambio);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('Signature:', 96, 689 - cambio);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text(`${myObj.firma}`, 210, 683 - cambio);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('CÓDIGO DE SEGURIDAD ALFANUMÉRICO: ', 96, 710 - cambio);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text(`${myObj.cod_seguridad}`, 320, 710 - cambio);

      myPdf.lineCap('butt')
        .fill('#4183C4')
        .moveTo(65, 240)
        .lineTo(547, 240)
        .stroke();// linea horizontal

      myPdf.lineCap('butt')
        .fill('#4183C4')
        .moveTo(65, 745)
        .lineTo(547, 745)
        .stroke();// linea horizontal

      myPdf.lineCap('butt')
        .fill('#4183C4')
        .moveTo(65, 240)
        .lineTo(65, 745)
        .stroke();// linea vertical

      myPdf.lineCap('butt')
        .fill('#4183C4')
        .moveTo(547, 240)
        .lineTo(547, 745)
        .stroke();// linea vertical

      const escribir = fs.createWriteStream(output);
      myPdf.pipe(escribir);
      myPdf.end();
      escribir.on('finish', () => resolve(true));
    });
  }

  async function apostillaDocB (datosApostilla, outputPath) {
    debug('Generacion de Apostilla en PDF');
    let item;
    try {
      item = await createApostillaPDfB(datosApostilla, outputPath);
    } catch (e) {
      return res.error(e);
    }
    if (!item) {
      return res.error(new Error(`Error al crear documento de Apostilla`));
    }
    return res.success(item);
  }

  return {
    apostillaDocB
  };
};
