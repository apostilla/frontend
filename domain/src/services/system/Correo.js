'use strict';

const debug = require('debug')('apostilla:service:envio_correo');
const { texto } = require('common');

module.exports = function fundempresaService (repositories, res) {
  const { parametros } = repositories;
  const { correo } = require('common');

  function enviarFunction (datosEmail) {
    return new Promise((resolve, reject) => {
      correo.enviar({
        para: datosEmail.para,
        titulo: datosEmail.titulo,
        mensaje: datosEmail.mensaje,
        html: datosEmail.html,
        attachments: datosEmail.attachments
      })
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
    });
  }

  async function enviar (data, tipoTemplate = 'TEMPLATE_CORREO_BASE') {
    debug('Enviando email');
    let emailPlantilla = await parametros.findByName(tipoTemplate);
    data.html = emailPlantilla.valor;
    data.html = texto.nano(data.html, { mensaje: data.mensaje, year: new Date().getFullYear() });
    let email;
    try {
      email = await enviarFunction(data);
    } catch (e) {
      return res.error(e);
    }
    if (!email) {
      return res.error(new Error(`Error al enviar email`));
    }

    return res.success(email);
  }
  return {
    enviar
  };
};
