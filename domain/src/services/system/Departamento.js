'use strict';

const debug = require('debug')('apostilla:service:departamento');

module.exports = function userService (repositories, res) {
  const { departamentos } = repositories;

  async function findAll (params = {}) {
    debug('Lista de departamentos|filtros');

    let lista;
    try {
      lista = await departamentos.findAll(params);
    } catch (e) {
      return res.error(e);
    }

    if (!lista) {
      return res.error(new Error(`Error al obtener la lista de departamentos`));
    }

    return res.success(lista);
  }

  async function findById (id) {
    debug('Buscando departamento por ID');

    let user;
    try {
      user = await departamentos.findById(id);
    } catch (e) {
      return res.error(e);
    }

    if (!user) {
      return res.error(new Error(`departamento ${id} not found`));
    }

    return res.success(user);
  }

  async function createOrUpdate (data) {
    debug('Crear o actualizar departamento');

    let user;
    try {
      user = await departamentos.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    if (!user) {
      return res.error(new Error(`El departamento no pudo ser creado`));
    }

    return res.success(user);
  }

  async function deleteItem (id) {
    debug('Eliminando departamento');

    let deleted;
    try {
      deleted = await departamentos.deleteItem(id);
    } catch (e) {
      return res.error(e);
    }

    if (deleted === -1) {
      return res.error(new Error(`No existe el departamento`));
    }

    if (deleted === 0) {
      return res.error(new Error(`El departamento ya fue eliminado`));
    }

    return res.success(deleted > 0);
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
