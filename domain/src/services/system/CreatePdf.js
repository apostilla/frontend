'use strict';

const debug = require('debug')('apostilla:service:segip');
const path = require('path');
const fs = require('fs');
const PDFDOC = require('pdfkit');

module.exports = function segipService (repositories, res) {
  async function createApostillaPDf (myObj, output) {
    return new Promise(async (resolve, reject) => {
      const {parametros} = repositories;
      let myPdf = new PDFDOC();

      let urlPortal;
      let url;
      try {
        urlPortal = await parametros.findByName('URL_PORTAL');
        url = urlPortal.valor;
      } catch (e) {
        return res.error(e);
      }

      let logo1 = path.join(__dirname.replace('domain/src/services/system', 'common/public/images'), 'logo_apostilla_2.png');
      let logo2 = path.join(__dirname.replace('domain/src/services/system', 'common/public/images'), 'logo_rree.png');
      let logo3 = path.join(__dirname.replace('domain/src/services/system', 'common/public/images'), 'fondo.png');

      if (output === undefined) {
        output = path.join(__dirname.replace('domain/src/services/system', 'common/public'), 'output.pdf');
      }

      /* myPdf.rect(120, 131, 409, 41)
        .fill('#ececec'); // relleno 1

      myPdf.rect(120, 172, 105, 28)
        .fill('#f5f5f5'); // relleno 2

      myPdf.rect(120, 200, 409, 28)
        .fill('#f5f5f5'); // relleno 3

      myPdf.rect(120, 228, 214, 19)
        .fill('#f5f5f5'); // relleno 4

      myPdf.rect(120, 339, 409, 34)
        .fill('#ececec'); // relleno 7

      myPdf.rect(120, 373, 105, 130)
        .fill('#f5f5f5'); // relleno 8

      myPdf.rect(316, 373, 54, 28)
        .fill('#f5f5f5'); // relleno 9

      myPdf.rect(316, 466, 94, 37)
        .fill('#f5f5f5'); // relleno 9 */

      myPdf.image(logo2, 104, 27, {width: 150});
      myPdf.image(logo1, 360, 32, {width: 150});
      myPdf.image(logo3, 121, 160, {width: 370});

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 13)
        .text('APOSTILLE', 275, 125);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('(Convention de La Haye du 5 octobre de 1961)', 207, 141);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('1. País:', 109, 165);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('Country/Pays:', 121, 175)
        .text(`${myObj.pais}`, 300, 170);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('El presente documento público', 121, 190);
      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('This public document / Le présent acte public', 121, 203);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('2. ha sido firmado por', 109, 217);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('has been signed by', 121, 227)
        .text('a été signé par', 121, 240)
        .text(`${myObj.firmadoPor}`, 350, 225);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('3. quien actúa en calidad de', 109, 254);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('acting in the capacity of', 121, 264);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('agissant en qualité de', 121, 275)
        .text(`${myObj.cargo}`, 350, 264);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('4. y está revestido del sello / timbre de ', 109, 290);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('bears the seal / stamp of', 121, 300);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('est revêtu du sceau / timbre de', 121, 310)
        .text(`${myObj.selloDe?myObj.selloDe:''}`, 350, 302);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('Certificado', 280, 330);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('Certified / Attesté', 275, 342);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('5. en', 109, 365);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('at/à', 121, 375)
        .text(`${myObj.lugar}`, 240, 370);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('6. el día', 324, 365);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('the/le', 335, 375)
        .text(`${myObj.fecha}`, 430, 370);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('7. por', 109, 386);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('by/ par', 121, 402)
        .text(`${myObj.por}`, 330, 400);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('8. bajo el número', 109, 420);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('N.º', 121, 432)
        .text('sous n°', 121, 440)
        .text(`${myObj.nroApostilla}`, 320, 430);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('9. sello / Timbre:', 109, 455);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('Seal/ stamp:', 121, 467)
        .text('Sceau / timbre:', 121, 475)
        .text(`${myObj.sello?myObj.sello:''}`, 210, 465, {align: 'center', width: 100});

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('CÓDIGO DE SEGURIDAD: ', 120, 500)

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text(`${myObj.cod_seguridad}`, 240, 500);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('10. Firma:', 324, 455);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text('Signature:', 341, 467)
        .text('Signature:', 341, 475);

      myPdf.fontSize(9)
        .font(path.join(__dirname.replace('domain/src/services/system', 'common/public/fonts'), 'VERDANA.TTF'), 9)
        .text(`${myObj.firma}`, 430, 462);

      myPdf.fontSize(8).text('Esta Apostilla certifica únicamente la autenticidad de la firma, la calidad en que el signatario del documento haya actuado y, en su caso, la identidad del sello o timbre del que el documento público esté revestido', 120, 520, {
        align: 'center',
        lineBreak: true,
        width: 402
      });
      myPdf.fontSize(8).text('(Esta Apostilla no certifica el contenido del documento para el cual se expidió)', 120, 550, {
        align: 'center',
        lineBreak: true,
        width: 402
      });

      myPdf.fontSize(8).text('[Esta Apostilla se puede verificar en la dirección siguiente: ' + url + '.]', 120, 570, {
        align: 'center',
        lineBreak: true,
        width: 402
      });

      myPdf.fontSize(8).text('This Apostille only certifies the authenticity of the signature and the capacity of the person who has signed the public document, and, when appropriate, the identity of the seal or stamp which the public document bears.', 120, 595, {
        align: 'center',
        lineBreak: true,
        width: 402
      });
      myPdf.fontSize(8).text('This Apostille does not certify the content of the document for which it was issued.', 120, 623, {
        align: 'center',
        lineBreak: true,
        width: 402
      });

      myPdf.fontSize(8).text('To verify the issuance of this Apostille, see ' + url + '.]', 120, 640, {
        align: 'center',
        lineBreak: true,
        width: 402
      });

      myPdf.fontSize(8).text('Cette Apostille atteste uniquement la véracité de la signature, la qualité en laquelle le signataire de l\'acte a agi et, le cas échéant, l\'identité du sceau ou timbre dont cet acte public est revêtu.', 120, 665, {
        align: 'center',
        lineBreak: true,
        width: 402
      });
      myPdf.fontSize(8).text('Cette Apostille ne certifie pas le contenu de l\'acte pour lequel elle a été émise.', 120, 685, {
        align: 'center',
        lineBreak: true,
        width: 402
      });

      myPdf.fontSize(8).text('[Cette Apostille peut être vérifiée à l’adresse suivante : ' + url + '.]', 120, 695, {
        align: 'center',
        lineBreak: true,
        width: 402
      });

      myPdf.lineCap('butt')
        .fill('#4183C4')
        .moveTo(120, 653)
        .lineTo(522, 653)
        .stroke();// linea horizontal

      myPdf.lineCap('butt')
        .fill('#4183C4')
        .moveTo(120, 583)
        .lineTo(522, 583)
        .stroke();// linea horizontal

      const escribir = fs.createWriteStream(output);
      myPdf.pipe(escribir);
      myPdf.end();
      escribir.on('finish', () => resolve(true));
    });
  }

  async function apostillaDoc (datosApostilla, outputPath) {
    debug('Generacion de Apostilla en PDF');
    let item;
    try {
      item = await createApostillaPDf(datosApostilla, outputPath);
    } catch (e) {
      return res.error(e);
    }
    if (!item) {
      return res.error(new Error(`Error al crear documento de Apostilla`));
    }
    return res.success(item);
  }

  return {
    apostillaDoc
  };
};
