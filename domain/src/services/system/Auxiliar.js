'use strict';

const debug = require('debug')('apostilla:service:auxiliar');
const { encrypt } = require('../../lib/util');
module.exports = function userService (repositories, res) {
  const { auxiliares } = repositories;

  async function findAll (params = {}, idRol, idEntidad) {
    debug('Lista de auxiliares|filtros');
    let lista;
    try {
      params['id_entidad'] = idEntidad;

      switch (idRol) {
        case 1: // ROL: SUPERADMIN
          params['listado_id_roles'] = [1, 2, 3, 4, 5, 6, 7, 8];
          params['id_entidad'] = null; // Lista de todas las entidades
          break;
        case 2: // ROL: ADMIN CANCILLERIA
          params['listado_id_roles'] = [2, 3, 4, 5];
          params['id_entidad'] = null; // Lista de todas las entidades
          break;
        case 5: // ROL: ADMIN ENTIDAD
          params['listado_id_roles'] = [5, 6, 7];
          break;
        default: // ROL: REVISOR CANCILLERIA, FIRMADOR CANCILLERIA, REVISOR ENTIDAD, FIRMADOR ENTIDAD, SOLICITANTE
          params['listado_id_roles'] = [];
      }

      lista = await auxiliares.findAll(params);
    } catch (e) {
      return res.error(e);
    }

    if (!lista) {
      return res.error(new Error(`Error al obtener la lista de auxiliares`));
    }

    return res.success(lista);
  }

  async function findById (id) {
    debug('Buscando auxiliar por ID');

    let user;
    try {
      user = await auxiliares.findById(id);
    } catch (e) {
      return res.error(e);
    }

    if (!user) {
      return res.error(new Error(`Auxiliar ${id} not found`));
    }

    return res.success(user);
  }

  async function createOrUpdate (data) {
    debug('Crear o actualizar auxiliar');

    let user;
    try {
      user = await auxiliares.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    if (!user) {
      return res.error(new Error(`El auxiliar no pudo ser creado`));
    }

    return res.success(user);
  }

  async function deleteItem (id) {
    debug('Eliminando auxiliar');

    let deleted;
    try {
      deleted = await auxiliares.deleteItem(id);
    } catch (e) {
      return res.error(e);
    }

    if (deleted === -1) {
      return res.error(new Error(`No existe el usuario`));
    }

    if (deleted === 0) {
      return res.error(new Error(`El auxiliar ya fue eliminado`));
    }

    return res.success(deleted > 0);
  }

  async function userExist (usuario, contrasena) {
    debug('Lista de auxiliar existente');

    let user;
    try {
      user = await auxiliares.findByUsername(usuario);
    } catch (e) {
      return res.error(e);
    }

    if (!user) {
      return res.error(new Error(`No existe el usuario`));
    }

    if (user.contrasena !== encrypt(contrasena)) {
      return res.error(new Error(`La contraseña es incorrecta`));
    }
    if (user.estado === 'INACTIVO') {
      return res.error(new Error(`El auxiliar se encuentra deshabilitado. Consulte con el administrador del sistema.`));
    }    
    return res.success(user);
  }

  async function getUser (usuario, include = true) {
    debug('Buscando auxiliar por ID');

    let user;
    try {
      user = await auxiliares.findByUsername(usuario, include);
    } catch (e) {
      return res.error(e);
    }

    if (!user) {
      return res.error(new Error(`Auxiliar ${usuario} not found`));
    }

    return res.success(user);
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem,
    userExist,
    getUser
  };
}
;
