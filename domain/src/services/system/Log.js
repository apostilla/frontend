'use strict';

const debug = require('debug')('apostilla:service:log');

module.exports = function rolService (repositories, res) {
  const { logs } = repositories;

  async function findAll (params = {}) {
    debug('Lista de logs|filtros');

    let lista;
    try {
      lista = await logs.findAll(params);
    } catch (e) {
      return res.error(e);
    }

    if (!lista) {
      return res.error(new Error(`Error al obtener la lista de logs`));
    }

    return res.success(lista);
  }

  async function findById (id) {
    debug('Buscando rol por ID');

    let rol;
    try {
      rol = await logs.findById(id);
    } catch (e) {
      return res.error(e);
    }

    if (!rol) {
      return res.error(new Error(`Rol ${id} not found`));
    }

    return res.success(rol);
  }

  async function createOrUpdate (data) {
    debug('Crear o actualizar rol');

    let rol;
    try {
      rol = await logs.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    if (!rol) {
      return res.error(new Error(`El rol no pudo ser creado`));
    }

    return res.success(rol);
  }

  async function deleteItem (id) {
    debug('Eliminando rol');

    let deleted;
    try {
      deleted = await logs.deleteItem(id);
    } catch (e) {
      return res.error(e);
    }

    if (!deleted) {
      return res.error(new Error(`No se puede eliminar el log`));
    }

    return res.success(deleted);
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
