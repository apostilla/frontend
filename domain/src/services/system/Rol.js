'use strict';

const debug = require('debug')('apostilla:service:rol');

module.exports = function rolService (repositories, res) {
  const { roles } = repositories;

  async function findAll (params = {}, rol) {
    debug('Lista de roles|filtros');

    let lista;
    try {
      // id -> rol nombre

      // 1 -> SUPERADMIN
      // 2 -> ADMIN CANCILLERIA
      // 3 -> REVISOR CANCILLERIA
      // 4 -> FIRMADOR CANCILLERIA

      // 5 -> ADMIN ENTIDAD
      // 6 -> REVISOR ENTIDAD
      // 7 -> FIRMADOR ENTIDAD

      // 8 -> SOLICITANTE

      switch (rol) {
        case 1: // rol: SUPERADMIN
          params['listado_id_roles'] = [1, 2, 3, 4, 5, 6, 7, 8];
          break;
        case 2: // rol: ADMIN  CANCILLERIA
          params['listado_id_roles'] = [2, 3, 4, 5];
          break;
        case 3: // rol: REVISOR   CANCILLERIA
          params['listado_id_roles'] = [];
          break;
        case 4: // rol: FIRMADOR CANCILLERIA
          params['listado_id_roles'] = [];
          break;
        case 5: // rol: ADMIN  ENTIDAD
          params['listado_id_roles'] = [5, 6, 7];
          break;
        case 6: // rol: REVISOR ENTIDAD
          params['listado_id_roles'] = [];
          break;
        case 7: // rol: FIRMADOR ENTIDAD
          params['listado_id_roles'] = [];
          break;
        case 8: // rol: SOLICITANTE
          params['listado_id_roles'] = [];
          break;
      }

      lista = await roles.findAll(params);
    } catch (e) {
      return res.error(e);
    }

    if (!lista) {
      return res.error(new Error(`Error al obtener la lista de roles`));
    }

    return res.success(lista);
  }

  async function findById (id) {
    debug('Buscando rol por ID');

    let rol;
    try {
      rol = await roles.findById(id);
    } catch (e) {
      return res.error(e);
    }

    if (!rol) {
      return res.error(new Error(`Rol ${id} not found`));
    }

    return res.success(rol);
  }

  async function createOrUpdate (data) {
    debug('Crear o actualizar rol');

    let rol;
    try {
      rol = await roles.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    if (!rol) {
      return res.error(new Error(`El rol no pudo ser creado`));
    }

    return res.success(rol);
  }

  async function deleteItem (id) {
    debug('Eliminando rol');

    let deleted;
    try {
      deleted = await roles.deleteItem(id);
    } catch (e) {
      return res.error(e);
    }

    if (deleted === -1) {
      return res.error(new Error(`No existe el rol`));
    }

    if (deleted === 0) {
      return res.error(new Error(`El rol ya fue eliminado`));
    }

    return res.success(deleted > 0);
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
