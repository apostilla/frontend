'use strict';

const debug = require('debug')('apostilla:service:parametro');

module.exports = function parametroService (repositories, res) {
  const { parametros } = repositories;

  async function findAll (params = {}, idRol) {
    debug('Lista de parametros|filtros');

    let lista;
    try {
      if (idRol === 2) {
        params.parametros = [
          'EMAIL_ORIGEN',
          'EMAIL_HOST',
          'EMAIL_PORT',
          'COSTO_APOSTILLA',
          'MAX_FILESIZE'
        ];
      }
      lista = await parametros.findAll(params);
    } catch (e) {
      return res.error(e);
    }

    if (!lista) {
      return res.error(new Error(`Error al obtener la lista de parametros`));
    }

    return res.success(lista);
  }

  async function findById (id) {
    debug('Buscando parametro por ID');

    let parametro;
    try {
      parametro = await parametros.findById(id);
    } catch (e) {
      return res.error(e);
    }

    if (!parametro) {
      return res.error(new Error(`Parametro ${id} not found`));
    }

    return res.success(parametro);
  }

  async function getParameter (nombre) {
    debug('Buscando parámetro por nombre');

    let parametro;
    try {
      parametro = await parametros.findByName(nombre);
    } catch (e) {
      return res.error(e);
    }

    if (!parametro) {
      return res.error(new Error(`Parametro ${nombre} not found`));
    }

    return res.success(parametro);
  }

  async function createOrUpdate (data) {
    debug('Crear o actualizar parametro');

    let parametro;
    try {
      parametro = await parametros.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    if (!parametro) {
      return res.error(new Error(`El parametro no pudo ser creado`));
    }

    return res.success(parametro);
  }

  async function deleteItem (id) {
    debug('Eliminando parametro');

    let deleted;
    try {
      deleted = await parametros.deleteItem(id);
    } catch (e) {
      return res.error(e);
    }

    if (deleted === -1) {
      return res.error(new Error(`No existe el parámetro`));
    }

    if (deleted === 0) {
      return res.error(new Error(`El parámetro ya fue eliminado`));
    }

    return res.success(deleted > 0);
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem,
    getParameter
  };
};
