'use strict';

const debug = require('debug')('apostilla:service:municipio');

module.exports = function userService (repositories, res) {
  const { municipios } = repositories;

  async function findAll (params = {}) {
    debug('Lista de municipios|filtros');

    let lista;
    try {
      lista = await municipios.findAll(params);
    } catch (e) {
      return res.error(e);
    }

    if (!lista) {
      return res.error(new Error(`Error al obtener la lista de municipios`));
    }

    return res.success(lista);
  }

  async function findById (id) {
    debug('Buscando municipio por ID');

    let user;
    try {
      user = await municipios.findById(id);
    } catch (e) {
      return res.error(e);
    }

    if (!user) {
      return res.error(new Error(`municipio ${id} not found`));
    }

    return res.success(user);
  }

  async function createOrUpdate (data) {
    debug('Crear o actualizar municipio');

    let user;
    try {
      user = await municipios.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    if (!user) {
      return res.error(new Error(`El municipio no pudo ser creado`));
    }

    return res.success(user);
  }

  async function deleteItem (id) {
    debug('Eliminando municipio');

    let deleted;
    try {
      deleted = await municipios.deleteItem(id);
    } catch (e) {
      return res.error(e);
    }

    if (deleted === -1) {
      return res.error(new Error(`No existe el municipio`));
    }

    if (deleted === 0) {
      return res.error(new Error(`El municipio ya fue eliminado`));
    }

    return res.success(deleted > 0);
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
