'use strict';

const debug = require('debug')('apostilla:service:notificacion');

module.exports = function notificacionService (repositories, res) {
  const { notificaciones } = repositories;

  async function findAll (params = {}) {
    debug('Lista de notificaciones|filtros');

    let lista;
    try {
      lista = await notificaciones.findAll(params);
    } catch (e) {
      return res.error(e);
    }

    if (!lista) {
      return res.error(new Error(`Error al obtener la lista de notificaciones`));
    }

    return res.success(lista);
  }

  async function findById (id) {
    debug('Buscando notificacion por ID');

    let notificacion;
    try {
      notificacion = await notificaciones.findById(id);
    } catch (e) {
      return res.error(e);
    }

    if (!notificacion) {
      return res.error(new Error(`Notificacion ${id} not found`));
    }

    return res.success(notificacion);
  }

  async function createOrUpdate (data) {
    debug('Crear o actualizar notificacion');

    let notificacion;
    try {
      notificacion = await notificaciones.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    if (!notificacion) {
      return res.error(new Error(`El notificacion no pudo ser creado`));
    }

    return res.success(notificacion);
  }

  async function deleteItem (id) {
    debug('Eliminando notificacion');

    let deleted;
    try {
      deleted = await notificaciones.deleteItem(id);
    } catch (e) {
      return res.error(e);
    }

    if (deleted === -1) {
      return res.error(new Error(`No existe la notificación`));
    }

    if (deleted === 0) {
      return res.error(new Error(`La notificación ya fue eliminada`));
    }

    return res.success(deleted > 0);
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
