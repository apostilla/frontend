'use strict';

const debug = require('debug')('apostilla:service:provincia');

module.exports = function userService (repositories, res) {
  const { provincias } = repositories;

  async function findAll (params = {}) {
    debug('Lista de provincias|filtros');

    let lista;
    try {
      lista = await provincias.findAll(params);
    } catch (e) {
      return res.error(e);
    }

    if (!lista) {
      return res.error(new Error(`Error al obtener la lista de provincias`));
    }

    return res.success(lista);
  }

  async function findById (id) {
    debug('Buscando provincia por ID');

    let user;
    try {
      user = await provincias.findById(id);
    } catch (e) {
      return res.error(e);
    }

    if (!user) {
      return res.error(new Error(`provincia ${id} not found`));
    }

    return res.success(user);
  }

  async function createOrUpdate (data) {
    debug('Crear o actualizar provincia');

    let user;
    try {
      user = await provincias.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    if (!user) {
      return res.error(new Error(`El provincia no pudo ser creado`));
    }

    return res.success(user);
  }

  async function deleteItem (id) {
    debug('Eliminando provincia');

    let deleted;
    try {
      deleted = await provincias.deleteItem(id);
    } catch (e) {
      return res.error(e);
    }

    if (deleted === -1) {
      return res.error(new Error(`No existe la provincia`));
    }

    if (deleted === 0) {
      return res.error(new Error(`La provincia ya fue eliminada`));
    }

    return res.success(deleted > 0);
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
