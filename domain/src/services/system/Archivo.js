'use strict';

const debug = require('debug')('apostilla:service:archivo');

module.exports = function archivoService (repositories, res) {
  const { archivos } = repositories;

  async function findAll (params = {}) {
    debug('Lista de archivos|filtros');

    let lista;
    try {
      lista = await archivos.findAll(params);
    } catch (e) {
      return res.error(e);
    }

    if (!lista) {
      return res.error(new Error(`Error al obtener la lista de archivos`));
    }

    return res.success(lista);
  }

  async function findById (id) {
    debug('Buscando archivo por ID');

    let archivo;
    try {
      archivo = await archivos.findById(id);
    } catch (e) {
      return res.error(e);
    }

    if (!archivo) {
      return res.error(new Error(`archivo ${id} not found`));
    }

    return res.success(archivo);
  }

  async function createOrUpdate (data) {
    debug('Crear o actualizar archivo');

    let archivo;
    try {
      archivo = await archivos.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    if (!archivo) {
      return res.error(new Error(`El archivo no pudo ser creado`));
    }

    return res.success(archivo);
  }

  async function deleteItem (id) {
    debug('Eliminando archivo');

    let deleted;
    try {
      deleted = await archivos.deleteItem(id);
    } catch (e) {
      return res.error(e);
    }

    if (deleted === -1) {
      return res.error(new Error(`No existe el archivo`));
    }

    if (deleted === 0) {
      return res.error(new Error(`El archivo ya fue eliminado`));
    }

    return res.success(deleted > 0);
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
