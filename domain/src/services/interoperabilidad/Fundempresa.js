'use strict';

const debug = require('debug')('apostilla:service:fundempresa');
const request = require('request');

module.exports = function fundempresaService (repositories, res) {
  const { parametros } = repositories;

  function getMatriculas (nit, fndUrl, fndToken) {
    return new Promise((resolve, reject) => {
      var req = {
        url: `${fndUrl}${nit}/matriculas/`,
        method: 'GET',
        headers: {
          Authorization: `Bearer ${fndToken}`,
          'Content-Type': 'application/json'
        },
        json: true
      };
      request.get(req, function (error, response, body) {
        if (error) {
          reject(error);
          return;
        }
        var lasMatriculas = [];
        if (body && body.SrvMatriculaConsultaNitResult && body.SrvMatriculaConsultaNitResult.MatriculasResult && body.SrvMatriculaConsultaNitResult.MatriculasResult.length) {
          body.SrvMatriculaConsultaNitResult.MatriculasResult.forEach(function (matricula) {
            if (matricula['CtrResult'] && matricula['CtrResult'] === 'D-EXIST') {
              lasMatriculas.push({
                matricula: (parseInt(matricula.IdMatricula)).toString(),
                razon_social: matricula.RazonSocial
              });
            }
          });
        }
        if (res.statusCode === 503) {
          reject(new Error('El servicio de FUNDEMPRESA no está disponible en estos momentos.'));
        }

        resolve(lasMatriculas);
      });
    });
  }

  async function buscarMatriculas (nit) {
    debug('Buscando matrículas por nit');
    const token = await parametros.findByName('FUNDEMPRESA_TOKEN');
    const urlServicio = await parametros.findByName('FUNDEMPRESA_OBTENER_MATRICULAS_URL');
    let lista;
    try {
      lista = await getMatriculas(nit, urlServicio.valor, token.valor);
    } catch (e) {
      return res.error(e);
    }
    if (!lista) {
      return res.error(new Error(`Error al obtener la lista de matrículas`));
    }
    if (lista.length === 0) {
      return res.warning(new Error(`No existen matrículas asociado al nit: ${nit}`));
    }

    return res.success(lista);
  }
  return {
    buscarMatriculas
  };
};
