'use strict';

const debug = require('debug')('apostilla:service:segip');
const request = require('request');
module.exports = function segipServicePersona (repositories, res) {
  const { parametros } = repositories;
  function BuscarPersonaSegip (ci, complemento, fechaNacimiento, url, token) {
    debug('SEGIP - Datos a buscar', ci, complemento, fechaNacimiento);
    return new Promise((resolve, reject) => {
      let urlServicio;
      if (complemento === null || complemento === undefined || complemento === '') {
        urlServicio = `${url}${ci}?fecha_nacimiento=${fechaNacimiento}`;
      } else {
        urlServicio = `${url}${ci}?fecha_nacimiento=${fechaNacimiento}&complemento=${complemento}`;
      }
      // urlServicio = `${url}${ci}`;
      debug('SEGIP - Petición:', urlServicio);
      let options = {
        url: urlServicio,
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json'
        },
        json: true
      };
      request.get(options, (error, response, body) => {
        debug('SEGIP - buscar persona Respuesta', body);
        if (error) {
          reject(error);
          return;
        }
        if (body.error) {
          if (body.error === 'No se encontro el recurso') {
            return reject(new Error(`No se encontraron datos para el CI: ${ci}`));
          }
          return reject(new Error(body.error));
        }
        if (typeof body === 'string' && body.match(/Not Found/) !== null && body.match(/Not Found/).length === 1) {
          return reject(new Error(`No se encontraron datos para el CI: ${ci}`));
        }
        if (body.res && body.res === 'err') {
          return reject(new Error(`No se encontraron datos para la fecha de nacimiento: ${fechaNacimiento}`));
        }
        if (typeof body.ConsultaDatoPersonaEnJsonResult === 'undefined') {
          return reject(new Error('Error con el servicio web'));
        }
        if (body.ConsultaDatoPersonaEnJsonResult.DatosPersonaEnFormatoJson === 'null') {
          resolve({
            status: response.statusCode,
            persona: {
              estado: body.ConsultaDatoPersonaEnJsonResult.CodigoRespuesta,
              mensaje_estado: body.ConsultaDatoPersonaEnJsonResult.DescripcionRespuesta
            },
            mensaje: `Error con el estado: ${response.statusCode}`
          });
        }
        if (typeof body.ConsultaDatoPersonaEnJsonResult === 'undefined') {
          return reject(new Error('Error con el servicio web'));
        }
        if (typeof body.ConsultaDatoPersonaEnJsonResult.DatosPersonaEnFormatoJson === 'undefined') {
          return reject(new Error('Error con el servicio web'));
        }
        let persona = JSON.parse(body.ConsultaDatoPersonaEnJsonResult.DatosPersonaEnFormatoJson);
        if (!persona) {
          return reject(new Error(`No se encontró datos de esta persona con ci: ${ci} y fecha de nacimiento: ${fechaNacimiento}`));
        }

        let fechaMMddyy = persona.FechaNacimiento.split('/');
        let fechaYYmmdd = [fechaMMddyy[2], fechaMMddyy[1], fechaMMddyy[0]].join('-');
        if (response.statusCode === 200) {
          resolve({
            persona: {
              nombres: persona.Nombres,
              paterno: persona.PrimerApellido === '--' ? '' : persona.PrimerApellido,
              materno: persona.SegundoApellido === '--' ? '' : persona.SegundoApellido,
              tipo_documento: 'CI',
              nro_documento: persona.NumeroDocumento,
              complemento: persona.Complemento,
              lugar_expedicion: persona.LugarNacimientoDepartamento,
              nacionalidad: persona.LugarNacimientoPais,
              genero: '',
              fecha_nacimiento: fechaYYmmdd,
              fecha_nacimiento_original: persona.FechaNacimiento,
              estado: body.ConsultaDatoPersonaEnJsonResult.CodigoRespuesta,
              mensaje_estado: body.ConsultaDatoPersonaEnJsonResult.DescripcionRespuesta
            },
            mensaje: body.ConsultaDatoPersonaEnJsonResult.DescripcionRespuesta
          });
        } else {
          resolve({
            status: response.statusCode,
            mensaje: `Error con status: ${response.statusCode}`,
            data: body
          });
        }
      });
    });
  }

  async function buscarPersona (ci, complemento, fechaNacimiento, esServicio) {
    debug('Busqueda de persona');
    const token = await parametros.findByName('SEGIP_TOKEN');
    const urlServicio = await parametros.findByName('SEGIP_BUSCAR_PERSONA_URL');
    let item;
    try {
      item = await BuscarPersonaSegip(ci, complemento, fechaNacimiento, urlServicio.valor, token.valor);
    } catch (e) {
      return res.error(e);
    }
    if (!item) {
      return res.error(new Error(`Error al realizar la búsqueda`));
    }
    if (item.count === 0) {
      return res.error(new Error(`No existen los datos solicitados`));
    }
    let persona = item.persona;

    if (persona.estado !== '2') {
      if (persona.estado === '3' || persona.estado === '4') {
        let mensaje = 'Debe acudir al SEGIP para regularizar su situación.';
        if (persona.mensaje_estado.indexOf('FALLECIDO') !== -1) {
          mensaje = 'Lo sentimos mucho.';
        }
        if(esServicio) return res.success(mensaje);
        return res.error(new Error(`${persona.mensaje_estado} - ${mensaje}`));
      }
      return res.error(new Error(`No existe resultado para su búsqueda. Revise que los datos: ci, fecha de nacimiento sean correctos.`));
    }

    if (persona.fecha_nacimiento_original !== fechaNacimiento) {
      return res.error(new Error(`La fecha de nacimiento ingresada no es la correcta`));
    }

    return res.success(item);
  }
  return {
    buscarPersona
  };
};
