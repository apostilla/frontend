'use strict';

const debug = require('debug')('apostilla:sms:documento');
const request = require('request');
module.exports = function smsService (repositories, res) {
  const { parametros } = repositories;

  async function enviarSms (mensaje, telefonos) {
    const urlSms = await parametros.findByName('SMS_URL');

    return new Promise((resolve, reject) => {
      const url = `${urlSms.valor}sms`;
      let options = {
        url,
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: {
          mensaje,
          telefonos
        },
        json: true
      };
      debug('SMS - Petición:', options);
      request(options, (error, response, body) => {
        debug('SMS - Respuesta enviar SMS', body);
        if (error) {
          return reject(error);
        }

        if (response.statusCode === 500) {
          reject(new Error('Ocurrió un error con el servicio SMS, vuelva a intentarlo en unos minutos.'));
        } else if (body.mensaje || body.message) {
          return reject(new Error(body.mensaje || body.message));
        }

        if (response.statusCode === 200) {
          if (body.estado) {
            resolve(body);
          } else {
            reject(new Error('Ocurrió un error al mandar el SMS'));
          }
        } else {
          reject(new Error(body.mensaje || body.mensaje || 'No se pudo conectar con el servicio SMS.'));
        }
      });
    });
  }

  async function enviar (mensaje, telefonos) {
    debug('SMS - Enviar SMS', mensaje, telefonos);
    let sms;
    try {
      sms = await enviarSms(mensaje, telefonos);
    } catch (e) {
      return res.error(e);
    }

    return res.success(sms);
  }

  return { enviar };
};
