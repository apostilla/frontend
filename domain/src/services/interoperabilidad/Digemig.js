'use strict';

const debug = require('debug')('apostilla:digemig');
const request = require('request');
const paises = require('./paises');

module.exports = function digemigService (repositories, res) {
  const { parametros } = repositories;

  async function buscarDocumento (nroDocumento, fechaNacimiento) {
    const token = await parametros.findByName('DIGEMIG_TOKEN');
    const urlServicio = await parametros.findByName('DIGEMIG_DOCUMENTO_URL');

    return new Promise((resolve, reject) => {
      const url = `${urlServicio.valor}${nroDocumento}?fechaNacimiento=${fechaNacimiento}`;
      let options = {
        url,
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token.valor}`,
          'Content-Type': 'application/json'
        },
        json: true
      };
      debug('DIGEMIG - Petición:', options);
      request(options, (error, response, body) => {
        debug('DIGEMIG - Respuesta buscar documento', body);
        if (error) {
          return reject(error);
        }

        if (body.codigo) {
          let mensaje = null;
          if (body.codigo === '-93') {
            mensaje = `No existe la persona con el documento ${nroDocumento} y fecha de nacimiento ${fechaNacimiento}.`;
          } else {
            mensaje = body.detalle || 'Ocurrió un error desconocido al momento de obtener la persona.';
          }
          return reject(new Error(mensaje));
        }

        if (response.statusCode === 500) {
          return reject(new Error('Ocurrió un error con el servicio de migración, vuelva a intentarlo en unos minutos.'));
        } else if (body.mensaje || body.message) {
          return reject(new Error(body.mensaje || body.message));
        }

        if (response.statusCode === 200) {
          if (body.codNacionalidad) {
            body.codNacionalidad = paises[body.codNacionalidad] || body.codNacionalidad;
          }
          resolve(body);
        } else {
          reject(new Error(body.mensaje || body.mensaje || 'No se pudo conectar con el servicio de migración.'));
        }
      });
    });
  }

  async function buscar (nroDocumento, fechaNacimiento) {
    debug('DIGEMIG - Buscar por documento', nroDocumento, fechaNacimiento);
    let persona;
    try {
      persona = await buscarDocumento(nroDocumento, fechaNacimiento);
    } catch (e) {
      return res.error(e);
    }

    return res.success(persona);
  }

  async function certificacion (data) {
    debug('DIGEMIG - Certificación');

    data = {
      'StrNombres': data.nombres,
      'StrPrimerApellido': data.primer_apellido,
      'StrSegundoApellido': data.segundo_apellido,
      'StrOtrosApellidos': '',
      'DateFechaNacimiento': data.fecha_nacimiento,
      'StrNumeroDocumento': data.nro_documento,
      'StrCorreoElecronico': data.email,
      'StrCodigoAgetic': data.id_tramite ? data.id_tramite + '' : ''
    };

    debug('Para certificación de Digemig: ' + JSON.stringify(data));

    let result;
    try {
      result = await _certificacion(data);
    } catch (err) {
      return res.error(err);
    }

    return res.success(result);
  }

  async function _certificacion (data) {
    const urlServicio = await parametros.findByName('DIGEMIG_CERTIFICACION');

    return new Promise((resolve, reject) => {
      let options = {
        url: `${urlServicio.valor}`,
        method: 'POST',
        json: true,
        body: data
      };
      debug('DIGEMIG - Petición:', options);
      request(options, (error, response, body) => {
        debug('DIGEMIG - Respuesta certificación', body);
        if (error) {
          return reject(error);
        }

        resolve(body);
      });
    });
  }

  async function verificacion (data) {
    debug('DIGEMIG - Verificación');
    const urlServicio = await parametros.findByName('DIGEMIG_VERIFICACION');
    console.log(urlServicio);
  }

  return {
    buscar,
    certificacion,
    verificacion
  };
};
