'use strict';

const debug = require('debug')('apostilla:service:minsalud');
const request = require('request');
module.exports = function segipServicePersona (repositories, res) {
  const { parametros } = repositories;

  function BuscarFormacionAcademicaMinSalud (matricula, apPaterno, apMaterno, url, token) {
    debug('Min. Salud - Matriculas - Datos a buscar', matricula, apPaterno, apMaterno);
    return new Promise((resolve, reject) => {
      let urlServicio;
      urlServicio = `${url}${matricula}/formacionesAcademicas?apPaterno=${apPaterno}&apMaterno=${apMaterno}`;

      debug('Min Salud - Petición:', urlServicio);
      let options = {
        url: urlServicio,
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json'
        },
        json: true
      };
      request.get(options, (error, response, body) => {
        debug('Min Salud - buscar Formación Academica - Respuesta');
        debug(JSON.stringify(body));
        if (error) {
          reject(error);
          return;
        }

        if (response.statusCode === 200) {
          debug('response Min Salud: ' + JSON.stringify(response));
          resolve(response);
        } else {
          resolve({
            status: response.statusCode,
            mensaje: `Error con status: ${response.statusCode}`,
            data: body
          });
        }
      });
    });
  }

  async function buscarPersona (ci, apPaterno, apMaterno) {
    debug('Busqueda de persona');
    const token = await parametros.findByName('SIREPRO_MATRICULAS_TOKEN');
    const urlServicio = await parametros.findByName('SIREPRO_MATRICULAS_URL');

    let item;

    try {
      item = await BuscarFormacionAcademicaMinSalud(ci, apPaterno, apMaterno, urlServicio, token);
    } catch (e) {
      return res.error(e);
    }
    if (!item) {
      return res.error(new Error(`Error al realizar la búsqueda en el SIREPRO`));
    }
    if (item.count === 0) {
      return res.error(new Error(`No existen los datos solicitados en el servicio de búsqueda del Min - Salud`));
    }

    return res.success(item);
  }
  return {
    buscarPersona
  };
};
