'use strict';

const debug = require('debug')('apostilla:service:segip');
const request = require('request');

module.exports = function segipService (repositories, res) {
  const { parametros } = repositories;

  function contrastarInfoSegip (_listaCampo, tipoPersona, segipUrl, segipToken) {
    return new Promise((resolve, reject) => {
      const listaCampos = {
        NumeroDocumento: _listaCampo.numero_documento,
        Complemento: _listaCampo.complemento,
        Nombres: _listaCampo.nombres,
        PrimerApellido: _listaCampo.primer_apellido,
        SegundoApellido: _listaCampo.segundo_apellido,
        FechaNacimiento: _listaCampo.fecha_nacimiento,
        LugarNacimientoPais: _listaCampo.lugar_nacimiento_pais,
        LugarNacimientoDepartamento: _listaCampo.lugar_nacimiento_departamento,
        LugarNacimientoProvincia: _listaCampo.lugar_nacimiento_provincia,
        LugarNacimientoLocalidad: _listaCampo.lugar_nacimiento_localidad
      };
      const lista = JSON.stringify(listaCampos);
      request({
        url: `${segipUrl}contrastacion?tipo_persona=${tipoPersona}&lista_campo=${lista}`,
        method: 'GET',
        headers: {
          Authorization: `Bearer ${segipToken}`,
          'Content-Type': 'application/json'
        },
        json: true
      }, (err, res, body) => {
        if (err) {
          reject(err);
          return;
        }
        if (body && body.ConsultaDatoPersonaContrastacionResult) {
          body = body.ConsultaDatoPersonaContrastacionResult;
          resolve({
            status: body.CodigoRespuesta,
            data: JSON.parse(body.ContrastacionEnFormatoJson)
          });
        } else {
          if (res.statusCode === 503) {
            reject(new Error('El servicio del SEGIP no está disponible en estos momentos.'));
          } else {
            reject(new Error('La respuesta no es correcta en el servicio de contrastación: '));
          }
        }
      });
    });
  }

  async function contrastacion (listaCampos, tipoPersona) {
    debug('Contrastación de persona');
    const token = await parametros.findByName('SEGIP_TOKEN');
    const urlServicio = await parametros.findByName('SEGIP_CONTRASTACION_URL');
    let item;
    try {
      item = await contrastarInfoSegip(listaCampos, tipoPersona, urlServicio.valor, token.valor);
    } catch (e) {
      return res.error(e);
    }
    if (!item) {
      return res.error(new Error(`Error al realizar la contrastación con segip`));
    }
    if (item.count === 0) {
      return res.warning(new Error(`No existen datos del servicio de contrastación`));
    }

    return res.success(item);
  }
  return {
    contrastacion
  };
};
