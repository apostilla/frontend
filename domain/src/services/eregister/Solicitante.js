'use strict';

const debug = require('debug')('apostilla:service:solicitante');

module.exports = function userService (repositories, res) {
  const { solicitantes } = repositories;

  async function findAll (params = {}) {
    debug('Lista de solicitantes|filtros');

    let lista;
    try {
      lista = await solicitantes.findAll(params);
    } catch (e) {
      return res.error(e);
    }

    if (!lista) {
      return res.error(new Error(`Error al obtener la lista de solicitantes`));
    }

    return res.success(lista);
  }

  async function findById (id) {
    debug('Buscando solicitante por ID');

    let user;
    try {
      user = await solicitantes.findById(id);
    } catch (e) {
      return res.error(e);
    }

    if (!user) {
      return res.error(new Error(`solicitante ${id} not found`));
    }

    return res.success(user);
  }

  async function createOrUpdate (data) {
    debug('Crear o actualizar solicitante');

    let user;
    try {
      user = await solicitantes.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    if (!user) {
      return res.error(new Error(`El solicitante no pudo ser creado`));
    }

    return res.success(user);
  }

  async function deleteItem (id) {
    debug('Eliminando solicitante');

    let deleted;
    try {
      deleted = await solicitantes.deleteItem(id);
    } catch (e) {
      return res.error(e);
    }

    if (deleted === -1) {
      return res.error(new Error(`No existe el solicitante`));
    }

    if (deleted === 0) {
      return res.error(new Error(`El solicitante ya fue eliminado`));
    }

    return res.success(deleted > 0);
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
