'use strict';

const debug = require('debug')('apostilla:service:tramite');
const geoip = require('geoip-lite');
const publicIp = require('public-ip');
const { randomStr, getDepartamento, getDepartamentoReal } = require('../../lib/util');
const moment = require('moment');
const uuid = require('uuid');

module.exports = function tramiteService (repositories, res) {
  const Correo = require('../system/Correo')(repositories, res);
  const SegipPersona = require('../interoperabilidad/SegipPersona')(repositories, res);
  const Digemig = require('../interoperabilidad/Digemig')(repositories, res);
  const Sms = require('../interoperabilidad/Sms')(repositories, res);
  const { tramites, tramites_doc, solicitantes, parametros, pagos, documentos, entidades, archivos, transaction, paises_destinos } = repositories;

  async function findAll (params = {}) {
    debug('Lista de tramites|filtros');

    let lista;
    try {
      lista = await tramites.findAll(params);
    } catch (e) {
      return res.error(e);
    }

    if (!lista) {
      return res.error(new Error(`Error al obtener la lista de tramites`));
    }

    return res.success(lista);
  }

  async function findById (id) {
    debug('Buscando tramite por ID');

    let tramite;
    try {
      tramite = await tramites.findById(id);
    } catch (e) {
      return res.error(e);
    }

    if (!tramite) {
      return res.error(new Error(`Tramite ${id} not found`));
    }

    return res.success(tramite);
  }

  async function create (data, ip) {
    debug('Crear trámite con sus documentos', data);

    let tramite;
    let pdfPagos = [];
    let esServicio = false;

    try {
      if (!data.id) {
        let tramitesDigemig = await parametros.findByName('DIGEMIG_TRAMITES');
        tramitesDigemig = tramitesDigemig.valor.split(',');
        console.log('TRAMITES DIGEMIG', tramitesDigemig);

        if (!data.documentos || data.documentos.length === 0) {
          return res.error(new Error('Debe agregar por lo menos un documento para crear el trámite.'));
        }

        if (data.complemento && data.complemento.length) {
          data.nroDoc = [data.nroDoc, data.complemento].join('-');
        }

        const t = await transaction.create();

        /** Verificando si existe el solicitante registrado previamente */
        let solicitante = await solicitantes.findByDoc(data.tipoDoc, data.nroDoc);

        console.log('Solicitante encontrado: ' + JSON.stringify(solicitante));

        if (!solicitante || !solicitante.id) {
            // /** Actualizando email y teléfono si el solicitante ya fue registrado previamente */
            // if (solicitante && solicitante.id) {
            //   data.id_solicitante = solicitante.id;
            //   data.email = data.email || solicitante.email;
            //   // data.telefono = data.telefono || solicitante.telefono;
            // }

            /** Creando o actualizando solicitante */
            let persona;
            if (!data.observacion && data.tipoDoc === 'CI') { // obtener datos de persona no observada
                const {nroDocumento, complemento} = getNroDocumento(data.nroDoc);
                try {
                    console.log("*************************************************************************");
                    console.log("*************************************************************************");
                    console.log("*************************************************************************");
                    console.log("*************************************************************************");
                    console.log("Revisando la data antes de validar con el segip", data);

                    const fechaNacimiento = moment(data.fecNacimiento).format('DD/MM/YYYY');
                    const datos =  {
                        nombres: data.nombres,
                        paterno: data.primerAp,
                        materno: data.segundoAp,
                        tipo_documento: data.tipoDoc,
                        nro_documento: data.nroDoc,
                        nacionalidad: data.nacionalidad,
                        fecha_nacimiento: moment(data.fecha_nacimiento).format('YYYY-MM-DD'),
                        fecha_nacimiento_original: fechaNacimiento,
                    };
                    if(data.nombres && (data.primerAp || data.segundoAp) && data.nacionalidad) {
                        esServicio = true;
                    }
                    persona = await SegipPersona.buscarPersona(nroDocumento, complemento, fechaNacimiento, esServicio, datos);
                } catch (e) {
                    transaction.rollback(t);
                    return res.error(e);
                }
            }

            if (!data.observacion && !esServicio) {
                if (!persona) {
                    debug('no se encontro titular: ' + JSON.stringify(data));
                    transaction.rollback(t);
                    return res.error(new Error('El titular no existe en la base de datos de SEGIP'));
                }

                if (!persona.data.persona) {
                    debug('no se encontro titular: ' + JSON.stringify(data));
                    transaction.rollback(t);
                    return res.error(new Error(persona.message));
                }

                if (data.nombres && !data.observacion ) { // si se envio el nombre de una persona sin observaciones, se valida el nombre con el segip
                    if (persona.data.persona.nombres !== data.nombres) {
                        transaction.rollback(t);
                        return res.error(new Error('El nombre introducido por el titular no coinciden con los datos de SEGIP'));
                    }
                }
                if (data.paterno && !data.observacion) {
                    if (persona.data.persona.paterno !== data.primerAp) {
                        transaction.rollback(t);
                        return res.error(new Error('El Ap. Paterno introducido por el titular no coinciden con los datos de SEGIP'));
                    }
                }
                if (data.materno && !data.observacion) {
                    if (persona.data.persona.materno !== data.segundoAp) {
                        transaction.rollback(t);
                        return res.error(new Error('El Ap. Materno introducido por el titular no coinciden con los datos de SEGIP'));
                    }
                }
            }

            if (persona && !esServicio) { // si hay una persona sin observaciones
                solicitante = await solicitantes.createOrUpdate({
                    id: data.id_solicitante,
                    nombres: persona.data.persona.nombres,
                    primer_apellido: persona.data.persona.paterno,
                    segundo_apellido: persona.data.persona.materno,
                    email: data.email,
                    telefono: data.telefono,
                    fecha_nacimiento: data.fecNacimiento ? moment(data.fecNacimiento, 'YYYY-MM-DD').format('MMM/DD/YYYY') : null,
                    tipo_documento: data.tipoDoc,
                    nro_documento: data.nroDoc,
                    nacionalidad: data.nacionalidad,
                    observacion: data.observacion || '',
                    _user_created: data._user_created
                }, t);
            } else { // persona con observaciones

                console.log('fecha original: ' + data.fecNacimiento);
                console.log('nueva fecha con moment: ' + moment(data.fecNacimiento, 'YYYY-MM-DD').format('MMM/DD/YYYY'));
                console.log('nueva fecha con new date: ' + new Date(data.fecNacimiento));

                solicitante = await solicitantes.createOrUpdate({
                    id: data.id_solicitante,
                    nombres: data.nombres,
                    primer_apellido: data.primerAp,
                    segundo_apellido: data.segundoAp,
                    email: data.email,
                    telefono: data.telefono,
                    fecha_nacimiento: data.fecNacimiento ? moment(data.fecNacimiento).format('MMM/DD/YYYY') : null,
                    tipo_documento: data.tipoDoc,
                    nro_documento: data.nroDoc,
                    nacionalidad: data.nacionalidad,
                    observacion: data.observacion || '',
                    _user_created: data._user_created
                }, t);
            }
        }

        else {
            solicitante = await solicitantes.createOrUpdate({
                id: solicitante.id,
                email: data.email,
                telefono: data.telefono,
                _user_created: data._user_created
            }, t);
            console.log(`Solicitante actualizado: ${JSON.stringify(solicitante)}`)
        }

        data.id_solicitante = solicitante.id;

        /** Creando trámite para los documentos solicitados */
        tramite = await tramites.createOrUpdate({
          id_solicitante: data.id_solicitante,
          fecha_inicio: new Date(),
          cod_seguimiento: await getCodigoSeguimiento(), // Verificamos y obtenermos que el código de seguimiento no exista
          tipo_documento: data.factura_tipo_documento,
          nro_documento: data.factura_nro_documento,
          titular_factura: data.factura_titular_factura,
          _user_created: data._user_created
        }, t);

        // Obteniendo datos de geolocalización del solicitante
        let geo;
        let ipPublica = '';

        await publicIp.v4().then(i => {
          ipPublica = i;
        });

        debug('ip de la solicitud: ' + ip);

        geo = await geoip.lookup(ip);

        console.log('ip Pública: ' + ipPublica);

        console.log('GEOLOCALIZACIÓN:', geo);

        let departamento = data.regional || getDepartamento(geo);

        /** Creando documentos de la solicitud */
        debug('Creando documentos de la solicitud');

        for (let i in data.documentos) {
          let documento;
          if (data.documentos[i].id) {
            documento = await documentos.findById(data.documentos[i].id);
            if (documento) {
              debug('Documento obtenido con id de documento: ' + JSON.stringify(documento));
            } else {
              transaction.rollback(t);
              return res.error(new Error(`No existe documento con el id ${data.documentos[i].id}`));
            }
          }

          if (data.documentos[i].codigoPortal) {
            if (data.documentos[i].pais) {
              let paises = await paises_destinos.findAll();
              let paisBuscado = paises.rows.filter(pais => pais.sigla.toLowerCase() === data.documentos[i].pais.toLocaleLowerCase());
              if (paisBuscado[0]) {
                data.documentos[i].digital = true;
                debug('Pais buscado: ' + JSON.stringify(paisBuscado));
              } else {
                data.documentos[i].digital = false;
                debug('El pais buscado no es parte de la Haya o no existe');
              }
            }

            documento = await documentos.findByCode(data.documentos[i].codigoPortal);
            if (documento) {
              debug('Documento obtenido con codigo de portal: ' + JSON.stringify(documento));
            } else {
              transaction.rollback(t);
              return res.error(new Error(`No existe documento con el código de portal ${data.documentos[i].codigoPortal}`));
            }
          }

          let archivo;
          if (data.documentos[i].pdf) {
            const base64Data = data.documentos[i].pdf;
            const rutaAbsoluta = await parametros.findByName('PATH_DOCS');
            const rutaRelativa = await parametros.findByName('PATH_PDFS');
            let nombre = uuid.v1();
            const titulo = 'Desde base 64';
            const mimetype = 'application/pdf';
            const ruta = rutaRelativa.valor;
            const rutaPdf = `${rutaAbsoluta.valor}${ruta}/${nombre}.pdf`; // sera algo como:  /home/ramiro/Documents/documentos/b57aac20-f56f-11e7-8d9e-ddc7ba8c960a.pdf

            archivo = await archivos.createOrUpdate({
              titulo,
              nombre,
              mimetype,
              ruta,
              _user_created: data._user_created,
              _created_at: new Date()
            }, t);

            await require('fs').writeFile(rutaPdf, base64Data, 'base64', function (err) {
              if (err) {
                transaction.rollback(t);
                return res.error(err);
              }
              debug('Nuevo PDF desde base 64: ' + rutaPdf);
            });
          }

          debug('Documento con entidad: ' + JSON.stringify(documento));
          let entidad = await entidades.findById(documento.id_entidad);

          debug('Entidad recuperada: ' + JSON.stringify(entidad));

          if (entidad.sigla === 'DIRNOPLU') {
            departamento = data.regional || getDepartamentoReal(geo);
            debug('La entidad es Dirnoplu en: ' + departamento);
          }

          let nuevoEstado = documento.derivar === 'ENTIDAD' ? 'CREADO' : 'FIRMADO_ENTIDAD';

          if (entidad.sigla === 'MINOPSV') {
            if (data.documentos[i].pdf){
              nuevoEstado = 'REVISADO_ENTIDAD';
            }
            else {
              transaction.rollback(t);
              return res.error(new Error(`No se pudo crear el documento de MINOPSV`));
            }
          }

          let doc = await tramites_doc.createOrUpdate({
            id_tramite: tramite.id,
            id_documento: documento.id,
            digital: data.documentos[i].digital,
            otros: data.documentos[i].otros,
            id_archivo: archivo ? archivo.id : 1,
            _user_created: data._user_created,
            estado: nuevoEstado,
            departamento
          }, t);

          if(!doc.id){
            transaction.rollback(t);
            return res.error(new Error(`No se pudo crear el documento `));
          }

          /** Creando pago para la ENTIDAD o CANCILLERIA, segun el documento */
          let pago = {
            tipo: 'CANCILLERIA', // documento.derivar // ahora el pago se hace en cancilleria
            id_tramite_doc: doc.id,
            _user_created: data._user_created
          };

          // Agregando datos solo si en la solicitud existe un tramitador
          if (data.tipoSolicitante === 'TRAMITADOR') {
            if (!data.tramitador_observacion && data.tipoDoc === 'CI') {
              let persona;

              if (!data.tramitador_nroDoc) {
                transaction.rollback(t);
                return res.error(new Error('El tramitador no existe en la base de datos de SEGIP'));
              }

              const {nroDocumento, complemento} = getNroDocumento(data.tramitador_nroDoc);
              try {
                persona = await SegipPersona.buscarPersona(nroDocumento, complemento, moment(data.tramitador_fecNacimiento).format('DD/MM/YYYY'));
                debug('Tramitador encontrado: ' + JSON.stringify(persona));
                if (!persona) {
                  debug('no hay tramitador encontrado: ' + JSON.stringify(data));
                  transaction.rollback(t);
                  return res.error(new Error('El tramitador no existe en la base de datos de SEGIP'));
                }

                if (!persona.data.persona) {
                  debug('no hay tramitador encontrado: ' + JSON.stringify(data));
                  transaction.rollback(t);
                  return res.error(new Error(persona.message));
                }

                pago.titular_factura = `${persona.data.persona.nombres} ${persona.data.persona.paterno} ${persona.data.persona.materno}`;
              } catch (e) {
                transaction.rollback(t);
                return res.error(e);
              }
            } else {
              pago.titular_factura = `${data.tramitador_primerAp} ${data.tramitador_segundoAp} ${data.tramitador_nombres}`;
            }
            pago.tipo_documento = data.tramitador_tipoDoc;
            pago.nro_documento = data.tramitador_nroDoc;
            pago.fecha_nacimiento = data.tramitador_fecNacimiento ? new Date(data.tramitador_fecNacimiento) : null;
            pago.observacion = data.tramitador_observacion || '';
          }

          await pagos.createOrUpdate(pago, t);

          // DIGEMIG - Obteniendo certificación de pago
          console.log('CODIGO PORTAL', data.documentos[i].codigo_portal, tramitesDigemig.indexOf(data.documentos[i].codigo_portal));
          if (data.documentos[i].codigo_portal && tramitesDigemig.indexOf(data.documentos[i].codigo_portal) !== -1) {
            debug('DIGEMIG - Obteniendo certificación de pago');
            let datos = Object.assign({}, solicitante);
            datos.fecha_nacimiento = moment(datos.fecha_nacimiento).utcOffset(0).format('DD/MM/YYYY');
            datos.id_tramite = doc.id;
            let result = await Digemig.certificacion(datos);
            if (result.code === 1) {
              pdfPagos.push({
                id: doc.id,
                documento,
                pdf: result.data
              });
            } else {
              transaction.rollback(t);
              return res.error(new Error(result.message));
            }
          }
        }

        // URL del portal de la apostilla
        let urlPortal = await parametros.findByName('URL_PORTAL');
        let urlSeguimiento = `${urlPortal.valor}#/seguimiento`;

        /** Enviando correo electrónico */
        if (solicitante.email) {
          let correo = {
            para: [solicitante.email],
            titulo: '[Apostilla] ¡Código de seguimiento!',
            mensaje: `
              <p style="text-align: center; margin-bottom: 30px;">¡Su trámite se ha creado correctamente!</p>
              <p>El código de seguimiento a su solicitud es:</p>
              <div style="text-align: center;">
                <div style="font-size: 18px; display: inline-block; padding: 5px 8px; margin: 10px 0 20px; background-color: #eee;">
                  ${tramite.cod_seguimiento}
                </div>
              </div>
              Guarde este código de seguimiento para poder ver el estado actual de su solicitud ingresando
              <strong><a href="${urlSeguimiento}" target="_blank">aquí</a></strong>
              o copie esta url en su navegador <a href="${urlSeguimiento}" target="_blank">${urlSeguimiento}</a>.
            `
          };

          // Adjuntando archivos PDF de pago
          if (pdfPagos.length) {
            let attachments = [];
            pdfPagos.map(item => {
              attachments.push({
                filename: `boleta-de-pago-${item.id}.pdf`,
                content: item.pdf,
                encoding: 'base64'
              });
            });
            correo.attachments = attachments;
          }
          let enviado = await Correo.enviar(correo);
          if (enviado.code === -1) {
              transaction.rollback(t);
              console.log(`Error enviando correo: ` + JSON.stringify(enviado));
              return res.error(enviado.message || 'Se produjo un error al tratar de enviarle el correo electrónico con su código de seguimiento, intente nuevamente.');
          }
        }

        transaction.commit(t); // Guardando cambios con la transacción

        /** Enviando mensaje por sms al número de contacto proporcionado */
        if (solicitante.telefono) {
          const mensaje = `APOSTILLA - CODIGO DE SEGUIMIENTO: ${tramite.cod_seguimiento} - VISITE ${urlSeguimiento} PARA VER EL ESTADO DE SU TRAMITE.`;
          await Sms.enviar(mensaje, [solicitante.telefono]);
        }
      } else {
        tramite = await tramites.createOrUpdate(data);
      }
    } catch (e) {
      return res.error(e);
    }

    if (!tramite) {
      return res.error(new Error(`El trámite no pudo ser creado`));
    }

    tramite.pagos = pdfPagos;

    return res.success(tramite);
  }

  function getNroDocumento (nroDoc) {
    let nroDocumento = nroDoc.split('-');
    let complemento;
    if (nroDocumento.length > 1) {
      complemento = nroDocumento[1];
      nroDocumento = nroDocumento[0];
    } else {
      nroDocumento = nroDoc;
    }
    return {
      nroDocumento,
      complemento
    };
  }

  async function getCodigoSeguimiento () {
    let codigo, code;
    do {
      codigo = randomStr(8);
      console.log('CODIGO', codigo);
      code = await tramites.findOneCode(codigo);
    } while (code);

    console.log('CODIGO RETURN', codigo);
    return codigo;
  }

  async function findByCode (code) {
    debug('Buscando trámite por CODE');

    let tramite;
    try {
      tramite = await tramites.findByCode(code);
    } catch (e) {
      return res.error(e);
    }

    if (!tramite) {
      return res.error(new Error(`Trámite ${code} not found`));
    }

    return res.success(tramite);
  }

  async function findByCode2 (code) {
    debug('Buscando trámite por CODE (Con menos datos de respuesta)');

    let tramite;
    try {
      tramite = await tramites.findByCode2(code);
    } catch (e) {
      return res.error(e);
    }

    if (!tramite) {
      return res.error(new Error(`Trámite ${code} not found`));
    }

    return res.success(tramite);
  }

  async function createOrUpdate (data) {
    debug('Crear o actualizar tramite');

    let user;
    try {
      user = await tramites.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    if (!user) {
      return res.error(new Error(`El tramite no pudo ser creado`));
    }

    return res.success(user);
  }

  async function deleteItem (id) {
    debug('Eliminando tramite');

    let deleted;
    try {
      deleted = await tramites.deleteItem(id);
    } catch (e) {
      return res.error(e);
    }

    if (deleted === -1) {
      return res.error(new Error(`No existe el trámite`));
    }

    if (deleted === 0) {
      return res.error(new Error(`El trámite ya fue eliminado`));
    }

    return res.success(deleted > 0);
  }

  return {
    findAll,
    findById,
    create,
    createOrUpdate,
    deleteItem,
    findByCode,
    findByCode2
  };
};
