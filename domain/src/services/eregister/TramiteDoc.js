'use strict';

const debug = require('debug')('apostilla:service:tramite_doc');
// const { randomStr, generateNroApostilla } = require('../../lib/util');

module.exports = function tramiteDocService (repositories, res) {
  const Correo = require('../system/Correo')(repositories, res);
  const { tramites_doc, tramites, parametros, solicitantes, auxiliares, transaction } = repositories;

  async function findAll (params = {}, idRol, idEntidad) {
    debug('Lista de tramites_doc|filtros');

    let lista;
    try {
      debug(params);
      switch (idRol) {
        case 5: // rol: Administrador entidad
          params.listado_tramites = ['CREADO', 'OBSERVADO_ENTIDAD_FIRMANTE', 'OBSERVADO_ENTIDAD_REVISOR', 'OBSERVADO_CANCILLERIA_REVISOR', 'REVISADO_ENTIDAD', 'REVISADO_ENTIDAD', 'FIRMADO_ENTIDAD'];
          params.id_entidad = idEntidad;
          break;
        case 6: // rol: Revisor Entidad
          params.listado_tramites = ['CREADO', 'OBSERVADO_ENTIDAD_FIRMANTE'];
          params.id_entidad = idEntidad;
          break;
        case 7: // rol: Firmador Entidad
          params.listado_tramites = ['CREADO','REVISADO_ENTIDAD','OBSERVADO_CANCILLERIA_REVISOR'];

          // const AuxiliaresEntidad = await auxiliares.findAll({id_entidad : idEntidad});

          // if (parseInt(AuxiliaresEntidad.count) ===  0) { // si el firmador de entidad no tiene auxiliares podra ver documentos en estado "Creado"
          //   params.listado_tramites.push('CREADO');
          // }

          params.id_entidad = idEntidad;
          // params.pago_entidad = true; deshabilitado validacion de pago entidad, dado que ya no se paga en la entidad

          break;
        case 3: // rol: Revisor Cancilleria
          params.listado_tramites = ['FIRMADO_ENTIDAD', 'OBSERVADO_CANCILLERIA_FIRMANTE'];
          break;
        case 4: // rol: Firmador Cancilleria
          params.listado_tramites = ['REVISADO_CANCILLERIA', 'APOSTILLADO'];
           params.pago_cancilleria = true;
          break;
      }

      if (params.no_estado && params.listado_tramites.length) {
        params.listado_tramites = params.listado_tramites.filter(estado => estado !== params.no_estado);
      }

      lista = await tramites_doc.findAll(params);
    } catch (e) {
      return res.error(e);
    }

    if (!lista) {
      return res.error(new Error(`Error al obtener la lista de tramites_doc`));
    }

    return res.success(lista);
  }

  async function historial (params = {}, idRol, idEntidad) {
    debug('Lista de tramites_doc|historial');

    let lista;
    try {
      debug(params);
      switch (idRol) {
        case 5: // rol: Administrador entidad
          params.listado_tramites = ['OBSERVADO_ENTIDAD_FIRMANTE', 'OBSERVADO_CANCILLERIA_REVISOR', 'REVISADO_ENTIDAD', 'REVISADO_ENTIDAD', 'FIRMADO_ENTIDAD'];
          params.id_entidad = idEntidad;
          break;
        case 6: // rol: Revisor Entidad
          params.listado_tramites = ['OBSERVADO_ENTIDAD_REVISOR', 'REVISADO_ENTIDAD'];
          params.id_entidad = idEntidad;
          break;
        case 7: // rol: Firmador Entidad
          params.listado_tramites = ['OBSERVADO_ENTIDAD_FIRMANTE', 'FIRMADO_ENTIDAD'];
          params.id_entidad = idEntidad;
          break;
        case 3: // rol: Revisor Cancilleria
          params.listado_tramites = ['OBSERVADO_CANCILLERIA_REVISOR', 'REVISADO_CANCILLERIA'];
          break;
        case 4: // rol: Firmador Cancilleria
          params.listado_tramites = ['OBSERVADO_CANCILLERIA_FIRMANTE', 'APOSTILLADO'];
          break;
      }

      if (params.no_estado && params.listado_tramites.length) {
        params.listado_tramites = params.listado_tramites.filter(estado => estado !== params.no_estado);
      }

      lista = await tramites_doc.findAll(params);
    } catch (e) {
      return res.error(e);
    }

    if (!lista) {
      return res.error(new Error(`Error al obtener la lista de tramites_doc`));
    }

    return res.success(lista);
  }

  async function para_firma (params = {}, idRol, idEntidad) {
    debug('Lista de tramites_doc|historial');

    let lista;
    try {
      debug(params);
      switch (idRol) {
        case 5: // rol: Administrador entidad
          params.listado_tramites = ['OBSERVADO_ENTIDAD_FIRMANTE', 'OBSERVADO_CANCILLERIA_REVISOR', 'REVISADO_ENTIDAD', 'REVISADO_ENTIDAD', 'FIRMADO_ENTIDAD'];
          params.id_entidad = idEntidad;
          break;
        case 6: // rol: Revisor Entidad
          params.listado_tramites = ['OBSERVADO_ENTIDAD_REVISOR', 'REVISADO_ENTIDAD'];
          params.id_entidad = idEntidad;
          break;
        case 7: // rol: Firmador Entidad
          params.listado_tramites = ['REVISADO_ENTIDAD'];
          params.id_entidad = idEntidad;
          break;
        case 3: // rol: Revisor Cancilleria
          params.listado_tramites = ['OBSERVADO_CANCILLERIA_REVISOR', 'REVISADO_CANCILLERIA'];
          break;
        case 4: // rol: Firmador Cancilleria
          params.listado_tramites = ['OBSERVADO_CANCILLERIA_FIRMANTE', 'APOSTILLADO'];
          break;
      }

      debug('Para firma: ' + params.listado_tramites);

      if (params.no_estado && params.listado_tramites.length) {
        params.listado_tramites = params.listado_tramites.filter(estado => estado !== params.no_estado);
      }

      lista = await tramites_doc.findAll(params);
    } catch (e) {
      return res.error(e);
    }

    if (!lista) {
      return res.error(new Error(`Error al obtener la lista de tramites_doc`));
    }

    return res.success(lista);
  }

  async function findById (id) {
    debug('Buscando tramite_doc por ID');

    let tramite;
    try {
      tramite = await tramites_doc.findById(id);
    } catch (e) {
      return res.error(e);
    }

    if (!tramite) {
      return res.error(new Error(`Tramites_doc ${id} not found`));
    }

    return res.success(tramite);
  }

  async function findByArchivoId (id_archivo) {
    debug('Buscando tramite_doc por id_archivo');

    let tramite;
    try {
      tramite = await tramites_doc.findByArchivoId(id_archivo);
    } catch (e) {
      return res.error(e);
    }

    if (!tramite) {
      return res.error(new Error(`Tramites_doc id_archivo ${id_archivo} not found`));
    }

    return res.success(tramite);
  }

  async function findByCode (code, number, date) {
    debug('Buscando tramite_doc por Código de seguridad, Nro. Apostilla y Fecha Exp.');

    let tramite;
    try {
      tramite = await tramites_doc.findByCode(code, number, date);
    } catch (e) {
      return res.error(e);
    }

    if (!tramite) {
      if (date) {
        date = [date.getDate(), date.getMonth() + 1, date.getFullYear()].join('/');
      }
      return res.error(new Error(`El documento con Código de Seguridad: ${code}, Nro. de Apostilla: ${number} y Fecha Exp.: ${date} no existe.`));
    }

    return res.success(tramite);
  }

  async function createOrUpdate (data) {
    debug('Crear o actualizar tramite_doc');

    let user;
    try {
      const t = await transaction.create();
      if (data.id) {

        let tramiteDocActual = await tramites_doc.findById(data.id);

        if (tramiteDocActual.estado && data.estado) { // verificamos que cambie de estado
          let sw = false;

          if (tramiteDocActual.estado === 'CREADO' && data.estado === 'REVISADO_ENTIDAD') {
            sw = true
          }

          if (tramiteDocActual.estado === 'CREADO' && data.estado === 'FIRMADO_ENTIDAD') {
            sw = true
          }

          if (tramiteDocActual.estado === 'CREADO' && data.estado === 'OBSERVADO_ENTIDAD_REVISOR') {
            sw = true
          }

          if (tramiteDocActual.estado === 'CREADO' && data.estado === 'OBSERVADO_ENTIDAD_FIRMANTE') {
            sw = true
          }

          if (tramiteDocActual.estado === 'REVISADO_ENTIDAD' && data.estado === 'OBSERVADO_ENTIDAD_FIRMANTE') {
            sw = true
          }

          if (tramiteDocActual.estado === 'OBSERVADO_ENTIDAD_FIRMANTE' && data.estado === 'FIRMADO_ENTIDAD') {
            sw = true
          }

          if (tramiteDocActual.estado === 'OBSERVADO_ENTIDAD_FIRMANTE' && data.estado === 'REVISADO_ENTIDAD') {
            sw = true
          }

          if (tramiteDocActual.estado === 'OBSERVADO_ENTIDAD_FIRMANTE' && data.estado === 'OBSERVADO_ENTIDAD_REVISOR') {
            sw = true
          }

          if (tramiteDocActual.estado === 'REVISADO_ENTIDAD' && data.estado === 'FIRMADO_ENTIDAD') {
            sw = true
          }

          if (tramiteDocActual.estado === 'FIRMADO_ENTIDAD' && data.estado === 'REVISADO_CANCILLERIA') {
            sw = true
          }

          if (tramiteDocActual.estado === 'FIRMADO_ENTIDAD' && data.estado === 'OBSERVADO_CANCILLERIA_REVISOR') {
            sw = true
          }

          if (tramiteDocActual.estado === 'OBSERVADO_CANCILLERIA_REVISOR' && data.estado === 'FIRMADO_ENTIDAD') {
            sw = true
          }

          if (tramiteDocActual.estado === 'OBSERVADO_CANCILLERIA_REVISOR' && data.estado === 'OBSERVADO_ENTIDAD_FIRMANTE') {
            sw = true
          }

          if (tramiteDocActual.estado === 'OBSERVADO_CANCILLERIA_FIRMANTE' && data.estado === 'OBSERVADO_CANCILLERIA_REVISOR') {
            sw = true
          }

          if (tramiteDocActual.estado === 'REVISADO_CANCILLERIA' && data.estado === 'OBSERVADO_CANCILLERIA_FIRMANTE') {
            sw = true
          }

          if (tramiteDocActual.estado === 'REVISADO_CANCILLERIA' && data.estado === 'APOSTILLADO') {
            sw = true
          }

          if (tramiteDocActual.estado === 'OBSERVADO_CANCILLERIA_FIRMANTE' && data.estado === 'REVISADO_CANCILLERIA') {
            sw = true
          }

          if (!sw) {
            transaction.rollback(t);
            let mensaje = "";

            if (tramiteDocActual.estado === data.estado) {
              mensaje = `El documento ya se encuentra en estado: ${data.estado}`
            } else {
              mensaje = `No se puede cambiar del estado ${tramiteDocActual.estado} a ${data.estado}`;
            }

            console.log(mensaje);
            return res.error(new Error(mensaje));
          }
        }
      }

      user = await tramites_doc.createOrUpdate(data, t);

      let tramite = await tramites.findById(user.id_tramite);

      let solicitante = await solicitantes.findById(tramite.id_solicitante);

      let urlPortal = await parametros.findByName('URL_PORTAL');

      console.log('Buscando: ' + data.estado + ': ' + ['OBSERVADO_ENTIDAD_REVISOR', 'OBSERVADO_ENTIDAD_FIRMANTE'].indexOf(data.estado) !== -1);

      if (solicitante.email && ['OBSERVADO_ENTIDAD_REVISOR', 'OBSERVADO_ENTIDAD_FIRMANTE'].indexOf(data.estado) !== -1) {
        const correo = {
          para: [solicitante.email],
          titulo: '[Apostilla] ¡Observaciones en su trámite!',
          mensaje: `
              <p>Su trámite tiene la siguiente observación:</p>
              <div style="text-align: center;">
                <div style="font-size: 18px; display: inline-block; padding: 5px 8px; margin: 10px 0 20px; background-color: #eee;">
                  ${data.observacion}
                </div>
              </div>
              Realice una nueva solicitud 
              <strong><a href="${urlPortal.valor}" target="_blank">aquí</a></strong>  
              subsanando las observaciones mencionadas
              o copie esta url en su navegador <a href="${urlPortal.valor}" target="_blank">${urlPortal.valor}</a>. para hacer una nueva solicitud
            `
        };
        let enviado = await Correo.enviar(correo);
        if (enviado.code === -1) {
          transaction.rollback(t);
          return res.error(enviado.message || 'Se produjo un error al tratar de enviarle el correo electrónico con su observación, intente nuevamente.');
        } else {
          console.log('Correo enviado: ' + JSON.stringify(enviado))
        }
      }
      transaction.commit(t);

    } catch (e) {
      return res.error(e);
    }

    if (!user) {
      return res.error(new Error(`El tramite_doc no pudo ser creado`));
    }

    return res.success(user);
  }

  async function modifiedStatus (data, rol, entidad) {
    debug('Modificar estado del tramite_doc', data, rol, entidad);

    let tramiteDoc;
    try {
      switch (rol) {
        case 7: // Rol firmante entidad
          if (entidad !== 1 && data.estado.includes('OBSERVADO')) {
            debug('Observado por entidad');
            data.estado = 'OBSERVADO_ENTIDAD_FIRMANTE';
          }
          break;
        case 4: // Rol firmante cancilleria
          /*
          firma Cancilleria
          //TODO: verificar pago de cancilleria para habilitar firmado
          */
          if (entidad === 1 && data.estado.includes('OBSERVADO')) {
            debug('NO apostillado, observado');
            data.estado = 'OBSERVADO_CANCILLERIA_FIRMANTE';
          }
          break;
      }
      tramiteDoc = await tramites_doc.createOrUpdate(data);
      // Registro de firma digital en tabla firma
    } catch (e) {
      return res.error(e);
    }

    if (!tramiteDoc) {
      return res.error(new Error(`El tramite_doc no pudo modificar el status`));
    }

    return res.success(tramiteDoc);
  }

  async function deleteItem (id) {
    debug('Eliminando tramite_doc');

    let deleted;
    try {
      deleted = await tramites_doc.deleteItem(id);
    } catch (e) {
      return res.error(e);
    }

    if (deleted === -1) {
      return res.error(new Error(`No existe el documento del trámite`));
    }

    if (deleted === 0) {
      return res.error(new Error(`El documento del trámite ya fue eliminado`));
    }

    return res.success(deleted > 0);
  }

  return {
    findAll,
    findById,
    findByCode,
    findByArchivoId,
    createOrUpdate,
    modifiedStatus,
    deleteItem,
    historial,
    para_firma
  };
};
