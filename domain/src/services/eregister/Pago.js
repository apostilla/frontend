'use strict';

const debug = require('debug')('apostilla:service:pago');

module.exports = function userService (repositories, res) {
  const { pagos } = repositories;

  async function findAll (params = {}, idRol, idEntidad) {
    debug('Lista de pagos|filtros');

    let lista;
    try {
      // Filtrando por entidades SUPERADMIN | ADMIN CANCILLERIA | SOLICITANTE
      if ([1, 2, 3, 4, 8].indexOf(idRol) === -1) {
        params.id_entidad = idEntidad;
      }
      if (idRol === 6 || idRol === 7) {
        params.tipo = 'ENTIDAD';
      }
      lista = await pagos.findAll(params);
      debug('LISTA PAGOS', lista);
    } catch (e) {
      return res.error(e);
    }

    if (!lista) {
      return res.error(new Error(`Error al obtener la lista de pagos`));
    }

    return res.success(lista);
  }

  async function findById (id) {
    debug('Buscando pago por ID');

    let user;
    try {
      user = await pagos.findById(id);
    } catch (e) {
      return res.error(e);
    }

    if (!user) {
      return res.error(new Error(`pago ${id} not found`));
    }

    return res.success(user);
  }

  async function findIdCpt (id) {
    debug('Buscando pago por ID');

    let pago;
    try {
      pago = await pagos.findByIdCpt(id);
    } catch (e) {
      return res.error(e);
    }

    if (!pago) {
      return res.error(new Error(`pago ${id} not found`));
    }

    return res.success(pago);
  }

  async function createOrUpdate (data) {
    debug('Crear o actualizar pago');

    let user;
    try {
      user = await pagos.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    if (!user) {
      return res.error(new Error(`El pago no pudo ser creado`));
    }

    return res.success(user);
  }

  async function deleteItem (id) {
    debug('Eliminando pago');

    let deleted;
    try {
      deleted = await pagos.deleteItem(id);
    } catch (e) {
      return res.error(e);
    }

    if (deleted === -1) {
      return res.error(new Error(`No existe el pago`));
    }

    if (deleted === 0) {
      return res.error(new Error(`El pago ya fue eliminado`));
    }

    return res.success(deleted > 0);
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem,
    findIdCpt
  };
};
