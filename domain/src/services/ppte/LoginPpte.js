'use strict';

const debug = require('debug')('apostilla:service:ppte-login');
const request = require('request');

module.exports = function ppteLoginx (repositories, res) {
  const { parametros, entidades } = repositories;

  function loginPpte (usuario, password, ppteLoginUrl) {
    return new Promise((resolve, reject) => {
      var req = {
        url: ppteLoginUrl,
        body: {
          usuario,
          password
        },
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        json: true
      };
      request.post(req, (error, res, body) => {
        if (error) {
          reject(error);
          return;
        }
        if (res.statusCode === 200) {
          debug('Respuesta correcta PPTE');
          return resolve(body);
        } else {
          if (res.statusCode === 503) {
            debug('Es servicio del PPTE no está disponible en estos momentos');
            return reject(new Error('Es servicio no está disponible en estos momentos'));
          } else {
            debug('Respuesta incorrecta PPTE');
            return reject(body);
          }
        }
      });
    });
  }
  // obtener de la entidad en PPTE
  function obtenerInfoPpte (tokenPpte, ppteTramitesUrl) {
    return new Promise((resolve, reject) => {
      var req = {
        url: ppteTramitesUrl,
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `JWT ${tokenPpte}`
        },
        json: true
      };
      request.get(req, (error, res, body) => {
        if (error) {
          reject(error);
          return;
        }
        if (res.statusCode === 200) {
          debug('Respuesta correcta PPTE');
          return resolve(body);
        } else {
          if (res.statusCode === 503) {
            debug('El servicio del PPTE no está disponible en estos momentos');
            return reject(new Error('El servicio no está disponible en estos momentos'));
          } else {
            debug('Respuesta incorrecta PPTE');
            return reject(body);
          }
        }
      });
    });
  }

  // crear o acualizar token de tramite en PPTE
  function crearActualizarTokenTramite (token, urlServicioTokenTram, verbo, xbody) {
    return new Promise((resolve, reject) => {
      var req = {
        url: urlServicioTokenTram,
        body: xbody,
        method: verbo,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `JWT ${token}`
        },
        json: true
      };
      debug('Datos para crear token del trámite', req);
      request(req, (error, res, body) => {
        debug('Respuesta Crear token trámite', body);
        if (error) {
          reject(error);
          return;
        }
        if (res.statusCode === 200) {
          debug('Respuesta correcta PPTE');
          return resolve(body);
        } else {
          if (res.statusCode === 503) {
            debug('Es servicio del PPTE no está disponible en estos momentos');
            return reject(new Error('Es servicio no está disponible en estos momentos'));
          } else {
            debug('Respuesta incorrecta PPTE');
            return reject(body);
          }
        }
      });
    });
  }

  // crear cpt
  function crearCpt (token, xbody, urlServicioCrearCpt) {
    return new Promise((resolve, reject) => {
      var req = {
        url: urlServicioCrearCpt,
        body: xbody,
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `JWT ${token}`
        },
        json: true
      };
      debug('Datos para crear CPT');
        debug( JSON.stringify(req));
      request(req, (error, res, body) => {
        if (!res) {
          debug('Error PPTE: ' + error);
          return reject(new Error('Es servicio no está disponible en estos momentos'));
        }
        debug('Generando CPT', res.statusCode, body);
        if (error) {
          reject(error);
          return;
        }
        if (res.statusCode === 200) {
          debug('Respuesta correcta PPTE');
          return resolve(body);
        } else {
          if (res.statusCode === 503) {
            debug('Es servicio del PPTE no está disponible en estos momentos');
            return reject(new Error('Es servicio no está disponible en estos momentos'));
          } else if (res.statusCode === 401) {
            return reject(new Error('No se puede generar el código CPT debido a  que el token para realizar el CPT ha expirado, contactese con el administrador del sistema.'));
          } else {
            debug('Respuesta incorrecta PPTE');
            return reject(body);
          }
        }
      });
    });
  }

  // login que guarda datos en db
  async function ppteLogin (usuario, password, idEntidad) {
    debug('Login con el PPTE');
    const urlServicio = await parametros.findByName('PPTE_LOGIN_URL');
    let datos;
    try {
      datos = await loginPpte(usuario, password, urlServicio.valor);
    } catch (e) {
      return res.error(e);
    }
    if (!datos) {
      return res.error(new Error(`Error al obtener cuenta`));
    }
    if (datos.length === 0) {
      return res.warning(new Error(`No existe la cuenta`));
    }

    let entidad = {
      id: idEntidad,
      usuario_ppte: usuario,
      pass_ppte: password,
      info: datos.usuario // este objto contiene datos, pero el mas importante es el idEntidad en el PPTE
    };
    let entidadUpdated = await entidades.createOrUpdate(entidad);
    return res.success(entidadUpdated);
  }

  // login que devuelve datos de respuesta de PPTE
  async function ppteLoginToken (usuario, password) {
    debug('Login con el PPTE y devoler respuesta de PPTE');
    const urlServicio = await parametros.findByName('PPTE_LOGIN_URL');
    let datos;
    try {
      datos = await loginPpte(usuario, password, urlServicio.valor);
    } catch (e) {
      return res.error(e);
    }
    if (!datos) {
      return res.error(new Error(`Error al obtener cuenta`));
    }
    if (datos.length === 0) {
      return res.warning(new Error(`No existe la cuenta`));
    }
    return res.success(datos);
  }

  // Obtener info de de tramites en ppte por entidad
  async function ppteInfoTramitesEntidad (token, idEntidad, idTramite = null) {
    debug('Inicia sesión en Plataforma de pagos y devuelve trámites de la Plataforma de pagos');
    const urlServicioDocsOriginal = await parametros.findByName('PPTE_TRAMITES_URL');
    let urlServicioDocs = (urlServicioDocsOriginal.valor).replace('{id_entidad}', idEntidad);

    if (idTramite) {
      let limite = urlServicioDocs.indexOf('limit')
      if (limite>0){
        limite--
        urlServicioDocs = urlServicioDocs.substring(0, limite)
      }//quitanto 'limite' a la URL, si es que existe en la URL, y si hay un 'idtramite' en la URL
      urlServicioDocs = `${urlServicioDocs}/${idTramite}`;
    }

    console.log('URL:', urlServicioDocs);

    let datos;
    try {
      datos = await obtenerInfoPpte(token, urlServicioDocs);
    } catch (e) {
      return res.error(e);
    }
    if (!datos) {
      return res.error(new Error(`Error al obtener cuenta`));
    }
    if (datos.length === 0) {
      return res.warning(new Error(`No existe la cuenta`));
    }
    debug('--Datos: '+urlServicioDocs+' :'+JSON.stringify(datos));
    return res.success(datos);
  }

  // crear token para tramite
  async function crearTokenTramite (token, verbo, idEntidad, idTramite, id, transicion, idUsuarioApp) {
    debug('Crear o actualizar token para tramite');
    const urlServicioOriginal = await parametros.findByName('PPTE_TOKEN_TRAMITE_URL');
    let urlServicioTokenTram = (urlServicioOriginal.valor).replace('{id_entidad}', idEntidad).replace('{id_tramite}', idTramite).replace('{id_token}', id);
    const xbody = {
      transicion: transicion,
      usuarioAplicacionId: idUsuarioApp
    };
    let datos;
    try {
      datos = await crearActualizarTokenTramite(token, urlServicioTokenTram, verbo, xbody);
    } catch (e) {
      return res.error(e);
    }
    if (!datos) {
      return res.error(new Error(`Error al obtener cuenta`));
    }
    if (datos.length === 0) {
      return res.warning(new Error(`No existe la cuenta`));
    }
    return res.success(datos);
  }

  // Obtener usuarios aplicacion de la entidad
  async function ppteObtenerUsuariosApp (token) {
    debug('Obtener usuarios Aplicaciones');
    const urlServicioUsuariosApp = await parametros.findByName('PPTE_USUARIOSAPLICACION_URL');
    let datos;
    try {
      datos = await obtenerInfoPpte(token, urlServicioUsuariosApp.valor);
    } catch (e) {
      return res.error(e);
    }
    if (!datos) {
      return res.error(new Error(`Error al obtener cuenta`));
    }
    if (datos.length === 0) {
      return res.warning(new Error(`No existe la cuenta`));
    }
    // procesando respuesta datos
    let apps = [];
    datos.datos.map(el => {
      if (el.estado === 'VERIFICADO') {
        apps.push({
          id_usuario_app: el.usuarioAplicacion.id,
          endpoint: el.usuarioAplicacion.endpoint,
          id_entidad: el.entidad.id,
          codigo_entidad: el.entidad.codigo
        });
      }
    });
    return res.success(apps);
  }

  // Crear cpt
  async function ppteCrearCpt (token, cptObj) {
    debug('Obtener usuarios Aplicaciones');
    const urlServicioUsuariosApp = await parametros.findByName('PPTE_CPT_URL');
    let datos;
    try {
      datos = await crearCpt(token, cptObj, urlServicioUsuariosApp.valor);
    } catch (e) {
      return res.error(e);
    }
    if (!datos) {
      return res.error(new Error(`Error al obtener cuenta`));
    }
    if (datos.length === 0) {
      return res.warning(new Error(`No existe la cuenta`));
    }
    return res.success(datos);
  }

  return {
    ppteLogin,
    ppteLoginToken,
    ppteInfoTramitesEntidad,
    ppteObtenerUsuariosApp,
    crearTokenTramite,
    ppteCrearCpt
  };
};
