'use strict';

const { config, errors } = require('common');
const db = require('infrastructure');
const util = require('./src/lib/util');
const path = require('path');
const debug = require('debug')('apostilla:domain');

module.exports = async function () {
  const repositories = await db(config.db).catch(errors.handleFatalError);

  // Cargando todos los servicios que se encuentran en la carpeta services y en sus subcarpetas
  let services = util.loadServices(path.join(__dirname, 'src', 'services'), repositories, { exclude: ['index.js', 'paises.js'] });
  services = util.convertLinealObject(services);
  debug('Capa del dominio - Servicios cargados');

  return services;
};
