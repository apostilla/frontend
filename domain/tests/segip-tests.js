'use strict';

const test = require('ava');
const { errors, config } = require('common');
const domain = require('../');

let services;

test.beforeEach(async () => {
  if (!services) {
    services = await domain(config.db).catch(errors.handleFatalError);
  }
});

test.serial('Segip#contrastacion - correct', async t => {
  const { Segip } = services;
  const persona = {
    'numero_documento': '7614958',
    'nombres': 'DANIELA'
  };
  const tipoPersona = 1;
  let res = await Segip.contrastacion(persona, tipoPersona);
  console.log('RESPUESTA SEGIP - contrastación', res);
  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.status, '2', 'Respuesta correcta');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Segip#buscarPersona - correct', async t => {
  const { SegipPersona } = services;
  const persona = { // buscar en datos fake para probar
    ci: '5602708',
    complemento: '1H',
    fechaNacimiento: '21/07/1983'
  };
  // const tipoPersona = 1;
  let res = await SegipPersona.buscarPersona(persona.ci, persona.complemento, persona.fechaNacimiento);
  console.log('RESPUESTA SEGIP - buscar Persona', res);
  t.is(res.code, 1, 'Respuesta correcta');
  t.true(!!res.data.persona, 'Respuesta correcta');
  t.is(res.message, 'OK', 'Mensaje correcto');
});
