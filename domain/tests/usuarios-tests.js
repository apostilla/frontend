'use strict';

const test = require('ava');
const { errors, config } = require('common');
const domain = require('../');

let services;

test.beforeEach(async () => {
  if (!services) {
    services = await domain(config.db).catch(errors.handleFatalError);
  }
});

test.serial('Usuario#findAll', async t => {
  const { Usuario } = services;
  let res = await Usuario.findAll();

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.count, 21, 'Se tiene 21 registros en la bd');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Usuario#findById', async t => {
  const { Usuario } = services;
  const id = 1;

  let res = await Usuario.findById(id);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.id, id, 'Se recuperó el registro mediante un id');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Usuario#userExist - exist', async t => {
  const { Usuario } = services;
  const usuario = 'admin';
  const password = '123456';

  let res = await Usuario.userExist(usuario, password);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.usuario, usuario, 'Se verificó la existencia del usuario y contraseña');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Usuario#userExist - not exist', async t => {
  const { Usuario } = services;
  const usuario = 'admin1234';
  const password = '123456';

  let res = await Usuario.userExist(usuario, password);

  t.is(res.code, -1, 'Respuesta incorrecta');
  t.is(res.message, 'No existe el usuario', 'Mensaje correcto');
});

test.serial('Usuario#userExist - password error', async t => {
  const { Usuario } = services;
  const usuario = 'admin';
  const password = '1234567';

  let res = await Usuario.userExist(usuario, password);

  t.is(res.code, -1, 'Respuesta incorrecta');
  t.is(res.message, 'La contraseña es incorrecta', 'Mensaje correcto');
});

test.serial('Usuario#createOrUpdate - update', async t => {
  const { Usuario } = services;
  const newData = {
    id: 2,
    email: 'adminadmin@mail.com',
    usuario: 'adminadmin',
    segundo_apellido: 'Administrador',
    id_rol: 1
  };

  let res = await Usuario.createOrUpdate(newData);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.email, newData.email, 'Actualizando registro usuario');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Usuario#createOrUpdate - new', async t => {
  const { Usuario } = services;
  let usuario = `admin-${parseInt(Math.random() * 10000)}`;
  const nuevoUsuario = {
    usuario,
    contrasena: '123456',
    email: `${usuario}@mail.com`,
    nombres: 'Administrador',
    primer_apellido: 'Administrador',
    estado: 'ACTIVO',
    id_rol: 1,
    id_entidad: 1,
    _user_created: 1,
    _created_at: new Date()
  };

  let res = await Usuario.createOrUpdate(nuevoUsuario);
  usuario = res.data;

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(typeof usuario.id === 'number', 'Comprobando que el nuevo usuario tenga un id');
  t.is(usuario.usuario, nuevoUsuario.usuario, 'Creando registro - usuario');
  t.is(usuario.email, nuevoUsuario.email, 'Creando registro - email');
  t.is(usuario.contrasena, nuevoUsuario.contrasena, 'Creando registro - contraseña');
  t.is(usuario.nombres, nuevoUsuario.nombres, 'Creando registro - nombres');
  t.is(res.message, 'OK', 'Mensaje correcto');

  test.idUser = usuario.id;
});

test.serial('Usuario#findAll#filter#usuario - not found', async t => {
  const { Usuario } = services;
  let res = await Usuario.findAll({ usuario: 'abcde' });

  t.is(res.code, 0, 'Respuesta con advertencia');
  t.is(res.message, 'No existen usuarios', 'Mensaje correcto');
});

test.serial('Usuario#findAll#filter#usuario', async t => {
  const { Usuario } = services;
  let res = await Usuario.findAll({ usuario: 'admin' });
  let lista = res.data;

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(lista.count, 3, 'Se tiene 3 registros en la bd');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Usuario#findAll#filter#email', async t => {
  const { Usuario } = services;
  let res = await Usuario.findAll({ email: 'admin' });
  let lista = res.data;

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(lista.count, 3, 'Se tiene 3 registros en la bd');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Usuario#findAll#filter#nombre_completo', async t => {
  const { Usuario } = services;
  let res = await Usuario.findAll({ nombre_completo: 'admin' });
  let lista = res.data;

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(lista.count, 3, 'Se tiene 3 registros en la bd');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Usuario#findAll#filter#id_rol', async t => {
  const { Usuario } = services;
  let res = await Usuario.findAll({ id_rol: 1 });
  let lista = res.data;

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(lista.count, 3, 'Se tiene 3 registros en la bd');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

// Delete logically user
test.serial('Usuario#delete', async t => {
  const { Usuario } = services;
  let res = await Usuario.deleteItem(test.idUser);
  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data, 'Usuario eliminado');
  t.is(res.message, 'OK', 'Mensaje correcto');
});
