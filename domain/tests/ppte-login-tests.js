'use strict';

const test = require('ava');
const { errors, config } = require('common');
const domain = require('../');

let services;

test.beforeEach(async () => {
  if (!services) {
    services = await domain(config.db).catch(errors.handleFatalError);
  }
});

test.serial('PPTE#ppteLogin', async t => {
  const { LoginPpte } = services;
  let res = await LoginPpte.ppteLogin('jsillo', 'ContrasenaSegura', 8);
  console.log('RESPUESTA PPTE', res);
  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.message, 'OK', 'Mensaje correcto');
});
