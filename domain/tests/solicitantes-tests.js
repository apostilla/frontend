'use strict';

const test = require('ava');
const { errors, config } = require('common');
const domain = require('../');

let services;

test.beforeEach(async () => {
  if (!services) {
    services = await domain(config.db).catch(errors.handleFatalError);
  }
});

test.serial('Solicitante#findAll', async t => {
  const { Solicitante } = services;
  let res = await Solicitante.findAll();

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data.count >= 20, 'Se tiene 20 registros en la bd');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Solicitante#findById', async t => {
  const { Solicitante } = services;
  const id = 1;

  let res = await Solicitante.findById(id);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.id, id, 'Se recuperó el registro mediante un id');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Solicitante#createOrUpdate - new', async t => {
  const { Solicitante } = services;

  const nuevoSolicitante = {
    tipo: 'NATURAL',
    nombres: 'fernando',
    primer_apellido: 'conde',
    segundo_apellido: 'conde',
    email: 'test@gmail.com',
    telefono: '1234567890',
    fecha_nacimiento: '1999-10-10',
    tipo_documento: 'CI',
    nro_documento: '123456789',
    direccion: 'xxxx xxxx xxx xx',
    id_departamento: 1,
    id_provincia: 1,
    id_municipio: 1,
    _user_created: 1,
    _created_at: new Date()
  };

  let res = await Solicitante.createOrUpdate(nuevoSolicitante);
  let newSolicitante = res.data;

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(typeof newSolicitante.id === 'number', 'Comprobando que el nuevo Departamento tenga un id');

  t.is(newSolicitante.tipo, nuevoSolicitante.tipo, 'Creando registro - tipo');
  t.is(newSolicitante.nombres, nuevoSolicitante.nombres, 'Creando registro - nombres');
  t.is(newSolicitante.primer_apellido, nuevoSolicitante.primer_apellido, 'Creando registro - primer_apellido');
  t.is(newSolicitante.segundo_apellido, nuevoSolicitante.segundo_apellido, 'Creando registro - segundo_apellido');
  t.is(newSolicitante.email, nuevoSolicitante.email, 'Creando registro - email');
  t.is(newSolicitante.telefono, nuevoSolicitante.telefono, 'Creando registro - telefono');
  t.is(newSolicitante.fecha_nacimiento, nuevoSolicitante.fecha_nacimiento, 'Creando registro - fecha_nacimiento');
  t.is(newSolicitante.tipo_documento, nuevoSolicitante.tipo_documento, 'Creando registro - tipo_documento');
  t.is(newSolicitante.nro_documento, nuevoSolicitante.nro_documento, 'Creando registro - nro_documento');
  t.is(newSolicitante.direccion, nuevoSolicitante.direccion, 'Creando registro - direccion');

  t.is(res.message, 'OK', 'Mensaje correcto');
  test.id = newSolicitante.id;
});

test.serial('Solicitante#createOrUpdate - update', async t => {
  const { Solicitante } = services;
  const newData = { id: test.id, nro_documento: '123ABCDE' };

  let res = await Solicitante.createOrUpdate(newData);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.nro_documento, newData.nro_documento, 'Actualizando registro Solicitante');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Solicitante#findAll#filter#Departamento ', async t => {
  const { Solicitante } = services;
  let res = await Solicitante.findAll({ id_departamento: 1 });

  t.is(res.code, 1, 'Respuesta exitosa');
  t.true(res.data.count >= 1, 'Se recuperó el registro mediante un id_departamento');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Solicitante#findAll#filter#Provincia ', async t => {
  const { Solicitante } = services;
  let res = await Solicitante.findAll({ id_provincia: 1 });

  t.is(res.code, 1, 'Respuesta exitosa');
  t.true(res.data.count >= 1, 'Se recuperó el registro mediante un id_provincia');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Solicitante#findAll#filter#Provincia ', async t => {
  const { Solicitante } = services;
  let res = await Solicitante.findAll({ id_municipio: 1 });

  t.is(res.code, 1, 'Respuesta exitosa');
  t.true(res.data.count >= 1, 'Se recuperó el registro mediante un id_municipio');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Solicitante#findAll#filter#Tipo ', async t => {
  const { Solicitante } = services;
  let res = await Solicitante.findAll({ tipo: 'NATURAL' });

  t.is(res.code, 1, 'Respuesta exitosa');
  t.true(res.data.count >= 1, 'Se recuperó el registro mediante un tipo');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Solicitante#findAll#filter#TipoDocumento', async t => {
  const { Solicitante } = services;
  let res = await Solicitante.findAll({ tipo_documento: 'PASAPORTE' });

  t.is(res.code, 1, 'Respuesta exitosa');
  t.true(res.data.count >= 1, 'Se recuperó el registro mediante un tipo_documento');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Solicitante#findAll#filter#NombreCompleto', async t => {
  const { Solicitante } = services;
  let res = await Solicitante.findAll({ nombre_completo: 'a' });

  t.is(res.code, 1, 'Respuesta exitosa');
  t.true(res.data.count >= 1, 'Se recuperó el registro mediante un nombre_completo');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Solicitante#delete', async t => {
  const { Solicitante } = services;
  let res = await Solicitante.deleteItem(test.id);

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data, 'Solicitante eliminado');
  t.is(res.message, 'OK', 'Mensaje correcto');
});
