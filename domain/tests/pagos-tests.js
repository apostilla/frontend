'use strict';

const test = require('ava');
const { errors, config } = require('common');
const domain = require('../');

let services;

test.beforeEach(async () => {
  if (!services) {
    services = await domain(config.db).catch(errors.handleFatalError);
  }
});

test.serial('Pago#findAll', async t => {
  const { Pago } = services;
  let res = await Pago.findAll();

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data.count >= 10, 'Se tiene 10 registros en la bd');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Pago#findById', async t => {
  const { Pago } = services;
  const id = 1;

  let res = await Pago.findById(id);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.id, id, 'Se recuperó el registro mediante un id');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Pago#createOrUpdate - update', async t => {
  const { Pago } = services;
  const newData = { id: 10, estado: 'PENDIENTE' };

  let res = await Pago.createOrUpdate(newData);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.estado, newData.estado, 'Actualizando registro Pago');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Pago#createOrUpdate - new', async t => {
  const { Pago } = services;

  const nuevoPago = {
    cpt: { 'name': 'Book the First', 'author': { 'first_name': 'Bob', 'last_name': 'White' } },
    tipo: 'ENTIDAD',
    fecha_pago: '2017-11-10',
    estado: 'PENDIENTE',
    id_tramite_doc: 1,
    nro_documento: '123456',
    titular_factura: 'CONDE',
    _user_created: 1,
    _created_at: new Date()
  };

  let res = await Pago.createOrUpdate(nuevoPago);
  let pagoVar = res.data;

  t.is(res.code, 1, 'Respuesta correcta');

  t.true(typeof pagoVar.id === 'number', 'Comprobando que el nuevo tramite tenga un id');
  t.is(pagoVar.tipo, nuevoPago.tipo, 'Creando registro - tipo');
  t.is(pagoVar.estado, nuevoPago.estado, 'Creando registro - estado');
  t.is(pagoVar.id_tramite_doc, nuevoPago.id_tramite_doc, 'Creando registro - id_tramite_doc');

  test.id = pagoVar.id;
});

test.serial('Pago#findAll#filter#Tipo ', async t => {
  const { Pago } = services;
  let res = await Pago.findAll({ tipo: 'CANCILLERIA' });

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data.count >= 1, 'Se tiene registros en la bd');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Pago#findAll#filter#Estado', async t => {
  const { Pago } = services;
  let res = await Pago.findAll({ estado: 'HABILITADO' });

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data.count >= 1, 'Se tiene registros en la bd');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Pago#findAll#filter#nroDocumento', async t => {
  const { Pago } = services;
  let res = await Pago.findAll({ nro_documento: '123456' });

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data.count >= 1, 'Se tiene registros en la bd');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Pago#findAll#filter#titularFactura', async t => {
  const { Pago } = services;
  let res = await Pago.findAll({ titular_factura: 'CONDE' });

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data.count >= 1, 'Se tiene registros en la bd');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Pago#delete', async t => {
  const { Pago } = services;
  let res = await Pago.deleteItem(test.id);

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data, 'Pago eliminado');
  t.is(res.message, 'OK', 'Mensaje correcto');
});
