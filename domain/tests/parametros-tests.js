'use strict';

const test = require('ava');
const { errors, config } = require('common');
const domain = require('../');

let services;

test.beforeEach(async () => {
  if (!services) {
    services = await domain(config.db).catch(errors.handleFatalError);
  }
});

test.serial('Parametro#findAll', async t => {
  const { Parametro } = services;
  let res = await Parametro.findAll();

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data.count >= 9, 'Se tiene más de 9 registros en la bd');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Parametro#findById', async t => {
  const { Parametro } = services;
  const id = 1;

  let res = await Parametro.findById(id);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.id, id, 'Se recuperó el registro mediante un id');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Parametro#getParameter', async t => {
  const { Parametro } = services;
  const nombre = 'PATH_DOCS';

  let res = await Parametro.getParameter(nombre);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.nombre, nombre, 'Se recuperó el registro mediante un nombre');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Parametro#createOrUpdate - new', async t => {
  const { Parametro } = services;
  const nuevoParametro = {
    nombre: `NUEVO-${parseInt(Math.random() * 100000)}`,
    valor: 'el valor',
    label: 'el nuevo label',
    descripcion: 'askljdfklasd',
    _user_created: 1,
    _created_at: new Date()
  };

  let res = await Parametro.createOrUpdate(nuevoParametro);
  let parametro = res.data;

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(typeof parametro.id === 'number', 'Comprobando que el nuevo parametro tenga un id');
  t.is(parametro.nombre, nuevoParametro.nombre, 'Creando registro - nombre');
  t.is(parametro.valor, nuevoParametro.valor, 'Creando registro - valor');
  t.is(parametro.label, nuevoParametro.label, 'Creando registro - label');
  t.is(parametro.descripcion, nuevoParametro.descripcion, 'Creando registro - descripcion');
  t.is(res.message, 'OK', 'Mensaje correcto');

  test.idParametro = parametro.id;
});

test.serial('Parametro#createOrUpdate - update', async t => {
  const { Parametro } = services;
  const newData = {
    id: test.idParametro,
    valor: 'Nuevo valor de parámetro'
  };

  let res = await Parametro.createOrUpdate(newData);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.valor, newData.valor, 'Actualizando registro Parametro');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Parametro#findAll#filter#archivo-nombre - not found', async t => {
  const { Parametro } = services;
  let res = await Parametro.findAll({ nombre: 'Esto no existe' });

  t.is(res.code, 0, 'Respuesta con advertencia');
  t.is(res.message, 'No existen parametros', 'Mensaje correcto');
});

test.serial('Parametro#delete', async t => {
  const { Parametro } = services;
  let res = await Parametro.deleteItem(test.idParametro);

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data, 'Parametro eliminado');
  t.is(res.message, 'OK', 'Mensaje correcto');
});
