'use strict';

const test = require('ava');
const { errors, config } = require('common');
const domain = require('../');

let services;

test.beforeEach(async () => {
  if (!services) {
    services = await domain(config.db).catch(errors.handleFatalError);
  }
});

test.serial('Sms#enviar', async t => {
  const { Sms } = services;
  const mensaje = `APOSTILLA - CODIGO DE SEGUIMIENTO: ABC123 - VISITE https://test.agetic.gob.bo/apostilla/#/seguimiento PARA VER EL ESTADO DE SU TRAMITE.`;
  const telefono = ['70520083', '70523060'];
  let res = await Sms.enviar(mensaje, telefono);
  console.log('RESPUESTA SMS - enviar', res);
  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.message, 'OK', 'Mensaje correcto');
});
