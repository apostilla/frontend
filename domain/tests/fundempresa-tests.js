'use strict';

const test = require('ava');
const { errors, config } = require('common');
const domain = require('../');

let services;

test.beforeEach(async () => {
  if (!services) {
    services = await domain(config.db).catch(errors.handleFatalError);
  }
});

test.serial('Fundempresa#buscarMatriculas', async t => {
  const { Fundempresa } = services;
  let res = await Fundempresa.buscarMatriculas('283672023');
  console.log('RESPUESTA Matrículas - Fundempresa ', res);
  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.message, 'OK', 'Mensaje correcto');
});
