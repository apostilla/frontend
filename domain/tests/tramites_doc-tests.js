'use strict';

const test = require('ava');
const { errors, config } = require('common');
const domain = require('../');

let services;

test.beforeEach(async () => {
  if (!services) {
    services = await domain(config.db).catch(errors.handleFatalError);
  }
});

test.serial('TramiteDoc#findAll', async t => {
  const { TramiteDoc } = services;
  let res = await TramiteDoc.findAll();

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data.count >= 20, 'Se tiene 20 registros en la bd');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('TramiteDoc#findById', async t => {
  const { TramiteDoc } = services;
  const id = 1;

  let res = await TramiteDoc.findById(id);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.id, id, 'Se recuperó el registro mediante un id');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('TramiteDoc#createOrUpdate - update', async t => {
  const { TramiteDoc } = services;
  const newData = {
    id: 1,
    cod_seguridad: '5353435d22' + Math.floor((Math.random() * 1000) + 1)
  };

  let res = await TramiteDoc.createOrUpdate(newData);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.cod_seguridad, newData.cod_seguridad, 'Actualizando registro TramiteDoc');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('TramiteDoc#createOrUpdate - new', async t => {
  const { TramiteDoc } = services;
  const nuevoTramiteDoc = {
    pago_entidad: true,
    pago_cancilleria: true,
    cod_seguridad: 'sdfjl5345' + Math.floor((Math.random() * 1000) + 1),
    nro_apostilla: '45345',
    fecha_expedicion: new Date(),
    estado: 'FIRMADO_ENTIDAD',
    observacion: '',
    id_tramite: 1,
    id_documento: 1,
    id_archivo: 1,
    _user_created: 1,
    _created_at: new Date()
  };

  let res = await TramiteDoc.createOrUpdate(nuevoTramiteDoc);
  let tramiteDoc = res.data;

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(typeof tramiteDoc.id === 'number', 'Comprobando que el nuevo tramiteDoc tenga un id');
  t.is(tramiteDoc.pago_entidad, nuevoTramiteDoc.pago_entidad, 'Creando registro - pago_entidad');
  t.is(tramiteDoc.pago_cancilleria, nuevoTramiteDoc.pago_cancilleria, 'Creando registro - pago_cancilleria');
  t.is(tramiteDoc.cod_seguridad, nuevoTramiteDoc.cod_seguridad, 'Creando registro - cod_seguridad');
  t.is(tramiteDoc.nro_apostilla, nuevoTramiteDoc.nro_apostilla, 'Creando registro - nro_apostilla');
  t.is(tramiteDoc.fecha_expedicion.getTime(), nuevoTramiteDoc.fecha_expedicion.getTime(), 'Creando registro - fecha_expedicion');
  t.is(tramiteDoc.estado, nuevoTramiteDoc.estado, 'Creando registro - estado');
  t.is(tramiteDoc.observacion, nuevoTramiteDoc.observacion, 'Creando registro - observacion');
  t.is(tramiteDoc.id_tramite, nuevoTramiteDoc.id_tramite, 'Creando registro - id_tramite');
  t.is(tramiteDoc.id_documento, nuevoTramiteDoc.id_documento, 'Creando registro - id_documento');
  t.is(tramiteDoc.id_archivo, nuevoTramiteDoc.id_archivo, 'Creando registro - id_archivo');
  t.is(res.message, 'OK', 'Mensaje correcto');

  test.idTramiteDoc = tramiteDoc.id;
});

test.serial('TramiteDoc#findAll#filter#archivo-cod_seguridad - not found', async t => {
  const { TramiteDoc } = services;
  let res = await TramiteDoc.findAll({ cod_seguridad: 'noexisteesto' });

  t.is(res.code, 0, 'Respuesta con advertencia');
  t.is(res.message, 'No existen tramites_doc', 'Mensaje correcto');
});

test.serial('TramiteDoc#delete', async t => {
  const { TramiteDoc } = services;
  let res = await TramiteDoc.deleteItem(test.idTramiteDoc);

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data, 'TramiteDoc eliminado');
  t.is(res.message, 'OK', 'Mensaje correcto');
});
