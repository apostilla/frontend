'use strict';

const test = require('ava');
const { errors, config } = require('common');
const domain = require('../');

let services;

test.beforeEach(async () => {
  if (!services) {
    services = await domain(config.db).catch(errors.handleFatalError);
  }
});

test.serial('Departamento#findAll', async t => {
  const { Departamento } = services;
  let res = await Departamento.findAll();

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.count, 9, 'Se tiene 10 registros en la bd');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Departamento#findById', async t => {
  const { Departamento } = services;
  const id = 1;

  let res = await Departamento.findById(id);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.id, id, 'Se recuperó el registro mediante un id');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Departamento#createOrUpdate - new', async t => {
  const { Departamento } = services;

  // let lista = await Departamento.findAll();
  const nuevoDepartamento = {
    // id: lista.data.count + 1000,
    id: Math.random() * (50000 - 11),
    nombre: 'test',
    codigo: 'TEST',
    _user_created: 1,
    _created_at: new Date()
  };

  let res = await Departamento.createOrUpdate(nuevoDepartamento);
  let departamentoVar = res.data;

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(typeof departamentoVar.id === 'number', 'Comprobando que el nuevo Departamento tenga un id');
  t.is(departamentoVar.Departamento, nuevoDepartamento.Departamento, 'Creando registro - Departamento');
  t.is(departamentoVar.codigo, nuevoDepartamento.codigo, 'Creando registro - codigo');
  t.is(departamentoVar.nombre, nuevoDepartamento.nombre, 'Creando registro - nombre');
  t.is(res.message, 'OK', 'Mensaje correcto');

  test.id = departamentoVar.id;
});

test.serial('Departamento#createOrUpdate - update', async t => {
  const { Departamento } = services;
  const newData = { id: test.id, codigo: 'NN' };

  let res = await Departamento.createOrUpdate(newData);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.codigo, newData.codigo, 'Actualizando registro Departamento');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Departamento#findAll#filter#Departamento ', async t => {
  const { Departamento } = services;
  let res = await Departamento.findAll({ codigo: 'abcdes' });

  t.is(res.code, 0, 'Respuesta con advertencia');
  t.is(res.message, 'No existen departamentos', 'Mensaje correcto');
});

test.serial('Departamento#findAll#filter#codigo', async t => {
  const { Departamento } = services;
  let variable = 'CBBA';
  let res = await Departamento.findAll({ codigo: variable });
  let lista = res.data;

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(lista.count > 0, 'Se recuperó el registro mediante un codigo');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Departamento#delete', async t => {
  const { Departamento } = services;
  let res = await Departamento.deleteItem(test.id);

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data, 'Departamento eliminado');
  t.is(res.message, 'OK', 'Mensaje correcto');
});
