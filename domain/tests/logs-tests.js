'use strict';

const test = require('ava');
const { errors, config } = require('common');
const domain = require('../');

let services;

test.beforeEach(async () => {
  if (!services) {
    services = await domain(config.db).catch(errors.handleFatalError);
  }
});

// test.serial('Log#findAll', async t => {
//   const { Log } = services;
//   let res = await Log.findAll();

//   t.is(res.code, 1, 'Respuesta correcta');
//   t.is(res.data.count, 50, 'Se tiene 50 registros en la bd');
//   t.is(res.message, 'OK', 'Mensaje correcto');
// });

test.serial('Log#findById', async t => {
  const { Log } = services;
  const id = 1;

  let res = await Log.findById(id);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.id, id, 'Se recuperó el registro mediante un id');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Log#createOrUpdate - update', async t => {
  const { Log } = services;
  const newData = {
    id: 2,
    nivel: 'ERROR',
    tipo: 'BASE DE DATOS',
    mensaje: 'Sequelize bd',
    ip: '255.255.255.33',
    referencia: 'sequelize bd error',
    fecha: new Date()
  };

  let res = await Log.createOrUpdate(newData);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.mensaje, newData.mensaje, 'Actualizando registro nombre');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Log#createOrUpdate - new', async t => {
  const { Log } = services;
  const nuevoLog = {
    nivel: 'ERROR',
    tipo: 'BASE DE DATOS',
    mensaje: 'Error sequelize',
    referencia: 'Muchos errores sequelize',
    ip: '255.255.255.1',
    fecha: new Date()
  };

  let res = await Log.createOrUpdate(nuevoLog);
  let log = res.data;

  t.true(typeof log.id === 'number', 'Comprobando que el nuevo log tenga un id');
  t.is(log.tipo, nuevoLog.tipo, 'Creando registro - tipo');
  t.is(log.mensaje, nuevoLog.mensaje, 'Creando registro - mensaje');
  t.is(log.referencia, nuevoLog.referencia, 'Creando registro - referencia');
  t.is(log.ip, nuevoLog.ip, 'Creando registro - ip');
  t.is(log.fecha.getTime(), nuevoLog.fecha.getTime(), 'Creando registro - fecha');

  test.idLog = log.id;
});

test.serial('Log#findAll#filter#mensaje', async t => {
  const { Log } = services;
  let res = await Log.findAll({ mensaje: 'sequel' });

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.count, 3, 'Se tiene 2 registros en la bd');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Log#findAll#filter#tipo', async t => {
  const { Log } = services;
  let res = await Log.findAll({ tipo: 'BASE DE DATOS' });

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.count, 3, 'Se tiene 2 registros en la bd');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

// test.serial('Log#findAll#filter#referencia', async t => {
//   const { Log } = services;
//   let res = await Log.findAll({ referencia: 'seque' });

//   t.is(res.code, 1, 'Respuesta correcta');
//   t.is(res.data.count, 3, 'Se tiene 2 registros en la bd');
//   t.is(res.message, 'OK', 'Mensaje correcto');
// });

test.serial('Log#findAll#filter#ip', async t => {
  const { Log } = services;
  let res = await Log.findAll({ ip: '255.255.255' });

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.count, 3, 'Se tiene 2 registros en la bd');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Log#delete', async t => {
  const { Log } = services;
  let res = await Log.deleteItem(test.idLog);

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data, 'Log eliminado');
  t.is(res.message, 'OK', 'Mensaje correcto');
});
