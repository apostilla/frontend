'use strict';

const test = require('ava');
const { errors, config } = require('common');
const domain = require('../');

let services;

test.beforeEach(async () => {
  if (!services) {
    services = await domain(config.db).catch(errors.handleFatalError);
  }
});

test.serial('Documento#findAll', async t => {
  const { Documento } = services;
  let res = await Documento.findAll();

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.count, 20, 'Se tiene 20 registros en la bd');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Documento#findById', async t => {
  const { Documento } = services;
  const id = 1;

  let res = await Documento.findById(id);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.id, id, 'Se recuperó el registro mediante un id');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Documento#createOrUpdate - update', async t => {
  const { Documento } = services;
  const newData = {
    id: 2,
    nombre: 'Este es el nuevo nombre'
  };

  let res = await Documento.createOrUpdate(newData);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.nombre, newData.nombre, 'Actualizando registro Documento');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Documento#createOrUpdate - new', async t => {
  const { Documento } = services;
  const nuevoDocumento = {
    id_entidad: 1,
    nombre: 'Otro nomas mas.',
    descripcion: 'Todo bien con este documento',
    estado: 'ACTIVO',
    token_ppte: 'uuu4535345jk345354j3j',
    codigo_portal: 'dddddd',
    _user_created: 1,
    _created_at: new Date()
  };

  let res = await Documento.createOrUpdate(nuevoDocumento);
  let documento = res.data;

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(typeof documento.id === 'number', 'Comprobando que el nuevo documento tenga un id');
  t.is(documento.nombre, nuevoDocumento.nombre, 'Creando registro - nombre');
  t.is(res.message, 'OK', 'Mensaje correcto');

  test.idDocumento = documento.id;
});

test.serial('Documento#findAll#filter#archivo-id_entidad - not found', async t => {
  const { Documento } = services;
  let res = await Documento.findAll({ id_entidad: 99 });

  t.is(res.code, 0, 'Respuesta con advertencia');
  t.is(res.message, 'No existen documentos', 'Mensaje correcto');
});

test.serial('Documento#delete', async t => {
  const { Documento } = services;
  let res = await Documento.deleteItem(test.idDocumento);

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data, 'Documento eliminado');
  t.is(res.message, 'OK', 'Mensaje correcto');
});
