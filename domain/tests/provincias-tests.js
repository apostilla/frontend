'use strict';

const test = require('ava');
const { errors, config } = require('common');
const domain = require('../');

let services;

test.beforeEach(async () => {
  if (!services) {
    services = await domain(config.db).catch(errors.handleFatalError);
  }
});

test.serial('Provincia#findAll', async t => {
  const { Provincia } = services;
  let res = await Provincia.findAll();

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.count, 112, 'Se tiene 112 registros en la bd');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Provincia#findById', async t => {
  const { Provincia } = services;
  const id = 1;

  let res = await Provincia.findById(id);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.id, id, 'Se recuperó el registro mediante un id');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Provincia#createOrUpdate - update', async t => {
  const { Provincia } = services;
  const newData = { id: 1, id_departamento: 1 };

  let res = await Provincia.createOrUpdate(newData);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.id_departamento, newData.id_departamento, 'Actualizando registro Provincia');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Provincia#createOrUpdate - new', async t => {
  const { Provincia } = services;

  const nuevoProvincia = {
    id: Math.random() * (50000 - 113),
    nombre: 'test',
    id_departamento: 1,
    _user_created: 1,
    _created_at: new Date()
  };
  let res = await Provincia.createOrUpdate(nuevoProvincia);
  let provincia = res.data;

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(typeof provincia.id === 'number', 'Comprobando que el nuevo provincia tenga un id');
  t.is(provincia.provincia, nuevoProvincia.provincia, 'Creando registro - provincia');
  t.is(provincia.id_departamento, nuevoProvincia.id_departamento, 'Creando registro - id_departamento');
  t.is(provincia.nombre, nuevoProvincia.nombre, 'Creando registro - nombre');

  t.is(res.message, 'OK', 'Mensaje correcto');
  test.id = provincia.id;
});

test.serial('Provincia#findAll#filter#idDepartamento', async t => {
  const { Provincia } = services;
  let res = await Provincia.findAll({ id_departamento: 1 });

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Provincia#delete', async t => {
  const { Provincia } = services;
  let res = await Provincia.deleteItem(test.id);

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data, 'Provincia eliminado');
  t.is(res.message, 'OK', 'Mensaje correcto');
});
