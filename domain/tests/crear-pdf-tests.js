'use strict';

const test = require('ava');
const { errors, config } = require('common');
const domain = require('../');

let services;

test.beforeEach(async () => {
  if (!services) {
    services = await domain(config.db).catch(errors.handleFatalError);
  }
});

test.serial('CreatePdf#apostillaDoc', async t => {
  const { CreatePdf } = services;
  let myObj = {
    pais: 'Bolivia',
    firmadoPor: 'Juan Perez',
    cargo: 'Notario oficial',
    selloDe: 'Ana Perez',
    lugar: 'La Paz',
    fecha: '16/07/2018',
    por: 'Chavo del Ocho',
    nroApostilla: '1234-34-3422-22',
    sello: 'Sello',
    firma: 'La Firma'
  };

  let res = await CreatePdf.apostillaDoc(myObj);
  console.log('RESPUESTA crearcoion de apotilla - CreatePDF ', res);
  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.message, 'OK', 'Mensaje correcto');
});
