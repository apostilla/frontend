'use strict';

const test = require('ava');
const { errors, config } = require('common');
const domain = require('../');

let services;

test.beforeEach(async () => {
  if (!services) {
    services = await domain(config.db).catch(errors.handleFatalError);
  }
});

test.serial('Firma#findAll', async t => {
  const { Firma } = services;
  let res = await Firma.findAll();

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.count, 10, 'Se tiene 10 registros en la bd');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Firma#findById', async t => {
  const { Firma } = services;
  const id = 1;

  let res = await Firma.findById(id);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.id, id, 'Se recuperó el registro mediante un id');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Firma#createOrUpdate - update', async t => {
  const { Firma } = services;
  const newData = {
    id: 2,
    id_usuario: 1
  };

  let res = await Firma.createOrUpdate(newData);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.id_usuario, newData.id_usuario, 'Actualizando registro Firma');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Firma#createOrUpdate - new', async t => {
  const { Firma } = services;
  const nuevoFirma = {
    base64: 'erqq534fsadfsadfasfasfasf443423hhsrqw3',
    fecha_firma: new Date(),
    id_archivo: 1,
    id_usuario: 1,
    _user_created: 1,
    _created_at: new Date()
  };

  let res = await Firma.createOrUpdate(nuevoFirma);
  let firma = res.data;

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(typeof firma.id === 'number', 'Comprobando que el nuevo firma tenga un id');
  t.is(firma.base64, nuevoFirma.base64, 'Creando registro - base64');
  t.is(firma.fecha_firma.getTime(), nuevoFirma.fecha_firma.getTime(), 'Creando registro - fecha_firma');
  t.is(firma.id_archivo, nuevoFirma.id_archivo, 'Creando registro - id_archivo');
  t.is(firma.id_usuario, nuevoFirma.id_usuario, 'Creando registro - id_usuario');
  t.is(res.message, 'OK', 'Mensaje correcto');

  test.idFirma = firma.id;
});

test.serial('Firma#findAll#filter#archivo-id_usuario - not found', async t => {
  const { Firma } = services;
  let res = await Firma.findAll({ id_usuario: 99 });

  t.is(res.code, 0, 'Respuesta con advertencia');
  t.is(res.message, 'No existen firmas', 'Mensaje correcto');
});

test.serial('Firma#delete', async t => {
  const { Firma } = services;
  let res = await Firma.deleteItem(test.idFirma);

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data, 'Firma eliminado');
  t.is(res.message, 'OK', 'Mensaje correcto');
});
