'use strict';

const test = require('ava');
const { errors, config } = require('common');
const domain = require('../');

let services;

test.beforeEach(async () => {
  if (!services) {
    services = await domain(config.db).catch(errors.handleFatalError);
  }
});

test.serial('Tramite#findAll', async t => {
  const { Tramite } = services;
  let res = await Tramite.findAll();

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data.count >= 20, 'Se tiene 20 registros en la bd');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Tramite#findById', async t => {
  const { Tramite } = services;
  const id = 1;

  let res = await Tramite.findById(id);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.id, id, 'Se recuperó el registro mediante un id');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Tramite#createOrUpdate - new', async t => {
  const { Tramite } = services;
  const nuevoTramite = {
    id_solicitante: 1,
   // id_pais_destino: 1,
    fecha_inicio: new Date(),
    estado: 'SOLICITADO',
    cod_seguimiento: '' + parseInt(Math.random() * (10000000 - 1)),
    _user_created: 1,
    _created_at: new Date()
  };

  let res = await Tramite.createOrUpdate(nuevoTramite);
  let tramite = res.data;

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(typeof tramite.id === 'number', 'Comprobando que el nuevo tramite tenga un id');
  t.is(tramite.id_solicitante, nuevoTramite.id_solicitante, 'Creando registro - id_solicitante');
  // t.is(tramite.id_pais_destino, nuevoTramite.id_pais_destino, 'Creando registro - id_pais_destino');
  t.is(tramite.fecha_inicio.getTime(), nuevoTramite.fecha_inicio.getTime(), 'Creando registro - fecha_inicio');
  t.is(tramite.estado, nuevoTramite.estado, 'Creando registro - estado');
  t.is(tramite.cod_seguimiento, nuevoTramite.cod_seguimiento, 'Creando registro - cod_seguimiento');

  t.is(res.message, 'OK', 'Mensaje correcto');

  test.id = tramite.id;
});

test.serial('Tramite#create - new', async t => {
  const { Tramite } = services;
  const nuevoTramite = {
    'tipoDoc': 'CI',
    'nroDoc': '5602708',
    'complemento': 'AB',
    'fecNacimiento': '1983-07-21T04:00:00.000Z',
    'nombres': 'RICARDO',
    'paterno': 'AGUILERA',
    'materno': 'JIMENEZ',
    'nacionalidad': 'BOLIVIA',
    'email': 'mail@mail.com',
    'telefono': '12345678',
    'documentos': [11, 9],
    '_user_created': 1
  };

  let res = await Tramite.create(nuevoTramite);
  let tramite = res.data;

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(typeof tramite.id === 'number', 'Comprobando que el nuevo tramite tenga un id');

  t.is(res.message, 'OK', 'Mensaje correcto');

  test.code = tramite.cod_seguimiento;
});

test.serial('Tramite#findByCode', async t => {
  const { Tramite } = services;

  let res = await Tramite.findByCode(test.code);
  console.log(res);

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(!!res.data, 'Se recuperó el registro mediante un code');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Tramite#createOrUpdate - update', async t => {
  const { Tramite } = services;
  const newData = {
    id: test.id,
    cod_seguimiento: '' + parseInt(Math.random() * (10000000 - 1))
  };

  let res = await Tramite.createOrUpdate(newData);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.cod_seguimiento, newData.cod_seguimiento, 'Actualizando registro Tramite');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

/*
test.serial('Tramite#findAll#filter#archivo-id_pais_destino - not found', async t => {
  const { Tramite } = services;
  let res = await Tramite.findAll({ id_pais_destino: 999 });

  t.is(res.code, 0, 'Respuesta con advertencia');
  t.is(res.message, 'No existen tramites', 'Mensaje correcto');
});
*/
test.serial('Tramite#delete', async t => {
  const { Tramite } = services;
  let res = await Tramite.deleteItem(test.id);

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data, 'Tramite eliminado');
  t.is(res.message, 'OK', 'Mensaje correcto');
});
