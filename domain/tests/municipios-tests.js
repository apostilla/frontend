'use strict';

const test = require('ava');
const { errors, config } = require('common');
const domain = require('../');

let services;

test.beforeEach(async () => {
  if (!services) {
    services = await domain(config.db).catch(errors.handleFatalError);
  }
});

test.serial('Municipio#findAll', async t => {
  const { Municipio } = services;
  let res = await Municipio.findAll();

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data.count >= 339, 'Se tiene 339 registros en la bd');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Municipio#findById', async t => {
  const { Municipio } = services;
  const id = 1;

  let res = await Municipio.findById(id);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.id, id, 'Se recuperó el registro mediante un id');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Municipio#createOrUpdate - update', async t => {
  const { Municipio } = services;
  const newData = { id: 1, id_provincia: 1 };

  let res = await Municipio.createOrUpdate(newData);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.id_provincia, newData.id_provincia, 'Actualizando registro Municipio');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Municipio#createOrUpdate - new', async t => {
  const { Municipio } = services;

  const nuevoMunicipio = {
    id: Math.random() * (50000 - 340),
    nombre: 'test',
    id_provincia: 1,
    _user_created: 1,
    _created_at: new Date()
  };

  let res = await Municipio.createOrUpdate(nuevoMunicipio);
  let municipioVar = res.data;

  t.is(res.code, 1, 'Respuesta correcta');

  t.true(typeof municipioVar.id === 'number', 'Comprobando que el nuevo Municipio tenga un id');

  t.is(municipioVar.municipio, nuevoMunicipio.municipio, 'Creando registro - municipio');
  t.is(municipioVar.id_provincia, nuevoMunicipio.id_provincia, 'Creando registro - id_provincia');
  t.is(municipioVar.nombre, nuevoMunicipio.nombre, 'Creando registro - nombre');

  t.is(res.message, 'OK', 'Mensaje correcto');

  test.id = municipioVar.id;
});

test.serial('Municipio#findAll#filter#idProvincias ', async t => {
  const { Municipio } = services;
  let res = await Municipio.findAll({ id_provincia: 1 });

  t.is(res.code, 1, 'Respuesta con advertencia');
  t.true(res.data.count > 1, 'Se recuperó el registro mediante un id_provincia');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Municipio#delete', async t => {
  const { Municipio } = services;
  let res = await Municipio.deleteItem(test.id);

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data, 'Municipio eliminado');
  t.is(res.message, 'OK', 'Mensaje correcto');
});
