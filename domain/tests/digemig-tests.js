'use strict';

const test = require('ava');
const { errors, config } = require('common');
const domain = require('../');

let services;

test.beforeEach(async () => {
  if (!services) {
    services = await domain(config.db).catch(errors.handleFatalError);
  }
});

test.serial('Digemig#buscarPersona', async t => {
  const { Digemig } = services;
  const persona = { // buscar en datos fake para probar
    nroDocumento: '27.651.163',
    fechaNacimiento: '14/10/1979'
  };
  let res = await Digemig.buscar(persona.nroDocumento, persona.fechaNacimiento);
  console.log('RESPUESTA DIGEMIG - buscar Persona', res);
  t.is(res.code, 1, 'Respuesta correcta');
  t.true(!!res.data, 'Respuesta correcta');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Digemig#certificacion', async t => {
  const { Digemig } = services;

  let data = {
    'nombres': 'RICARDO',
    'primer_apellido': 'AGUILERA',
    'segundo_apellido': 'JIMENEZ',
    'fecha_nacimiento': '21/07/1983',
    'nro_documento': '5602708',
    'email': 'ricardo@gmail.com',
    'id_tramite': 1
  };

  let res = await Digemig.certificacion(data);
  console.log('RESPUESTA DIGEMIG - CERTIFICACIÓN', res);
  t.is(res.code, 1, 'Respuesta correcta');
  t.true(typeof res.data === 'string', 'Respuesta correcta');
  t.is(res.message, 'OK', 'Mensaje correcto');
});
