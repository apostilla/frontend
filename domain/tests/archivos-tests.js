'use strict';

const test = require('ava');
const { errors, config } = require('common');
const domain = require('../');

let services;

test.beforeEach(async () => {
  if (!services) {
    services = await domain(config.db).catch(errors.handleFatalError);
  }
});

test.serial('Archivo#findAll', async t => {
  const { Archivo } = services;
  let res = await Archivo.findAll();

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.count, 10, 'Se tiene 10 registros en la bd');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Archivo#findById', async t => {
  const { Archivo } = services;
  const id = 1;

  let res = await Archivo.findById(id);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.id, id, 'Se recuperó el registro mediante un id');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Archivo#createOrUpdate - update', async t => {
  const { Archivo } = services;
  const newData = {
    id: 2,
    nombre: 'Nombre Archivo - Nuevo'
  };

  let res = await Archivo.createOrUpdate(newData);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.nombre, newData.nombre, 'Actualizando registro Archivo');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Archivo#createOrUpdate - new', async t => {
  const { Archivo } = services;
  const nuevoArchivo = {
    nombre: 'AAAA',
    titulo: 'BBBB',
    mimetype: 'PDF',
    ruta: '/home',
    tamano: 3,
    img_ancho: 3,
    img_alto: 3,
    es_imagen: true,
    _user_created: 1,
    _created_at: new Date()
  };

  let res = await Archivo.createOrUpdate(nuevoArchivo);
  let archivo = res.data;

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(typeof archivo.id === 'number', 'Comprobando que el nuevo archivo tenga un id');
  t.is(archivo.nombre, nuevoArchivo.nombre, 'Creando registro - base64');
  t.is(res.message, 'OK', 'Mensaje correcto');

  test.idArchivo = archivo.id;
});

test.serial('Archivo#findAll#filter#archivo-nombre - not found', async t => {
  const { Archivo } = services;
  let res = await Archivo.findAll({ nombre: 'No exite esto' });

  t.is(res.code, 0, 'Respuesta con advertencia');
  t.is(res.message, 'No existen archivos', 'Mensaje correcto');
});

test.serial('Archivo#delete', async t => {
  const { Archivo } = services;
  let res = await Archivo.deleteItem(test.idArchivo);

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data, 'Archivo eliminado');
  t.is(res.message, 'OK', 'Mensaje correcto');
});
