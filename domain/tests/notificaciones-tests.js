'use strict';

const test = require('ava');
const { errors, config } = require('common');
const domain = require('../');

let services;

test.beforeEach(async () => {
  if (!services) {
    services = await domain(config.db).catch(errors.handleFatalError);
  }
});

test.serial('Notificacion#findAll', async t => {
  const { Notificacion } = services;
  let res = await Notificacion.findAll();

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.count, 10, 'Se tiene 10 registros en la bd');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Notificacion#findById', async t => {
  const { Notificacion } = services;
  const id = 1;

  let res = await Notificacion.findById(id);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.id, id, 'Se recuperó el registro mediante un id');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Notificacion#createOrUpdate - update', async t => {
  const { Notificacion } = services;
  const newData = {
    id: 2,
    titulo: 'Nuevo titulo para notificacion'
  };

  let res = await Notificacion.createOrUpdate(newData);

  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.titulo, newData.titulo, 'Actualizando registro notificacion');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('Notificacion#createOrUpdate - new', async t => {
  const { Notificacion } = services;
  const nuevoNotificacion = {
    id_sender: 1,
    id_receiver: 1,
    tipo: 'MENSAJE',
    titulo: 'Titulo random',
    email_sender: 'yo@gmail.com',
    mensaje: 'Este es el mensjae',
    estado: 'VISTO',
    _user_created: 1,
    _created_at: new Date()
  };

  let res = await Notificacion.createOrUpdate(nuevoNotificacion);
  let notificacion = res.data;

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(typeof notificacion.id === 'number', 'Comprobando que el nuevo notificacion tenga un id');
  t.is(notificacion.id_sender, nuevoNotificacion.id_sender, 'Creando registro - id_sender');
  t.is(notificacion.id_receiver, nuevoNotificacion.id_receiver, 'Creando registro - id_receiver');
  t.is(notificacion.tipo, nuevoNotificacion.tipo, 'Creando registro - tipo');
  t.is(notificacion.titulo, nuevoNotificacion.titulo, 'Creando registro - titulo');
  t.is(notificacion.email_sender, nuevoNotificacion.email_sender, 'Creando registro - email_sender');
  t.is(notificacion.mensaje, nuevoNotificacion.mensaje, 'Creando registro - mensaje');
  t.is(notificacion.estado, nuevoNotificacion.estado, 'Creando registro - estado');
  t.is(res.message, 'OK', 'Mensaje correcto');

  test.idNotificacion = notificacion.id;
});

test.serial('Notificacion#findAll#filter#archivo-titulo - not found', async t => {
  const { Notificacion } = services;
  let res = await Notificacion.findAll({ titulo: 'NO_EXISTE' });

  t.is(res.code, 0, 'Respuesta con advertencia');
  t.is(res.message, 'No existen notificaciones', 'Mensaje correcto');
});

test.serial('Notificacion#delete', async t => {
  const { Notificacion } = services;
  let res = await Notificacion.deleteItem(test.idNotificacion);

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data, 'Notificacion eliminado');
  t.is(res.message, 'OK', 'Mensaje correcto');
});
