'use strict';

const test = require('ava');
const { errors, config } = require('common');
const domain = require('../');

let services;

test.beforeEach(async () => {
  if (!services) {
    services = await domain(config.db).catch(errors.handleFatalError);
  }
});

test.serial('Correo#enviar', async t => {
  const { Correo } = services;
  const objCorreo = {
    para: ['rhuayller@agetic.gob.bo'],
    titulo: '[Cancilleria Bolivia] Tu documento ha sido apostillado!',
    mensaje: 'Este es el texto bla bla'
  }
  let res = await Correo.enviar(objCorreo);
  console.log('RESPUESTA Correo ', res);
  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.message, 'OK', 'Mensaje correcto');
});
