#!/usr/bin/env bash
echo Instalando paquetes en:
pwd
echo Instalando common
cd common
rm -rf node_modules
npm i
echo Instalando infrastructure
cd ../infrastructure
rm -rf node_modules
npm i
echo Instalando domain
cd ../domain
rm -rf node_modules
npm i
echo Instalando application
cd ../application
rm -rf node_modules
npm i
echo Instalando web
cd ../web
rm -rf node_modules
npm i
echo Finalizado