'use strict';

const debug = require('debug')('apostilla:api');
const { generateToken, generateTokenSolicitante, validarAuxiliar } = require('../lib/auth');
const api = require('./api');
const auth = require('express-jwt');
const { config } = require('common');

module.exports = async function setupApi (app, services) {
  debug('Iniciando API-REST');

  // Registrando API-REST
  app.use('/api', validarAuxiliar, api(services));

  // login
  app.post('/auth', async (req, res, next) => {
    debug('Autenticación de usuario');
    const { Usuario, Modulo, Parametro } = services;
    const { usuario, contrasena } = req.body;
    const routes = {
      1: 'entidades',
      2: 'usuarios',
      3: 'tramitesDoc',
      4: 'tramitesDoc',
      5: 'usuarios',
      6: 'tramitesDoc',
      7: 'para_firma',
      8: ''
    };
    let respuesta;

    try {
      // Verificando que exista el usuario/contraseña
      let user = await Usuario.userExist(usuario, contrasena);
      if (user.code === -1) {
        return res.status(403).send(user);
      }
      user = user.data;

      // Obteniendo menu
      let menu = await Modulo.getMenu(user.id_rol);
      let permissions = menu.data.permissions;
      menu = menu.data.menu;

      // Bloqueando el ingreso del usuario solicitante y generando el token de solicitante
      if (user.id_rol === 8) {
        console.log('-------------------------------');
        console.log(permissions);
        console.log('-------------------------------');
        console.log(generateTokenSolicitante(usuario, permissions));
        return next(new Error('No existe el usuario'));
      }

      // Generando token
      let token = await generateToken(Parametro, usuario, permissions);

      // Formateando permisos
      let permisos = {};
      permissions.map(item => (permisos[item] = true));

      respuesta = {
        menu,
        token,
        permisos,
        usuario: {
          'id': user.id,
          'usuario': user.usuario,
          'nombres': user.nombres,
          'primer_apellido': user.primer_apellido,
          'segundo_apellido': user.segundo_apellido,
          'email': user.email,
          'departamento': user.departamento,
          'id_entidad': user.id_entidad,
          'id_rol': user.id_rol,
          'entidad': user['entidad.nombre'],
          'sigla_entidad': user['entidad.sigla'],
          'rol': user['rol.nombre'],
          'lang': 'es'
        },
        redirect: routes[user.id_rol]
      };
    } catch (e) {
      return next(e);
    }
    res.send(respuesta);
  });

  app.post('/autenticar', async (req, res, next) => {
    debug('Autenticación de usuario');
    const { Auxiliar, Modulo, Parametro } = services;
    const { usuario, contrasena } = req.body;
    const routes = {
      1: 'entidades',
      2: 'usuarios',
      3: 'tramitesDoc',
      4: 'tramitesDoc',
      5: 'usuarios',
      6: 'tramitesDoc',
      7: 'para_firma',
      8: ''
    };
    let respuesta;

    try {
      // Verificando que exista el usuario/contraseña
      let user = await Auxiliar.userExist(usuario, contrasena);
      if (user.code === -1) {
        return res.status(403).send(user);
      }
      user = user.data;

      // Obteniendo menu
      let menu = await Modulo.getMenu(user.id_rol);
      let permissions = menu.data.permissions;
      menu = menu.data.menu;

      // Bloqueando el ingreso del usuario solicitante y generando el token de solicitante
      if (user.id_rol === 8) {
        console.log('-------------------------------');
        console.log(permissions);
        console.log('-------------------------------');
        console.log(generateTokenSolicitante(usuario, permissions));
        return next(new Error('No existe el usuario'));
      }

      // Generando token
      let token = await generateToken(Parametro, usuario, permissions);

      // Formateando permisos
      let permisos = {};
      permissions.map(item => (permisos[item] = true));

      respuesta = {
        menu,
        token,
        permisos,
        usuario: {
          'id': user.id,
          'usuario': user.usuario,
          'nombres': user.nombres,
          'primer_apellido': user.primer_apellido,
          'segundo_apellido': user.segundo_apellido,
          'email': user.email,
          'departamento': user.departamento,
          'id_entidad': user.id_entidad,
          'id_rol': user.id_rol,
          'entidad': user['entidad.nombre'],
          'sigla_entidad': user['entidad.sigla'],
          'rol': user['rol.nombre'],
          'lang': 'es'
        },
        redirect: routes[user.id_rol]
      };
    } catch (e) {
      return next(e);
    }
    res.send(respuesta);
  });

  // ruta para escuchar ppte cuando alguien paga cpt
  app.post('/ppte', auth(config.auth), async (req, res) => {
    debug('Escuchar ppte');
    const { idCpt } = req.body;
    const { Pago, TramiteDoc, Correo } = services;

    try {
      // Actualzar table pagos con respuesta de ppte
      let pago = await Pago.findIdCpt((idCpt + '').trim());
      if (pago.code === -1) {
        console.log(`[PAGO] No existe el CPT: ${idCpt}`);
        return res.status(400).send({ // ese es el formato de respuesta esperado por ppte
          respuesta: `No existe el CPT: ${idCpt}`
        });
      }
      debug('____________________________________________');
      debug(`Datos recepcionados \n ${JSON.stringify(pago)}`);
      const pagoObj = {
        id: pago.data.id,
        estado: 'PAGADO',
        fecha_pago: new Date()
      };
      await Pago.createOrUpdate(pagoObj); // actualizar tabla pagos
      const idTramiteDoc = parseInt(pago.data.id_tramite_doc);
      // Actualizando pago
      let data = { id: idTramiteDoc };
      // TODO: Basta con esas validaciones??, no es necesario considerar la cantidad de cuentas? y montos??
      if (pago.data.tipo === 'ENTIDAD') {
        data.pago_entidad = true;
        data.pago_cancilleria = true;
      } else {
        data.pago_cancilleria = true;
      }
      await TramiteDoc.createOrUpdate(data);
      // enviar correo de confirmacion
      const tramiteDocObj = await TramiteDoc.findById(idTramiteDoc);
      if (tramiteDocObj.data['tramite.solicitante.email']) { // verificar existencia de email
        const correo = {
          para: [tramiteDocObj.data['tramite.solicitante.email']],
          titulo: '[Apostilla] ¡Pago realizado!',
          mensaje: `
            <p>Pago realizado correctamente!</p>
            <p><strong>CPT: </strong>${pago.data.cpt.cpt}</p>
            <p>
              <strong>Detalle: </strong>: ${tramiteDocObj.data['documento.nombre']}<br />
              ${tramiteDocObj.data['documento.descripcion']}
              <br />
              <strong>Costo total: </strong>: ${tramiteDocObj.data['documento.monto'][0].monto} Bs.
            </p>
          `
        };
        Correo.enviar(correo);
      }
      res.send({ // ese es el formato de respuesta esperado por ppte
        respuesta: 'ok'
      });
    } catch (e) {
      res.error(e);
    }
  });

  return app;
};
