'use strict';

const express = require('express');
const asyncify = require('express-asyncify');
const auth = require('express-jwt');
const { config } = require('common');
const api = asyncify(express.Router());
const system = require('./system');
const eRegister = require('./eRegister');
const eApostilla = require('./eApostilla');
const publicRoute = require('./public');

module.exports = function setupApi (services) {
  // Registrando API-REST
  api.use('*', auth(config.auth));
  api.use('/system', system(services));
  api.use('/eregister', eRegister(services));
  api.use('/eapostilla', eApostilla(services));
  api.use('/public', publicRoute(services));

  return api;
};
