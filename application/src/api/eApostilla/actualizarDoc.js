'use strict';

const debug = require('debug')('apostilla:api:tramiteDoc');
const guard = require('express-jwt-permissions')();
const base64 = require('file-base64');
const md5File = require('md5-file');
const { userData } = require('../../lib/auth');
let user;

module.exports = function setupModulo (api, services) {
  api.use('*', async (req, res, next) => {
    user = await userData(req, services);
    next();
  });
  // file download part
  api.post('/update/:id_archivo', guard.check(['tramitesDoc:update']), async (req, res, next) => {
    debug('actualizando archivo del trámite con el usuario: ' + JSON.stringify(user));
    const { Parametro, Archivo, TramiteDoc, Correo, Firma, Documento, Entidad} = services;
    const archivoId = req.params.id_archivo;
    const rutaAbsoluta = await Parametro.getParameter('PATH_DOCS');
    const archivoEncontrado = await Archivo.findById(archivoId);
    const rutaDocumento = `${rutaAbsoluta.data.valor}${archivoEncontrado.data.ruta}/${archivoEncontrado.data.nombre}.pdf`;
    const { pdf_base64_signed, idTramiteDoc, emailTramiteDoc, certificado, fecha_inicio, fecha_fin } = req.body;

    let TramiteDocGuardado = await TramiteDoc.findById(idTramiteDoc);

    console.log(`Tramite guardado antes de actualizar el idarchivo a ${idTramiteDoc}: ` + JSON.stringify(TramiteDocGuardado));

    // if (TramiteDocGuardado.data.id_archivo !== 1 && TramiteDocGuardado.data.id_archivo !== idTramiteDoc) {
    //   res.send(`El archivo ${TramiteDocGuardado.data.id_archivo} que intenta actualizar no es el mismo que se tiene guardado ${idTramiteDoc}`);
    // }

    const datos = {
      id: idTramiteDoc,
      fecha_inicio,
      fecha_fin
    };
    // decoding: actualiza el archivo (escaneado) con la firma
    base64.decode(pdf_base64_signed, rutaDocumento, async (err, output) => {
      if (err) {
        return res.send(err);
      }
      if (!certificado) return res.send('No existe el certificado')

      datos.estado = user.id_rol === 4 ? 'APOSTILLADO' : 'FIRMADO_ENTIDAD';

      if (parseInt(user.id_rol) !== 7 && parseInt(user.id_rol) !== 4) {
        console.log(`El usuario ${user.usuario} no tiene rol de firmador: ${JSON.stringify(user)}`);
        return res.status(401).send({
          code: -1,
          message: `El usuario ${user.usuario} no tiene rol de firmador`
        });

      } else {
        console.log(`El usuario ${user.usuario} tiene rol de firmador`);
      }

      debug('Firmando el documento: ' + JSON.stringify(datos));

      let tramite_doc_actualizar = await TramiteDoc.findById(datos.id);

      debug('tramite_doc_actualizar: ' + JSON.stringify(tramite_doc_actualizar));

      let documento = await Documento.findById(tramite_doc_actualizar.data.id_documento);

      debug('doc encontrado: ' + JSON.stringify(documento));

      let entidad = await Entidad.findById(documento.data.id_entidad);

      debug('Entidad para recuperar: ' + JSON.stringify(entidad));


      // Actualizando trámite
      console.log('Actualizando trámite: ' + JSON.stringify(datos));
      await TramiteDoc.createOrUpdate(datos);
      const hashPdf = md5File.sync(rutaDocumento);
      debug('url', rutaDocumento);
      debug('Pdf HASH', hashPdf);

      // update tabla firmas
      let createFirma = {
        base64: hashPdf,
        fecha_firma: new Date(),
        certificado_token: certificado,
        id_archivo: parseInt(archivoId),
        id_usuario: user.id,
        _user_created: user.id
      };

      await Firma.createOrUpdate(createFirma);
      // enviar correo si existe el correo y rol del firmante es de cancilleria, idUsuario = 4
      if (emailTramiteDoc && user.id_rol === 4) {
        let urlPortal = await Parametro.getParameter('URL_PORTAL');
        urlPortal = urlPortal.data.valor;

        const correo = {
          para: [emailTramiteDoc],
          titulo: '[Apostilla] ¡Tu documento ha sido apostillado!',
          mensaje: `
            <p>¡Su documento ha sido apostillado!</p>
            <strong>Para verificar la validez de su Apostilla puede ingresar
            <strong><a href="${urlPortal}#/verificar" target="_blank">aquí</a></strong>
            o copie esta url en su navegador <a href="${urlPortal}#/verificar}" target="_blank">${urlPortal}verificar</a>.
          `,
          attachments: [{ path: rutaDocumento, filename: 'Apostilla.pdf' }]
        };

        try {
          Correo.enviar(correo);
        } catch (e) {
          console.log(new Error(`Error al enviar correo a ${emailTramiteDoc} : ${e}`));
          console.log(e);
        }

      }
      res.send(output);
    });
  });
  return api;
};
