'use strict';

const debug = require('debug')('apostilla:api:tramiteDoc');
const guard = require('express-jwt-permissions')();
const request = require('request');
const { userData } = require('../../lib/auth');
let user;

module.exports = function entidadesDocsPpte (api, services) {
  api.use('*', async (req, res, next) => {
    user = await userData(req, services);
    next();
  });

  // file download part
  api.post('/descargar_entidades_docs', guard.check([]), async (req, res, next) => {
    debug('descargando entidades documentos');
    const { Parametro, Entidad } = services;

    try {
      /* const rutaAbsoluta = await Parametro.getParameter('PATH_DOCS'); */
      const urlGetEntidades = await Parametro.getParameter('PORTAL_TRAMITES_ENTIDADES_URL');
      const token = await Parametro.getParameter('PORTAL_TRAMITES_TOKEN');
      // obtener datos desde portal unico de trámites
      var settings = {
        url: urlGetEntidades.data.valor,
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token.data.valor}`,
          'Content-Type': 'application/json'
        },
        json: true
      };
      request.get(settings, async (error, response, body) => {
        if (error) {
          console.log(error);
          return next(error);
        }

        if (body.datos && body.datos.rows) {
          // guardar datos en entidades y documentos
          const lasEntidades = body.datos.rows;
          for (let entidad of lasEntidades) {
            // Comprobando para no crear la entidad Cancillería
            if (entidad.id_entidad === 331) continue;
            // entidad
            let newEntidadd = {
              nombre: entidad.denominacion,
              descripcion: '',
              sigla: entidad.sigla,
              codigo_portal: entidad.id_entidad.toString(),
              email: entidad.contacto_correo_electronico,
              telefonos: entidad.telefono,
              direccion: '',
              web: entidad.url_sitio_web,
              _user_created: 1
            };
            await Entidad.createOrUpdate(newEntidadd);
          }
          res.send({
            code: 1,
            message: 'Sincronización completada correctamente'
          });
        } else {
          return next(new Error('No se pudo establer conexión con la Plataforma de pago'));
        }
      });
    } catch (error) {
      return next(error);
    }
  });

  // obtener tramites por id de entidad
  api.get('/obtener_docs', guard.check([]), async (req, res, next) => {
    debug('obtener documentos de entidad');
    const { Parametro, Entidad, Documento, LoginPpte } = services;

    try {
      const urlGetDocsEntidad = await Parametro.getParameter('PORTAL_TRAMITES_DOC_URL');
      const token = await Parametro.getParameter('PORTAL_TRAMITES_TOKEN');
      let entidad = await Entidad.findById(user.id_entidad); // buscar el codigo_portal segun id_entidad
      // consultar PPTE . obtener token ppte y consultar tramites.
      let respuestaPpteLogin = await LoginPpte.ppteLoginToken(entidad.data.usuario_ppte, entidad.data.pass_ppte);
      if (respuestaPpteLogin.code === -1) {
        return next(new Error(respuestaPpteLogin.message));
      }
      let tokenPpte = respuestaPpteLogin.data.token;
      let idEntidadPpte = respuestaPpteLogin.data.usuario.idEntidad;
      let respuesta = await LoginPpte.ppteInfoTramitesEntidad(tokenPpte, idEntidadPpte);
      debug('respuestaaa: ' + JSON.stringify(respuesta));

      let ppteDatosTramitesEntidadrespuesta = await respuesta.data.datos.map(x => ({
        id: x.id,
        codigoPortalUnico: x.codigoPortalUnico,
        montos: x.montos
      }));
      // obtener usuariosAplicacion. Nos dara una lista de usuarios aplicaciones, segun lo que haya en el token
      let resultUsuariosApp = await LoginPpte.ppteObtenerUsuariosApp(tokenPpte);

      // obtener datos desde portal unico de trámites
      var setting = {
        url: urlGetDocsEntidad.data.valor.includes('id_entidad') ? urlGetDocsEntidad.data.valor.replace('{id_entidad}', entidad.data.codigo_portal) : urlGetDocsEntidad.data.valor,
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token.data.valor}`,
          'Content-Type': 'application/json'
        },
        json: true
      };
      request.get(setting, async (error, response, body) => {
        if (error) {
          console.log(error);
          return next(error);
        }
        debug('body::' + JSON.stringify(body));
        if (!body || !body.datos || body.datos.length === 0) {
          return next(new Error('No se pudo obtener los documentos.'));
        }

        const datos = body.datos.rows ? body.datos.rows : body.datos;
        const data = {
          portal: [],
          usuariosApp: resultUsuariosApp.data
        };

        // Obtiene los id's de entidades
        const entidadesID = [];
        datos.map(entidad => {
          if (entidadesID.indexOf(entidad.id_entidad) === -1) entidadesID.push(entidad.id_entidad);
        });
        if (entidadesID.indexOf(user.id_entidad) === -1) entidadesID.push(user.id_entidad);

        const docsExistentes = await Documento.findAll({id_entidad: { $in: entidadesID }});
        datos.map(entidad => {
          const tramites = entidad.Tramite ? entidad.Tramite : entidad;

          // debug('Antes de mapear tramites: ' + JSON.stringify(tramites));

          if (entidad.Tramite) {
            tramites.map(tramite => {
              if (docsExistentes.data.rows.filter(item => (item.codigo_portal + '') === (tramite.id_tramite + '')).length === 0) {
                data.portal.push({
                  nombre: tramite.titulo,
                  descripcion: tramite.objetivo,
                  codigo_portal: tramite.id_tramite,
                  precio: parseFloat((tramite.costo != null ? tramite.costo : 0)),
                  monto: tramite.montos,
                  sigla_entidad: entidad.sigla,
                  id_entidad: entidad.id_entidad
                });
              }
            });
          } else {
            if (docsExistentes.data.rows.filter(item => (item.codigo_portal + '') === (tramites.id_tramite + '')).length === 0) {
              data.portal.push({
                nombre: tramites.titulo,
                descripcion: tramites.objetivo,
                codigo_portal: tramites.id_tramite,
                precio: parseFloat((tramites.costo != null ? tramites.costo : 0)),
                monto: tramites.montos,
                sigla_entidad: entidad.sigla,
                id_entidad: entidad.id_entidad
              });
            }
          }
        });

        data.datosTramitesEntidadPpte = ppteDatosTramitesEntidadrespuesta;
        res.send({
          code: 1,
          message: 'OK',
          data
        });
      });
    } catch (error) {
      return next(error);
    }
  });

  // obtener tramites por id de entidad
  api.get('/obtener_doc/:id_documento', guard.check([]), async (req, res, next) => {
    debug('obtener documentos de entidad');
    const { Entidad, LoginPpte, Documento } = services;

    try {
      let entidad = await Entidad.findById(user.id_entidad); // buscar el codigo_portal segun id_entidad
      // consultar PPTE . obtener token ppte y consultar tramites.
      let respuestaPpteLogin = await LoginPpte.ppteLoginToken(entidad.data.usuario_ppte, entidad.data.pass_ppte);
      if (respuestaPpteLogin.code === -1) {
        return next(new Error(respuestaPpteLogin.message));
      }
      let tokenPpte = respuestaPpteLogin.data.token;
      let idEntidadPpte = respuestaPpteLogin.data.usuario.idEntidad;
      let respuesta = await LoginPpte.ppteInfoTramitesEntidad(tokenPpte, idEntidadPpte);
      let ppteDatosTramitesEntidadrespuesta = respuesta.data.datos.map(x => ({
        id: x.id,
        codigoPortalUnico: x.codigoPortalUnico,
        montos: x.montos
      })); //  [ { "id": 5, "codigoPortalUnico": "C001" }, { "id": 6, "codigoPortalUnico": "1566513" } ]
      // obtener usuariosAplicacion. Nos dara una lista de usuarios aplicaciones, segun lo que haya en el token
      let resultUsuariosApp = await LoginPpte.ppteObtenerUsuariosApp(tokenPpte);
      // obtener datos desde portal unico de trámites

      const { id_documento } = req.params;
      let documento = await Documento.findById(id_documento);

      res.send({
        code: 1,
        data: {
          documento: documento.data,
          datosTramitesEntidadPpte: ppteDatosTramitesEntidadrespuesta,
          usuariosApp: resultUsuariosApp.data
        },
        message: 'OK'
      });
    } catch (error) {
      return next(error);
    }
  });

  // verificar existencia de usuario y pass de ppte.
  api.get('/verificar_datos_usuario_ppte/', guard.check([]), async (req, res, next) => {
    debug('verificar si existe usuario y password ppte en entidad');
    const { Entidad, LoginPpte } = services;

    try {
      const idEntidad = parseInt(user.id_entidad);
      const entidad = await Entidad.findById(idEntidad);
      if (!!entidad.data.usuario_ppte && !!entidad.data.pass_ppte) {
        // loggearse en ppte para verificar que datos son correctos.
        const resultado = await LoginPpte.ppteLogin(entidad.data.usuario_ppte, entidad.data.pass_ppte, idEntidad);
        if (resultado.code === -1) { // resultado no exitoso
          res.send({
            code: 1,
            data: { redirect: 'auth' },
            message: 'No se pudo conectar a la Plataforma de pagos'
          });
        } else { // resultado existoso
          res.send({
            code: 1,
            data: { redirect: 'documents' },
            message: 'La conexión con la Plataforma de pagos se realizó correctamente'
          });
        }
      } else { // no existen datos de ppte en registro de dicha entidad
        res.send({
          code: 1,
          data: { redirect: 'auth' },
          message: 'No se pudo conectar a la Plataforma de pagos'
        });
      }
    } catch (error) {
      return next(error);
    }
  });

  // obtener token de tramite en ppte.
  api.post('/obtener_token_tramite/:id_entidad_ppte/:id_tramite/:id_usuario_app', guard.check([]), async (req, res, next) => {
    debug('obtener info trámite de entidad');
    const { Entidad, LoginPpte, Documento } = services;

    try {
      const idEntidadPpte = parseInt(req.params.id_entidad_ppte);
      const idTramite = parseInt(req.params.id_tramite);
      const idUsuarioApp = parseInt(req.params.id_usuario_app);
      const idEntidadApostilla = user.id_entidad;
      const documento = req.body; // ejemplo { nombre: 'servicio nic.bo dominio .bo',descripcion: null,codigo_portal: 652,precio: 980 }
      let entidad = await Entidad.findById(idEntidadApostilla); // buscar el codigo_portal segun id_entidad en la bd de apostilla
      // consultar PPTE . obtener token ppte y consultar tramites.
      let respuestaPpteLogin = await LoginPpte.ppteLoginToken(entidad.data.usuario_ppte, entidad.data.pass_ppte);
      let tokenPpte = respuestaPpteLogin.data.token;
      console.log('TOKEN', respuestaPpteLogin);
      let respuesta = await LoginPpte.ppteInfoTramitesEntidad(tokenPpte, idEntidadPpte, idTramite);
      if (respuesta.code === -1) {
        return res.send(respuesta);
      }
      if (respuesta && respuesta.code === 1 && respuesta.data && respuesta.data.usuariosAplicacionTramites) {
        let edit = !!respuesta.data.usuariosAplicacionTramites.length;
        let id = '';
        let transicion = 'crear';
        let verbo = 'POST';
        if (edit) { // hacer un patch. actualizar
          id = `/${respuesta.data.usuariosAplicacionTramites[0].id}`; // TODO: chequear por que [0]
          transicion = 'regenerar';
          verbo = 'PATCH';
        }
        let respToken = await LoginPpte.crearTokenTramite(tokenPpte, verbo, idEntidadPpte, idTramite, id, transicion, idUsuarioApp);
        if (respToken.code === -1) {
          return res.send(respToken);
        }
        // Sumando el precio
        let precio = 0;

        // Sumatoria del precio del documento, no incluye el precio de apostilla el cual es la posicion 0
        documento.monto.map((item, indice) => { if (indice > 0) precio += item.monto; });
        let newDoc = { // crear documento
          nombre: documento.nombre,
          descripcion: documento.descripcion,
          precio,
          cuenta: documento.monto[0].cuenta,
          codigo_portal: documento.codigo_portal,
          token_ppte: respToken.data.token,
          id_entidad: entidad.data.id || idEntidadApostilla, // documento.id_entidad
          tramite_ppte: respuesta.data,
          monto: documento.monto,
          _user_created: user.id
        };

        debug('Documento_: ' + JSON.stringify(documento));
        debug('newDoc: ' + JSON.stringify(newDoc));
        let docExistenteObj = {
          codigo_portal: (documento.codigo_portal).toString()
        };
        let docExistente = await Documento.findAll(docExistenteObj, user.id_rol, idEntidadApostilla);
        if (docExistente.code === 1 && !!docExistente.data.rows.length) { // documento con ese codigo existe.
          newDoc.id = docExistente.data.rows[0].id;
        }
        await Documento.createOrUpdate(newDoc);
        res.send({
          code: 1,
          message: 'Token obtenido'
        });
      } else {
        return next(new Error('No se pudo obtener el token'));
      }
    } catch (error) {
      return next(error);
    }
  });

  // Login ppte y guardar datos en apostilla
  api.post('/login_ppte', guard.check([]), async (req, res, next) => {
    debug('login ppte y guardar datos en ppte');
    const { LoginPpte } = services;
    try {
      if (req.body) {
        const { username, password } = req.body;
        const idEntidad = user.id_entidad;
        const resultado = await LoginPpte.ppteLogin(username, password, idEntidad);
        if (resultado.code === -1) { // resultado no exitoso
          if (resultado.message && resultado.message.indexOf('EAI_AGAIN') !== -1) {
            resultado.message = 'No se pudo conectar con la Plaforma de pagos';
          }
          res.send(resultado);
        } else { // resultado existoso
          res.send({
            code: 1,
            data: true,
            message: 'Se conectó correctamente a la Plataforma de Pagos'
          });
        }
      }
    } catch (error) {
      return next(error);
    }
  });

  // generar cpt
  api.post('/generar_cpt', guard.check([]), async (req, res, next) => {
    const { LoginPpte, Documento, Correo, Pago } = services;

    try {
      debug('req.body: ' + JSON.stringify(req.body));

      const { cpt_ppte, datos_tramite_apostilla } = req.body;
      const documento = await Documento.findById(parseInt(datos_tramite_apostilla.id_documento));
      if (documento && !!documento.data.token_ppte) {
        // create cpt on ppte
        const cpteCreado = await LoginPpte.ppteCrearCpt(documento.data.token_ppte, cpt_ppte);

        if (cpteCreado.code === -1) {
          return res.send(cpteCreado);
        }
        // actualizar tabla pagos
        const pagoObjBuscar = {
          tipo: datos_tramite_apostilla.tipo_institucion,
          id_tramite_doc: datos_tramite_apostilla.id_tramite_doc
        };

        let pagos = await Pago.findAll(pagoObjBuscar, datos_tramite_apostilla.id_rol, datos_tramite_apostilla.id_entidad);

        const pagoObj = {
          id: pagos.data.rows[0].id,
          cpt: cpteCreado.data,
          id_cpt: cpteCreado.data.idCpt,
          estado: 'HABILITADO'
        };

        await Pago.createOrUpdate(pagoObj);

        // Enviar correo
        if (datos_tramite_apostilla.email) {
          const correo = {
            para: [datos_tramite_apostilla.email],
            titulo: '[Apostilla] Código de pago (CPT)',
            mensaje: `
              <p>El código de pago de su solicitud es el:</p>
              <div style="text-align: center;">
                <div style="font-size: 18px; display: inline-block; padding: 5px 8px; margin: 10px 0 20px; background-color: #eee;">
                  ${cpteCreado.data.cpt}
                </div>
              </div>
            `
          };
          Correo.enviar(correo);
        }
        res.send(cpteCreado);
      } else {
        res.send({
          code: -1,
          message: 'El documento no cuenta con un Token válido o no tiene uno para conectarse con la Plataforma de Pago, contactese con el administrador del sistema.'
        });
      }
    } catch (error) {
      return next(error);
    }
  });

  return api;
};
