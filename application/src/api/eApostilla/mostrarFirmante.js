'use strict';

const debug = require('debug')('apostilla:api:tramiteDoc');
const guard = require('express-jwt-permissions')();
const moment = require('moment');
const exec = require('child_process').exec;

module.exports = function exportCsv (api, services) {
  api.get('/mostrar_firmante/:id_archivo', guard.check(['tramitesDoc:read']), async (req, res) => {
    debug('Exportar a csv');
    const { Firma } = services;
    let resp = await Firma.findByArchivo(parseInt(req.params.id_archivo));

    if(resp.code == -1){
      res.send(resp);
    }

    let firmante = {
      firmante_nombre_completo: `${resp.data['usuario.nombres']} ${resp.data['usuario.primer_apellido']} ${resp.data['usuario.segundo_apellido']}`.replace('null', '').replace(/\s\s+/g, ' ').trim(),
      firmante_cargo: `${resp.data['usuario.cargo']}` == 'null' ? '' : `${resp.data['usuario.cargo']}`,
      firmante_telefono: `${resp.data['usuario.telefono']}` == 'null' ? '' : `${resp.data['usuario.telefono']}`,
      firmante_departamento: `${resp.data['usuario.departamento']}` == 'null' ? '' : `${resp.data['usuario.departamento']}`,
      firmante_email: `${resp.data['usuario.email']}`,
      entidad_nombre: `${resp.data['usuario.entidad.nombre']}`,
      entidad_direccion: `${resp.data['usuario.entidad.direccion']}` == 'null' ? '' : `${resp.data['usuario.entidad.direccion']}`,
      entidad_telefonos: `${resp.data['usuario.entidad.telefonos']}` == 'null' ? '' : `${resp.data['usuario.entidad.telefonos']}`,
      entidad_email: `${resp.data['usuario.entidad.email']}` == 'null' ? '' : `${resp.data['usuario.entidad.email']}`,
      fecha_firma: moment(`${resp.data['fecha_firma']}`).format('DD/MM/YYYY, h:mm a'),
      certicado_token: resp.data.certificado_token
    };
    res.send(firmante);
  });

  const ejecutarComando = (comando) => new Promise(async (resolve, reject) => {
    await exec(comando, (error, stdout, stderr) => {
      try {
        if (error) throw Error(error);
        const resp = JSON.parse(stdout);
        return resolve(resp);
      } catch (e) {
        return reject(e);
      }
    });
  });

  api.get('/mostrar_firma/:id_archivo', guard.check(['tramitesDoc:read']), async (req, res) => {

    const { Parametro, Usuario, TramiteDoc, Documento, Entidad, Archivo } = services;

    const rutaAbsoluta = await Parametro.getParameter('PATH_DOCS');

    const rutaJar = await Parametro.getParameter('PATH_JAR');

    const tramite_doc = await TramiteDoc.findByArchivoId(parseInt(req.params.id_archivo));

    const documento = await Documento.findById(tramite_doc.data.id_documento);

    const entidad = await Entidad.findById(documento.data.id_entidad);

    const user = await Usuario.findAll({
      departamento: tramite_doc.data.departamento,
      id_entidad: entidad.data.id,
      id_rol: 7
    }, 7, entidad.data.id);

    debug('usuario encontrado firmador encontrado: ' + JSON.stringify(user));

    const archivo = await Archivo.findById(parseInt(req.params.id_archivo));

    const rutaArchivo = `${rutaAbsoluta.data.valor}${archivo.data.ruta}/${archivo.data.nombre}.pdf`;

    const command = [`java -jar ${rutaJar.data.valor} -doc ${rutaArchivo}`];

    let respuesta = await ejecutarComando(command+'');
    console.log('respuesta de busqueda de firma:' + JSON.stringify(respuesta));

    try {
        let response;

        if (!respuesta.data) {
            response = {message: 'El documento no esta firmado (y tiene un formato diferente)'};
            console.log('El documento no esta firmado (y tiene un formato diferente)');
        }
        else if (parseInt(respuesta.data.length) === 0) {
            response = {message: 'El documento no esta firmado'};
            console.log('El documento no esta firmado');
        } else if (archivo.code === -1) {
            response = {message: 'Archivo no encontrado'};
            console.log('Archivo no encontrado');
        } else if (parseInt(user.data.count) === 0) {
            response = {message: 'No existe usuario firmador de entidad'};
            console.log('No existe usuario firmador de entidad');
        } else {
            response = {
                firmante_nombre_completo: `${user.data.rows[0].nombres} ${user.data.rows[0].primer_apellido} ${user.data.rows[0].segundo_apellido}`,
                firmante_cargo: `${user.data.rows[0].cargo}`,
                firmante_telefono: `${user.data.rows[0].telefono}`,
                firmante_departamento: `${user.data.rows[0].departamento}`,
                firmante_email: `${user.data.rows[0].email}`,
                entidad_nombre: `${user.data.rows[0]['entidad.nombre']}`,
                entidad_direccion: '',
                entidad_telefonos: '',
                entidad_email: '',
                fecha_firma: '',
                certicado_token: respuesta.data ? respuesta.data[0] : ''
            };
            console.log('firmante encontrado: ' + JSON.stringify(response));
        }

        res.send(response);

    } catch (e) {
        console.log(e);
        console.log('El documento no esta firmado (error al leer)');
        res.send({message: 'El documento no esta firmado'});
    }

  });

  return api;
};
