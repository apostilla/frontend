'use strict';

const debug = require('debug')('apostilla:api:tramiteDoc');
const guard = require('express-jwt-permissions')();
const fs = require('fs');
const moment = require('moment');
const pdftk = require('node-pdftk');
const exec = require('child_process').exec;
const {
	userData
} = require('../../lib/auth');
const {
	randomStr
} = require('../../lib/util');

let user;

module.exports = function setupModulo(api, services) {
	api.use('*', async (req, res, next) => {
		user = await userData(req, services);
		next();
	});

	// file download part - Creando documento apostillado y uniendo con el documento de la entidad
	api.post('/apostilla/:idArchivo/:idTramiteDoc', guard.check(['tramitesDoc:update']), async (req, res, next) => {
		debug('descargando archivo del trámite');
		console.log('[+] Este es el servicio con problemas de transaccion');

		try {
			const {
				nombreComunSubject,
				organizacionSubject
			} = req.body;
			const {
				Parametro,
				Archivo,
				CreatePdfB,
				TramiteDoc
			} = services;
			const {
				idTramiteDoc,
				idArchivo
			} = req.params;
			const rutaAbsoluta = await Parametro.getParameter('PATH_DOCS');
			const archivoEncontrado = await Archivo.findById(idArchivo);
			const rutaDocumento = `${rutaAbsoluta.data.valor}${archivoEncontrado.data.ruta}/${archivoEncontrado.data.nombre}.pdf`;

			const tramiteDocGuardado = await TramiteDoc.findById(idTramiteDoc);

			if (parseInt(tramiteDocGuardado.data.id_archivo) !== parseInt(idArchivo)) {
				return next(new Error(`El archivo a firmar ${tramiteDocGuardado.data.id_archivo} no es el mismo que se tiene guardado ${idArchivo}`));
			}

			if (user.id_rol === 4) { // es firmante de cancilleria
				console.log('Creando Apostilla + Documento Entidad - Merge | Trámite: ', idTramiteDoc);
				const estadoRevisado = tramiteDocGuardado.data.estado == 'REVISADO_CANCILLERIA';
				const tieneNumeroApostilla = tramiteDocGuardado.data.nro_apostilla !== null && tramiteDocGuardado.data.nro_apostilla !== '';
				const tieneCodigoSeguridad = tramiteDocGuardado.data.cod_seguridad !== null && tramiteDocGuardado.data.cod_seguridad !== '';

				// Si la apostilla ya fue generada pero no firmada retorna el documento
				if (estadoRevisado && tieneNumeroApostilla && tieneCodigoSeguridad) return res.sendFile(rutaDocumento);

				// throw Error('[+] muajajajaja')
				// duplicar documento
				const rutaDocumentoCopy = `${rutaAbsoluta.data.valor}${archivoEncontrado.data.ruta}/${archivoEncontrado.data.nombre}_copia.pdf`;
				const rutaDocumentoApostilla = `${rutaAbsoluta.data.valor}${archivoEncontrado.data.ruta}/${archivoEncontrado.data.nombre}_apostilla.pdf`;
				await fs.copyFile(rutaDocumento, rutaDocumentoCopy, (err) => {
					if (err) throw err;
				});
				// Obteniendo Nro de apostilla
				let apostilla = await Parametro.getParameter('NRO_APOSTILLA');
				let nroApostilla = parseInt(apostilla.data.valor) + 1;
				let codSeguridad = randomStr(10);
				let datos = {
					id: idTramiteDoc,
					cod_seguridad: codSeguridad,
					nro_apostilla: nroApostilla,
					fecha_expedicion: new Date()
				};

				/*  // buscar en tabla firmante datos del firmante anterior
				    let archivo = await Firma.findByArchivo(idArchivo);
				*/

				// buscar en tabla 'TramiteDoc' datos del firmante del documento
				let tramiteDoc = await TramiteDoc.findById(idTramiteDoc);
				console.log("tramiteDoc encontrado: " + JSON.stringify(tramiteDoc));
				if (tramiteDoc.data.estado !== 'REVISADO_CANCILLERIA') {
					console.log(`No se puede crear apostilla para el trámite en estado ${tramiteDoc.data.estado}`);
					return res.send(`No se puede crear apostilla para el trámite en estado ${tramiteDoc.data.estado}`);
				}

				// llenar datos de apostilla con las del usuario
				let myObj = {
					pais: 'Bolivia',
					firmadoPor: `${tramiteDoc.data['firmante_documento_nombre']}`.toUpperCase().replace(/\s\s+/g, ' ').trim(), // remueve espacios
					cargo: tramiteDoc.data['firmante_documento_cargo'],
					selloDe: tramiteDoc.data['firmante_documento_entidad'],
					lugar: tramiteDoc.data['departamento'],
					fecha: moment(datos.fecha_expedicion).format('DD/MM/YYYY'),
					por: nombreComunSubject,
					nroApostilla: datos.nro_apostilla,
					sello: organizacionSubject,
					firma: 'FIRMADO DIGITALMENTE',
					cod_seguridad: codSeguridad
				};

				debug('Creando apostilla con los siguientes datos: ' + JSON.stringify(myObj));

				// Actualizando trámite
				await TramiteDoc.createOrUpdate(datos);
				// Actualizando número de apostilla
				await Parametro.createOrUpdate({
					id: apostilla.data.id,
					valor: nroApostilla
				});

				// crear apostilla para el documento
				let resultCreadoApostilla = await CreatePdfB.apostillaDocB(myObj, rutaDocumentoApostilla);

				if (resultCreadoApostilla.data) {
					const files = [rutaDocumentoCopy];
					pdftk.input(rutaDocumentoApostilla)
						.attachFiles(files)
						.toPage(1)
						.output(rutaDocumento)
						.then(async (buffer) => {
							debug('Nuevo PDF : rutaDocumento ' + rutaDocumento);

							res.sendFile(rutaDocumento);
						})
						.catch(error => {
							debug('Error en nuevo PDF: ' + error);
							res.send(error);
						});
				}
			} else { // rol es de firmante de entidad entocnes solo enviar el archivo (sin la apostilla)
				res.sendFile(rutaDocumento);
			}
		} catch (error) {
			return next(error);
		}
	});

	api.post('/apostilla_para_imprimir/:idArchivo/:idTramiteDoc', guard.check(['tramitesDoc:update']), async (req, res, next) => {
		debug('descargando archivo del trámite');

		try {
			const {
				Parametro,
				Archivo,
				CreatePdfA,
				TramiteDoc,
				Firma
			} = services;
			const {
				idTramiteDoc,
				idArchivo
			} = req.params;
			const rutaAbsoluta = await Parametro.getParameter('PATH_DOCS');
			const archivoEncontrado = await Archivo.findById(idArchivo);
			const rutaDocumento = `${rutaAbsoluta.data.valor}${archivoEncontrado.data.ruta}/${archivoEncontrado.data.nombre}.pdf`;

			console.log("ruta del pdf : " + rutaDocumento);

			if (user.id_rol === 4) { // es firmante de cancilleria
				console.log('Creando Apostilla + Documento Entidad - Merge | Trámite: ', idTramiteDoc);
				// duplicar documento

				const rutaDocumentoApostilla = `${rutaAbsoluta.data.valor}${archivoEncontrado.data.ruta}/${archivoEncontrado.data.nombre}_apostilla_imprimir.pdf`;

				let tramiteDoc = await TramiteDoc.findById(idTramiteDoc);

				console.log("Tramite doc rescatado: " + JSON.stringify(tramiteDoc));

				const archivo = await Archivo.findById(tramiteDoc.data.id_archivo);

				const rutaArchivo = `${rutaAbsoluta.data.valor}${archivo.data.ruta}/${archivo.data.nombre}.pdf`;

				const rutaJar = await Parametro.getParameter('PATH_JAR');

				const command = [`java -jar ${rutaJar.data.valor} -doc ${rutaArchivo}`];

				let respuesta = await ejecutarComando(command + '');

				console.log('respuesta de busqueda de firma para hoja membretada:' + JSON.stringify(respuesta));

				let certificadoToken = respuesta.data[0];

				let nombreComunSubject = certificadoToken.nombreComunSubject;
				let organizacionSubject = certificadoToken.organizacionSubject;


				// llenar datos de apostilla con las del usuario
				let myObj = {
					pais: 'Bolivia',
					firmadoPor: `${tramiteDoc.data['firmante_documento_nombre']}`.toUpperCase().replace(/\s\s+/g, ' ').trim(), // remueve espacios
					cargo: tramiteDoc.data['firmante_documento_cargo'],
					selloDe: tramiteDoc.data['firmante_documento_entidad'],
					lugar: tramiteDoc.data['departamento'],
					fecha: moment(tramiteDoc.data['fecha_expedicion']).format('DD/MM/YYYY'),
					por: nombreComunSubject,
					nroApostilla: tramiteDoc.data['nro_apostilla'],
					sello: organizacionSubject,
					firma: 'FIRMADO DIGITALMENTE',
					cod_seguridad: tramiteDoc.data['cod_seguridad']
				};

				debug('Creando apostilla para imprimir con los siguientes datos: ' + JSON.stringify(myObj));

				// crear apostilla para el documento
				let resultCreadoApostilla = await CreatePdfA.apostillaDocA(myObj, rutaDocumentoApostilla);


				if (resultCreadoApostilla.data) {
					res.sendFile(rutaDocumentoApostilla);
				} else {
					res.send({
						finalizado: false,
						mensaje: 'Error al crear documento para impresión'
					});
				}
			} else { // rol es de firmante de entidad entocnes solo enviar el archivo (sin la apostilla)
				res.sendFile(rutaDocumento);
			}
		} catch (error) {
			console.log("Error al crear documento para impresión: " + error);
			return next(error);
		}
	});

	const ejecutarComando = (comando) => new Promise(async (resolve, reject) => {
		await exec(comando, (error, stdout, stderr) => {
			try {
				if (error) throw Error(error);
				const resp = JSON.parse(stdout);
				return resolve(resp);
			} catch (e) {
				return reject(e);
			}
		});
	});

	return api;
};
