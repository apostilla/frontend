/* req.body es un array de objetos ej
[
  {
      "nombre": "jose",
      "ciudad": "LP"
  },
{
      "nombre": "luis",
      "ciudad": "SCZ"
  }    
] */

'use strict';

const debug = require('debug')('apostilla:api:tramiteDoc');
const guard = require('express-jwt-permissions')();
const json2csv = require('json2csv');
const fs = require('fs');
const uuid = require('uuid');

module.exports = function exportCsv (api, services) {
  api.post('/csv', guard.check(['tramitesDoc:read']), async (req, res) => {
    debug('Exportar a csv');
    const { Parametro } = services;
    const rutaAbsoluta = await Parametro.getParameter('PATH_DOCS');
    const rutaRelativa = await Parametro.getParameter('PATH_CSV');// 
    const rutaCsv = `${rutaAbsoluta.data.valor}${rutaRelativa.data.valor}/${uuid.v1()}.csv`;

    const fields = Object.keys(req.body[0]);
    const myData = req.body;
    const csv = json2csv({ data: myData, fields: fields });
    fs.writeFile(rutaCsv, csv, function(err) {
      if (err) throw err;
      res.sendFile(rutaCsv);
    });
  });

  return api;
};
