'use strict';

const debug = require('debug')('apostilla:api:tramiteDoc');
const guard = require('express-jwt-permissions')();
const { generateTokenEntidad } = require('../../lib/auth');

module.exports = function setupModulo (api, services) {
  // file download part
  api.post('/generar_token/:idEntidad', guard.check(['tramitesDoc:read']), async (req, res, next) => {
    debug('Generando token para la entidad');
    const { Modulo } = services;
    const { idEntidad } = req.params;

    try {
      let menu = await Modulo.getMenu(8);
      let permissions = menu.data.permissions;
      let token = generateTokenEntidad(idEntidad, permissions);

      res.send({ token });
    } catch (err) {
      return next(err);
    }
  });
  return api;
};
