'use strict';

const debug = require('debug')('apostilla:api:usuario');
const guard = require('express-jwt-permissions')();
const { randomStr } = require('../../lib/util');

module.exports = function setupUsuario (api, services) {
  api.get('/persona-segip/:ci', guard.check(['segip:read']), async (req, res, next) => {
    debug('Buscando persona en SEGIP');
    const { SegipPersona } = services;
    const { ci } = req.params;
    const { fechaNacimiento, complemento } = req.query;

    let persona;
    try {
      persona = await SegipPersona.buscarPersona(ci, complemento, fechaNacimiento);
    } catch (e) {
      return next(e);
    }

    res.send(persona);
  });

  api.get('/persona-digemig/:nroDocumento', guard.check(['segip:read']), async (req, res, next) => {
    debug('Buscando persona en DIGEMIG');
    const { Digemig } = services;
    const { nroDocumento } = req.params;
    const { fechaNacimiento } = req.query;

    let persona;
    try {
      persona = await Digemig.buscar(nroDocumento, fechaNacimiento);
    } catch (e) {
      return next(e);
    }

    res.send(persona);
  });

  // cambiar contrasena
  api.patch('/cambiar_pass/:user_id', guard.check(['usuarios:update']), async (req, res, next) => {
    debug('Cambiar contraseña de usuario');
    const { encrypt } = require('../../../../domain/src/lib/util');
    const { Usuario } = services;
    const idUser = parseInt(req.params.user_id);
    const { password, newPassword } = req.body;
    let userFound = await Usuario.findById(idUser);

    if (userFound.data.contrasena === encrypt(password)) {
      let updateUsrObj = {
        id: idUser,
        contrasena: encrypt(newPassword)
      };
      try {
        await Usuario.createOrUpdate(updateUsrObj);
        res.send({
          finalizado: true,
          mensaje: 'Contraseña actualizada exitosamente.'
        });
      } catch (e) {
        return next(e);
      }
    } else {
      res.send({
        finalizado: false,
        mensaje: 'Contraseña original no coincide.'
      });
    }
  });

  // desactivar cuenta
  api.patch('/desactivar_cuenta/:user_id', guard.check(['usuarios:update']), async (req, res, next) => {
    debug('Desactivar cuenta de usuario');
    const { encrypt } = require('../../../../domain/src/lib/util');
    const { Usuario } = services;
    const idUser = parseInt(req.params.user_id);
    const { password } = req.body;
    let userFound = await Usuario.findById(idUser);

    if (userFound.data.contrasena === encrypt(password)) {
      let updateUsrObj = {
        id: idUser,
        estado: 'INACTIVO'
      };
      try {
        await Usuario.createOrUpdate(updateUsrObj);
        res.send({
          finalizado: true,
          mensaje: 'Cuenta desactivada exitosamente.'
        });
      } catch (e) {
        return next(e);
      }
    } else {
      res.send({
        finalizado: false,
        mensaje: 'Contraseña no coincide.'
      });
    }
  });

  api.post('/generar_pass/:user_id', guard.check(['usuarios:update']), async (req, res, next) => {
    debug('Restablecer contraseña de usuario');
    const { encrypt } = require('../../../../domain/src/lib/util');
    const { Usuario, Correo, Parametro } = services;
    const idUser = parseInt(req.params.user_id);
    let userFound = await Usuario.findById(idUser);

    if (userFound.data) {
      const newPassword = randomStr(10);
      let updateUsrObj = {
        id: idUser,
        contrasena: encrypt(newPassword)
      };
      try {
        await Usuario.createOrUpdate(updateUsrObj);

        // Enviar correo
        if (userFound.data.email) {

          let urlAdmin = await Parametro.getParameter('URL_ADMIN');
          urlAdmin = urlAdmin.data.valor;

          const correo = {
            para: [userFound.data.email],
            titulo: '[Apostilla] Contraseña Actualizada',
            mensaje: `
              <p>Estimado/a ${userFound.data.nombres}, su nueva contraseña nueva es:</p>
              <div style="text-align: center;">
                <div style="font-size: 18px; display: inline-block; padding: 5px 8px; margin: 10px 0 20px; background-color: #eee;">
                  ${newPassword}
                </div>
              </div>
              
              <strong>Use esta contraseña para iniciar sesión  
              <strong><a href="${urlAdmin}/" target="_blank">aquí</a></strong> 
              o copie esta url en su navegador <a href="${urlAdmin}" target="_blank">${urlAdmin}</a>.
              
              <div style = "margin-left: auto; margin-right:auto;"> Sistema de pagos AGETIC - Trámite Apostilla</div>
            `
          };
          Correo.enviar(correo);
        }

        res.send({
          finalizado: true,
          mensaje: 'Contraseña restablecida y enviada al correo del usuario.'
        });
      } catch (e) {
        return next(e);
      }
    } else {
      res.send({
        finalizado: false,
        mensaje: 'Usuario no encontrado'
      });
    }
  });

  return api;
};
