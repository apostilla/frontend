'use strict';

const debug = require('debug')('apostilla:api:modulo');
const guard = require('express-jwt-permissions')();
const { userData, generateToken } = require('../../lib/auth');
let user;

module.exports = function setupModulo (api, services) {
  api.use('*', async (req, res, next) => {
    user = await userData(req, services);
    next();
  });

  api.get('/menu', guard.check(['modulos:read']), async (req, res, next) => {
    debug('Obteniendo menú y permisos');
    const { Modulo, Parametro } = services;
    let menu;
    let token;
    let permisos = {};

    try {
      // Obteniendo menu
      menu = await Modulo.getMenu(user.id_rol);
      let permissions = menu.data.permissions;
      menu = menu.data.menu;

      // Generando token
      token = await generateToken(Parametro, user.usuario, permissions);

      // Formateando permisos
      permissions.map(item => (permisos[item] = true));
    } catch (e) {
      return next(e);
    }

    res.send({
      permisos,
      menu,
      token
    });
  });

  return api;
};
