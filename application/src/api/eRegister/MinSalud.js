'use strict';

const debug = require('debug')('apostilla:api:tramiteDoc');
const guard = require('express-jwt-permissions')();

module.exports = function setupTramite (api, services) {
  api.get('/minsalud/:matricula/:apPaterno/:apMaterno', guard.check(['tramites:read']), async (req, res, next) => {
    debug('buscando formación académica por matricula');

    const { MinSaludMatriculas } = services;
    const { matricula, apPaterno, apMaterno } = req.params;

    let MinSalud;
    try {
      MinSalud = await MinSaludMatriculas.buscarPersona(matricula, apPaterno, apMaterno);
    } catch (e) {
      return next(e);
    }

    res.send(MinSalud);
  });

  return api;
};
