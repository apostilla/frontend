'use strict';

const debug = require('debug')('apostilla:api:eRegister');
const express = require('express');
const asyncify = require('express-asyncify');
const { loadRoutes } = require('../../lib/util');
let routes = asyncify(express.Router());

module.exports = function setupERegister (services) {
  debug('Iniciando rutas módulo eRegister');

  return loadRoutes(__dirname, { exclude: ['index.js'] }, services, routes);
};
