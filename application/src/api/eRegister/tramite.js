'use strict';

const debug = require('debug')('apostilla:api:tramiteDoc');
const guard = require('express-jwt-permissions')();

module.exports = function setupTramite (api, services) {
  api.get('/tramite/:code', guard.check(['tramites:read']), async (req, res, next) => {
    debug('buscando trámite por código');

    const { Tramite } = services;
    const { code } = req.params;

    let tramite;
    try {
      tramite = await Tramite.findByCode(code);
    } catch (e) {
      return next(e);
    }

    res.send(tramite);
  });

  api.get('/tramiteInfo/:code', guard.check(['tramites:read']), async (req, res, next) => {
    debug('buscando trámite por código');

    const { Tramite } = services;
    const { code } = req.params;

    let tramite;
    try {
      tramite = await Tramite.findByCode2(code);
    } catch (e) {
      return next(e);
    }

    let respuesta = JSON.parse(JSON.stringify(tramite).replace(/solicitante./g, ''));
    respuesta = JSON.parse(JSON.stringify(respuesta).replace(/documento./g, ''));
    respuesta = JSON.parse(JSON.stringify(respuesta).replace(/infoDoc/g, 'documentos'));
    respuesta = JSON.parse(JSON.stringify(respuesta).replace(/FIRMADO_ENTIDAD/g, 'CREADO'));

    res.send(respuesta);
  });

  return api;
};
