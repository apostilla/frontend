'use strict';

const debug = require('debug')('apostilla:api:tramiteDoc');
const guard = require('express-jwt-permissions')();
const uuid = require('uuid');
const path = require('path');
const mime = require('mime');
const fs = require('fs');
const { userData } = require('../../lib/auth');
const { sizeFile } = require('../../lib/util');
let user;

module.exports = function setupTramiteDoc (api, services) {
  api.use('*', async (req, res, next) => {
    user = await userData(req, services);
    next();
  });

  // file upload part
  api.post('/upload/:idTramiteDoc/:idArchivo', guard.check(['tramitesDoc:update']), async (req, res, next) => {
    debug('Subiendo archivo pdf del trámite');

    if (!req.files) {
      return res.status(400).send('Debe adjuntar el documento pdf a subir.');
    }

    let nombre = uuid.v1();
    const { Parametro, Archivo, TramiteDoc } = services;
    const rutaAbsoluta = await Parametro.getParameter('PATH_DOCS');
    const rutaRelativa = await Parametro.getParameter('PATH_PDFS');//
    const titulo = req.files.file.name;
    const mimetype = 'application/pdf';
    const ruta = rutaRelativa.data.valor;
    const rutaPdf = `${rutaAbsoluta.data.valor}${ruta}/${nombre}.pdf`; // sera algo como:  /home/ramiro/Documents/documentos/b57aac20-f56f-11e7-8d9e-ddc7ba8c960a.pdf

    let TramiteDocGuardado = await TramiteDoc.findById(req.params.idTramiteDoc);

    console.log(`Tramite guardado antes de actualizar el archivo /upload/${req.params.idTramiteDoc}/${req.params.idArchivo}: ` + JSON.stringify(TramiteDocGuardado));

    if (parseInt(TramiteDocGuardado.data.id_archivo) !== parseInt(req.params.idArchivo)) {
      console.log(`El archivo ${TramiteDocGuardado.data.id_archivo} que intenta modificar no es el mismo que se tiene guardado ${req.params.idArchivo}`);
      return res.status(400).send(`El archivo que intenta modificar no es el mismo que se tiene guardado`);
    }

    const archivo = await Archivo.createOrUpdate({
      titulo,
      nombre,
      mimetype,
      ruta,
      _user_created: user.id,
      _created_at: new Date()
    });
    console.log('Archivo creado', archivo);
    let file = req.files.file;
    file.mv(rutaPdf, async function (err) { // Use the mv() method to place the file somewhere on your server
      if (err) {
        return next(new Error(err));
      }
      // actualizar id_archivo en tramites_doc con archivo creado
      await TramiteDoc.createOrUpdate({
        id: req.params.idTramiteDoc,
        id_archivo: archivo.data.id
      });
      res.send({
        code: 1,
        data: {
          id_archivo: archivo.data.id
        },
        message: 'Archivo subido y creado con éxito'
      });
    });
  });

  api.post('/uploadBase64/:idTramiteDoc', guard.check(['tramitesDoc:update']), async (req, res, next) => {
    debug('Subiendo archivo pdf del trámite');

    const { pdf, id_entidad } = req.body;

    let nombre = uuid.v1();
    const { Parametro, Archivo, TramiteDoc, Entidad } = services;
    const base64Data = pdf;
    const rutaAbsoluta = await Parametro.getParameter('PATH_DOCS');
    const rutaRelativa = await Parametro.getParameter('PATH_PDFS');

    const ruta = rutaRelativa.data.valor;
    const rutaPdf = `${rutaAbsoluta.data.valor}${ruta}/${nombre}.pdf`;

    let archivo;
    if(base64Data){
      const titulo = 'Desde base 64';
      const mimetype = 'application/pdf';

       archivo = await Archivo.createOrUpdate({
        titulo,
        nombre,
        mimetype,
        ruta,
        _user_created: user.id,
        _created_at: new Date()
      });

       let tramiteDoc = await TramiteDoc.findById(req.params.idTramiteDoc);
       debug('Tramite ' + req.params.idTramiteDoc + ' : ' + JSON.stringify(tramiteDoc));

       if (tramiteDoc.code === 1 ) {
         if (tramiteDoc.data.estado === 'APOSTILLADO') {
           return next(new Error('No se puede modificar el trámite'));
         }
       } else return next(new Error('No existe el trámite'));

      await require("fs").writeFile(rutaPdf, base64Data, 'base64', async function (err) {
        if (err) return next(new Error(err));
        debug('Nuevo PDF desde base 64: ' + rutaPdf);

        const entidad = await Entidad.findById(id_entidad);

        let estado = tramiteDoc.data.estado;

        if (entidad.data.sigla === 'DIGEMIG'){
          estado = 'REVISADO_ENTIDAD';
        }

        await TramiteDoc.createOrUpdate({
          id: req.params.idTramiteDoc,
          id_archivo: archivo.data.id,
          estado
        });
        res.send({
          code: 1,
          data: {
            id_archivo: archivo.data.id
          },
          message: { code: 1, message: 'Archivo subido y creado con éxito'}
        });

      });
    } else return res.status(400).send('Debe adjuntar el documento pdf en Base64');
  });

  // Genera contenido pdf
  api.post('/pdf', async (req, res, next) => {
    debug('Obteniendo pdf');
    const { Parametro, Archivo } = services;
    const { id_archivo } = req.body;
    const rutaAbsoluta = await Parametro.getParameter('PATH_DOCS');
    console.log('id_archivo ', id_archivo);
    const archivo = await Archivo.findById(parseInt(id_archivo));
    const rutaArchivo = `${rutaAbsoluta.data.valor}${archivo.data.ruta}/${archivo.data.nombre}.pdf`;
    console.log('rutaArchivo ', rutaArchivo);
    try {
      if (!fs.existsSync(rutaArchivo)) {
        // return next(new Error('No se encuentra el archivo solicitado.'));
        return res.send({
          code: -1,
          data: null,
          message: 'No se encuentra el archivo solicitado.'
        });
      }
      let respuesta = fs.readFileSync(rutaArchivo);
      res.send({
        code: 1,
        data: respuesta.toString('base64'),
        message: 'Pdf obtenido correctamente'
      });
    } catch (e) {
      return next(e);
    }
  });

  // Genera contenido pdf
  api.get('/pdf-download/:id_archivo', async (req, res, next) => {
    debug('Obteniendo pdf - download');
    const { Parametro, Archivo } = services;
    const { id_archivo } = req.params;
    const rutaAbsoluta = await Parametro.getParameter('PATH_DOCS');
    const archivo = await Archivo.findById(parseInt(id_archivo));
    const rutaArchivo = `${rutaAbsoluta.data.valor}${archivo.data.ruta}/${archivo.data.nombre}.pdf`;
    console.log('rutaArchivo ', rutaArchivo);
    try {
      if (!fs.existsSync(rutaArchivo)) {
        return next(new Error('No se encuentra el archivo solicitado.'));
      }

      const filename = path.basename(rutaArchivo);
      const mimetype = mime.lookup(rutaArchivo);
      res.setHeader('Content-disposition', `attachment; filename=${filename}`);
      res.setHeader('Content-type', mimetype);
      res.setHeader('Size-file', sizeFile(rutaArchivo));
      let respuesta = fs.readFileSync(rutaArchivo);
      res.send(respuesta);
    } catch (e) {
      return next(e);
    }
  });

  api.get('/pdf-download_apostilla/:id_archivo', async (req, res, next) => {
    debug('Obteniendo pdf - download');
    const { Parametro, Archivo } = services;
    const { id_archivo } = req.params;
    const rutaAbsoluta = await Parametro.getParameter('PATH_DOCS');
    const archivo = await Archivo.findById(parseInt(id_archivo));
    const rutaArchivo = `${rutaAbsoluta.data.valor}${archivo.data.ruta}/${archivo.data.nombre}_apostilla.pdf`;
    console.log('rutaArchivo ', rutaArchivo);
    try {
      if (!fs.existsSync(rutaArchivo)) {
        return next(new Error('No se encuentra el archivo solicitado.'));
      }

      const filename = path.basename(rutaArchivo);
      const mimetype = mime.lookup(rutaArchivo);
      res.setHeader('Content-disposition', `attachment; filename=${filename}`);
      res.setHeader('Content-type', mimetype);
      res.setHeader('Size-file', sizeFile(rutaArchivo));
      let respuesta = fs.readFileSync(rutaArchivo);
      res.send(respuesta);
    } catch (e) {
      return next(e);
    }
  });

  // Genera contenido pdf proxy
  api.get('/pdf-download-proxy/:id_archivo', async (req, res, next) => {
    debug('Obteniendo pdf - download');
    const { Parametro, Archivo } = services;
    const { id_archivo } = req.params;
    const rutaAbsoluta = await Parametro.getParameter('PATH_DOCS');
    const archivo = await Archivo.findById(parseInt(id_archivo));
    const rutaArchivo = `${rutaAbsoluta.data.valor}${archivo.data.ruta}/${archivo.data.nombre}.pdf`;
    console.log('rutaArchivo ', rutaArchivo);
    try {
      if (!fs.existsSync(rutaArchivo)) {
        return next(new Error('No se encuentra el archivo solicitado.'));
      }
      const filename = path.basename(rutaArchivo);
      const mimetype = mime.lookup(rutaArchivo);
      let pdf = fs.readFileSync(rutaArchivo);
      res.send({
        filename,
        mimetype,
        sizeFile: sizeFile(rutaArchivo),
        pdf: pdf.toString('base64')
      });
    } catch (e) {
      return next(e);
    }
  });

  return api;
};
