module.exports = {
  Query: `
    # Lista de documentos
    documentos(
      # Límite de la consulta para la paginación
      limit: Int,
      # Nro. de página para la paginación
      page: Int,
      # Campo a ordenar, "-campo" ordena DESC
      order: String,
      # Buscar por nombre
      nombre: String,
      # Buscar por estado
      estado: EstadoDocumento,
      # Buscar por id_entidad
      id_entidad: Int
      # Buscar por sigla
      sigla: String
    ): Documentos
    # Obtener un documento
    documento(id: Int!): Documento
  `,
  Mutation: `
    # Agregar documento
    documentoAdd(documento: NewDocumento!): Documento
    # Editar documento
    documentoEdit(id: Int!, documento: EditDocumento!): Documento
    # Eliminar documento
    documentoDelete(id: Int!): Delete
  `
};
