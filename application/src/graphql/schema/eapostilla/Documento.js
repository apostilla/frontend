module.exports = `
  # Escalar tipo Fecha
  #scalar Date
  # Escalar tipo Monto
  #scalar JSON

  # Documentos del sistema
  type Documento {
    # ID del Documento
    id: ID!
    # nombre de Documento
    nombre: String!
    # descripcion de Documento
    descripcion: String
    # tramite_ppte de Documento
    tramite_ppte: JSON
    # estado de Documento
    estado: EstadoDocumento!
    # token_ppte de Documento
    token_ppte: String
    # derivar el Documento
    derivar: String
    # Precio del documento
    precio: Float
    # Nro de cuenta para pagar el documento
    cuenta: Float
    # Objeto con montos y numeros de cuenta
    monto: JSON
    #Campos extra
    otros: JSON
    # Duración del trámite en días
    duracion: Int
    # Código del portal único de trámites
    codigo_portal: String
    # id_entidad de Documento
    id_entidad: Int!
    # entidad_nombre de Documento
    entidad_nombre: String
    # Sigla de la entidad de Documento
    entidad_codigo_portal: String
    # Codigo_portal de la entidad de Documento
    entidad_sigla: String
    # entidad_nombre de Documento
    entidad_descripcion: String
    # entidad_nombre de Documento
    entidad_tramite_ppte: String
    # entidad_nombre de Documento
    entidad_precio: String
    # Usuario que creo el registro
    _user_created: Int
    # Usuario que actualizó el registro
    _user_updated: Int
    # Fecha de creación del registro
    _created_at: Date
    # Fecha de actualización del registro
    _updated_at: Date
  }

  # Tipos de estado del Documento
  enum EstadoDocumento {
    # Documento activo
    ACTIVO
    # Documento inactivo
    INACTIVO
  }

  # Objeto para crear un Documento
  input NewDocumento {
    nombre: String!
    descripcion: String
    tramite_ppte: JSON
    token_ppte: String
    derivar: String
    codigo_portal: String
    precio: Float
    cuenta: String
    monto: JSON
    otros: JSON
    duracion: Int
    id_entidad: Int!
  }

  # Objeto para editar un Documento
  input EditDocumento {
    nombre: String
    descripcion: String
    tramite_ppte: JSON
    codigo_portal: String
    precio: Float
    cuenta: String
    monto: JSON
    otros: JSON
    estado: EstadoDocumento
    token_ppte: String
    derivar: String
    duracion: Int
    id_entidad: Int
  }
  # Objeto de paginación para Documento
  type Documentos {
    count: Int
    rows: [Documento]
  }
`;
