module.exports = `
  # Escalar tipo Fecha
  # scalar Date
  # scalar JSON

  # Firmas del sistema
  type Firma {
    # ID del Firma
    id: ID!
    # base64 de Firma
    base64: String!
    # Certificado de token de Firma
    certificado_token: JSON!
    # fecha_firma de Firma
    fecha_firma: Date!
    # id_archivo de Firma
    id_archivo: Int!
    # id_usuario de Firma
    id_usuario: Int!
    # archivo_nombre de Firma
    archivo_nombre: String
    # usuario_nombres de Firma
    usuario_nombres: String
    # Usuario que creo el registro
    _user_created: Int
    # Usuario que actualizó el registro
    _user_updated: Int
    # Fecha de creación del registro
    _created_at: Date
    # Fecha de actualización del registro
    _updated_at: Date
  }
  # Objeto para crear un Firma
  input NewFirma {
    base64: String!
    fecha_firma: Date!
    id_usuario: Int!
    id_archivo: Int!
  }

  # Objeto para editar un Firma
  input EditFirma {
    base64: String
    fecha_firma: Date
    id_usuario: Int
    id_archivo: Int
  }

  # Objeto de paginación para Firma
  type PaginationFirma {
    count: Int 
    rows: [Firma]
  }

  # Objeto de respuesta para paginación de Firmas
  type ResponseFirmas {
    code: Int
    data: PaginationFirma
    message: String
  }

  # Objeto de respuesta de Firma
  type ResponseFirma {
    code: Int
    data: Firma
    message: String
  }
`;
