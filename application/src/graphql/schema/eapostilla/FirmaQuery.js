module.exports = {
  Query: `
    # Lista de firmas
    firmas(
          # Límite de la consulta para la paginación
      limit: Int, 
      # Nro. de página para la paginación
      page: Int, 
      # Campo a ordenar, "-campo" ordena DESC
      order: String, 
      # Buscar por id_usuario
      id_usuario: Int,
      # Buscar por id_archivo
      id_archivo: Int
      # Buscar por fecha_firma
      fecha_firma: Date
    ): ResponseFirmas
    # Obtener un firma
    firma(id: Int!): ResponseFirma
  `,
  Mutation: `
    # Agregar Firma
    firmaAdd(firma: NewFirma!): ResponseFirma
    # Editar Firma
    firmaEdit(id: Int!, firma: EditFirma!): ResponseFirma
    # Eliminar Firma
    firmaDelete(id: Int!): Delete
  `
};
