module.exports = `
  # Escalar tipo Fecha

  # Archivos del sistema
  type Archivo {
    # id de Archivo
    id: ID!
    # nombre de Archivo
    nombre: String!
    # titulo de Archivo
    titulo: String
    # mimetype de Archivo
    mimetype: String!
    # ruta de Archivo
    ruta: String!
    # tamano de Archivo
    tamano: Float
    # img_ancho de Archivo
    img_ancho: Int
    # img_alto de Archivo
    img_alto: Int
    # es_imagen de Archivo
    es_imagen: Boolean
    # Usuario que creo el registro
    _user_created: Int
    # Usuario que actualizó el registro
    _user_updated: Int
    # Fecha de creación del registro
    _created_at: Date
    # Fecha de actualización del registro
    _updated_at: Date
  }

  # Objeto para crear un Archivo
  input NewArchivo {
    nombre: String!
    titulo: String
    mimetype: String!
    ruta: String!
    tamano: Float
    img_ancho: Int
    img_alto: Int
    es_imagen: Boolean!
  }

  # Objeto para editar un Archivo
  input EditArchivo {
    nombre: String
    titulo: String
    mimetype: String
    ruta: String
    tamano: Float
    img_ancho: Int
    img_alto: Int
    es_imagen: Boolean
  }

  # Objeto de paginación para Archivo
  type Archivos {
    count: Int 
    rows: [Archivo]
  }
`;
