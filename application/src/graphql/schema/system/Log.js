module.exports = `
  # Logs del sistema
  type Log {
    # ID del log
    id: ID!
    # nivel de log
    nivel: NivelLog!
    # tipo de log
    tipo: String
    # mensaje de log
    mensaje: String!
    # referencia de log
    referencia: String
    # ip de log
    ip: String
    # fecha de creación del log
    fecha: Date!
    # id de usuario
    id_usuario: Int
    # Username del usuario
    usuario_usuario: String
    # Nombres Usuario
    usuario_nombres: String
    # Primer apellido
    usuario_primer_apellido: String
    # Segundo apellid
    usuario_segundo_apellido: String
  }

  # Tipos de estado del log
  enum NivelLog {
    # Nivel ERROR
    ERROR
    # Nivel INFO
    INFO
    # Nivel ADVERTENCIA
    ADVERTENCIA
  }  

  # Objeto de paginación para log
  type Logs {
    count: Int 
    rows: [Log]
  }
`;
