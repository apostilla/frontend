module.exports = {
  Query: `
    # Lista de auxiliares
    auxiliares(
      # Límite de la consulta para la paginación
      limit: Int
      # Nro. de página para la paginación
      page: Int
      # Campo a ordenar, "-campo" ordena DESC
      order: String
      # Buscar por usuario
      usuario: String
      # Buscar por email
      email: String
      # Buscar por nombre completo
      nombre_completo: String
      # Buscar por departamento
      departamento: String
      # Buscar por estado
      estado: EstadoAuxiliar
      # Buscar por ID rol
      id_rol: Int
      # Buscar por ID entidad
      id_entidad: Int
    ): Auxiliares
    # Obtener un auxiliar
    auxiliar(id: Int!): Auxiliar
  `,
  Mutation: `
    # Agregar auxiliar
    auxiliarAdd(auxiliar: NewAuxiliar): Auxiliar
    # Editar auxiliar
    auxiliarEdit(id: Int!, auxiliar: EditAuxiliar): Auxiliar
    # Eliminar auxiliar
    auxiliarDelete(id: Int!): DeleteAuxiliar
  `
};
