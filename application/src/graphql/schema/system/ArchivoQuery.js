module.exports = {
  Query: `
    # Lista de archivos
    archivos(
      # Límite de la consulta para la paginación
      limit: Int, 
      # Nro. de página para la paginación
      page: Int, 
      # Campo a ordenar, "-campo" ordena DESC
      order: String, 
      # Buscar por nombre
      nombre: String
    ): Archivos
    # Obtener un archivo
    archivo(id: Int!): Archivo
  `,
  Mutation: `
    # Agregar archivo
    archivoAdd(archivo: NewArchivo!): Archivo
    # Editar archivo
    archivoEdit(id: Int!, archivo: EditArchivo!): Archivo
    # Eliminar archivo
    archivoDelete(id: Int!): Delete
  `
};
