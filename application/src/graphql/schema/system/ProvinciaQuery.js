module.exports = {
  Query: `
    # Lista de Provincias
    provincias(
      limit: Int, 
      page: Int, 
      order: String, 
      nombre: String, 
      id_departamento: Int
    ): Provincias
    # Obtener una provincia
    provincia(id: Int!): Provincia
  `,
  Mutation: `
    # Agregar provincia
    provinciaAdd(provincia: NewProvincia!): Provincia
    # Editar provincia
    provinciaEdit(id: Int!, provincia: EditProvincia!): Provincia
    # Eliminar provincia
    provinciaDelete(id: Int!): Delete    
  `
};
