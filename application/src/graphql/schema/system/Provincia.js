module.exports = `    

  # Provincias del sistema
  type Provincia {
    # ID del Provincia
    id: ID!        
    # Nombres del Provincia
    nombre: String!    
    # Id de Departamento
    id_departamento: Int!

    _user_created: Int
    _user_updated: Int
    _created_at: Date
    _updated_at: Date
  }

  # Objeto para crear un Provincia
  input NewProvincia {    
    nombre: String!    
    id_departamento: Int!
  }

  # Objeto para editar un Provincia
  input EditProvincia {
    id_departamento: Int!
    nombre: String!
  }

  # Objeto de paginación para Provincia
  type Provincias {
    count: Int 
    rows: [Provincia]
  }
`;
