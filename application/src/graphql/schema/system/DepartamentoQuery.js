module.exports = {
  Query: `
    # Lista de Departamentos
    departamentos(
      limit: Int, 
      page: Int, 
      order: String, 
      nombre: String, 
      codigo: String
    ): Departamentos
    # Obtener un departamento
    departamento(id: Int!): Departamento
  `,
  Mutation: `
    # Agregar departamento
    departamentoAdd(departamento: NewDepartamento!): Departamento
    # Editar departamento
    departamentoEdit(id: Int!, departamento: EditDepartamento!): Departamento
    # Eliminar departamento
    departamentoDelete(id: Int!): Delete
  `
};
