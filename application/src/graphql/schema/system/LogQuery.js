module.exports = {
  Query: `
    # Lista de logs
    logs(
      # Límite de la consulta para la paginación
      limit: Int, 
      # Nro. de página para la paginación
      page: Int, 
      # Campo a ordenar, "-campo" ordena DESC
      order: String, 
      # Buscar por nivel    
      nivel: NivelLog,
      # Buscar por tipo
      tipo: String,
      # Buscar por mensaje
      mensaje: String,
      # Buscar por referencia
      referencia: String,
      # Buscar por ip
      ip: String,
      # Buscar por fecha de creación
      fecha: Date,
      # Buscar por ID usuario
      id_usuario: String      
    ): Logs
    # Obtener un log
    log(id: Int!): Log
  `,
  Mutation: ``
};
