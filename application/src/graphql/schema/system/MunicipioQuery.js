module.exports = {
  Query: `
    # Lista de Municipios
    municipios(
      limit: Int, 
      page: Int, 
      order: String, 
      nombre: String, 
      id_provincia: Int
    ): Municipios
    # Obtener una municipio
    municipio(id: Int!): Municipio
  `,
  Mutation: `
    # Agregar municipio
    municipioAdd(municipio: NewMunicipio!): Municipio
    # Editar municipio
    municipioEdit(id: Int!, municipio: EditMunicipio!): Municipio
    # Eliminar municipio
    municipioDelete(id: Int!): Delete
  `
};
