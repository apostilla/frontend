module.exports = `    

  # Municipios del sistema
  type Municipio {
    # ID del Municipio
    id: ID!        
    # Nombres del Municipio
    nombre: String!    
    # Id de Provincia
    id_provincia: Int!

    _user_created: Int
    _user_updated: Int
    _created_at: Date
    _updated_at: Date
  }

  # Objeto para crear un Municipio
  input NewMunicipio {    
    nombre: String!    
    id_provincia: Int!   
  }

  # Objeto para editar un Municipio
  input EditMunicipio {
    id_provincia: Int!
    nombre: String!
  }

  # Objeto de paginación para Municipio
  type Municipios {
    count: Int 
    rows: [Municipio]
  }
`;
