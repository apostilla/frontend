module.exports = `
  # Escalar tipo Fecha
  scalar Date
  scalar JSON

  # Usuarios del sistema
  type Usuario {
    # ID del usuario
    id: ID!
    # Nombre de usuario
    usuario: String!
    # Contraseña del usuario
    contrasena: String!
    # Correo electrónico del usuario
    email: String
    # Nombres del usuario
    nombres: String!
    # Primer apellido
    primer_apellido: String
    # Segundo apellido
    segundo_apellido: String
    # Teléfono del usuario
    telefono: String
    # Información adicional del usuario
    tour: JSON
    # Estado del usuario
    estado: EstadoUsuario!
    # Cargo del usuario
    cargo: String
    # Departamento del usuario
    departamento: String
    # Id de la entidad
    id_entidad: Int!
    # Id de Rol
    id_rol: Int!
    # Nombre de la entidad
    entidad_nombre: String
    # Nombre del rol
    rol_nombre: String
    # Usuario que creo el registro
    _user_created: Int
    # Usuario que actualizó el registro
    _user_updated: Int
    # Fecha de creación del registro
    _created_at: Date
    # Fecha de actualización del registro
    _updated_at: Date
  }

  # Tipos de estado del usuario
  enum EstadoUsuario {
    # Usuario activo
    ACTIVO
    # Usuario inactivo
    INACTIVO
  }

  # Objeto para crear un usuario
  input NewUsuario {
    usuario: String!
    contrasena: String!
    email: String
    nombres: String!
    primer_apellido: String
    segundo_apellido: String
    telefono: String
    tour: JSON
    cargo: String
    departamento: String
    id_entidad: Int!
    id_rol: Int!
  }

  # Objeto para editar un usuario
  input EditUsuario {
    usuario: String
    email: String
    nombres: String
    primer_apellido: String
    segundo_apellido: String
    telefono: String
    tour: JSON
    estado: EstadoUsuario
    cargo: String
    departamento: String
    id_entidad: Int
    id_rol: Int
  }

  # Objeto de paginación para usuario
  type Usuarios {
    count: Int 
    rows: [Usuario]
  }

  # Objeto de respuesta de objeto eliminado
  type Delete {
    deleted: Boolean
  }
`;
