module.exports = {
  Query: `
    # Lista de parametros
    parametros(
      # Límite de la consulta para la paginación
      limit: Int, 
      # Nro. de página para la paginación
      page: Int, 
      # Campo a ordenar, "-campo" ordena DESC
      order: String, 
      # Buscar por nombre de Parametro
      nombre: String
      # Buscar por el valor del Parametro
      valor: String
    ): Parametros
    # Obtener un parametro
    parametro(id: Int!): Parametro
    # Obtener un parámetro por nombre
    parametroBuscar(name: String!): Parametro
  `,
  Mutation: `
    # Agregar parametro
    parametroAdd(parametro: NewParametro!): Parametro
    # Editar parametro
    parametroEdit(id: Int!, parametro: EditParametro!): Parametro
    # Eliminar parametro
    parametroDelete(id: Int!): Delete
  `
};
