module.exports = `

  # Parametros del sistema
  type Parametro {
    # id del Parametro
    id: ID!
    # nombre del Parametro
    nombre: String!
    # valor del Parametro
    valor: String!
    # label del Parametro
    label: String!
    # descripcion del Parametro
    descripcion: String
    # Usuario que creo el registro
    _user_created: Int
    # Usuario que actualizó el registro
    _user_updated: Int
    # Fecha de creación del registro
    _created_at: Date
    # Fecha de actualización del registro
    _updated_at: Date
  }

  # Objeto para crear un Parametro
  input NewParametro {
    nombre: String!
    valor: String!
    label: String!
    descripcion: String
  }

  # Objeto para editar un Parametro
  input EditParametro {
    nombre: String
    valor: String
    label: String
    descripcion: String
  }

  # Objeto de paginación para Parametro
  type Parametros {
    count: Int 
    rows: [Parametro]
  }

  # # Objeto de paginación para Parametro
  # type PaginationParametro {
  #   count: Int 
  #   rows: [Parametro]
  # }

  # # Objeto de respuesta para paginación de Parametros
  # type ResponseParametros {
  #   code: Int
  #   data: PaginationParametro
  #   message: String
  # }

  # # Objeto de respuesta de Parametro
  # type ResponseParametro {
  #   code: Int
  #   data: Parametro
  #   message: String
  # }
`;
