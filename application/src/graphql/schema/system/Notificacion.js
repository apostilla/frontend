module.exports = `
  # Notificacions del sistema
  type Notificacion {
    # ID de la Notificacion
    id: ID!
    # id_sender de la notificacion
    id_sender: Int
    # id_receiver de la notificacion
    id_receiver: Int
    # tipo de la notificacion
    tipo: TipoNotificacion!
    # titulo de la notificacion
    titulo: String!
    # email_sender de la notificacion
    email_sender: String
    # mensaje de la notificacion
    mensaje: String!
    estado: EstadoNotificacion!
    # Usuario que creo el registro
    _user_created: Int
    # Usuario que actualizó el registro
    _user_updated: Int
    # Fecha de creación del registro
    _created_at: Date
    # Fecha de actualización del registro
    _updated_at: Date
  }

  # Tipos de estado de la Notificacion
  enum TipoNotificacion {
    # Tipo de notificacion ALERTA
    ALERTA
    # Tipo de notificacion MENSAJE
    MENSAJE
    # Tipo de notificacion CPT
    CPT
    # Tipo de notificacion CONTACTO
    CONTACTO
    # Tipo de notificacion RECLAMO
    RECLAMO
  }
  # Estado de la Notificacion
  enum EstadoNotificacion {
    # Estado de notificacion CREADO
    CREADO
    # Estado de notificacion VISTO
    VISTO
    # Estado de notificacion LEIDO
    LEIDO
  }

  # Objeto para crear un Notificacion
  input NewNotificacion {
    tipo: TipoNotificacion!
    titulo: String!
    mensaje: String!
    estado: EstadoNotificacion!
  }

  # Objeto para editar un Notificacion
  input EditNotificacion {
    tipo: TipoNotificacion!
  }

  # Objeto de paginación para Notificacion
  type PaginationNotificacion {
    count: Int 
    rows: [Notificacion]
  }

  # Objeto de respuesta para paginación de Notificacions
  type ResponseNotificaciones {
    code: Int
    data: PaginationNotificacion
    message: String
  }

  # Objeto de respuesta de Notificacion
  type ResponseNotificacion {
    code: Int
    data: Notificacion
    message: String
  }
`;
