module.exports = {
  Query: `
    # Lista de notificaciones
    notificaciones(
      # Límite de la consulta para la paginación
      limit: Int, 
      # Nro. de página para la paginación
      page: Int, 
      # Campo a ordenar, "-campo" ordena DESC
      order: String, 
      # Buscar por tipo
      tipo: TipoNotificacion, 
      # Buscar por titulo
      titulo: String, 
      # Buscar por mensaje
      mensaje: String, 
      # Buscar por estado
      estado: EstadoNotificacion 
    ): ResponseNotificaciones
    # Obtener un notificacion
    notificacion(id: Int!): ResponseNotificacion
  `,
  Mutation: `
    # Agregar notificacion
    notificacionAdd(notificacion: NewNotificacion!): ResponseNotificacion
    # Editar notificacion
    notificacionEdit(id: Int!, notificacion: EditNotificacion!): ResponseNotificacion
    # Eliminar notificacion
    notificacionDelete(id: Int!): Delete
  `
};
