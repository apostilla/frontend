module.exports = `    

  # Departamentos del sistema
  type Departamento {
    # ID del Departamento
    id: ID!    
    # Codigo del Departamento
    codigo: String!    
    # Nombres del Departamento
    nombre: String!    
    _user_created: Int
    _user_updated: Int
    _created_at: Date
    _updated_at: Date
  }

  # Objeto para crear un Departamento
  input NewDepartamento {    
    codigo: String!    
    nombre: String!    
  }

  # Objeto para editar un Departamento
  input EditDepartamento {
    codigo: String
    nombre: String
  }

  # Objeto de paginación para Departamento
  type Departamentos {
    count: Int 
    rows: [Departamento]
  }
`;
