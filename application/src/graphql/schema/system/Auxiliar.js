module.exports = `
  # Escalar tipo Fecha
  # scalar Date
  # scalar JSON

  # Auxiliares del sistema
  type Auxiliar {
    # ID del auxiliar
    id: ID!
    # Nombre de usuario
    usuario: String!
    # Contraseña del auxiliar
    contrasena: String!
    # Correo electrónico del auxiliar
    email: String
    # Nombres del auxiliar
    nombres: String!
    # Primer apellido
    primer_apellido: String
    # Segundo apellido
    segundo_apellido: String
    # Teléfono del auxiliar
    telefono: String
    # Información adicional del auxiliar
    tour: JSON
    # Estado del auxiliar
    estado: EstadoAuxiliar!
    # Cargo del auxiliar
    cargo: String
    # Departamento del auxiliar
    departamento: String
    # Id de la entidad
    id_entidad: Int!
    # Id de Rol
    id_rol: Int!
    # Nombre de la entidad
    entidad_nombre: String
    # Nombre del rol
    rol_nombre: String
    # Auxiliar que creo el registro
    _user_created: Int
    # Auxiliar que actualizó el registro
    _user_updated: Int
    # Fecha de creación del registro
    _created_at: Date
    # Fecha de actualización del registro
    _updated_at: Date
  }

  # Tipos de estado del auxiliar
  enum EstadoAuxiliar {
    # Auxiliar activo
    ACTIVO
    # Auxiliar inactivo
    INACTIVO
  }

  # Objeto para crear un auxiliar
  input NewAuxiliar {
    usuario: String!
    contrasena: String!
    email: String
    nombres: String!
    primer_apellido: String
    segundo_apellido: String
    telefono: String
    tour: JSON
    cargo: String
    departamento: String
    id_entidad: Int!
    id_rol: Int!
  }

  # Objeto para editar un auxiliar
  input EditAuxiliar {
    usuario: String
    email: String
    nombres: String
    primer_apellido: String
    segundo_apellido: String
    telefono: String
    tour: JSON
    estado: EstadoAuxiliar
    cargo: String
    departamento: String
    id_entidad: Int
    id_rol: Int
  }

  # Objeto de paginación para auxiliar
  type Auxiliares {
    count: Int
    rows: [Auxiliar]
  }

  # Objeto de respuesta de objeto eliminado
  type DeleteAuxiliar {
    deleted: Boolean
  }
`;
