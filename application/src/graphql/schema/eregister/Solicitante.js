module.exports = `    

  # Solicitantes del sistema
  type Solicitante {
    # ID del Solicitante
    id: ID!        
    #tipo del Solicitante
    tipo: TipoSolicitante!
    #nombres del Solicitante
    nombres: String!
    #primer_apellido del Solicitante
    primer_apellido: String
    #segundo_apellido del Solicitante
    segundo_apellido: String
    #email del Solicitante
    email: String!
    #telefono del Solicitante
    telefono: String 
    #fecha_nacimiento del Solicitante
    fecha_nacimiento: Date
    #tipo_documento del Solicitante
    tipo_documento: TipoDocumentoSolicitante!
    #nro_documento del Solicitante
    nro_documento: Int!
    #direccion del Solicitante
    direccion: String
    #matricula del Solicitante
    matricula: String       

    # Id de departamento
    id_departamento: Int!
    # Nombre departamento
    departamento_nombre: String
    # Id de provincia
    id_provincia: Int!
    # Nombre provincia
    provincia_nombre: String
    # Id de municipio
    id_municipio: Int!
    # Nombre municipio
    municipio_nombre: String    

    _user_created: Int
    _user_updated: Int
    _created_at: Date
    _updated_at: Date
  }

  enum TipoSolicitante{
    NATURAL
    JURIDICA
  }

  # Tipos de estado del Solicitante
  enum TipoDocumentoSolicitante {        
    CI
    PASAPORTE
    NIT
  }

  # Objeto para crear un Solicitante
  input NewSolicitante {    
    tipo: TipoSolicitante!
    nombres: String!
    primer_apellido: String!
    segundo_apellido: String
    email: String!    
    telefono: String     
    fecha_nacimiento: Date!    
    tipo_documento: TipoDocumentoSolicitante!        
    nro_documento: Int!
    direccion: String    
    matricula: String
    id_departamento: Int!    
    id_provincia: Int!    
    id_municipio: Int!
  }

  # Objeto para editar un Solicitante
  input EditSolicitante {
    tipo: TipoSolicitante!    
    nombres: String
    primer_apellido: String
    segundo_apellido: String
    email: String!
    telefono: String 
    fecha_nacimiento: Date
    tipo_documento: TipoDocumentoSolicitante!
    nro_documento: Int
    direccion: String
    matricula: String       
    id_departamento: Int!
    id_provincia: Int!
    id_municipio: Int!
  }

  # Objeto de respuesta de Solicitante
  type Solicitantes {
    count: Int
    rows: [Solicitante]    
  }
`;
