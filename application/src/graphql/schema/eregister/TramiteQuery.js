module.exports = {
  Query: `
    # Lista de Tramites
    tramites(
      limit: Int, 
      page: Int, 
      order: String,
      estado: EstadoTramite,
      cod_seguimiento: String,
      fecha_inicio: Date,
      fecha_fin: Date,
      id_solicitante: Int
    ): Tramites
    # Obtener una tramite
    tramite(id: Int!): Tramite
  `,
  Mutation: `
    # Agregar tramite
    tramiteAdd(tramite: NewTramite!, ip: String): Tramite
    # Editar tramite
    tramiteEdit(id: Int!, tramite: EditTramite!): Tramite
    # Eliminar tramite
    tramiteDelete(id: Int!): Delete
  `
};
