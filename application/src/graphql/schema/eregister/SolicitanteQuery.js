module.exports = {
  Query: `
    # Lista de solicitante
    solicitantes(
      limit: Int, 
      page: Int, 
      order: String, 
      # Buscar por nombre completo
      nombre_completo: String, 
      tipo: String,    
      nombres: String,
      primer_apellido: String,
      segundo_apellido: String,
      email: String,
      telefono: String,
      fecha_nacimiento: Date,
      tipo_documento: String,
      nro_documento: Int,
      direccion: String,
      matricula: String,           
      id_departamento: Int,
      id_provincia: Int,
      id_municipio: Int,
      
    ): Solicitantes
    # Obtener una solicitante
    solicitante(id: Int!): Solicitante
  `,
  Mutation: `
    # Agregar solicitante
    solicitanteAdd(solicitante: NewSolicitante!): Solicitante
    # Editar solicitante
    solicitanteEdit(id: Int!, solicitante: EditSolicitante!): Solicitante
    # Eliminar solicitante
    solicitanteDelete(id: Int!): Delete
  `
};
