module.exports = {
  Query: `
    # Lista de pago
    pagos(
      limit: Int, 
      page: Int, 
      order: String,
      cpt: JSON
      tipo: TipoPago
      fecha_pago: Date
      _created_at: Date
      estado: EstadoPago
      id_tramite_doc: Int
      nro_documento: String
      titular_factura: String
    ): Pagos
    # Obtener una pago
    pago(id: Int!): Pago
  `,
  Mutation: `
    # Agregar pago
    pagoAdd(pago: NewPago!): Pago
    # Editar pago
    pagoEdit(id: Int!, pago: EditPago!): Pago
    # Eliminar pago
    pagoDelete(id: Int!): Delete
  `
};
