module.exports = `    
  # Escalar tipo Fecha
  #scalar Date
  #scalar JSON

  # TramiteDocs del sistema
  type TramiteDoc {
    # ID del TramiteDoc
    id: ID!        
    # pago_entidad del TramiteDoc
    pago_entidad: Boolean!
    # pago_cancilleria del TramiteDoc
    pago_cancilleria: Boolean!
    # cod_seguridad del TramiteDoc
    cod_seguridad: String
    # nro_apostilla del TramiteDoc
    nro_apostilla: String
    # fecha_expedicion del TramiteDoc
    fecha_expedicion: Date
    # estado del TramiteDoc
    estado: EstadoTramiteDoc!
    # observacion del TramiteDoc
    observacion: String
    # departamento del TramiteDoc
    departamento: String
    # fecha_inicio vigencia
    fecha_inicio: Date
    # fecha_fin vigencia
    fecha_fin: Date
    # firmante_documento_nombre
    firmante_documento_nombre: String,
    # firmante_documento_cargo
    firmante_documento_cargo: String,
    # firmante_documento_entidad
    firmante_documento_entidad: String,
    # Solicitud digital o impresa del tramite
    digital: Boolean!
    # Campos extra
    otros: JSON
    # ID de la entidad
    documento_id_entidad: String
    # Nombre del documento
    documento_nombre: String
    # Derivar el documento
    documento_derivar: String
    # Descripción del documento
    documento_descripcion: String
    # Precio del documento
    documento_precio: Float
    # Nro de cuenta para pagar el documento
    documento_cuenta: String
    # Objeto con montos y números de cuenta
    documento_monto: JSON
    # Duración en días de la validez del documento
    documento_duracion: Int
    # Nombre de la entidad
    documento_entidad_nombre: String
    # sigla del Nombre de la entidad
    documento_entidad_sigla: String
    # Nombre del archivo del documento
    archivo_nombre: String
    # Título para la propiedad alt del archivo
    archivo_titulo: String
    # Tamaño del archivo
    archivo_tamano: Float
    # Id de documento
    id_documento: Int!
    # Id de archivo
    id_archivo: Int!
    # Id de tramite
    id_tramite: Int!
    # tramite_cod_seguimieto de tramie
    tramite_cod_seguimiento: String
    # Fecha de inicio de la solicitud del trámite
    tramite_fecha_inicio: Date
    # Fecha final de la solicitud del trámite
    tramite_fecha_fin: Date
    # solicitante_id de Tramite doc
    tramite_solicitante_id: Int
    # solicitante_nombres de Tramite doc
    tramite_solicitante_nombres: String
    # solicitante_primer_apellido de Tramite doc
    tramite_solicitante_primer_apellido: String
    # solicitante_segundo_apellido de Tramite doc
    tramite_solicitante_segundo_apellido: String
    # Tipo documento del solicitante
    tramite_solicitante_tipo_documento: String
    # nro de documento del solicitante
    tramite_solicitante_nro_documento: String
    # observacion del solicitante
    tramite_solicitante_observacion: String
    # email del solicitante
    tramite_solicitante_email: String
    # Teléfono del solicitante
    tramite_solicitante_telefono: String
    # Fecha de nacimiento solicitante
    tramite_solicitante_fecha_nacimiento: String
    _user_created: Int
    _user_updated: Int
    _created_at: Date
    _updated_at: Date
  }

  # Tipos de estado del TramiteDoc
  enum EstadoTramiteDoc {        
    CREADO
    REVISADO_ENTIDAD
    OBSERVADO_ENTIDAD_REVISOR
    OBSERVADO_ENTIDAD_FIRMANTE
    FIRMADO_ENTIDAD
    REVISADO_CANCILLERIA
    OBSERVADO_CANCILLERIA_REVISOR
    OBSERVADO_CANCILLERIA_FIRMANTE
    APOSTILLADO
  }

  # Objeto para crear un TramiteDoc
  input NewTramiteDoc {
    id_documento: Int!
    id_archivo: Int!
    id_tramite: Int
  }

  # Objeto para editar un TramiteDoc
  input EditTramiteDoc {
    pago_entidad: Boolean
    pago_cancilleria: Boolean
    cod_seguridad: String
    nro_apostilla: String
    fecha_expedicion: Date
    estado: EstadoTramiteDoc!
    observacion: String   
    firmante_documento_nombre: String
    firmante_documento_cargo: String
    firmante_documento_entidad: String
    departamento: String
    fecha_inicio: Date,
    fecha_fin: Date
  }

  # Objeto para modificar Estado
  input ModifiedStatusTramiteDoc {    
    estado: EstadoTramiteDoc!
  }

  # Objeto de paginación para Documento
  type TramiteDocs {
    count: Int 
    rows: [TramiteDoc]
  }
`;
