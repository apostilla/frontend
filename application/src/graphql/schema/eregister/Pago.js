module.exports = `

  # Pagos del sistema
  type Pago {
    # ID del Pago
    id: ID!
    #cpt del Pago
    cpt: JSON
    #tipo del Pago
    tipo: TipoPago!
    #fecha_pago del Pago
    fecha_pago: Date
    #estado del Pago
    estado: EstadoPago!
    #id_tramite_doc del Pago
    id_tramite_doc: Int!
    #tipo_documento del Pago
    tipo_documento: TipoDocumentoPago
    #nro_documento del Pago
    nro_documento: String
    # fecha_nacimiento
    fecha_nacimiento: Date
    # observacion
    observacion: String
    #titular_factura del Pago
    titular_factura: String
    _user_created: Int
    _user_updated: Int
    _created_at: Date
    _updated_at: Date
  }

  enum TipoPago {
    ENTIDAD
    CANCILLERIA
  }

  enum TipoDocumentoPago {
    CI
    NIT
    PASAPORTE
    EXTRANJERO
  }

  # Tipos de estado del Pago
  enum EstadoPago {
    # Pago estado PENDIENTE
    PENDIENTE
    # Pago estado HABILITADO
    HABILITADO
    # Pago estado PAGADO
    PAGADO
  }

  # Objeto para crear un Pago
  input NewPago {
    cpt: JSON!
    tipo: TipoPago!
    id_tramite_doc: Int!
    nro_documento: String!
    titular_factura: String!
  }

  # Objeto para editar un Pago
  input EditPago {
    cpt: JSON
    tipo: TipoPago
    fecha_pago: Date
    estado: EstadoPago
    id_tramite_doc: Int
    nro_documento: String
    titular_factura: String
  }
  # Objeto de paginación para Pago
  type Pagos {
    count: Int
    rows: [Pago]
  }
`;
