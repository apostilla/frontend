module.exports = `

  # Tramites del sistema
  type Tramite {
    # ID del Tramite
    id: ID!
    #Fecha_inicio del Tramite
    fecha_inicio: Date!
    #Fecha_fin del Tramite
    fecha_fin: Date
    #estado del Tramite
    estado: EstadoTramite!
    #cod_seguimiento del Tramite
    cod_seguimiento: String!
    # tipo_documento
    tipo_documento: TipoDocumentoFactura
    # nro_documento
    nro_documento: String
    # titular_factura
    titular_factura: String
    # Id de solicitante
    id_solicitante: Int!
    # Nombre solicitante
    solicitante_nombres: String
    # Primer apellido solicitante
    solicitante_primer_apellido: String
    # Segundo apellido solicitante
    solicitante_segundo_apellido: String
    # Pagos del trámite
    pagos: [PagoTramite]
    _user_created: Int
    _user_updated: Int
    _created_at: Date
    _updated_at: Date
  }

  # Objeto de pagos con pdf
  type PagoTramite {
    id: Int
    documento: Documento,
    pdf: String
  }

  enum TipoDocumentoFactura {
    CI
    NIT
    PASAPORTE
    EXTRANJERO
  }

  # Tipos de estado del tramite
  enum EstadoTramite {
    SOLICITADO
    EN_PROCESO
    FINALIZADO
  }

  # Objeto para crear un Tramite
  input NewTramite {
    documentos: [JSON]!
    tipoDoc: String!
    nroDoc: String!
    fecNacimiento: String!
    complemento: String
    nacionalidad: String
    nombres: String!
    primerAp: String
    segundoAp: String
    email: String
    telefono: String
    regional: String
    observacion: String
    tipoSolicitante: String!
    tramitador_fechaNac: String
    tramitador_fecNacimiento: String
    tramitador_nombres: String
    tramitador_nroDoc: String
    tramitador_primerAp: String
    tramitador_segundoAp: String
    tramitador_tipoDoc: String
    tramitador_observacion: String
    nombreFactura: String
    docFactura: String
    tipoFactura: String,
    factura_tipo_documento: TipoDocumentoFactura
    factura_nro_documento: String
    factura_titular_factura: String
  }

  # Objeto para editar un Tramite
  input EditTramite {
    #fecha_inicio: Date!
    fecha_fin: Date
    estado: EstadoTramite!
    #id_solicitante: Int!
  }

  # Objeto de paginación para Tramite
  type Tramites {
    count: Int
    rows: [Tramite]
  }
`;
