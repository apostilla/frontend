module.exports = {
  Query: `
    # Lista de TramiteDoc
    tramiteDocs(
      # id de tramited_doc
      id: Int
      # limit para TramiteDoc
      limit: Int
      # page para TramiteDoc
      page: Int
      # order para TramiteDoc
      order: String
      # estado para TramiteDoc
      estado: EstadoTramiteDoc
      # estado que no se quiere mostrar en el listado
      no_estado: EstadoTramiteDoc
      # observacion para TramiteDoc
      observacion: String
      # Departamento/regional para TramiteDoc
      departamento: String
      # nombre del firmante del documento
      firmante_documento_nombre: String
      # cargo del firmante del documento
      firmante_documento_cargo: String
      # entidad del firmante del documento
      firmante_documento_entidad: String
      # pago_entidad para TramiteDoc
      pago_entidad: Boolean
      # pago_cancilleria para TramiteDoc
      pago_cancilleria: Boolean
      # cod_seguridad para TramiteDoc
      cod_seguridad: String
      # nro_apostilla para TramiteDoc
      nro_apostilla: String
      # solicitante nombre_completo
      nombre_completo: String
      # solicitante nro_documento
      nro_documento: String
      # solicitante tipo_documento
      tipo_documento: TipoDocumentoSolicitante
      # fecha_expedicion para TramiteDoc
      fecha_expedicion: Date
      # id_tramite para TramiteDoc
      id_tramite: Int
      # id_documento para TramiteDoc
      id_documento: Int
      # id_archivo para TramiteDoc
      id_archivo: Int
      #Codigo de seguimiento
      cod_seguimiento: String
    ): TramiteDocs
    # Historial de TramiteDoc
    historial(
    # id de tramited_doc
      id: Int
      # limit para TramiteDoc
      limit: Int
      # page para TramiteDoc
      page: Int
      # order para TramiteDoc
      order: String
      # estado para TramiteDoc
      estado: EstadoTramiteDoc
      # estado que no se quiere mostrar en el listado
      no_estado: EstadoTramiteDoc
      # observacion para TramiteDoc
      observacion: String
      # Departamento/regional para TramiteDoc
      departamento: String
      # nombre del firmante del documento
      firmante_documento_nombre: String
      # cargo del firmante del documento
      firmante_documento_cargo: String
      # entidad del firmante del documento
      firmante_documento_entidad: String
      # pago_entidad para TramiteDoc
      pago_entidad: Boolean
      # pago_cancilleria para TramiteDoc
      pago_cancilleria: Boolean
      # cod_seguridad para TramiteDoc
      cod_seguridad: String
      # nro_apostilla para TramiteDoc
      nro_apostilla: String
      # solicitante nombre_completo
      nombre_completo: String
      # solicitante nro_documento
      nro_documento: String
      # solicitante tipo_documento
      tipo_documento: TipoDocumentoSolicitante
      # fecha_expedicion para TramiteDoc
      fecha_expedicion: Date
      # id_tramite para TramiteDoc
      id_tramite: Int
      # id_documento para TramiteDoc
      id_documento: Int
      # id_archivo para TramiteDoc
      id_archivo: Int
      #Codigo de seguimiento
      cod_seguimiento: String
    ): TramiteDocs

    para_firma(
    # id de tramited_doc
      id: Int
      # limit para TramiteDoc
      limit: Int
      # page para TramiteDoc
      page: Int
      # order para TramiteDoc
      order: String
      # estado para TramiteDoc
      estado: EstadoTramiteDoc
      # estado que no se quiere mostrar en el listado
      no_estado: EstadoTramiteDoc
      # observacion para TramiteDoc
      observacion: String
      # Departamento/regional para TramiteDoc
      departamento: String
      # nombre del firmante del documento
      firmante_documento_nombre: String
      # cargo del firmante del documento
      firmante_documento_cargo: String
      # entidad del firmante del documento
      firmante_documento_entidad: String
      # pago_entidad para TramiteDoc
      pago_entidad: Boolean
      # pago_cancilleria para TramiteDoc
      pago_cancilleria: Boolean
      # cod_seguridad para TramiteDoc
      cod_seguridad: String
      # nro_apostilla para TramiteDoc
      nro_apostilla: String
      # solicitante nombre_completo
      nombre_completo: String
      # solicitante nro_documento
      nro_documento: String
      # solicitante tipo_documento
      tipo_documento: TipoDocumentoSolicitante
      # fecha_expedicion para TramiteDoc
      fecha_expedicion: Date
      # id_tramite para TramiteDoc
      id_tramite: Int
      # id_documento para TramiteDoc
      id_documento: Int
      # id_archivo para TramiteDoc
      id_archivo: Int
      #Codigo de seguimiento
      cod_seguimiento: String
    ): TramiteDocs
    # Obtener un tramite
    tramiteDoc(id: Int!): TramiteDoc
    # Obtener un tramite median su código de seguridad y nro de apostilla
    tramiteBuscar(code: String!, number: String!, date: Date!): TramiteDoc
  `,
  Mutation: `
    # Agregar tramite
    tramiteDocAdd(tramiteDoc: NewTramiteDoc!): TramiteDoc
    # Editar tramite
    tramiteDocEdit(id: Int!, tramiteDoc: EditTramiteDoc!): TramiteDoc
    # Cambiar estado
    tramiteDocModifiedStatus(id: Int!, tramiteDoc: ModifiedStatusTramiteDoc!): TramiteDoc
    # Eliminar tramite
    tramiteDocDelete(id: Int!): Delete
  `
};
