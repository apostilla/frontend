'use strict';

const { makeExecutableSchema } = require('graphql-tools');
const { loadSchemas } = require('../../lib/util');
const resolvers = require('../resolvers');

module.exports = function setupScheme (services) {
  let files = loadSchemas(__dirname);
  let Query = '';
  let Mutation = '';

  for (let i in files.queries) {
    Query += files.queries[i].Query;
    Mutation += files.queries[i].Mutation;
  }

  const rootQuery = `
    # Consultas Apostilla
    type Query {
      ${Query}
    }
    
    # Mutaciones Apostilla
    type Mutation {
      ${Mutation}
    }
  `;

  const schema = makeExecutableSchema({
    typeDefs: [rootQuery].concat(files.schemas),
    resolvers: resolvers(services)
  });

  return schema;
};
