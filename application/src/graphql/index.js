'use strict';

const debug = require('debug')('apostilla:graphql');
const { graphqlExpress, graphiqlExpress } = require('graphql-server-express');
const auth = require('express-jwt');
const schema = require('./schema');
const { config } = require('common');
const { verify, validarAuxiliar } = require('../lib/auth');

module.exports = async function setupGraphql (app, services) {
  debug('Iniciando servidor GraphQL');

  // Agregando verificación con JWT
  app.use('/graphql', validarAuxiliar, auth(config.auth));

  // Obteniendo token y datos del usuario
  app.use('/graphql', async (req, res, next) => {
    let data;
    try {
      data = await verify(req.headers.authorization.replace('Bearer ', ''), config.auth.secret);

      // Obteniendo usuario
      const { Usuario, Auxiliar } = services;
      let usuario = null;
      if(req.headers.auxiliar == true) usuario = await Auxiliar.getUser(data.usuario, false);
      else usuario = await Usuario.getUser(data.usuario, false);
      usuario = usuario.data;

      req.context = {
        id_usuario: usuario.id,
        id_entidad: usuario.id_entidad,
        id_rol: usuario.id_rol,
        permissions: data.permissions,
        departamento: usuario.departamento,
        auxiliar: req.headers.auxiliar,
        ip: req.ip
      };
    } catch (e) {
      return next(e);
    }
    next();
  });

  // Creando endpoint de entrada de GraphQL
  app.use('/graphql',
    graphqlExpress(req => ({
      schema: schema(services),
      formatError: (error) => {
        return {
          code: -1,
          data: error.name,
          message: error.message
        };
      },
      context: req.context
    }))
  );

  // Habilitando GraphiQL solo para desarrollo
  if (typeof process.env.NODE_ENV === 'undefined' || process.env.NODE_ENV !== 'production') {
    app.use('/graphiql',
      graphiqlExpress({
        endpointURL: '/graphql'
      })
    );
  }

  return app;
};
