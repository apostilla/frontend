'use strict';
const { permissions } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { Parametro } = services;

  return {
    Query: {
      parametros: async (_, args, context) => {
        permissions(context, 'parametros:read');

        let items = await Parametro.findAll(args, context.id_rol);
        return items.data;
      },
      parametro: async (_, args, context) => {
        permissions(context, 'parametros:read');

        let item = await Parametro.findById(args.id);
        return item.data;
      },
      parametroBuscar: async (_, args, context) => {
        permissions(context, 'parametros:read|tramitesDoc:read');

        let item = await Parametro.getParameter(args.name);
        return item.data;
      }
    },
    Mutation: {
      parametroAdd: async (_, args, context) => {
        permissions(context, 'parametros:create');

        args.parametro._user_created = context.id_usuario;
        let item = await Parametro.createOrUpdate(args.parametro);
        return item.data;
      },
      parametroEdit: async (_, args, context) => {
        permissions(context, 'parametros:update');

        args.parametro._user_updated = context.id_usuario;
        args.parametro._updated_at = new Date();
        args.parametro.id = args.id;
        let item = await Parametro.createOrUpdate(args.parametro);
        return item.data;
      },
      parametroDelete: async (_, args, context) => {
        permissions(context, 'parametros:delete');

        let deleted = await Parametro.deleteItem(args.id);
        return { deleted: deleted.data };
      }
    }
  };
};
