'use strict';
const { removeDots } = require('../../../lib/util');
const { permissions } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { Log } = services;

  return {
    Query: {
      logs: async (_, args, context) => {
        permissions(context, 'logs:read');

        let items = await Log.findAll(args);
        return removeDots(items.data);
      },
      log: async (_, args, context) => {
        permissions(context, 'logs:read');

        let item = await Log.findById(args.id);
        return removeDots(item.data);
      }
    },
    Mutation: {}
  };
};
