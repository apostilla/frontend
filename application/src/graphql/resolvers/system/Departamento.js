'use strict';
const { removeDots } = require('../../../lib/util');
const { permissions } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { Departamento } = services;

  return {
    Query: {
      departamentos: async (_, args, context) => {
        permissions(context, 'departamentos:read');

        let lista = await Departamento.findAll(args);
        return removeDots(lista.data);
      },
      departamento: async (_, args, context) => {
        permissions(context, 'departamentos:read');

        let item = await Departamento.findById(args.id);
        return removeDots(item.data);
      }
    },
    Mutation: {
      departamentoAdd: async (_, args, context) => {
        permissions(context, 'departamentos:create');

        args.departamento._user_created = context.id_usuario;
        let item = await Departamento.createOrUpdate(args.departamento);
        return removeDots(item.data);
      },
      departamentoEdit: async (_, args, context) => {
        permissions(context, 'departamentos:update');

        args.departamento._user_updated = context.id_usuario;
        args.departamento._updated_at = new Date();
        args.departamento.id = args.id;
        let item = await Departamento.createOrUpdate(args.departamento);
        return removeDots(item.data);
      },
      departamentoDelete: async (_, args, context) => {
        permissions(context, 'departamentos:delete');

        let deleted = await Departamento.deleteItem(args.id);
        return { deleted: deleted.data };
      }

    }
  };
};
