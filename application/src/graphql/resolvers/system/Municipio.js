'use strict';
const { removeDots } = require('../../../lib/util');
const { permissions } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { Municipio } = services;

  return {
    Query: {
      municipios: async (_, args, context) => {
        permissions(context, 'municipios:read');

        let lista = await Municipio.findAll(args);
        return removeDots(lista.data);
      },
      municipio: async (_, args, context) => {
        permissions(context, 'municipios:read');

        let item = await Municipio.findById(args.id);
        return removeDots(item.data);
      }
    },
    Mutation: {
      municipioAdd: async (_, args, context) => {
        permissions(context, 'municipios:create');

        args.municipio._user_created = context.id_usuario;
        let item = await Municipio.createOrUpdate(args.municipio);
        return removeDots(item.data);
      },
      municipioEdit: async (_, args, context) => {
        permissions(context, 'municipios:update');

        args.municipio._user_updated = context.id_usuario;
        args.municipio._updated_at = new Date();
        args.municipio.id = args.id;
        let item = await Municipio.createOrUpdate(args.municipio);
        return removeDots(item.data);
      },
      municipioDelete: async (_, args, context) => {
        permissions(context, 'municipios:delete');

        let deleted = await Municipio.deleteItem(args.id);
        return { deleted: deleted.data };
      }
    }
  };
};
