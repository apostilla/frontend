'use strict';
const { removeDots } = require('../../../lib/util');
const { permissions } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { Archivo } = services;

  return {
    Query: {
      archivos: async (_, args, context) => {
        permissions(context, 'archivos:read');

        let lista = await Archivo.findAll(args);
        return removeDots(lista.data);
      },
      archivo: async (_, args, context) => {
        permissions(context, 'archivos:read');

        let item = await Archivo.findById(args.id);
        return removeDots(item.data);
      }
    },
    Mutation: {
      archivoAdd: async (_, args, context) => {
        permissions(context, 'archivos:create');

        args.archivo._user_created = context.id_usuario;
        let item = await Archivo.createOrUpdate(args.archivo);
        return removeDots(item.data);
      },
      archivoEdit: async (_, args, context) => {
        permissions(context, 'archivos:update');

        args.archivo._user_updated = context.id_usuario;
        args.archivo._updated_at = new Date();
        args.archivo.id = args.id;
        let item = await Archivo.createOrUpdate(args.archivo);
        return removeDots(item.data);
      },
      archivoDelete: async (_, args, context) => {
        permissions(context, 'archivos:delete');

        let deleted = await Archivo.deleteItem(args.id);
        return { deleted: deleted.data };
      }

    }
  };
};
