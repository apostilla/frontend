'use strict';
const { removeDots } = require('../../../lib/util');
const { permissions } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { Auxiliar } = services;

  return {
    Query: {
      auxiliares: async (_, args, context) => {
        permissions(context, 'auxiliares:read');

        let lista = await Auxiliar.findAll(args, context.id_rol, context.id_entidad);
        return removeDots(lista.data);
      },
      auxiliar: async (_, args, context) => {
        permissions(context, 'auxiliares:read');

        let item = await Auxiliar.findById(args.id);
        return removeDots(item.data);
      }
    },
    Mutation: {
      auxiliarAdd: async (_, args, context) => {
        permissions(context, 'auxiliares:create');

        args.auxiliar._user_created = context.id_usuario;
        let item = await Auxiliar.createOrUpdate(args.auxiliar);
        return removeDots(item.data);
      },
      auxiliarEdit: async (_, args, context) => {
        permissions(context, 'auxiliares:update');

        args.auxiliar._user_updated = context.id_usuario;
        args.auxiliar._updated_at = new Date();
        args.auxiliar.id = args.id;
        let item = await Auxiliar.createOrUpdate(args.auxiliar);
        return removeDots(item.data);
      },
      auxiliarDelete: async (_, args, context) => {
        permissions(context, 'auxiliares:delete');

        let deleted = await Auxiliar.deleteItem(args.id);
        return { deleted: deleted.data };
      }
    }
  };
};
