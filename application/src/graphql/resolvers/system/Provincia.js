'use strict';
const { removeDots } = require('../../../lib/util');
const { permissions } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { Provincia } = services;

  return {
    Query: {
      provincias: async (_, args, context) => {
        permissions(context, 'provincias:read');

        let lista = await Provincia.findAll(args);
        return removeDots(lista.data);
      },
      provincia: async (_, args, context) => {
        permissions(context, 'provincias:read');

        let item = await Provincia.findById(args.id);
        return removeDots(item.data);
      }
    },
    Mutation: {
      provinciaAdd: async (_, args, context) => {
        permissions(context, 'provincias:create');

        args.provincia._user_created = context.id_usuario;
        let item = await Provincia.createOrUpdate(args.provincia);
        return removeDots(item.data);
      },
      provinciaEdit: async (_, args, context) => {
        permissions(context, 'provincias:update');

        args.provincia._user_updated = context.id_usuario;
        args.provincia._updated_at = new Date();
        args.provincia.id = args.id;
        let item = await Provincia.createOrUpdate(args.provincia);
        return removeDots(item.data);
      },
      provinciaDelete: async (_, args, context) => {
        permissions(context, 'provincias:delete');

        let deleted = await Provincia.deleteItem(args.id);
        return { deleted: deleted.data };
      }
    }
  };
};
