'use strict';
const { removeDots } = require('../../../lib/util');
const { permissions } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { Notificacion } = services;

  return {
    Query: {
      notificaciones: async (_, args, context) => { 
        permissions(context, 'notificaciones:read');

        let lista = await Notificacion.findAll(args);
        return removeDots(lista.data);
      },
      notificacion: async (_, args, context) => { 
        permissions(context, 'notificaciones:read');

        let lista = await Notificacion.findById(args.id);
        return removeDots(lista.data);
      }
    },
    Mutation: {
      notificacionAdd: async (_, args, context) => {
        permissions(context, 'notificaciones:create');

        args.notificacion._user_created = context.id_usuario;
        let item = await Notificacion.createOrUpdate(args.notificacion);
        return removeDots(item.data);
      },
      notificacionEdit: async (_, args, context) => {
        permissions(context, 'notificaciones:update');

        args.notificacion._user_updated = context.id_usuario;
        args.notificacion._updated_at = new Date();
        args.notificacion.id = args.id;
        let item = await Notificacion.createOrUpdate(args.notificacion);
        return removeDots(item.data);
      },
      notificacionDelete: async (_, args, context) => {
        permissions(context, 'notificaciones:delete');

        let deleted = await Notificacion.deleteItem(args.id);
        return { deleted: deleted.data };
      }
    }
  };
};
