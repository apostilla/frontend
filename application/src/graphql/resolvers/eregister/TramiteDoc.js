'use strict';
const { removeDots } = require('../../../lib/util');
const { permissions } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { TramiteDoc } = services;

  return {
    Query: {
      tramiteDocs: async (_, args, context) => {
        permissions(context, 'tramitesDoc:read');

        let items = await TramiteDoc.findAll(args, context.id_rol, context.id_entidad);
        return removeDots(items.data);
      },
      historial: async (_, args, context) => {
        permissions(context, 'tramitesDoc:read');

        let items = await TramiteDoc.historial(args, context.id_rol, context.id_entidad);
        return removeDots(items.data);
      },
      para_firma: async (_, args, context) => {
        permissions(context, 'tramitesDoc:read');

        let items = await TramiteDoc.para_firma(args, context.id_rol, context.id_entidad);
        return removeDots(items.data);
      },
      tramiteDoc: async (_, args, context) => {
        permissions(context, 'tramitesDoc:read');

        let item = await TramiteDoc.findById(args.id);
        return removeDots(item.data);
      },
      tramiteBuscar: async (_, args, context) => {
        permissions(context, 'tramitesDoc:read');

        let item = await TramiteDoc.findByCode(args.code, args.number, args.date);
        return removeDots(item.data);
      }
    },
    Mutation: {
      tramiteDocAdd: async (_, args, context) => {
        permissions(context, 'tramitesDoc:create');

        args.tramiteDoc._user_created = context.id_usuario;
        let item = await TramiteDoc.createOrUpdate(args.tramiteDoc);
        return removeDots(item.data);
      },
      tramiteDocEdit: async (_, args, context) => {
        permissions(context, 'tramitesDoc:update');

        // if (context.auxiliar) {
        //   args.tramiteDoc._user_updated_auxiliar = context.id_usuario;
        //   args.tramiteDoc._updated_at_auxiliar = new Date();
        // }
        // else {
        //   args.tramiteDoc._user_updated = context.id_usuario;
        //   args.tramiteDoc._updated_at = new Date();
        // }

        args.tramiteDoc._user_updated = context.id_usuario;
        args.tramiteDoc._updated_at = new Date();

        args.tramiteDoc.id = args.id;
        let item = await TramiteDoc.createOrUpdate(args.tramiteDoc);
        return removeDots(item.data);
      },
      tramiteDocModifiedStatus: async (_, args, context) => {
        permissions(context, 'tramitesDoc:firma');

        // if (context.auxiliar) {
        //   args.tramiteDoc._user_updated_auxiliar = context.id_usuario;
        //   args.tramiteDoc._updated_at_auxiliar = new Date();
        // }
        // else {
        //   args.tramiteDoc._user_updated = context.id_usuario;
        //   args.tramiteDoc._updated_at = new Date();
        // }

        args.tramiteDoc._user_updated = context.id_usuario;
        args.tramiteDoc._updated_at = new Date();

        args.tramiteDoc.id = args.id;
        let item = await TramiteDoc.modifiedStatus(args.tramiteDoc, context.id_rol, context.id_entidad);
        return removeDots(item.data);
      },
      tramiteDocDelete: async (_, args, context) => {
        permissions(context, 'tramitesDoc:delete');

        let deleted = await TramiteDoc.deleteItem(args.id);
        return { deleted: deleted.data };
      }
    }
  };
};
