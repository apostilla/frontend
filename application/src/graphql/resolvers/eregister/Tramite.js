'use strict';
const { removeDots } = require('../../../lib/util');
const { permissions } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { Tramite } = services;

  return {
    Query: {
      tramites: async (_, args, context) => {
        permissions(context, 'tramites:read');

        let lista = await Tramite.findAll(args);
        return removeDots(lista.data);
      },
      tramite: async (_, args, context) => {
        permissions(context, 'tramites:read');

        let item = await Tramite.findById(args.id);
        return removeDots(item.data);
      }
    },
    Mutation: {
      tramiteAdd: async (_, args, context) => {
        permissions(context, 'tramites:create');

        args.tramite._user_created = context.id_usuario;
        let item = await Tramite.create(args.tramite, args.ip);
        console.log('RESPUESTA TRAMITE', item);
        return removeDots(item.data);
      },
      tramiteEdit: async (_, args, context) => {
        permissions(context, 'tramites:update');

        args.tramite._user_updated = context.id_usuario;
        args.tramite._updated_at = new Date();
        args.tramite.id = args.id;
        let item = await Tramite.createOrUpdate(args.tramite);
        return item.data;
      },
      tramiteDelete: async (_, args, context) => {
        permissions(context, 'tramites:delete');

        let deleted = await Tramite.deleteItem(args.id);
        return { deleted: deleted.data };
      }
    }
  };
};
