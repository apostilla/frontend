'use strict';
const { removeDots } = require('../../../lib/util');
const { permissions } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { Solicitante } = services;

  return {
    Query: {
      solicitantes: async (_, args, context) => {
        permissions(context, 'solicitantes:read');

        let lista = await Solicitante.findAll(args);        
        return removeDots(lista.data);
      },
      solicitante: async (_, args, context) => {
        permissions(context, 'solicitantes:read');

        let item = await Solicitante.findById(args.id);        
        return removeDots(item.data);
      }
    },
    Mutation: {
      solicitanteAdd: async (_, args, context) => {
        permissions(context, 'solicitantes:create');

        args.solicitante._user_created = context.id_usuario;
        let item = await Solicitante.createOrUpdate(args.solicitante);
        return removeDots(item);
      },
      solicitanteEdit: async (_, args, context) => {
        permissions(context, 'solicitantes:update');

        args.solicitante._user_updated = context.id_usuario;
        args.solicitante._updated_at = new Date();
        args.solicitante.id = args.id;
        let item = await Solicitante.createOrUpdate(args.solicitante);
        return removeDots(item);
      },
      solicitanteDelete: async (_, args, context) => {
        permissions(context, 'solicitantes:delete');

        let deleted = await Solicitante.deleteItem(args.id);
        return { deleted: deleted.data };
      }
    }
  };
};
