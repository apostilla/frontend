'use strict';
const { removeDots } = require('../../../lib/util');
const { permissions } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { Documento } = services;

  return {
    Query: {
      documentos: async (_, args, context) => {
        permissions(context, 'documentos:read|tramitesDoc:read');

        let items = await Documento.findAll(args, context.id_rol, context.id_entidad);
        return removeDots(items.data);
      },
      documento: async (_, args, context) => {
        permissions(context, 'documentos:read|tramitesDoc:read');

        let item = await Documento.findById(args.id);
        return removeDots(item.data);
      }
    },
    Mutation: {
      documentoAdd: async (_, args, context) => {
        permissions(context, 'documentos:create');

        args.documento._user_created = context.id_usuario;
        let item = await Documento.createOrUpdate(args.documento);
        return removeDots(item.data);
      },
      documentoEdit: async (_, args, context) => {
        permissions(context, 'documentos:update');

        args.documento._user_updated = context.id_usuario;
        args.documento._updated_at = new Date();
        args.documento.id = args.id;
        let item = await Documento.createOrUpdate(args.documento);
        return removeDots(item.data);
      },
      documentoDelete: async (_, args, context) => {
        permissions(context, 'documentos:delete');

        let deleted = await Documento.deleteItem(args.id);
        return { deleted: deleted.data };
      }
    }
  };
};
