'use strict';
const { removeDots } = require('../../../lib/util');
const { permissions } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { Firma } = services;

  return {
    Query: {
      firmas: async (_, args, context) => {
        permissions(context, 'firmas:read');

        let lista = await Firma.findAll(args);
        return removeDots(lista);
      },
      firma: async (_, args, context) => {
        permissions(context, 'firmas:read');

        let item = await Firma.findById(args.id);
        return removeDots(item);
      }
    },
    Mutation: {
      firmaAdd: async (_, args, context) => {
        permissions(context, 'firmas:create');

        args.firma._user_created = context.id_usuario;
        let item = await Firma.createOrUpdate(args.firma);
        return removeDots(item);
      },
      firmaEdit: async (_, args, context) => {
        permissions(context, 'firmas:update');

        args.firma._user_updated = context.id_usuario;
        args.firma._updated_at = new Date();
        args.firma.id = args.id;
        let item = await Firma.createOrUpdate(args.firma);
        return removeDots(item);
      },
      firmaDelete: async (_, args, context) => {
        permissions(context, 'firmas:delete');

        let deleted = await Firma.deleteItem(args.id);
        return { deleted: deleted.data };
      }
    }
  };
};
