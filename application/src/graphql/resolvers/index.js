'use strict';

const { Date, JSON } = require('./scalars');
const { loadResolvers } = require('../../lib/util');

let Query = {};
let Mutation = {};

module.exports = (services) => {
  // Cargando Resolvers
  loadResolvers(__dirname, { exclude: ['index.js', 'scalars.js'] }, services, Query, Mutation);

  let resolvers = {
    Query,
    Mutation,
    Date,
    JSON
  };

  return resolvers;
};
