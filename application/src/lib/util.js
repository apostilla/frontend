'use strict';

const fs = require('fs');
const path = require('path');

function removeDotsItem (item) {
  for (let key in item) {
    item[key.replace(/\./gi, '_')] = item[key];
  }
  return item;
}

function removeDots (response) {
  if (response) {
    if (response.rows) {
      let items = response.rows;
      items.map((item, index) => {
        items[index] = removeDotsItem(item);
      });
      response.rows = items;
    } else {
      if (response) {
        response = removeDotsItem(response);
      }
    }
  }
  return response;
}

/**
 *
 * @param {array} elements: Array de elementos a eliminar de 'list'
 * @param {array} list: Array de elementos
 */
function removeAll (elements, list) {
  var ind;

  for (var i = 0, l = elements.length; i < l; i++) {
    while ((ind = list.indexOf(elements[i])) > -1) {
      list.splice(ind, 1);
    }
  }
}

let schemas = [];
let queries = {};

function loadSchemas (PATH) {
  loadSchemasFiles(PATH, { exclude: ['index.js'] });

  return {
    schemas,
    queries
  };
}

/**
 * Carga los schemas de la carpeta especificada
 *
 * @param {string} PATH: Path del directorio de donde se cargará los modelos del sistema
 * @param {object} opts: Json de configuración
 */
function loadSchemasFiles (PATH, opts = {}) {
  let files = fs.readdirSync(PATH);

  if (opts.exclude) {
    removeAll(opts.exclude, files);
  }

  files.forEach(function (file) {
    let pathFile = path.join(PATH, file);
    if (fs.statSync(pathFile).isDirectory()) {
      loadSchemas(pathFile, opts);
    } else {
      file = file.replace('.js', '');
      if (file.indexOf('Query') !== -1) {
        queries[file] = require(pathFile);
      } else {
        schemas.push(require(pathFile));
      }
    }
  });
}

function loadResolvers (PATH, opts = {}, services, Query, Mutation) {
  let files = fs.readdirSync(PATH);

  if (opts.exclude) {
    removeAll(opts.exclude, files);
  }

  files.forEach(function (file) {
    let pathFile = path.join(PATH, file);
    if (fs.statSync(pathFile).isDirectory()) {
      loadResolvers(pathFile, opts, services, Query, Mutation);
    } else {
      const Resolver = require(pathFile)(services);
      Query = Object.assign(Query, Resolver.Query);
      Mutation = Object.assign(Mutation, Resolver.Mutation);
    }
  });
}

function loadRoutes (PATH, opts = {}, services, routes) {
  let files = fs.readdirSync(PATH);

  if (opts.exclude) {
    removeAll(opts.exclude, files);
  }

  files.forEach(function (file) {
    let pathFile = path.join(PATH, file);
    if (fs.statSync(pathFile).isDirectory()) {
      return loadRoutes(pathFile, opts, services, routes);
    } else {
      routes = require(pathFile)(routes, services);
    }
  });

  return routes;
}

function sizeFile (filename) {
  const stats = fs.statSync(filename);
  const tamanoArchivo = stats['size'];
  const tamanoPrev = (tamanoArchivo / 1000000.0);
  return Math.round(tamanoPrev * 100) / 100;
}

function randomStr (m) {
  let s = '';
  const r = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789';
  for (let i = 0; i < m; i++) {
    s += r.charAt(Math.floor(Math.random() * r.length));
  }
  return s;
}

module.exports = {
  removeDots,
  loadSchemas,
  loadResolvers,
  loadRoutes,
  sizeFile,
  randomStr
};
