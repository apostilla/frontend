'use strict';

const debug = require('debug')('apostilla:app');
const http = require('http');
const chalk = require('chalk');
const express = require('express');
const asyncify = require('express-asyncify');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const { errors, config } = require('common');
const domain = require('domain-app');
const api = require('./src/api');
const graphql = require('./src/graphql');

const port = process.env.PORT || 3000;
const app = asyncify(express());
const server = http.createServer(app);

app.use(bodyParser.json({ limit: '300mb' }));
app.use(fileUpload());
app.use(cors());

(async function (app) {
  try {
    // Iniciando Capa del dominio
    let services = await domain(config.db);
    // Iniciando API-REST
    app = await api(app, services);
    // Iniciando GRAPHQL
    app = await graphql(app, services);
  } catch (e) {
    console.log(e);
    throw new Error(`Error al iniciar el servidor: ${e.message}`);
  }

  // Express Error Handler
  app.use((err, req, res, next) => {
    debug(`Error: ${err.message}`);

    if (err.message.match(/not found/)) {
      return res.status(404).send({ code: -1, message: err.message });
    }

    if (err.message.match(/jwt expired/)) {
      return res.status(401).send({ code: -1, message: 'Su sesión ha expirado, ingrese nuevamente al sistema.' });
    }

    if (err.message.match(/No authorization/)) {
      return res.status(403).send({ code: -1, message: 'No tiene permisos para realizar esta operación.' });
    }

    if (err.message.match(/EAI_AGAIN/)) {
      return res.status(400).send({ code: -1, message: 'El servicio de la Plataforma de Pagos y/o el Portal único no se encuentra activo en estos momentos, vuelva a intentar dentro de unos minutos.' });
    }

    res.status(500).send({ code: -1, message: err.message });
  });
})(app);

process.on('uncaughtException', errors.handleFatalError);
process.on('unhandledRejection', errors.handleFatalError);

server.listen(port, () => {
  console.log(`${chalk.green('[apostilla-app]')} server listening on port ${port}`);
});
